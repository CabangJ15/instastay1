<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('sendEmail', 'APIController@sendEmail');
Route::post('sendConcern', 'APIController@sendConcern');
Route::post('importEmails', 'APIController@importEmails');

// get reports
Route::get('/searchViaDate', 'APIController@searchViaDate');

// update user account
Route::put('changePassword', 'UsersController@changePassword');


// deletes
Route::delete('deleteRow', 'APIController@deleteRow');
