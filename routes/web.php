<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');
Route::get('/hotelier', 'PageController@hotelier');
Route::post('/hotelier_inquiries', 'PageController@hotelier_inquiries');
Route::get('/wait', 'PageController@wait');
Route::get('/contact', 'PageController@contact');
Route::post('/contactMail', 'PageController@contactMail');
Route::get('/terms-of-use', 'PageController@termsOfUse');

// Route::get('/admin', 'PageController@admin');
Route::get('instanet', [ 'as' => 'login', 'uses' => 'PageController@login']);
Route::get('admin', [ 'as' => 'login', 'uses' => 'PageController@admin']);
Route::get('/forgot_password', 'PageController@forgot_password');
Route::post('/password_reset', 'PageController@password_reset');
Route::get('/resetPassword/{code}', 'PageController@resetPassword');
Route::post('/passwordReset/{code}', 'PageController@passwordReset');
Route::post('/authenticate', 'AuthController@authenticate');
Route::get('/logout', 'AuthController@logout');
Route::resource('/hotels', 'HotelController');
Route::resource('/hotel', 'HotelsController');
Route::resource('/photos', 'PhotosController');
Route::resource('/reviews', 'ReviewsController');
Route::resource('/tax', 'TaxController');
Route::resource('/tax_setting', 'TaxSettingController');
Route::resource('/facilities', 'FacilitiesController');
Route::resource('/room_type', 'RoomTypeController');
Route::get('/room_type/{hotel_id}/create', 'RoomTypeController@create');
Route::post('/room_type/{hotel_id}', 'RoomTypeController@store');
Route::post('/room_type/{hotel_id}/all', 'RoomTypeController@storeAll');
Route::post('/uploadImage/{hotel_id}', 'PhotosController@uploadImage');
Route::post('/fetchRoomType/{hotel_id}', 'RoomTypeController@fetchRoomType');
Route::post('/facilities_change/{hotel_id}', 'FacilitiesController@facilities_change');
Route::post('/fetchTaxSettings/{hotel_id}', 'TaxSettingController@fetchTaxSettings');
Route::post('/tax_setting/{hotel_id}/create', 'TaxSettingController@store');
Route::resource('/property_contact', 'PropertyContactController');
Route::post('/add_property_contact/{hotel_id}', 'PropertyContactController@store');
Route::get('/property_contacts/{hotel_id}/create', 'PropertyContactController@create');
Route::resource('/booking', 'BookingController');
Route::resource('/inventory', 'InventoryController');
Route::get('/inventoryYear/{hotel_id}', 'InventoryController@inventoryYear');
Route::post('/fetchBooking/{hotel_id}', 'BookingController@fetchBooking');
Route::post('/fetchCheckIn/{hotel_id}', 'BookingController@fetchCheckIn');
Route::post('/fetchCheckOut/{hotel_id}', 'BookingController@fetchCheckOut');
Route::post('/searchPropertyContact', 'PropertyContactController@searchPropertyContact');
Route::post('/makeCalendar', 'InventoryController@makeCalendar');
Route::post('/makeCalendarYear', 'InventoryController@makeCalendarYear');
Route::post('/changeCalendar', 'InventoryController@changeCalendar');
Route::post('/inventoryBulkApply/{hotel_id}', 'InventoryController@inventoryBulkApply');
Route::resource('/main_facilities', 'MainFacilitiesController');
Route::post('/fetchMainFacilities', 'MainFacilitiesController@fetchMainFacilities');
Route::post('/fetchSubFacilities', 'SubFacilitiesController@fetchSubFacilities');
Route::post('/fetchGalleryFacilities', 'GalleryFacilitiesController@fetchGalleryFacilities');
Route::resource('/sub_facilities', 'SubFacilitiesController');
Route::resource('/gallery_facilities', 'GalleryFacilitiesController');
Route::resource('/room_rate', 'RoomRateController');
Route::resource('/bed', 'BedController');
Route::resource('/amenities', 'AmenitiesController');
Route::post('/fetchAmenities', 'AmenitiesController@fetchAmenities');
Route::post('/fetchBed', 'BedController@fetchBed');
Route::post('/fetchRoomRate', 'RoomRateController@fetchRoomRate');
Route::post('/createRoomRate', 'RoomRateController@createRoomRate');
Route::get('/createAdjustDailyRate', 'RoomRateController@createAdjustDailyRate');
Route::post('/createAdjustDailyRate', 'RoomRateController@createAdjustDailyRate');
Route::post('/addRoomRate/{room_type_id}', 'RoomRateController@addRoomRate');
Route::post('/addRoomRateDaily/{room_type_id}', 'RoomRateController@addRoomRateDaily');
Route::get('/adjust_daily_rate/{hotel_id}', 'RoomRateController@adjustDailyRate');
Route::post('/fetchHotel', 'HotelsController@fetchHotel');
Route::post('/removeImage', 'PhotosController@removeImage');

Route::resource('/user', 'UserController');
Route::post('/fetchUser', 'UserController@fetchUser');

Route::resource('/userGroup', 'UserGroupController');
Route::post('/fetchUserGroup', 'UserGroupController@fetchUserGroup');
Route::post('/fetchTaxType', 'TaxController@fetchTaxType');
Route::post('/resetPassword/{user_id}', 'UserController@resetPassword');
Route::get('/test', 'PageController@test');
Route::get('/test1', 'PageController@test1');

Route::resource('/adminDashboard', 'AdminController');
Route::get('/properties', 'AdminController@properties');
Route::post('/fetchProperties', 'AdminController@fetchProperties');

Route::get('/leads', 'AdminController@leads');
Route::post('/fetchLeads', 'AdminController@fetchLeads');
Route::post('/assignSalesRep/{id}', 'AdminController@assignSalesRep');
Route::get('/contacted/{id}', 'AdminController@contacted');
Route::get('/proposal/{id}', 'AdminController@proposal');
Route::get('/deactivate/{id}', 'AdminController@deactivate');
Route::get('/activate/{id}', 'AdminController@activate');
Route::post('/fetchAddHotel/{id}', 'AdminController@fetchAddHotel');
Route::post('/addHotel', 'AdminController@addHotel');
