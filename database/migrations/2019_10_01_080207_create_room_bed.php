<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomBed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_bed', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id')->unsigned()->nullalbe();
            $table->foreign('room_id')->references('id')->on('tbl_room');
            $table->integer('bed_id')->unsigned()->nullalbe();
            $table->foreign('bed_id')->references('id')->on('bed');
            $table->integer('qty');
            $table->integer('created_by')->unsigned()->nullalbe();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamp('timestamp')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_bed');
    }
}
