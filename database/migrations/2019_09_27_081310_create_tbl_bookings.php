<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('or_number')->nullable();
            $table->integer('user_id')->unsigned()->nullalbe();
            $table->foreign('user_id')->references('id')->on('tbl_user');
            $table->integer('hotel_id')->unsigned();
            $table->foreign('hotel_id')->references('id')->on('tbl_hotels');
            $table->integer('room_id')->unsigned()->nullalbe();
            $table->foreign('room_id')->references('id')->on('tbl_room');
            $table->date('check_in_date');
            $table->time('check_in_time');
            $table->date('check_out_date');
            $table->time('check_out_time');
            $table->integer('payment_type');
            $table->float('total_amount', 15, 4)->default(0);
            $table->string('redeem_code')->nullable();
            $table->text('meta')->nullable();
            $table->integer('guest_count')->default(0);
            $table->boolean('is_paid')->default(0);
            $table->integer('is_cancel')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_bookings');
    }
}
