<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsActionOnHotelier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotelier', function (Blueprint $table) {
            //
            $table->integer('is_action')->after('sales_id')->nullable();
            $table->dateTime('action_at')->after('is_action')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotelier', function (Blueprint $table) {
            //
        });
    }
}
