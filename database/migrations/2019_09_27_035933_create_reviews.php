<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')->references('id')->on('tbl_user');
            $table->integer('hotel_id')->unsigned();
            $table->foreign('hotel_id')->references('id')->on('tbl_hotels');
            $table->integer('rom_type_id')->unsigned();
            $table->foreign('rom_type_id')->references('id')->on('tbl_room');
            $table->integer('star')->nullable();
            $table->text('reviews')->nullable();
            $table->timestamp('timestamp')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
