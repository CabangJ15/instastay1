<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotelier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fname')->nullable();
            $table->string('jobtitle')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('hname')->nullable();
            $table->string('address')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('contact')->nullable();
            $table->string('hotel_email')->nullable();
            $table->string('company')->nullable();
            $table->string('c_address')->nullable();
            $table->string('c_city')->nullable();
            $table->string('c_postal_code')->nullable();
            $table->string('no_hotel')->nullable();
            $table->text('description')->nullable();
            $table->integer('sales_id')->unsigned()->nullable();
            $table->foreign('sales_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotelier');
    }
}
