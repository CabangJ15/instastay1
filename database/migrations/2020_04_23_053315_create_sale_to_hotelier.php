<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleToHotelier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_to_hotelier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sales_id')->unsigned()->nullable();
            $table->foreign('sales_id')->references('id')->on('users');
            $table->bigInteger('hotelier_id')->unsigned()->nullable();
            $table->foreign('hotelier_id')->references('id')->on('hotelier');
            $table->integer('created_by')->unsigned()->nullalbe();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_to_hotelier');
    }
}
