<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDateTaxSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('date_tax_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id')->unsigned()->nullalbe();
            $table->foreign('hotel_id')->references('id')->on('tbl_hotels');
            $table->date('sDate');
            $table->date('eDate');
            $table->integer('tax_setting_id')->unsigned()->nullalbe();
            $table->foreign('tax_setting_id')->references('id')->on('tax_setting');
            $table->float('value', 15, 4)->default(0);
            $table->integer('is_free')->default(0);
            $table->integer('created_by')->unsigned()->nullalbe();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamp('timestamp')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('date_tax_setting');
    }
}
