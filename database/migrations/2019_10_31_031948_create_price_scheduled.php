<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceScheduled extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('price_scheduled', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id')->unsigned()->nullalbe();
            $table->foreign('room_id')->references('id')->on('tbl_room');
            $table->string('description');
            $table->date('date_applied');
            $table->integer('is_promo')->default(0);
            $table->float('price', 14, 4)->default(0);
            $table->integer('created_by')->unsigned()->nullalbe();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamp('timestamp')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_scheduled');
    }
}
