<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserToHotel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_to_hotel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullalbe();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('hotel_id')->unsigned()->nullalbe();
            $table->foreign('hotel_id')->references('id')->on('tbl_hotels');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_to_hotel');
    }
}
