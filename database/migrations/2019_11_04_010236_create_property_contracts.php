<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id')->unsigned()->nullalbe();
            $table->foreign('hotel_id')->references('id')->on('tbl_hotels');
            $table->string('fname');
            $table->string('email')->nullable();
            $table->string('job_role')->nullable();
            $table->string('land_line')->nullable();
            $table->string('tel_nos')->nullable();
            $table->text('img_file')->nullable();
            $table->integer('country_id');
            $table->longText('responsibility')->nullable();
            $table->integer('created_by')->unsigned()->nullalbe();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_contracts');
    }
}
