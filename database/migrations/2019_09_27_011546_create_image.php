<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('image_ref_id')->unsigned();
            $table->foreign('image_ref_id')->references('id')->on('img_ref');
            $table->integer('hotel_id')->unsigned();
            $table->foreign('hotel_id')->references('id')->on('tbl_hotels');
            $table->integer('room_type_id')->unsigned();
            $table->foreign('room_type_id')->references('id')->on('tbl_room');
            $table->string('image');
            $table->integer('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamp('timestamp')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image');
    }
}
