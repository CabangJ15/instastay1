<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRoom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_room', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id')->unsigned();
            $table->foreign('hotel_id')->references('id')->on('tbl_hotels');
            $table->string('room_name')->nullable();
            $table->text('description')->nullable();
            $table->integer('guest')->nullable();
            $table->integer('max_guest')->nullable();
            $table->integer('extra_bed')->nullable();
            $table->integer('max_extra_bed')->default(0);
            $table->float('room_size', 15,4)->default(0);
            $table->integer('bathroom')->default(0);
            $table->integer('no_rooms')->default(0);
            $table->integer('alert')->default(0);
            $table->integer('is_child_allowed')->default(0);
            $table->integer('free_child')->default(0);
            $table->integer('max_child')->default(0);
            $table->integer('is_baby_cots')->default(0);
            $table->string('img_file')->nullable();
            $table->text('policies')->nullable();
            $table->text('amenities_list')->nullable();
            $table->integer('flag')->default(1);
            $table->integer('sort')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users');
            $table->dateTime('date_created')->nullable();
            $table->timestamp('timestamp')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_room');
    }
}
