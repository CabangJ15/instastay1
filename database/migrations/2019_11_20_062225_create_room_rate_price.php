<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomRatePrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_rate_price', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id')->unsigned()->nullalbe();
            $table->foreign('room_id')->references('id')->on('tbl_room');
            $table->integer('room_rate_id')->unsigned()->nullalbe();
            $table->foreign('room_rate_id')->references('id')->on('room_rate');
            $table->date('date_applied')->nullable();
            $table->float('amount', 14, 4)->default(0);
            $table->integer('promotion')->default(0);
            $table->integer('created_by')->unsigned()->nullalbe();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_rate_price');
    }
}
