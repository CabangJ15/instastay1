<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tax_type_id')->unsigned()->nullalbe();
            $table->foreign('tax_type_id')->references('id')->on('tax_type');
            $table->integer('charge_type_id')->unsigned()->nullalbe();
            $table->foreign('charge_type_id')->references('id')->on('charge_type');
            $table->integer('tax_setting_type_id')->unsigned()->nullalbe();
            $table->foreign('tax_setting_type_id')->references('id')->on('tax_setting_type');
            $table->integer('created_by')->unsigned()->nullalbe();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamp('timestamp')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_setting');
    }
}
