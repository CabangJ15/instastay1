<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeAvailability extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_availability', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('room_id')->unsigned()->nullalbe();
            $table->foreign('room_id')->references('id')->on('tbl_room');
            $table->time('c_in');
            $table->time('c_out');
            $table->float('amount', 15, 4)->default(0);
            $table->integer('alloted')->default(0);
            $table->integer('created_by')->unsigned()->nullalbe();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_availability');
    }
}
