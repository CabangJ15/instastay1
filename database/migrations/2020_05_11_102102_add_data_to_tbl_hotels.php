<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataToTblHotels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_hotels', function (Blueprint $table) {
            //
            $table->string('legal_entity_name')->after('hotel_name')->nullable();
            $table->text('about')->after('address')->nullable();
            $table->string('city')->after('about')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_hotels', function (Blueprint $table) {
            //
        });
    }
}
