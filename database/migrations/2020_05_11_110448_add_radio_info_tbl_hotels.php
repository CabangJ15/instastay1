<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRadioInfoTblHotels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_hotels', function (Blueprint $table) {
            //
            $table->integer('has_channel_manager')->after('postcode')->default(0);
            $table->integer('has_diff_name')->after('has_channel_manager')->default(0);
            $table->integer('is_chain_hotel')->after('has_diff_name')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_hotels', function (Blueprint $table) {
            //
        });
    }
}
