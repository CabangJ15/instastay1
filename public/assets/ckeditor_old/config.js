/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.extraPlugins = 'removeformat';
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for a single toolbar row.
	config.toolbarGroups = [
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'forms' },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'links' },
		{ name: 'insert' },
		// { name: 'styles' },
		{ name: 'colors' },
		{ name: 'tools' },
		{ name: 'others' },
		{ name: 'about' }
	];

	// The default plugins included in the basic setup define some buttons that
	// are not needed in a basic editor. They are removed here.
	config.removeButtons = 'Cut,Copy,Paste,Undo,Redo,Anchor,Underline,Strike,Subscript,Superscript';

	// Dialog windows are also simplified.
	config.removeDialogTabs = 'link:advanced';
	// var pathname = window.location.href;
	// alert(pathname)
	// config.contentsCss = pathname+'admin/assets/ckeditor_old/font/font.css';
	// //the next line add the new font to the combobox in CKEditor
	// config.font_names = 'Myriad Pro/myriad;' + config.font_names;
	// config.font_names = 'Myriad Pro Bold/myriadbold;' + config.font_names;
	// config.font_names = 'Calibri/calibri;' + config.font_names;
	// config.font_names = 'Calibri Bold/calibribold;' + config.font_names;
};

CKEDITOR.config.colorButton_colors =
'4d2b10,5c9b34,f2f39b,'+
'000,800000,8B4513,2F4F4F,008080,000080,4B0082,696969,' +
'B22222,A52A2A,DAA520,006400,40E0D0,0000CD,800080,808080,' +
'F00,FF8C00,FFD700,008000,0FF,00F,EE82EE,A9A9A9,' +
'FFA07A,FFA500,FFFF00,00FF00,AFEEEE,ADD8E6,DDA0DD,D3D3D3,' +
'FFF0F5,FAEBD7,FFFFE0,F0FFF0,F0FFFF,F0F8FF,E6E6FA,FFF';