<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    //
    use SoftDeletes;
    protected $table = 'image';

    protected $fillable = [
        'hotel_id', 
        'room_type_id', 
        'image_ref_id', 
        'image', 
        'created_by'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
