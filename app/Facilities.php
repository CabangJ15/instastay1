<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facilities extends Model
{
    //
    use SoftDeletes;
    protected $table = 'facilities';

    protected $fillable = [
        'facilities_ref_id', 
        'facilities', 
        'icon_file', 
        'created_by'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function facilities_main(){
        return $this->belongsTo('App\FacilitiesRef', 'facilities_ref_id', 'id');
    }

    public function hotel(){
        return $this->hasmany('App\HotelFacilities', 'facilities_id', 'id');
    }
}
