<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tbl_user';

    protected $fillable = [
        'username',
        'password',
        'token',
        'timestamp'
    ];

    public function info(){
        return $this->belongsTo('App\CustomerInfo', 'id', 'user_id');
    }
}
