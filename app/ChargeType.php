<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChargeType extends Model
{
    //
    use SoftDeletes;
    protected $table = 'charge_type';

    protected $fillable = [
        'charge_type', 
        'crated_by'
    ];
}
