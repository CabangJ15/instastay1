<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Amenities extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tbl_amenities';

    protected $fillable = [
        'amenities',
        'icon_file'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
