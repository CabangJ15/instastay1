<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DateTaxSetting extends Model
{
    //
    use SoftDeletes;
    protected $table = 'date_tax_setting';

    protected $fillable = [
        'hotel_id', 
        'sDate', 
        'eDate', 
        'tax_setting_id',
        'value',
    ];
}
