<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bed extends Model
{
    //
    use SoftDeletes;
    protected $table = 'bed';

    protected $fillable = [
        'bed'
    ];
}
