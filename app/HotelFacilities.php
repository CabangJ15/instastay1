<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelFacilities extends Model
{
    //
    use SoftDeletes;
    protected $table = 'hotel_facilities';

    protected $fillable = [
        'hotel_id', 
        'facilities_id', 
        'created_by'
    ];

    public function facilities(){
        return $this->hasOne('App\Facilities', 'id', 'facilities_id');
    }
}
