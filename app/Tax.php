<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tax extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tax_type';

    protected $fillable = [
        'tax_type', 
        'created_by'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
