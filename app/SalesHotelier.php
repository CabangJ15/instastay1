<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesHotelier extends Model
{
    //
    use SoftDeletes;
    protected $table = 'sale_to_hotelier';

    protected $fillable = [
        'sales_id', 
        'hotelier_id', 
        'created_by'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function sales(){
        return $this->belongsTo('App\User', 'sales_id', 'id');
    }

    public function hotelier(){
        return $this->belongsTo('App\Hotelier', 'hotelier_id', 'id');
    }
}
