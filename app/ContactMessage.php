<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactMessage extends Model
{
    //
    use SoftDeletes;
    protected $table = 'contact_message';

    protected $fillable = [
        'fname',
        'email',
        'message'
    ];
}
