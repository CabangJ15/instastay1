<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tbl_bookings';

    protected $fillable = [
        'user_id',
        'hotel_id',
        'booking_info',
        'is_paid',
        'status',
        'timestamp'
    ];
    
    public function hotel(){
        return $this->belongsTo('App\Hotel', 'hotel_id', 'id');
    }

    public function room_type(){
        return $this->belongsTo('App\Room', 'room_id', 'id');
    }

    public function customer(){
        return $this->belongsTo('App\Customer', 'user_id', 'id');
    }

    public function transaction(){
        return $this->belongsTo('App\Transaction', 'id', 'booking_id');
    }
}
