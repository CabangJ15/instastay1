<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tbl_hotels';

    protected $fillable = [
        'legal_entity_name', 
        'hotel_name', 
        'website', 
        'created_by', 
        'email',
        'telnos',
        'biznos',
        'star',
        'category_id',
        'country_id',
        'region_id',
        'city_id',
        'bank',
        'account_number',
        'tin_number',
        'min',
        'max',
        'latitude',
        'longitude',
        'address',
        'about',
        'city',
        'postcode',
        'img_file',
        'created_by',
        'flag',
        'is_active',
        'approve_by',
        'date_approve',
        'has_channel_manager',
        'has_diff_name',
        'is_chain_hotel',
        'hotelier_id'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function meta(){
        return $this->hasOne('App\HotelMeta', 'hotel_id', 'id');
    }

    public function facilities(){
        return $this->hasMany('App\HotelFacilities', 'hotel_id', 'id');
    }
}
