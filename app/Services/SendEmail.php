<?php
namespace App\Services;

use Carbon\Carbon;
use Illuminate\Database\Connection;
use App\EmailNotification;
use Mail;
use App\ContactMessage as ContactMessage;

class SendEmail
{
    public function sendEmail($email_template, $data){

        $name = $data['name'];
        $code = "";

        if(isset($data['code'])):
            $code = $data['code'];
        endif;

        // $view = view($email_template, compact('name','code'));

        // $email = new \SendGrid\Mail\Mail(); 
        // $email->setFrom("admin@instastay.app", "Julyeth Caladiao");
        // $email->setSubject($data['subject']);
        // $email->addTo($data['to'], $data['name']);
        // // $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
        // $email->addContent(
        //     "text/html", "$view"
        // );
        // $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

        // try {
        //     $response = $sendgrid->send($email);
        //     // print $response->statusCode() . "\n";
        //     // print_r($response->headers());
        //     // print $response->body() . "\n";
        // } catch (Exception $e) {
        //     // echo 'Caught exception: '. $e->getMessage() ."\n";
        // }

        Mail::send($email_template, $data, function($message) use ($data){
            $message->to($data['to'],$data['name'])->subject($data['subject']);
            $message->from("community@instastay.app", "Julyeth Caladiao");
        });
    }



    public function resetMail($code, $data){
        // return array($code, $data);

        $data = [
            'name'=>$data['name'],
            'username'=>$data['email'],
            'subject'=>"Reset Password Link",
            'to' => $data['email'],
            'code' => $code['code']
        ];

        // return view("email.resetPassword", $data);

        return $this->sendEmail("email.resetPassword", $data);
    }

    public function welcomeMail($user){

        // return $user;

        $data = [
            'name'=>$user['fname'],
            'username'=>$user['email'],
            'subject'=>"Thank you for choosing Instastay",
            'to'=> $user['email']
        ];

        return $this->sendEmail("email.welcome", $data);
    }

    public function contactMail($data){
        // return $data;

        $data = [
            'name'=>$data['fname'],
            'username'=>$data['email'],
            'subject'=>"Reach out to Instastay Helpdesk",
            'to' => $data['email'],
            'message' => $data['text']
        ];

        $this->addContactMessage($data);

        $this->sendEmail("email.contact", $data);
    }

    public function addContactMessage($data){
        return ContactMessage::create([
            'fname' => $data['name'],
            'email' => $data['to'],
            'message' => $data['message']
            ]);
    }


}

