<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimeAvailability extends Model
{
    //
    use SoftDeletes;
    protected $table = 'time_availability';

    protected $fillable = [
        'room_id',
        'c_in',
        'c_out',
        'amount',
        'alloted',
        'created_by'
    ];
}
