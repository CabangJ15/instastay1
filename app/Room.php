<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tbl_room';

    protected $fillable = [
        'hotel_id', 
        'room_name', 
        'description', 
        'sTime', 
        'eTime', 
        'guest',
        'max_guest',
        'extra_bed',
        'max_extra_bed',
        'room_size',
        'bathroom',
        'no_rooms',
        'alert',
        'is_child_allowed',
        'free_child',
        'max_child',
        'is_baby_cots',
        'img_file',
        'policies',
        'amenities_list'
    ];

    public function amenities(){
        return $this->hasMany('App\RoomAmenities', 'room_id', 'id');
    }

    public function images(){
        return $this->hasMany('App\RoomImage', 'link_id', 'id');
    }

    public function availability(){
        return $this->hasMany('App\RoomAvailability', 'room_id', 'id');
    }

    public function bed(){
        return $this->hasMany('App\RoomBed', 'room_id', 'id');
    }

    public function transaction(){
        return $this->hasMany('App\Transaction', 'room_id', 'id');
    }
}
