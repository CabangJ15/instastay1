<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerInfo extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tbl_user_info';

    protected $fillable = [
        'user_id',
        'image',
        'firstname',
        'lastname',
        'mobile',
        'birthdate',
        'redeem_code',
        'redeem_coins',
        'gender',
        'did_reset',
        'timestamp',
        'email'
    ];
}
