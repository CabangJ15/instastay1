<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelMeta extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tbl_hotels_meta';

    protected $fillable = [
        'hotel_id', 
        'name', 
        'detail', 
        'email',
        'timestamp'
    ];
}
