<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    //
    protected $table = "tbl_transactions";
    use SoftDeletes;

    protected $fillable = [
        'booking_id',
        'user_id',
        'room_id',
        'hotel_id',
        'or_number',
        'room_name',
        'pricing',
        'is_promo',
        'is_cancel',
        'created_by',
        'datestamp',
        'timestamp',
        'date_check_out',
        'time_date_check_out',
    ];

    public function booking(){
        return $this->belongsTo('App\Booking', 'booking_id', 'id');
    }

    public function hotel(){
        return $this->belongsTo('App\Hotel', 'hotel_id', 'id');
    }

    public function room_type(){
        return $this->belongsTo('App\Room', 'room_id', 'id');
    }

    public function customer(){
        return $this->belongsTo('App\Customer', 'user_id', 'id');
    }
}
