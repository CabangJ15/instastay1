<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomBed extends Model
{
    //
    use SoftDeletes;
    protected $table = 'room_bed';

    protected $fillable = [
        'room_id', 
        'bed_id', 
        'qty',
        'created_by'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function bed(){
        return $this->hasOne('App\Bed', 'id', 'bed_id');
    }
}
