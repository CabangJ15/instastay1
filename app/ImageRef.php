<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImageRef extends Model
{
    //
    use SoftDeletes;
    protected $table = 'img_ref';

    protected $fillable = [
        'image_ref',
        'created_by'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function images(){
        return $this->hasMany('App\Image', 'image_ref_id', 'id')->orderBy('id', 'desc');
    }
}
