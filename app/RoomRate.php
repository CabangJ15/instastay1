<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomRate extends Model
{
    //
    use SoftDeletes;
    protected $table = 'room_rate';

    protected $fillable = [
        'room_rate', 
        'hour', 
        'created_by'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
