<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaxSetting extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tax_setting';

    protected $fillable = [
        'tax_type_id', 
        'charge_type_id', 
        'tax_setting_type_id',
        'created_by'
    ];

    public function tax_type(){
        return $this->belongsTo('App\Tax', 'tax_type_id', 'id');
    }

    public function tax_setting_type(){
        return $this->belongsTo('App\TaxSettingType', 'tax_setting_type_id', 'id');
    }

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function charge_type(){
        return $this->belongsTo('App\ChargeType', 'charge_type_id', 'id');
    }
}
