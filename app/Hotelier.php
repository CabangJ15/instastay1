<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotelier extends Model
{
    //
    use SoftDeletes;
    protected $table = 'hotelier';

    protected $fillable = [
        'fname',
        'jobtitle',
        'mobile',
        'email',
        'hname',
        'address',
        'city',
        'postal_code',
        'contact',
        'hotel_email',
        'company',
        'c_address',
        'c_city',
        'c_postal_code',
        'no_hotel',
        'description',
    ];

    public function hotel(){
        return $this->belongsTo('App\Hotel', 'id', 'hotelier_id');
    }
}
