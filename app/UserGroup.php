<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserGroup extends Model
{
    //
    protected $table = 'user_group';
    use SoftDeletes;

    protected $fillable = [
        'access', 'editable', 'user_group', 'flag', 'created_by'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'group_id', 'id');
    }

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
