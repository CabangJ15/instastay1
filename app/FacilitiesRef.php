<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacilitiesRef extends Model
{
    //
    use SoftDeletes;
    protected $table = 'facilities_ref';

    protected $fillable = [
        'facilities_ref', 
        'created_by'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function facilities(){
        return $this->hasmany('App\Facilities', 'facilities_ref_id', 'id');
    }
}
