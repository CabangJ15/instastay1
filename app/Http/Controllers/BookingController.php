<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Repositories\InststayRepositories as InststayRepositories;

class BookingController extends Controller
{

    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;

        $countries = $this->repo->fetchCountries();
        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotels->id);
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($access[27])):
            return redirect('/hotels');
        endif;

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));
        
        return view('admin.hotels.booking', compact('menu', 'hotels', 'countries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fetchBooking(Request $request, $hotel_id){
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $data = [];

        $query = $this->repo->fetchTransactionViaDate($request->all());

        foreach($query->where('hotel_id', $hotel_id) as $result):
            // foreach(unserialize($result->pricing) as $result1):
            //     if($result1['description'] != "SERVICE FEE" && $result1['description'] != "SERVICE FEE"):
            //         $room_rate = $result1['description'];
            //         $price = $result1['price'];
            //     endif;
            // endforeach;

            $now = date("Y-m-d H:i:s");
            $check_out = $result->date_check_out." ".$result->time_date_check_out;

            $check = DB::SELECT("SELECT TIMESTAMPDIFF(SECOND, '$now', '$check_out') as duration");

            if($check[0]->duration < 0):
                $status = "<b style='color: green'>Completed</b>";
            else:
                $status = "<b style='color: orange'>Pending</b>";
            endif;

            if($result->is_cancel != 0):
                $status = "<b style='color: red'>Cancelled</b>";
            endif;

            $data[] = array(
                $result->customer->info->firstname." ".$result->customer->info->lastname,
                date("M d, Y h:i:s A", strtotime($result->datestamp." ".$result->timestamp)),
                date("M d, Y h:i:s A", strtotime($check_out)),
                $result->room_type->room_name,
                '',
                date("M d, Y h:i:s A", strtotime($result->created_at)),
                $status,
                $result->total_amount,
                0,
                "BN-".STR_PAD($result->id, 10, "0", STR_PAD_LEFT)

                );
        endforeach;

        $res = array('data'=>$data);
        return json_encode($res);
    }

    public function fetchCheckOut(Request $request, $hotel_id){
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;
        
        $data = [];

        $query = $this->repo->fetchTransactionViaDate($request->all());

        foreach($query->where('hotel_id', $hotel_id) as $result):
            // foreach(unserialize($result->pricing) as $result1):
            //     if($result1['description'] != "SERVICE FEE" && $result1['description'] != "SERVICE FEE"):
            //         $room_rate = $result1['description'];
            //         $price = $result1['price'];
            //     endif;
            // endforeach;
            $now = date("Y-m-d H:i:s");

            $check_in =$result->datestamp." ".$result->timestamp;
            $check_out = $result->date_check_out." ".$result->time_date_check_out;

            $check = DB::SELECT("SELECT TIMESTAMPDIFF(SECOND, '$now', '$check_out') as duration, TIMESTAMPDIFF(SECOND, '$now', '$check_in') as duration1");

            // if($result->booking->id == 20):
            //     return $check;
            // endif;

            if($check[0]->duration < 0):
                $status = "<b style='color: green'>Completed</b>";
            else:
                $status = "<b style='color: orange'>Pending</b>";
            endif;

            if($result->is_cancel != 0):
                $status = "<b style='color: red'>Cancelled</b>";
            endif;

            if($check[0]->duration < 0 && $result->is_cancel == 0):
            $data[] = array(
                $result->customer->info->firstname." ".$result->customer->info->lastname,
                date("M d, Y h:i:s A", strtotime($check_in)),
                date("M d, Y h:i:s A", strtotime($check_out)),
                $result->room_type->room_name,
                '',
                date("M d, Y h:i:s A", strtotime($result->created_at)),
                $status,
                $price,
                0,
                "BN-".STR_PAD($result->id, 10, "0", STR_PAD_LEFT)

                );
            endif;
        endforeach;

        $res = array('data'=>$data);
        return json_encode($res);
    }

    public function fetchCheckIn(Request $request, $hotel_id){
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $data = [];

        $query = $this->repo->fetchTransactionViaDate($request->all());

        foreach($query->where('hotel_id', $hotel_id) as $result):
            // foreach(unserialize($result->pricing) as $result1):
            //     if($result1['description'] != "SERVICE FEE" && $result1['description'] != "SERVICE FEE"):
            //         $room_rate = $result1['description'];
            //         $price = $result1['price'];
            //     endif;
            // endforeach;
            $now = date("Y-m-d H:i:s");

            $check_in =$result->datestamp." ".$result->timestamp;
            $check_out = $result->date_check_out." ".$result->time_date_check_out;

            $check = DB::SELECT("SELECT TIMESTAMPDIFF(SECOND, '$now', '$check_out') as duration, TIMESTAMPDIFF(SECOND, '$now', '$check_in') as duration1");

            // if($result->booking->id == 20):
            //     return $check;
            // endif;

            if($check[0]->duration < 0):
                $status = "<b style='color: green'>Completed</b>";
            elseif($check[0]->duration1 < 0 && $check[0]->duration > 0):
                $status = "<b style='color: black'>On Going</b>";
            else:
                $status = "<b style='color: orange'>Pending</b>";
            endif;

            if($result->is_cancel != 0):
                $status = "<b style='color: red'>Cancelled</b>";
            endif;

            if($check[0]->duration > 0 &&$check[0]->duration1 < 0 && $result->is_cancel == 0):
            $data[] = array(
                $result->customer->info->firstname." ".$result->customer->info->lastname,
                date("M d, Y h:i:s A", strtotime($check_in)),
                date("M d, Y h:i:s A", strtotime($check_out)),
                $result->room_type->room_name,
                '',
                date("M d, Y h:i:s A", strtotime($result->created_at)),
                $status,
                $price,
                0,
                "BN-".STR_PAD($result->id, 10, "0", STR_PAD_LEFT)

                );
            endif;
        endforeach;

        $res = array('data'=>$data);
        return json_encode($res);
    }
}
