<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Services\SendEmail as SendEmail;
// use SendGrid\Mail\Mail as SendGrid;

use App\Repositories\InststayRepositories as InststayRepositories;

class PageController extends Controller
{
    //
    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        // $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }

    public function index(){

        return view('welcome');
    }

    public function hotelier(){

        $city = $this->repo->fetchCityViaCountryCode('ph');

        return view('hotelier', compact('city'));
    }

    public function hotelier_inquiries(Request $request){

        $this->send = new SendEmail();

        $this->repo->addHotelier($request->all());

        try {

        $email = $this->send->welcomeMail($request->all());

        } catch (Exception $e) {

            // echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        $city = $this->repo->fetchCityViaCountryCode('ph');

        return view('hotelier_success', compact('city'));

    }

    public function wait(){
    	return view('wait');
    }

    public function contact(){
    	return view('contact');
    }

    public function admin(){
        // $menu = view('partial.menu');
        if(Auth::check()):

            if(Auth::user()->user_group_id == 1):
                return redirect('hotels');
            else:
                return redirect('adminDashboard');
            endif;
        else:
            return view('adminLogin');
        endif;
        
    }

    public function login(){
        if(Auth::check()):

            if(Auth::user()->user_group_id == 1):
                return redirect('hotels');
            else:
                return redirect('adminDashboard');
            endif;
        else:
            return view('login');
        endif;
    }

    public function test(){

        // $email = new \SendGrid\Mail\Mail(); 
        // $email->setFrom("jeffreycabang@gmail.com", "Jeffrey Cabang");
        // $email->setSubject("Sending with SendGrid is Fun");
        // $email->addTo("jeffreycabang@gmail.com", "Jeffrey Cabang");
        // // $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
        // $email->addContent(
        //     "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
        // );
        // $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

        // try {
        //     $response = $sendgrid->send($email);
        //     print $response->statusCode() . "\n";
        //     print_r($response->headers());
        //     print $response->body() . "\n";
        // } catch (Exception $e) {
        //     // echo 'Caught exception: '. $e->getMessage() ."\n";
        // }

        $send = new SendEmail();

        // return view('email.welcome');

        $user_data = $this->repo->fetchHotelsViaUserId(1);

        return $email = $send->welcomeMail($user_data);
    }

    public function forgot_password(){

        return view('forgot_password');
    }

    public function password_reset(Request $request){
        // return $request->all();

        $this->send = new SendEmail();

        $check = $this->repo->checkEmail($request->get('email'));

        if($check == ""):
            return redirect('/forgot_password')->with('notification', 'User not Exist');
        else:
            // create email password link
            $reset = $this->repo->resetPasswordLink($check);

            $email = $this->send->resetMail($reset, $check);
        endif;

        return redirect('/instanet')->with('notification', 'Check your email for reset link');
    }

    public function resetPassword($code){

        $check = $this->repo->fetchResetPasswordViaCode($code);

        if($check == ""):
            return redirect('/forgot_password')->with('notification', 'Link has Expired');
        else:
            return view('resetPassword', compact('code'));
        endif;
    }

    public function passwordReset($code, Request $request){
        $this->validate($request, [
            'password'=>'required|confirmed',
        ]);

        $user = $this->repo->passwordReset($code, $request->all());

        if($user == ""):
            return redirect('/instanet');
        endif;

        if($user->user_group_id != 2):
            return redirect('/instanet')->with('notification', 'Reset Password Completed');
        else:
            return redirect('/admin')->with('notification', 'Reset Password Completed');
        endif;
    }

    public function termsOfUse(){
        return view('termsOfUse');
    }

    public function contactMail(Request $request){
        // return $request;

        $this->send = new SendEmail();
        $email = $this->send->contactMail($request->all());
        return redirect('contact');
    }
    
}
