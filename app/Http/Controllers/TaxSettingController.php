<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Repositories\InststayRepositories as InststayRepositories;

class TaxSettingController extends Controller
{
    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $hotel_id)
    {
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $this->repo->updateTaxSettings($request->all(), $hotel_id);

        return redirect('/tax_setting/'.$hotel_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;
        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotels->id);
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));
        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));
        
        return view('admin.hotels.tax_settings', compact('menu', 'hotels'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fetchTaxSettings($hotel_id){
        
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $date = Input::get('date');
        $date = explode("/", $date);
        $sDate = $date[1]."-".$date[0]."-01";
        $eDate = date("Y-m-t", strtotime($date[1]."-".$date[0]."-01"));

        $tax_setting = $this->repo->fetchTaxSettings();
        $hotel_settings = $this->repo->fetchTaxSettingsViaDate($sDate, $eDate, $hotel_id);
        $trading = 0;
        $label = $trading == 1 ? "checked" : "";
        $tax_setting_id = 0;
        $value = 0;
        $date = [];

        

        foreach($tax_setting as $result):
            foreach($hotel_settings->where('tax_setting_id', $result->id) as $result1):
                $trading = $result1->is_free;
                $label = $trading == 1 ? "checked" : "";
                $tax_setting_id = $result1->id;
                $value = $result1->value;
            endforeach;


            $checkbox = '<div id="ck-button"><label><input type="hidden" name="trading['.$result->id.']" value="'.$trading.'"><input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" '.$label.'><span><i class="fa fa-check"></i></span></label>';

            $data[] = array(
                date("d-M-Y", strtotime($sDate)).'<input type="hidden" name="sDate" value="'.$sDate.'"><input type="hidden" name="eDate" value="'.$eDate.'"><input type="hidden" name="tax_setting_id['.$result->id.']" value="'.$tax_setting_id.'">',
                date("d-M-Y", strtotime($eDate)),
                $result->tax_type->tax_type,
                $result->charge_type->charge_type,
                $result->tax_setting_type->tax_setting_type,
                '<center>'.$checkbox.'</center>',
                '<input step="any" class="form-control" value="'.$value.'" type="number" min="0" name="value['.$result->id.']">',
                '%',
                '',
                ''
                );
        endforeach;

        $res = array('data'=>$data);
        return json_encode($res);
    }
}
