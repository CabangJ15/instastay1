<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\HotelFacilities as HotelFacilities;
use App\FacilitiesRef as FacilitiesRef;
use App\Facilities as Facilities;

use App\Repositories\InststayRepositories as InststayRepositories;

class FacilitiesController extends Controller
{
    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;
        
        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;
        $ref = [];
        $idid = array();
        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotels->id);
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($access[23])):
            return redirect('/hotels');
        endif;

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));
        $x = 0;
        $facilities_ref = $this->repo->fetchFacilitiesRef();
        $facilites_ref_id = FacilitiesRef::get(['id']);
        foreach($facilites_ref_id as $ids):
            $check1 = Facilities::where('facilities_ref_id', $ids->id)->get(['id']);
            if(count($check1) != 0):
                if($x == 0):
                    array_push($idid, $ids->id);
                    $x++;
                endif;
                $check = HotelFacilities::where('hotel_id', $hotel_id)
                    ->whereIn('facilities_id', $check1)
                    ->count();

                $ref[$ids->id] = $check;
            endif;
            
        endforeach;
        
        return view('admin.hotels.facilities', compact('menu', 'hotels', 'facilities_ref', 'ref', 'idid'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function facilities_change($hotel_id){
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[23])):
            return "Err";
        endif;

        $data = Input::all();

        $this->repo->updateHotelFacilities($data, $hotel_id);

        return count($this->repo->checkFacilitiesRefCountViaHotelId($data['facilities_ref'], $hotel_id));
    }
}
