<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Carbon\CarbonPeriod;

use App\Repositories\InststayRepositories as InststayRepositories;

class RoomRateController extends Controller
{
    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->user_group_id != 2):
            return redirect('/adminDashboard');
        endif;

        $hotel_id = "";
        $room_type = [];
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menuAdmin', compact('hotel_id','room_type','access','editable'));

        return view('admin.room_rate.list', compact('menu', 'hotels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(Auth::user()->user_group_id != 2):
            return redirect('/adminDashboard');
        endif;

        $hotel_id = "";
        $room_type = [];
        $query = [];
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menuAdmin', compact('hotel_id','room_type','access','editable'));

        $room_rate_id = "";
        $room_rate = "";
        $hour = "";
        $label = "Add";
        $label1 = "Create";

        return view('admin.room_rate.create', compact('menu', 'hotels', 'label', 'label1', 'room_rate_id', 'room_rate', 'hour', 'query'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->repo->addRoomRate($request->all());

        return redirect('/room_rate');
    }

    /**
     * Display the specified resource.s
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;

        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotel_id);

        $sDate = date("Y-m-01");
        $eDate = date("Y-m-t");
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        return view('admin.hotels.room_rate', compact('menu', 'hotels', 'room_type', 'sDate', 'eDate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if(Auth::user()->user_group_id != 2):
            return redirect('/adminDashboard');
        endif;

        $hotel_id = "";
        $room_type = [];
        $query = $this->repo->fetchRoomRateViaId($id);
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menuAdmin', compact('hotel_id','room_type','access','editable'));

        $room_rate_id = $id;
        $room_rate = $query->room_rate;
        $hour = $query->hour;
        $label = "Update";
        $label1 = "Update";

        return view('admin.room_rate.create', compact('menu', 'hotels', 'label', 'label1', 'room_rate_id', 'room_rate', 'hour', 'query'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if(Auth::user()->user_group_id != 2):
            return redirect('/adminDashboard');
        endif;

        $this->repo->updateRoomRate($request->all(), $id);

        return redirect('/room_rate/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(Auth::user()->user_group_id != 2):
            return redirect('/adminDashboard');
        endif;

        $query = $this->repo->fetchRoomRateViaId($id);
        $query->delete();
    }

    public function fetchRoomRate(){
        if(Auth::user()->user_group_id != 2):
            return redirect('/adminDashboard');
        endif;
        
        $query = $this->repo->fetchRoomRate();

        $data = [];

        foreach($query as $result):
            $url = URL('/');
            $action = "";
            // if(isset($editable[58])):
                $action ="<a style='border: 1px solid #b8c7ce; margin-left: .5em;' href='$url/room_rate/$result->id/edit' style='float: right;' class='btn btn-info btn-flat btn-pri'>
                                    <i class='fa fa-pencil-square-o'></i> Edit
                                </a>";
                // if(count($result->images) == 0):
                $action .="<button data-id='$result->id' style='border: 1px solid #b8c7ce; margin-left: .5em;' style='float: right;' class='btn btn-danger btn-flat btn-pri icon-delete'>
                                <i class='fa fa-trash'></i> Delete
                            </a>";
                // endif;
            // endif;

            $data[] = array(
                $result->room_rate,
                $result->hour,
                $result->creator->name."<br>(". $result->creator->email.")",
                $action
            );

        endforeach;

        $res = array('data'=>$data);
        return json_encode($res);
    }

    public function createRoomRate(){
        $room_type_id = Input::get('room_type_id');
        $hotel_id = Input::get('hotel_id');

        $room_rate = $this->repo->fetchRoomRate();
        $room_rate_price = $this->repo->fetchRoomRatePriceNullDate($room_type_id);
        $time_availability = $this->repo->fetchTimeAvailabilityViaRoomId($room_type_id);

        return view('admin.room_rate.room_rate', compact('room_type_id','room_rate','room_rate_price','hotel_id','time_availability'));
    }

    public function addRoomRate(Request $request, $room_type_id){
        $hotel_id = $request->get('hotel_id');
        $room_rate = $request->get('room_rate');

        $this->repo->addRoomRateAll($room_rate, $room_type_id);
        $this->repo->addTimeAvailability($request->all(), $room_type_id);

        return redirect('/room_rate/'.$hotel_id);
    }

    public function adjustDailyRate($hotel_id){
        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;

        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotel_id);

        $sDate = date("Y-m-d");
        $eDate = date("Y-m-t");
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        return view('admin.hotels.adjust_daily_rate', compact('menu', 'hotels', 'room_type', 'sDate', 'eDate'));
    }

    public function createAdjustDailyRate(Request $request){

        $hotel_id = Input::get('hotel_id');
        $room_type_id = Input::get('room_type_id');
        $sDate = Input::get('sDate');
        $eDate = Input::get('eDate');
        $sun = Input::get('sun');
        $mon = Input::get('mon');
        $tue = Input::get('tue');
        $wed = Input::get('wed');
        $thu = Input::get('thu');
        $fri = Input::get('fri');
        $sat = Input::get('sat');

        $data['day'] = array();

        if($sun != 0):
            array_push($data['day'], 7);
        endif;

        if($mon != 0):
            array_push($data['day'], 1);
        endif;

        if($tue != 0):
            array_push($data['day'], 2);
        endif;

        if($wed != 0):
            array_push($data['day'], 3);
        endif;

        if($thu != 0):
            array_push($data['day'], 4);
        endif;

        if($fri != 0):
            array_push($data['day'], 5);
        endif;

        if($sat != 0):
            array_push($data['day'], 6);
        endif;

        if(count($data['day']) == 0):
            unset($data['day']);
        endif;

        $period = CarbonPeriod::create($sDate, $eDate);
        $dates = array();
        foreach($period as $date):
            $check = date("w", strtotime($date->format('Y-m-d')));
            if($check == 0):
                $check1 = 7;
            else:
                $check1 = $check;
            endif;
            if(isset($data['day'])):
                $test = array_search($check1, $data['day']);
                if($test !== false):
                    // echo array_search($check1, $data['day'])." Not Exist<br>";
                    array_push($dates, $date->format('Y-m-d'));
                endif;
            else:
                array_push($dates, $date->format('Y-m-d'));
            endif;
        endforeach;

        $room_rate = $this->repo->fetchRoomRate();
        $room_rate_price_null = $this->repo->fetchRoomRatePriceNullDate($room_type_id);
        $room_rate_price = $this->repo->fetchRoomRatePriceViaDate($room_type_id, $dates);

        return view('admin.hotels.create_daily_rate', compact('room_rate', 'room_rate_price', 'room_rate_price_null', 'dates', 'room_type_id', 'hotel_id'));
    }

    public function addRoomRateDaily(Request $request, $hotel_id){

        $this->repo->addRoomRateDailyAll($request->all());

        return redirect('/adjust_daily_rate/'.$hotel_id);
    }
}
