<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Repositories\InststayRepositories as InststayRepositories;

class AdminController extends Controller
{
    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $hotel_id = "";
        $room_type = [];

        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if($user_group->id == 1):
            return redirect('/hotels');
        endif;

        $calendar = "";

        $menu = view('partial.menuAdmin', compact('hotel_id','room_type','access','editable'));

        return view('admin.admin.dashboard', compact('menu','calendar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function properties(){
        $hotel_id = "";
        $room_type = [];

        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if($user_group->id == 1):
            return redirect('/hotels');
        endif;

        $calendar = "";

        $menu = view('partial.menuAdmin', compact('hotel_id','room_type','access','editable'));

        return view('admin.admin.properties', compact('menu','calendar'));
    }

    public function fetchProperties(){
        $query = $this->repo->fetchHotelier();
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $data = [];

        foreach($query->where('sales_id', '!=', NULL) as $result):
            $url = URL('/');
            $action = "";
            // if(isset($editable[9])):
                $action ='<div class="dropdown">
                  <button style="    width: 100%;
                    background: #717171;
                    min-width: 79px;background: #717171; color: #FFFFFF" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                  <span class="caret"></span></button>
                  <ul class="dropdown-menu">
                    <li style="background: #57D48D; opacity: 0.22;"><a style="color: black;" href="#">Contacted</a></li>
                    <li style="background: #57D48D; opacity: 0.43;"><a style="color: black;" href="#">Proposal</a></li>
                    <li style="background: #94EF1B; opacity: 0.56;"><a style="color: black;" href="#">Onboarding</a></li>
                    <li style="background: #FF9500; opacity: 0.56;"><a style="color: black;" href="#">Reset Password</a></li>
                    <li style="background: #04BF55; opacity: 0.56;"><a style="color: black;" href="#">Activate</a></li>
                    <li style="background: #DC2747; opacity: 0.43;"><a style="color: black;" href="#">Deactivate</a></li>
                  </ul>
                </div>';
                // endif;
            // endif;

            $data[] = array(
                $result->id,
                $result->hotel_name,
                $result->creator->name,
                '',
                $result->telnos,
                $result->biznos,
                $result->address,
                '',
                $result->created_at != "" ? date("M d, Y H:i:s", strtotime($result->created_at)) : '',
                '',
                '',
                '',
                $action
            );

        endforeach;

        $res = array('data'=>$data);
        return json_encode($res);
    }

    public function fetchLeads(){

      // return Auth::user()->user_group_id;

        if(Auth::user()->user_group_id == 2):

          $query = $this->repo->fetchHotelier();
          $label = "";

        else:

          $query = $this->repo->fetchHotelierViaSalesId(Auth::user()->id);

          $label = "disabled='disabled'";  

        endif;

        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $sales = $this->repo->fetchSaleRep();

        $data = [];

        
        

          if(Input::get('type') != 1):

            $data1 = $query->where('sales_id', NULL);

          else:
            $data1 = $query;
          endif;

          foreach($data1 as $result):

            // if(Input::get('type') != 0):
            //   $label =
            // endif;

              $option = "";

              $option .= "<select onchange='assignSalesRep(this.value, $result->id)' class='form-control mySelect' $label><option value=''>Select a Sales Representative</option>";

              
                foreach($sales as $result1):

                  $option .= "<option value='$result1->id'";
                  if($result1->id == $result->sales_id):
                    $option .=" selected";
                  endif;

                  $option .=">$result1->name</option>";

                
                endforeach;

          $option .= "</select>";

            $url = URL('/');
            $action = "";
            // if(isset($editable[9])):
                $action ='<div class="dropdown">
                  <button style="    width: 100%;
                    background: #717171;
                    min-width: 79px;background: #717171; color: #FFFFFF" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                  <span class="caret"></span></button>
                  <ul class="dropdown-menu">';
                  if($result->is_action == ""):
                    $action .= '<li style="background: #57D48D; opacity: 0.33;"><a style="color: black;" href="'.URL('/').'/contacted/'.$result->id.'">Contacted</a></li>';

                    $label = "";
                  endif;

                  if($result->is_action == 1):
                    $action .= '<li style="background: #57D48D; opacity: 0.43;"><a style="color: black;" href="'.URL('/').'/proposal/'.$result->id.'">Proposal</a></li>';
                    $label = "Contacted";
                  endif;

                  if($result->is_action == 2):
                    $action .= '<li style="background: #94EF1B; opacity: 0.56;"><a onclick="fetchAddHotel('.$result->id.')" style="cursor: pointer; color: black;" data-toggle="modal" data-target="#myModal">Onboarding</a></li>';
                    $label = "Proposal";
                  endif;

                  if($result->is_action >= 3):

                    if($result->is_action == 3):
                      $label = "Onboarding";
                    elseif($result->is_action == 4):
                      $label = "Activate";
                    elseif($result->is_action == 5):
                      $label = "Deactivate";
                    endif;
                    // <li style="background: #FF9500; opacity: 0.56;"><a style="color: black;" href="#">Reset Password</a></li>
                    $action .= '<li style="background: #04BF55; opacity: 0.56;"><a style="color: black;" href="'.URL('/').'/activate/'.$result->id.'"">Activate</a></li>
                    <li style="background: #DC2747; opacity: 0.43;"><a style="color: black;" href="'.URL('/').'/deactivate/'.$result->id.'">Deactivate</a></li>';

                  endif;

                  $action .='</ul>
                </div>';
                // endif;
            // endif;

            $data[] = array(
                STR_PAD($result->id, 11, '0', STR_PAD_LEFT),
                $result->hname,
                $result->fname,
                $result->email,
                $result->jobtitle,
                $result->contact,
                $result->mobile,
                $result->address,
                $result->city,
                $result->created_at != "" ? date("M d, Y H:i:s", strtotime($result->created_at)) : '',
                $option,
                $label,
                $result->action_at != "" ? date("M d, Y h:i:s A", strtotime($result->action_at)) : '',
                $action
            );

        endforeach;

        $res = array('data'=>$data);
        return json_encode($res);
    }

    public function leads(){
        $hotel_id = "";
        $room_type = [];

        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if($user_group->id == 1):
            return redirect('/hotels');
        endif;

        $calendar = "";

        $menu = view('partial.menuAdmin', compact('hotel_id','room_type','access','editable'));

        return view('admin.admin.leads', compact('menu','calendar'));
    }

    public function assignSalesRep($id, Request $request){
      return $this->repo->assignSalesRep($id, $request->all());
    }

    public function contacted($id){
      $this->repo->contacted($id);
      return redirect('/properties');

    }

    public function proposal($id){
      $this->repo->proposal($id);
      return redirect('/properties');

    }

    public function activate($id){
      $this->repo->activate($id);
      return redirect('/properties');

    }

    public function deactivate($id){
      $this->repo->deactivate($id);
      return redirect('/properties');

    }

    public function fetchAddHotel($id){
      $query = $this->repo->fetchHotelierViaId($id);

      $facebook = $query->hotel_email != "" ? explode("/", $query->hotel_email) : "";

      if($facebook != ""):

        if(count($facebook) < 2):
          $facebook = $facebook[1];
        else:
          $facebook = "";
        endif;

      endif;

      $hotels = array(
                "id"=> "",
                "hotel_name"=> $query->hname,
                "legal_entity_name"=> "",
                "email"=> $query->email,
                "website"=> "",
                "facebook"=> $facebook,
                "twitter"=> null,
                "instagram"=> null,
                "telnos"=> $query->mobile,
                "biznos"=> null,
                "star"=> null,
                "category_id"=> null,
                "country_id"=> null,
                "region_id"=> null,
                "city_id"=> null,
                "city"=> $query->city,
                "bank"=> null,
                "account_number"=> null,
                "tin_number"=> null,
                "latitude"=> 14.5712393,
                "longitude"=> 121.0660037,
                "about"=> "",
                "address"=> $query->address,
                "postcode"=> $query->postal_code,
                "img_file"=> "",
                "approve_by"=> null,
                "flag"=> null,
                "approve_at"=> null,
                "timestamp"=> null,
                "created_at"=> null,
                "updated_at"=> null,
                "deleted_at"=> null,
                "has_diff_name"=> 1,
                "has_channel_manager"=> 1,
                "is_chain_hotel"=> 1,
            );

      $hotels = (object)$hotels;

      $city = $this->repo->fetchCityViaCountryCode('ph');

      $view = view('admin.admin.addHotel', compact('hotels', 'city', 'id'));

      return "$view";
    }

    public function addHotel(Request $request){

      if ($request->hasFile('myfile')) {
            $image = $request->file('myfile');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('uploads');
            $image->move($destinationPath, $name);
        } else {
            $name = NULL;
        }

      $this->repo->addHotel($request->all(), $name);

      return redirect('/properties');

    }


}
