<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Repositories\InststayRepositories as InststayRepositories;

class UserGroupController extends Controller
{
    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $hotel_id = "";
        $room_type = [];

        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[17])):
            return redirect('/hotels');
        endif;

        $menu = view('partial.menuAdmin', compact('hotel_id','room_type','access','editable'));
        
        return view('admin.user_group.list', compact('menu','access','editable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[17])):
            return redirect('/hotels');
        endif;

        $hotel_id = "";
        $room_type = [];

        $menu = view('partial.menuAdmin', compact('hotel_id','room_type','access','editable'));

        $group_list = $this->repo->getGroup();
        // $menu = $this->repo->menu();
        $query = []; //branch data
        $user_group_id = "";
        $user_group = "";
        $flag = 1;
        $access_list = $this->fetchAccess();
        $editable1 = [];
        $editable2 = [];

        $label = "Add";
        $label1 = "Create";

        return view('admin.user_group.create', compact('menu','query','label','label1','group','user_group_id','user_group','access_list','flag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[17])):
            return redirect('/hotels');
        endif;

        $user = $this->repo->addUserGroup($request->all());

        return redirect('/user_group');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect('/user');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[17])):
            return redirect('/hotels');
        endif;

        $hotel_id = "";
        $room_type = [];

        $menu = view('partial.menuAdmin', compact('hotel_id','room_type','access','editable'));

        $group_list = $this->repo->getGroup();
        // $menu = $this->repo->menu();
        $query = $this->repo->fetchUserGroupViaId($id);
        $user_group_id = $id;
        $user_group = $query->user_group;
        $flag = $query->flag;
        $access_list = $this->fetchAccess();
        $editable1 = array_flip(explode(",", $query->access));
        $editable2 = array_flip(explode(",", $query->editable));

        $label = "Update";
        $label1 = "Update";

        return view('admin.user_group.create', compact('menu','query','label','label1','group','user_group_id','user_group','access_list','flag','editable1','editable2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[17])):
            return redirect('/hotels');
        endif;

        $user = $this->repo->updateUserGroup($request->all(), $id);

        return redirect('/user_group/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[17])):
            return redirect('/hotels');
        endif;

        $user = $this->repo->fetchUserGroupViaId($id);
        $user->delete();
    }

    public function fetchUserGroup(){

        $query = $this->repo->fetchUserGroup();
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $data = [];

        foreach($query->where('id', '!=', 1) as $result):
            $url = URL('/');
            $action = "";
                if(isset($editable[17])):
                    $action ="<a style='border: 1px solid #b8c7ce; margin-left: .5em;' href='$url/user_group/$result->id/edit' style='float: right;' class='btn btn-info btn-flat btn-pri'>
                                        <i class='fa fa-pencil-square-o'></i> Edit
                                    </a>";
                    if($result->id > 3):
                        $action .="<button data-id='$result->id' style='border: 1px solid #b8c7ce; margin-left: .5em;' style='float: right;' class='btn btn-danger btn-flat btn-pri icon-delete'>
                                        <i class='fa fa-trash'></i> Delete
                                    </a>";
                    endif;
                endif;

            $data[] = array(
                $result->user_group,
                $result->creator['name']."<br>(".$result->creator['email'].")",
                $action
            );

        endforeach;

        $res = array('data'=>$data);
        return json_encode($res);
    }

    public function fetchAccess(){
        return DB::select("
            SELECT b.id as id, a.access_name, CONCAT(a.access_name,' > ',b.access_name) as label, b.with_editable
            FROM
            (SELECT id, access_name, parent_id, with_editable FROM access) as a
            INNER JOIN
            (SELECT id, access_name, parent_id, with_editable FROM access) as b
            ON
            a.id = b.parent_id
            WHERE a.parent_id IS NULL
            UNION ALL
            SELECT id, access_name, access_name as label, with_editable  FROM access
            WHERE parent_id IS NULL
            UNION ALL
            SELECT b.id as id, a.access_name, CONCAT(c.access_name,' > ',a.access_name,' > ',
            b.access_name) as label, b.with_editable
            FROM
            (SELECT id, access_name, parent_id, with_editable FROM access) as a
            INNER JOIN
            (SELECT id, access_name, parent_id, with_editable FROM access) as b
            ON
            a.id = b.parent_id
            INNER JOIN
            (SELECT * FROM access) as c
            ON c.id = a.parent_id
            WHERE c.parent_id IS NULL
            UNION ALL
            SELECT b.id as id, a.access_name, CONCAT(d.access_name,' > ',c.access_name,' > ',a.access_name,' > ',b.access_name) as label, b.with_editable
            FROM
            (SELECT id, access_name, parent_id, with_editable FROM access) as a
            INNER JOIN
            (SELECT id, access_name, parent_id, with_editable FROM access) as b
            ON
            a.id = b.parent_id
            INNER JOIN
            (SELECT * FROM access) as c
            ON c.id = a.parent_id
            INNER JOIN
            (SELECT * FROM access) as d
            ON d.id = c.parent_id
            WHERE d.parent_id IS NULL
            UNION ALL
            SELECT b.id as id, a.access_name, CONCAT(e.access_name,' > ', d.access_name,' > ',c.access_name,' > ',a.access_name,' > ',b.access_name) as label, b.with_editable
            FROM
            (SELECT id, access_name, parent_id, with_editable  FROM access WHERE flag != 0) as a
            INNER JOIN
            (SELECT id, access_name, parent_id, with_editable  FROM access WHERE flag != 0) as b
            ON
            a.id = b.parent_id
            INNER JOIN
            (SELECT id, access_name, parent_id, with_editable  FROM access WHERE flag != 0) as c
            ON c.id = a.parent_id
            INNER JOIN
            (SELECT id, access_name, parent_id, with_editable  FROM access WHERE flag != 0) as d
            ON d.id = c.parent_id
            INNER JOIN
            (SELECT id, access_name, parent_id, with_editable  FROM access WHERE flag != 0) as e
            ON e.id = d.parent_id
            ORDER BY label, id
        ");
    }
}
