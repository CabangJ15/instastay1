<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Repositories\InststayRepositories as InststayRepositories;

class InventoryController extends Controller
{

    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;

        if(Input::get('date') == ""):
          $month = date("m");
          $year = date("Y");
          $today = date("Y-m-d");
        else:
          $date = explode("/", Input::get('date'));
          $month = $date[0];
          $year = $date[1];
          $today = $date[1]."-".$date[0]."-01";
          $today = date("Y-m-d");
        endif;

        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotel_id);
        // if(count($room_type) != 0):
        //   foreach($room_type as $result1):
        //     $room_id = $result1->id;
        //     $room_type_alert = $result1->alert;
        //     break;
        //   endforeach;
        //   $result = $this->repo->fetchRoomAvailabilityViaDate($year, $month, $hotel_id, $room_id);
        // else:
        //   $result = [];
        //   $room_type_alert = 0;

        // endif;

        // $calendar = $this->build_html_calendar($year, $month, $result, $room_type_alert);
        $calendar = "";

        $countries = $this->repo->fetchCountries();
        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotels->id);
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));
        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));
        
        return view('admin.hotels.inventory', compact('menu', 'hotels', 'countries', 'calendar', 'room_type', 'today'));
    }

    public function makeCalendar(){

      if(Input::get('date') == ""):
        $month = date("m");
        $year = date("Y");
      else:
        $date = explode("/", Input::get('date'));
        $month = $date[0];
        $year = $date[1];
      endif;

      $room_type = $this->repo->fetchRoomTypeViaHotelId(Input::get('hotel_id'));

      if(count($room_type) != 0):
          foreach($room_type->where('id', Input::get('room_type_id')) as $result1):
            $room_id = $result1->id;
            $room_type_alert = $result1->alert;
            break;
          endforeach;
          $result = $this->repo->fetchRoomAvailabilityViaDate($year, $month, Input::get('hotel_id'), $room_id);
        else:
          $result = [];
          $room_type_alert = 0;

        endif;

        $calendar = $this->build_html_calendar($year, $month, $result, $room_type_alert);

        return "$calendar";
    }

    public function inventoryYear($hotel_id)
    {
        //
      if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;
        
        $css_cal = 'calendar';
        $css_cal_row = 'calendar-row';
        $css_cal_day_head = 'calendar-day-head';
        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;

        $month = date("m");
        $year = Input::get('date') == "" ? date("Y") : Input::get('date');

        $headings = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];

        $date3 = $month."/".$year;

        $calendar_full = "";
        
        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotel_id);
        $countries = $this->repo->fetchCountries();
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));
        
        return view('admin.hotels.inventoryYear', compact('menu', 'hotels', 'countries', 'calendar_full', 'room_type', 'year', 'date3'));
    }

    public function makeCalendarYear(){
      $hotels = $this->repo->fetchHotelsViaId(Input::get('hotel_id'), Auth::user()->id);
      $date = explode("/", Input::get('date'));
      $month = $date[0];
      $year = $date[1];

      $room_type = $this->repo->fetchRoomTypeViaHotelId(Input::get('hotel_id'));

    if(count($room_type) != 0):
        foreach($room_type->where('id', Input::get('room_type_id')) as $result1):
          $room_id = $result1->id;
          $room_type_alert = $result1->alert;
          break;
        endforeach;
        $result = $this->repo->fetchRoomAvailabilityViaYear($year, Input::get('hotel_id'), $room_id);
      else:
        $result = [];
        $room_type_alert = 0;

      endif;

      $css_cal = 'calendar';
      $css_cal_row = 'calendar-row';
      $css_cal_day_head = 'calendar-day-head';
      $headings = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];

       $calendar_full = "<table>
       <tbody><tr>";
        for($x=1; $x <= 12; $x++):
           $month = str_pad($x, 2, "0", STR_PAD_LEFT);
           $date = date("M", strtotime($year."-".$month."-01"));
          if($x == 1):
            $url =URL('/')."/inventory/".$hotels->id."?date=".$month."/".$year;
            $calendar_full .=
            "<td><a style='color: black;' href=' $url'>".
            "<center><b>$date</b><br><br><table style='transform: rotate(-90deg) scaleX(-1);'cellpadding='0' cellspacing='0' class='{$css_cal}'>" .
            "<tr class='{$css_cal_row}'>" .
            "<td style='transform: rotate(-90deg) scaleX(-1);' class='{$css_cal_day_head}'><center><b>" .
            implode("</td><td style='transform: rotate(-90deg) scaleX(-1);'  class='{$css_cal_day_head}'><center><b>", $headings) .
            "</b></center></td></a>" .
            "</tr>";
          else:
            $url =URL('/')."/inventory/".$hotels->id."?date=".$month."/".$year;
            $calendar_full .= "<td><a style='color: black;' href='$url'><center><b>$date</b><br><br><table style='transform: rotate(-90deg)  scaleX(-1);' cellpadding='0' cellspacing='0' class='{$css_cal}'>";
          endif;

          $calendar_full .= $this->build_html_calendar1($year, $month, $x, $result, $room_type_alert);
          $calendar_full .= '</table></center><td>';
        endfor;
        $calendar_full .= "</tr></table>";

        return $calendar_full;
    }

    public function weeks($month, $year){
            $num_of_days = date("t", mktime(0,0,0,$month,1,$year)); 
            $lastday = date("t", mktime(0, 0, 0, $month, 1, $year)); 
            $no_of_weeks = 0; 
            $count_weeks = 0; 
            while($no_of_weeks < $lastday){ 
                $no_of_weeks += 7; 
                $count_weeks++; 
            } 
      return $count_weeks;
    } 


    function build_html_calendar1($year, $month, $x, $result, $room_type_alert, $events = null) {
      $column = $this->weeks($month, $year);
      // CSS classes
      $css_cal = 'calendar';
      $css_cal_row = 'calendar-row';
      $css_cal_day_head = 'calendar-day-head';
      $css_cal_day = 'calendar-day';
      $css_cal_day_number = 'day-number';
      $css_cal_day_blank = 'calendar-day-np';
      $css_cal_day_event = 'calendar-day-event';
      $css_cal_event = 'calendar-event';

      // Table headings

      // Days and weeks
      $calendar = "";

      $running_day = date('N', mktime(0, 0, 0, $month, 1, $year));
      $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));

      // Row for week one
      // $calendar .= "<tr class='{$css_cal_row}'><td colspan='7'><center>Test</center></td></tr>";
      $calendar .= "<tr class='{$css_cal_row}'>";

      // Print "blank" days until the first of the current week
      for ($x = 1; $x < $running_day; $x++) {
        $calendar .= "<td class='{$css_cal_day_blank}'> </td>";
      }
      $y = 0;
      // Keep going with days...
      for ($day = 1; $day <= $days_in_month; $day++) {

        // Check if there is an event today
        $cur_date = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
        $draw_event = false;
        if (isset($events) && isset($events[$cur_date])) {
          $draw_event = true;
        }

        $check = $result->where('date_applied', $cur_date);

        if(count($check) == 0):
          $test = 0;
          $test1 = 1;
        else:
          $test = 1;
          $test1 = 0;
        endif;

        if($test == 0):
          $label = "calendarRed";
          $label2 = "checked";
          $label1 = "<span class='myCalendarTag'>
              Closed
            </span>";
          $count = 0;
        else:
          $label2 = "";
          $label = "calendarGreen";
          $label1 = "";
          foreach($check as $result1):
            $count = $result1->no_room;
          endforeach;
        endif;

        if($room_type_alert > $count && $count != 0):
          $label = "calendarOrange";
        elseif($count == 0):
          $label = "calendarRed";
          $label2 = "checked";
          $label1 = "<span class='myCalendarTag'>
              Closed
            </span>";
          $test1 = 1;
        endif;

        // Day cell
        $calendar .= $draw_event ?
          "<td class='{$css_cal_day} {$css_cal_day_event}'>" :
          "<td class='{$css_cal_day}'>";

        // Add the day number
        $calendar .= "<div style='border: 1px solid; width: 1em; height: 1em;' class='{$css_cal_day_number} $label'></div>";

        // Insert an event for this day
        if ($draw_event) {
          $calendar .=
            "<div class='{$css_cal_event}'>" .
            "<a href='{$events[$cur_date]['href']}'>" .
            $events[$cur_date]['text'] .
            "</a>" .
            "</div>";
        }

        // Close day cell
        $calendar .= "</td>";

        // New row
        if ($running_day == 7) {
          $calendar .= "</tr>";
          if (($day + 1) <= $days_in_month) {
            // $calendar .= "<tr class='{$css_cal_row}'>";
          }
          $running_day = 1;
          $y++;

        }

        // Increment the running day
        else {
          $running_day++;
        }

        
      } // for $day

      // return $y;

      // Finish the rest of the days in the week
      if ($running_day != 1) {
        for ($x = $running_day; $x <= 7; $x++) {
          $calendar .= "<td class='{$css_cal_day_blank}'> </td>";
        }
      }

      // Final row
      $calendar .= "</tr>";

      // End the table
      // All done, return result
      return $calendar;
    }

    public function changeCalendar(){
      return $this->repo->changeCalendar(Input::all());
    }

    public function build_html_calendar($year, $month, $result, $room_type_alert, $events = null) {

      // CSS classes
      $css_cal = 'calendar';
      $css_cal_row = 'calendar-row';
      $css_cal_day_head = 'calendar-day-head';
      $css_cal_day = 'calendar-day';
      $css_cal_day_number = 'day-number';
      $css_cal_day_blank = 'calendar-day-np';
      $css_cal_day_event = 'calendar-day-event';
      $css_cal_event = 'calendar-event';

      // Table headings
      $headings = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];

      // Start: draw table
      $calendar =
        "";

      // Days and weeks
      $running_day = date('N', mktime(0, 0, 0, $month, 1, $year));
      $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));

      // Row for week one
      // $calendar .= "<tr class='{$css_cal_row}'>";

      // Print "blank" days until the first of the current week
      for ($x = 1; $x < $running_day; $x++) {
        $calendar .= "<div onclick='modalForm(\"\")' class='col-md-1 myCalendar' style='background: #F9F9F9E6 0% 0% no-repeat padding-box;'><h5 style='font-weight: bold;'></h5></div>";
      }

      // Keep going with days...
      for ($day = 1; $day <= $days_in_month; $day++) {

        // Check if there is an event today
        $cur_date = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
        $draw_event = false;
        if (isset($events) && isset($events[$cur_date])) {
          $draw_event = true;
        }

        // Day cell
        // $calendar .= $draw_event ?
        //   "<td class='{$css_cal_day} {$css_cal_day_event}'>" :
        //   "<td class='{$css_cal_day}'>";

        // Add the day number

        $check = $result->where('date_applied', $cur_date);

        if(count($check) == 0):
          $test = 0;
          $test1 = 1;
        else:
          $test = 1;
          $test1 = 0;
        endif;

        if($test == 0):
          $label = "calendarRed";
          $label2 = "checked";
          $label1 = "<span class='myCalendarTag'>
              Closed
            </span>";
          $count = 0;
        else:
          $label2 = "";
          $label = "calendarGreen";
          $label1 = "";
          foreach($check as $result1):
            $count = $result1->no_room;
          endforeach;
        endif;

        if($room_type_alert > $count && $count != 0):
          $label = "calendarOrange";
        elseif($count == 0):
          $label = "calendarRed";
          $label2 = "checked";
          $label1 = "<span class='myCalendarTag'>
              Closed
            </span>";
          $test1 = 1;
        endif;

        $cur_date1 = "";
        $calendar .= "<div id='Day$cur_date' class='col-md-1 $label myCalendar' style='box-shadow: 0 0 0 1px #FFFFFF;'>
            $label1
            <p class='calendarFont'>
              $day
            </p>
            <center onclick='modalForm(\"$cur_date\")'>
              <span onclick='modalForm(\"$cur_date\")' style='width: 100%' class='myCalendarText'>
                $count
              </span>
            </center>
            <div id='DayForm$cur_date' class='calendarModal'>

            <div class='form-group' style='padding: 1em;'>
            <label for='focusedinput' style='color: black; font-weight: bold;' class='col-sm-12 control-label'>".date("M d, Y", strtotime($cur_date))."</label>
            <hr>
              <label for='focusedinput' class='col-sm-6 control-label'>Close Out Day</label>
              <div class='col-md-6'>
                <div id='ck-button'>
                  <label class='switch'><input type='hidden' id='flag_$cur_date' name='flag_$cur_date' value='$test1'>
                    <input type='checkbox' onclick='this.previousSibling.value=1-this.previousSibling.value; closeOut(\"$cur_date\");' $label2>
                    <span class='slider round'></span>
                  </label>
                </div>
              </div>
            </div>
            <hr>
            <div class='form-group'style='padding: 1em;'>
              <label for='focusedinput' class='col-sm-6 control-label'>Change Allotment</label>
              <div class='col-md-6'>
              <input type='number'class='form-control' min='0' value='$count' id='allotment_$cur_date' name='allotment_$cur_date' required='required'>
              </div>
              <div class='col-md-12' style='margin-top: 1em'>
              <center><button onclick='updateCalendar(\"$cur_date\")' class='btn btn-success'>Update</button></center>
              </div>
            </div>
            </div>
          </div>";

        // Insert an event for this day
        if ($draw_event) {
          // $calendar .=
          //   "<div class='{$css_cal_event}'>" .
          //   "<a href='{$events[$cur_date]['href']}'>" .
          //   $events[$cur_date]['text'] .
          //   "</a>" .
          //   "</div>";
        }

        // Close day cell
        // $calendar .= "</td>";

        // New row
        if ($running_day == 7) {
          // $calendar .= "</tr>";
          if (($day + 1) <= $days_in_month) {
            // $calendar .= "<tr class='{$css_cal_row}'>";
          }
          $running_day = 1;
        }

        // Increment the running day
        else {
          $running_day++;
        }

      } // for $day

      // Finish the rest of the days in the week
      if ($running_day != 1) {
        for ($x = $running_day; $x <= 7; $x++) {
          $calendar .= "<div onclick='modalForm(\"\")' class='col-md-1myCalendar' style='box-shadow: 0 0 0 1px #c0b8b8; background: #F9F9F9E6 0% 0% no-repeat padding-box;'><h5 style='font-weight: bold;'></h5></div>";
        }
      }

      // Final row
      // $calendar .= "</tr>";

      // End the table
      // $calendar .= '</table>';

      // All done, return result
      return $calendar;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inventoryBulkApply(Request $request, $hotel_id){

      $this->repo->inventoryBulkApply($request->all());

      return redirect('/inventory/'.$hotel_id);
    }
}
