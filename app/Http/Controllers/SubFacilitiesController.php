<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Repositories\InststayRepositories as InststayRepositories;

class SubFacilitiesController extends Controller
{
    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $hotel_id = "";
        $room_type = [];
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        return view('admin.sub_facilities.list', compact('menu', 'hotels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $hotel_id = "";
        $room_type = [];
        $query = [];
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        $sub_facilities_id = "";
        $main_facilities_id = "";
        $sub_facilities = "";
        $icon_file = "/kcfinder/vc/uploads/images/blank.png";
        $main_facilities = $this->repo->fetchMainFacilities();

        $label = "Add";
        $label1 = "Create";

        return view('admin.sub_facilities.create', compact('menu', 'hotels', 'label', 'label1', 'sub_facilities_id', 'main_facilities_id', 'sub_facilities', 'main_facilities', 'query', 'icon_file'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->repo->addSubFacilities($request->all());

        return redirect('/sub_facilities');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $hotel_id = "";
        $room_type = [];
        $query = $this->repo->fetchSubFacilitiesViaId($id);
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        $sub_facilities_id = $id;
        $main_facilities_id = $query->facilities_ref_id;
        $sub_facilities = $query->facilities;
        $main_facilities = $this->repo->fetchMainFacilities();
        $icon_file = $query->icon_file == "" ? "/kcfinder/vc/uploads/images/blank.png" : $query->icon_file;

        $label = "Update";
        $label1 = "Update";

        return view('admin.sub_facilities.create', compact('menu', 'hotels', 'label', 'label1', 'sub_facilities_id', 'main_facilities_id', 'sub_facilities', 'main_facilities', 'query', 'icon_file'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->repo->updateSubFacilities($request->all(), $id);

        return redirect('/sub_facilities/'.$id."/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $query = $this->repo->fetchSubFacilitiesViaId($id);
        $query->delete();
    }

    public function fetchSubFacilities(){
        $query = $this->repo->fetchSubFacilities();

        $data = [];

        foreach($query as $result):
            $url = URL('/');
            $action = "";
            // if(isset($editable[58])):
                $action ="<a style='border: 1px solid #b8c7ce; margin-left: .5em;' href='$url/sub_facilities/$result->id/edit' style='float: right;' class='btn btn-info btn-flat btn-pri'>
                                    <i class='fa fa-pencil-square-o'></i> Edit
                                </a>";

                $action .="<button data-id='$result->id' style='border: 1px solid #b8c7ce; margin-left: .5em;' style='float: right;' class='btn btn-danger btn-flat btn-pri icon-delete'>
                                <i class='fa fa-trash'></i> Delete
                            </a>";

            // endif;

            $data[] = array(
                $result->facilities_main->facilities_ref,
                $result->facilities,
                $result->creator->name."<br>(". $result->creator->email.")",
                $action
            );

        endforeach;

        $res = array('data'=>$data);
        return json_encode($res);
    }
}
