<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Repositories\InststayRepositories as InststayRepositories;

class HotelController extends Controller
{
    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        // return Auth::user();

        if(Auth::user()->user_group_id != 1 ):
            return redirect('/adminDashboard');
        endif;

        $hotel_id = "";
        $room_type = [];
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));
        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        $hotels = $this->repo->fetchHotelsViaUserId(Auth::user()->id);
        
        return view('admin.hotels.dashboard', compact('menu', 'hotels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $hotel_id = "";
        $hotels = array(
                "id"=> "",
                "hotel_name"=> "",
                "legal_entity_name"=> "",
                "email"=> "",
                "website"=> "",
                "facebook"=> null,
                "twitter"=> null,
                "instagram"=> null,
                "telnos"=> null,
                "biznos"=> null,
                "star"=> null,
                "category_id"=> null,
                "country_id"=> null,
                "region_id"=> null,
                "city_id"=> null,
                "city"=> null,
                "bank"=> null,
                "account_number"=> null,
                "tin_number"=> null,
                "latitude"=> 14.5712393,
                "longitude"=> 121.0660037,
                "about"=> "",
                "address"=> "",
                "postcode"=> "",
                "img_file"=> "",
                "approve_by"=> null,
                "flag"=> null,
                "approve_at"=> null,
                "timestamp"=> null,
                "created_at"=> null,
                "updated_at"=> null,
                "deleted_at"=> null,
                "has_diff_name"=> 1,
                "has_channel_manager"=> 1,
                "is_chain_hotel"=> 1,
            );

        $hotels = (object)$hotels;
        // if($hotels == ""):
        //     return redirect('/hotels');
        // endif;
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));
        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotels->id);
        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        $city = $this->repo->fetchCityViaCountryCode('ph');
        
        return view('admin.hotels.edit', compact('menu', 'hotels', 'city'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        if ($request->hasFile('myfile')) {
            $image = $request->file('myfile');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('uploads');
            $image->move($destinationPath, $name);
        } else {
            $name = NULL;
        }

        $add = $this->repo->addHotels($request, $name);

        return redirect('/hotels/'.$add->id.'/edit');
    }

    public function build_html_calendar($year, $month){
        // return array($year, $month);

        $css_cal = 'calendar';
        $css_cal_row = 'calendar-row';
        $css_cal_day_head = 'calendar-day-head';
        $css_cal_day = 'calendar-day';
        $css_cal_day_number = 'day-number';
        $css_cal_day_blank = 'calendar-day-np';
        $css_cal_day_event = 'calendar-day-event';
        $css_cal_event = 'calendar-event';

        $headings = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];

        $calendar ="";

        $running_day = date('N', mktime(0, 0, 0, $month, 1, $year));
        $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));

        for ($x = 1; $x < $running_day; $x++) {
        $calendar .= "<div onclick='modalForm(\"\")' class='col-md-1 myCalendar' style='background: #F9F9F9E6 0% 0% no-repeat padding-box;'><h5 style='font-weight: bold;'></h5></div>";
          }

          for ($day = 1; $day <= $days_in_month; $day++) {

            // Check if there is an event today
            $cur_date = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
            $draw_event = false;
            if (isset($events) && isset($events[$cur_date])) {
              $draw_event = true;
            }

            $label2 = "";
            $label1 = "";
            $label = "";
            $count = "";
            $test1 = "";

            if($day < 5):
                $bgKulay = "background: #F0F0F0";
            else:
                $bgKulay = "background: #FFFFFF";
            endif;

            $cur_date1 = "";

            $calendar .= "<div id='Day$cur_date' class='col-md-1 $label myCalendar' style='box-shadow: 0 0 0 2px #FFFFFF; border: #707070 2px solid; $bgKulay'>
                $label1
                <p class='calendarFont' style='padding-left: 1em;
                padding-top: .5em;
                color: #707070FA;
                font-weight: bold;
                font-size: 15px;'>
                  $day
                </p>
                <br>";

                if($day < 5):


                    $calendar .= "<p style='margin-top: 30%;
                            margin-left: 2%;
                        '>
                        CLOSED
                    </p>";

                else:

                    $calendar .= "<p style='text-align: right;margin-right: 2%;
                        margin-bottom: 0px;'>
                        <span style='color: #0DC3D8; font-weight: bold;'>5</span> bookings
                    </p>
                    <p style='text-align: right;margin-right: 2%;
                        margin-bottom: 0px;'>
                        <span style='color: #0DC3D8; font-weight: bold;'>2</span> Arrivals
                    </p>
                    <p style='text-align: right;margin-right: 2%;
                        margin-bottom: 0px;'>
                        PHP 0.00
                    </p>";

                endif;
                
              $calendar .= "</div>";

            // Insert an event for this day
            if ($draw_event) {
            }

            // Close day cell
            // $calendar .= "</td>";

            // New row
            if ($running_day == 7) {
              // $calendar .= "</tr>";
              if (($day + 1) <= $days_in_month) {
                // $calendar .= "<tr class='{$css_cal_row}'>";
              }
              $running_day = 1;
            }

            // Increment the running day
            else {
              $running_day++;
            }

          } // for $day

          // Finish the rest of the days in the week
          if ($running_day != 1) {
            for ($x = $running_day; $x <= 7; $x++) {
              $calendar .= "<div onclick='modalForm(\"\")' class='col-md-1myCalendar' style='box-shadow: 0 0 0 1px #c0b8b8; background: #F9F9F9E6 0% 0% no-repeat padding-box;'><h5 style='font-weight: bold;'></h5></div>";
            }
          }

        return $calendar;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));
        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotels->id);
        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        $calendar = $this->build_html_calendar(date('Y'), date('m'));
        
        return view('admin.hotels.view', compact('menu', 'hotels', 'calendar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));
        
        $menu = view('partial.menu', compact('hotel_id'));
        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotels->id);

        $city = $this->repo->fetchCityViaCountryCode('ph');

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));
        
        return view('admin.hotels.edit', compact('menu', 'hotels', 'city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;
        
        if ($request->hasFile('myfile')) {
            $image = $request->file('myfile');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('uploads');
            $image->move($destinationPath, $name);
        } else {
            $name = NULL;
        }

        $this->repo->updateHotels($request, $id, $name);

        return redirect('/hotels/'.$id.'/edit');
        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
