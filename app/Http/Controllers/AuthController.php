<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Repositories\InststayRepositories as InststayRepositories;

class AuthController extends Controller
{
    //
    public function __construct(InststayRepositories  $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }

    public function authenticate(Request $request)
    {
        // $this->migrateUserToCompany($request->all());

        // $field = filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)
        //     ? 'email'
        //     : 'username';

        // $credential = [
        //     $field => $request->get('email'),
        //     'password' => $request->password,
        // ];

        $credential = [
            'email' => $request->get('email'),
            'password' => $request->password,
        ];

    	if (Auth::attempt($credential, 1)) {

            if($request->get('access') == 2):

                if(Auth::user()->user_group_id != 1):
                    return redirect()->intended('/adminDashboard');
                else:
                    Auth::logout();
                    return redirect('/admin')->with('notification', 'Kindly check credential');
                endif;

            else:
                if(Auth::user()->user_group_id == 1):
                    return redirect()->intended('/hotels');
                else:
                    Auth::logout();
                    return redirect('/instanet')->with('notification', 'Kindly check credential');
                endif;
            endif;

    		// return "Hello World";
			    // The user is being remembered...
			} else {
                if($request->get('access') == 1):
                    return redirect('/instanet')->with('notification', 'Kindly check credential');

                else:
                    return redirect('/admin')->with('notification', 'Kindly check credential');
                endif;
				
			}
    }

    public function logout(){
        if(Auth::user()->user_group_id == 1):
            Auth::logout();
            return redirect()->intended('/instanet');

        else:
            Auth::logout();
            return redirect()->intended('/admin');
        endif;
    }
}
