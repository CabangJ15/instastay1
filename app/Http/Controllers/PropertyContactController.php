<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\Repositories\InststayRepositories as InststayRepositories;

class PropertyContactController extends Controller
{
    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;
        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;

        $countries = $this->repo->fetchCountries();
        $property_contracts = $this->repo->fetchPropertyContractsViaHotelId($hotel_id);
        $status = 1;
        $select_id = "";
        $fname = "";
        $tel_nos = "";
        $land_line = "";
        $email = "";
        $job_role = "";
        $country_id = "";
        $responsibility = "";
        $email = "";
        $imgFile = "uploads/profile/noprof.png";
        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotels->id);
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));
        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        $list = $this->createViewPropertyContactList($select_id, $property_contracts, $hotels, $status);

        return view('admin.hotels.property_contact', compact('menu', 'hotels', 'countries', 'property_contracts', 'fname', 'tel_nos', 'land_line', 'email', 'responsibility', 'job_role', 'imgFile', 'country_id', 'status', 'select_id', 'list'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $file = Input::file('myfile');

        if ($file) {
            $path = "uploads/profile/";
            $new_filename = $this->repo->uploadImage($file, $path);
        } else {
            $new_filename = NULL;
        }

        $this->repo->addPropertyContracts($request->all(), $hotel_id, $new_filename);


        return redirect("property_contact/$hotel_id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;
        
        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;

        $countries = $this->repo->fetchCountries();
        $property_contracts = $this->repo->fetchPropertyContractsViaHotelId($hotel_id);
        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotels->id);
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));
        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        $x = 0;
        if(Input::get('id') != ""):
            if ((int)Input::get('id') == 0):
                return redirect("property_contact/$hotel_id");
            endif;
        endif;

        if(count($property_contracts) == 0):
            if(Input::get('id') == ""):
                $status = 1;

                $select_id = Input::get('id');
                $fname = "";
                $tel_nos = "";
                $land_line = "";
                $email = "";
                $job_role = "";
                $country_id = "";
                $responsibility = "";
                $email = "";
                $imgFile = "uploads/profile/noprof.png";
            else:
                // $result = $this->repo->fetchPropertyContractsViaVialId(Input::get('id'));
            endif;
        else:
            $status = 0;
            if(Input::get('id') == ""):
            
            foreach($property_contracts as $result):
                if($x == 0):
                    $select_id = $result->id;
                    $x++;
                else:
                    break;
                endif;
            endforeach;
            else:
                $select_id = Input::get('id');
            endif;

            $result = $this->repo->fetchPropertyContractsViaId($select_id, $hotel_id);

            if($result != ""):

            $fname = $result->fname;
            $tel_nos = $result->tel_nos;
            $land_line = $result->land_line;
            $email = $result->email;
            $job_role = $result->job_role;
            $responsibility = $result->responsibility;
            $country_id = $result->country_id;
            $email = $result->email;
            $imgFile = $result->img_file == "" ? "uploads/profile/noprof.png" : "uploads/profile/".$result->img_file;
            else:
                return redirect("property_contact/$hotel_id");
            endif;
        endif;

        $list = $this->createViewPropertyContactList($select_id, $property_contracts, $hotels);
        
        return view('admin.hotels.property_contact', compact('menu', 'hotels', 'countries', 'property_contracts', 'fname', 'tel_nos', 'land_line', 'email', 'responsibility', 'job_role', 'imgFile', 'country_id', 'status', 'select_id', 'list'));
    }

    public function searchPropertyContact(){
        $search = Input::get('search');
        $select_id = Input::get('select_id');
        $hotel_id = Input::get('hotel_id');

        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($search != ""):
        $property_contracts = DB::SELECT("
            SELECT * FROM property_contracts
                WHERE (fname LIKE '%$search%' OR email LIKE '%$search%' OR job_role LIKE '%$search%') 
                AND deleted_at IS NULL
                ORDER BY fname
            ");
        else:
        $property_contracts = DB::SELECT("
            SELECT * FROM property_contracts
                WHERE deleted_at IS NULL
                ORDER BY fname
            ");
        endif;

        return $list = $this->createViewPropertyContactList($select_id, $property_contracts, $hotels);

    }

    public function createViewPropertyContactList($select_id, $property_contracts, $hotels){
        return view('admin.hotels.property_contact_list', compact('menu', 'hotels', 'countries', 'property_contracts', 'fname', 'tel_nos', 'land_line', 'email', 'responsibility', 'job_role', 'imgFile', 'country_id', 'status', 'select_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $hotel_id)
    {
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $file = Input::file('myfile');

        if ($file) {
            $path = "uploads/profile/";
            $new_filename = $this->repo->uploadImage($file, $path);
        } else {
            $new_filename = NULL;
        }

        $this->repo->updatePropertyContracts($request->all(), $hotel_id, $new_filename);

        $id = $request->get('id');
        
        return redirect("property_contact/$hotel_id?id=$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
