<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Repositories\InststayRepositories as InststayRepositories;

class GalleryFacilitiesController extends Controller
{
    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $hotel_id = "";
        $room_type = [];
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        return view('admin.gallery_facilities.list', compact('menu', 'hotels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $hotel_id = "";
        $room_type = [];
        $query = [];
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[13])):
            return redirect('/hotels');
        endif;

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        $gallery_facilities_id = "";
        $gallery_facilities = "";
        $label = "Add";
        $label1 = "Create";

        return view('admin.gallery_facilities.create', compact('menu', 'hotels', 'label', 'label1', 'gallery_facilities_id', 'gallery_facilities', 'query'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[13])):
            return redirect('/hotels');
        endif;

        $this->repo->addGalleryFacilities($request->all());

        return redirect('/gallery_facilities');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $query = $this->repo->fetchGalleryFacilitiesViaId($id);

        $hotel_id = "";
        $room_type = [];
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));
        
        if(!isset($editable[13])):
            return redirect('/hotels');
        endif;

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));

        $gallery_facilities_id = $id;
        $gallery_facilities = $query->image_ref;
        $label = "Update";
        $label1 = "Update";

        return view('admin.gallery_facilities.create', compact('menu', 'hotels', 'label', 'label1', 'gallery_facilities_id', 'gallery_facilities', 'query'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));
        
        if(!isset($editable[13])):
            return redirect('/hotels');
        endif;

        $this->repo->updateGalleryFacilities($request->all(), $id);

        return redirect('/gallery_facilities/'.$id."/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));
        
        if(!isset($editable[13])):
            return redirect('/hotels');
        endif;
        
        $query = $this->repo->fetchGalleryFacilitiesViaId($id);
        $query->delete();
    }

    public function fetchGalleryFacilities(){
        $query = $this->repo->fetchGalleryFacilities();
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $data = [];

        foreach($query as $result):
            $url = URL('/');
            $action = "";
            if(isset($editable[13])):
                $action ="<a style='border: 1px solid #b8c7ce; margin-left: .5em;' href='$url/gallery_facilities/$result->id/edit' style='float: right;' class='btn btn-info btn-flat btn-pri'>
                                    <i class='fa fa-pencil-square-o'></i> Edit
                                </a>";
                if(count($result->images) == 0):
                $action .="<button data-id='$result->id' style='border: 1px solid #b8c7ce; margin-left: .5em;' style='float: right;' class='btn btn-danger btn-flat btn-pri icon-delete'>
                                <i class='fa fa-trash'></i> Delete
                            </a>";
                endif;
            endif;

            $data[] = array(
                $result->image_ref,
                $result->creator->name."<br>(". $result->creator->email.")",
                $action
            );

        endforeach;

        $res = array('data'=>$data);
        return json_encode($res);
    }
}
