<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Repositories\InststayRepositories as InststayRepositories;

class RoomTypeController extends Controller
{
    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;

        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotels->id);

        $bed = $this->repo->fetchBed();
        $amenities = $this->repo->fetchAmenities();
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));
        
        return view('admin.hotels.room_type.room_type', compact('menu', 'hotels', 'bed', 'amenities'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $this->repo->addRoom($request->all(), $hotel_id);

        return redirect('/room_type/'. $hotel_id);
    }

    public function storeAll(Request $request, $hotel_id)
    {
        //
        $this->repo->addRoomAll($request->all(), $hotel_id);

        return redirect('/room_type/'. $hotel_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel_id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $hotels = $this->repo->fetchHotelsViaId($hotel_id, Auth::user()->id);

        if($hotels == ""):
            return redirect('/hotels');
        endif;

        $bed = $this->repo->fetchBed();
        $amenities = $this->repo->fetchAmenities();
        $rooms = $this->repo->fetchRoomTypeViaHotelId($hotel_id);

        $room_type = $this->repo->fetchRoomTypeViaHotelId($hotels->id);
        
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));
        
        return view('admin.hotels.room_type', compact('menu', 'hotels', 'rooms', 'amenities', 'bed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;

        $room = $this->repo->fetchRoomTypeViaId($id);
        $bed = $this->repo->fetchBed();
        $amenities = $this->repo->fetchAmenities();
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $menu = view('partial.menu', compact('hotel_id','room_type','access','editable'));
        return view('admin.hotels.room_type.room_type_form', compact('menu', 'hotels', 'room', 'bed', 'room', 'amenities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fetchRoomType($hotel_id){
        if(Auth::user()->user_group_id != 1):
            return redirect('/adminDashboard');
        endif;
        
        $query = $this->repo->fetchRoomTypeViaHotelId($hotel_id);

        $data = [];

        $bed = $this->repo->fetchBed();
        $amenities = $this->repo->fetchAmenities();

        foreach($query as $result):

            $room = $result;


            $order_view = view('admin.hotels.room_type.room_type_form', compact('menu', 'hotels', 'room', 'bed', 'room', 'amenities'));

            $label = $result->flag == 1 ? "checked" : "";
            $action = '<div id="ck-button">
                           <label class="switch"><input type="hidden" name="flag['.$result->id.']" value="'.$result->flag.'"><input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" '.$label.'>
                           <span class="slider round"></span>
                           </label>
                        </div>';

             $data[] = array(
                "oink" => "",
                "id"=> "<b>".$result->id."</b>",
                "room_name"=> "<b>".$result->room_name."</b>",
                "image"=> "<b>Photos:</b> ".count($result->images),
                "person"=> "<b>Person:</b> ".$result->guest,
                "order"=> "Order Goes Here",
                "enable"=> $action,
                "product"=> "$order_view"
            );

        endforeach;

        $res = array('data'=>$data);
        return json_encode($res);
    }
}
