<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Repositories\InststayRepositories as InststayRepositories;

class UserController extends Controller
{
    public function __construct(InststayRepositories $InststayRepositories)
    {
        $this->repo = $InststayRepositories;
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $this->middleware('auth'); //admin
        // $this->middleware('guest');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        date_default_timezone_set('Asia/Manila');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if($user_group->id == 1):
            return redirect('/hotels');
        endif;

        if(!isset($editable[16])):
            return redirect('/hotels');
        endif;

        $hotel_id = "";
        $room_type = [];

        $menu = view('partial.menuAdmin', compact('hotel_id','room_type','access','editable'));
        
        return view('admin.user.list', compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[16])):
            return redirect('/hotels');
        endif;

        if($user_group->id == 1):
            return redirect('/hotels');
        endif;

        $hotel_id = "";
        $room_type = [];

        $menu = view('partial.menuAdmin', compact('hotel_id','room_type','access','editable'));

        $group_list = $this->repo->getGroup();
        $query = []; //branch data
        $user_id = "";
        $fname = "";
        $email = "";
        $group_id = "";
        $img_file = URL('/')."/assets/images/noprof.png";

        $label = "Add";
        $label1 = "Create";

        return view('admin.user.create', compact('menu','query','label','label1','group','user_id','fname','email','group_list','group','img_file','username'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[16])):
            return redirect('/hotels');
        endif;

        if($user_group->id == 1):
            return redirect('/hotels');
        endif;

        $this->validate($request, [
            'name'=>'required',
            'email'=>'required|unique:users,email',
            'group_id'=>'required'
        ]);

        $user = $this->repo->addUser($request->all());

        return redirect('/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect('/user');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[16])):
            return redirect('/hotels');
        endif;

        if($user_group->id == 1):
            return redirect('/hotels');
        endif;

        $hotel_id = "";
        $room_type = [];

        $menu = view('partial.menuAdmin', compact('hotel_id','room_type','access','editable'));

        $group_list = $this->repo->getGroup();

        $query = $this->repo->fetchUserViaId($id);

        $user_id = $query->id;
        $fname = $query->name;
        $email = $query->email;
        $group_id = $query->user_group_id;
        $img_file = $query->image_file == NULL ? URL('/')."/assets/images/noprof.png" : $query->image_file;

        $label = "Update";
        $label1 = "Update";

        return view('admin.user.create', compact('menu','query','label','label1','group','user_id','fname','email','group_list','group','group_id','img_file','username'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[16])):
            return redirect('/hotels');
        endif;

        if($user_group->id == 1):
            return redirect('/hotels');
        endif;

        $this->validate($request, [
            'name'=>'required',
            'email'=>'required|unique:users,email,'.$id,
            // 'username'=>'required|unique:users,username,'.$id,
            'group_id'=>'required'
        ]);

        $user = $this->repo->updateUser($request->all(), $id);

        return redirect('/user/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        if(!isset($editable[3])):
            return redirect('/dashboard');
        endif;

        if($user_group->id == 1):
            return redirect('/hotels');
        endif;

        $user = $this->repo->fetchUserViaId($id);
        $user->delete();
    }

    public function fetchUser(){

        $query = $this->repo->fetchUserAdmin();
        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));
        $editable = array_flip(explode(",", $user_group->editable));

        $data = [];

        foreach($query as $result):
            $url = URL('/');
            $action = "";
                if(isset($editable[16])):
                $action ="<a style='border: 1px solid #b8c7ce; margin-left: .5em;' href='$url/user/$result->id/edit' style='float: right;' class='btn btn-info btn-flat btn-pri'>
                                    <i class='fa fa-pencil-square-o'></i> Edit
                                </a>";
                    if($result->email != Auth::user()->email):

                    $action .="<button data-id='$result->id' style='border: 1px solid #b8c7ce; margin-left: .5em;' style='float: right;' class='btn btn-danger btn-flat btn-pri icon-delete'>
                                        <i class='fa fa-trash'></i> Delete
                                    </a>";

                    $action .="<button data-id='$result->id' style='border: 1px solid #b8c7ce; margin-left: .5em;' style='float: right;' class='btn btn-success btn-flat btn-pri icon-reset'>
                                        <i class='fa fa-repeat'></i> Reset Password
                                    </a>";

                    endif;
                endif;

            $data[] = array(
                $result->name,
                $result->email,
                $result->user_group->user_group,
                $result->creator['name']."<br>(".$result->creator['email'].")",
                $action
            );

        endforeach;

        $res = array('data'=>$data);
        return json_encode($res);
    }

    public function resetPassword($id){
        $user = $this->repo->resetPassword($id);
    }
}
