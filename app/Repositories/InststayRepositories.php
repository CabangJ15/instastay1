<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Images;
use Carbon\CarbonPeriod;
use App\UserGroup as UserGroup;
use App\User as User;
use App\Hotel as Hotel;
use App\Image as Image;
use App\ImageRef as ImageRef;
use App\Bed as Bed;
use App\Amenities as Amenities;
use App\Room as Room;
use App\RoomImage as RoomImage;
use App\FacilitiesRef as FacilitiesRef;
use App\Facilities as Facilities;
use App\HotelFacilities as HotelFacilities;
use App\DateTaxSetting as DateTaxSetting;
use App\Tax as Tax;
use App\TaxSetting as TaxSetting;
use App\TaxSettingType as TaxSettingType;
use App\Countries as Countries;
use App\UserHotel as UserHotel;
use App\RoomAvailability as RoomAvailability;
use App\PropertyContracts as PropertyContracts;
// use App\Transaction as Transaction;
use App\RoomRate as RoomRate;
use App\RoomRatePrice as RoomRatePrice;
use App\RoomBed as RoomBed;
use App\Booking as Booking;
use App\Customer as Customer;
use App\CustomerInfo as CustomerInfo;
use App\City as City;
use App\Hotelier as Hotelier;
use App\SalesHotelier as SalesHotelier;
use App\RoomAmenities as RoomAmenities;
use App\ResetPassword as ResetPassword;
use App\TimeAvailability as TimeAvailability;


class InststayRepositories
{
    public function menu($id){

        $user_group = Auth::user()->user_group;
        $access = array_flip(explode(",", $user_group->access));

        return view('partial.menu', compact('access'));
    }

    public function fetchHotelsViaUserId($user_id){
    	return User::where('id', $user_id)
            ->with('hotels')
            ->with('hotels.hotel')
            ->first();
    }

    public function fetchHotelsViaId($hotel_id, $user_id){
        $hotel_ids = UserHotel::where('user_id', $user_id)->get(['hotel_id']);
    	return Hotel::with('meta','creator')
            ->with('facilities')
            ->with('facilities.facilities')
    		->whereIn('id', $hotel_ids)
            ->where('id', $hotel_id)
    		->orderBy('hotel_name')
    		->first();
    }

    public function fetchBed(){
        return Bed::orderBy('bed')->get();
    }

    public function fetchAmenities(){
        return Amenities::orderBy('amenities')->get();
    }

    public function updateRoomAmenities($amenities, $room_id){
        $array = array();

        foreach($amenities as $key => $result):
            array_push($array, $key);

        endforeach;

        $check = $this->checkMissingAmenities($array, $room_id);

        foreach($check as $result):
            $result->delete();
        endforeach;

        foreach($array as $result):
            $check = $this->fetchRoomAmenititesViaIds($result, $room_id);

            if($check == ""):
                $this->addRoomAmenities($result, $room_id);

            endif;

        endforeach;
    }

    public function addRoomAmenities($amenities_id, $room_id){
        return RoomAmenities::create([
            'room_id' => $room_id,
            'amenities_id' => $amenities_id,
            'created_by' => Auth::user()->id
            ]);
    }

    public function fetchRoomAmenititesViaIds($amenities_id, $room_id){
        return RoomAmenities::where('amenities_id', $amenities_id)
            ->where('room_id', $room_id)
            ->first();
    }

    public function checkMissingAmenities($amenities, $room_id){
        return RoomAmenities::whereNotIn('amenities_id', $amenities)
            ->where('room_id', $room_id)
            ->get();
    }

    public function addRoomAll($data, $hotel_id){

        foreach(explode(",",$data['room_type_list']) as $result1):

            $room = $this->fetchRoomTypeViaId($result1);

            if(isset($data['amenities'])):

                $this->updateRoomAmenities($data['amenities'][$result1], $result1);

            endif;

            $room->room_name = $data['room_name'][$result1];
            $room->description = $data['description'][$result1];
            $room->guest = $data['person'][$result1];
            $room->max_guest = $data['max_person'][$result1];
            $room->extra_bed = $data['extra_bed'][$result1];
            // $room->sTime = $data['sTime'][$result1] == "" ? NULL : $data['sTime'][$result1].":00";
            // $room->eTime = $data['eTime'][$result1] == "" ? NULL : $data['eTime'][$result1].":00";
            $room->room_size = $data['room_size'][$result1];
            $room->bathroom = $data['bath_room'][$result1];
            // $room->amenities_list = $amenities == "" ? NULL : $amenities;
            // $room->no_rooms = $data['no_room'][$result1];
            $room->alert = $data['alert'][$result1];
            $room->free_child = $data['child_allow'][$result1];
            $room->is_child_allowed = $data['is_allow_children'][$result1];
            $room->is_baby_cots = $data['baby_cots'][$result1];
            $room->flag = $data['flag'][$result1];
            $room->sort = $data['sort'][$result1];
            $room->save();

            if(isset($data['bed'][$result1])):
                $test = array();

                foreach($data['bed'][$result1] as $key => $bed):
                    array_push($test, $key);
                endforeach;

                $check = $this->checkMissingRoomBed($result1, $test);

                foreach($check as $delete):
                    $delete->delete();
                endforeach;

                foreach($data['bed'][$result1] as $key => $bed):
                    $qty = $data['bedQty'][$result1][$key];
                    $bed_id = $key;
                    $room_id = $result1;

                    $check = $this->checkRoomBedViaRoomBedId($room_id, $bed_id);

                    if($check != ""):
                        $check->qty = $qty;
                        $check->save();
                    else:
                        $this->addRoomBed($room_id, $bed_id, $qty);
                    endif;
                endforeach;
            else:
                $check = $this->checkRoomBedViaId($result1);

                foreach($check as $delete):
                    $delete->delete();
                endforeach;
            endif;

            if(isset($data['image_id'][$result1])):
                $check = $this->checkMissingRoomImages($result1, $data['image_id'][$result1]);
            else:
                $check = $this->checkRoomImages($result1);
            endif;

            foreach($check as $delete):
                $delete->delete();
            endforeach;

            if(isset($data['image'][$result1])):
                if(count($data['image'][$result1]) != 0):
                    for($x=0; $x<count($data['image'][$result1]); $x++):
                        $result = $data['image'][$result1][$x];
                        $image_id = $data['image_id'][$result1][$x];
                        if($image_id == 0):
                            $imageName = $this->base64_to_jpeg_room($result, '', $result1, $room->hotel_id);
                        endif;
                    endfor;
                endif;

            endif;
        endforeach;
    }

    public function addRoomBed($room_id, $bed_id, $qty){
        return RoomBed::create([
            "room_id" => $room_id,
            "bed_id" => $bed_id,
            "qty" => $qty,
            "created_by" => Auth::user()->id
            ]);
    }

    public function checkRoomBedViaRoomBedId($room_id, $bed_id){
        return RoomBed::where('bed_id', $bed_id)
            ->where('room_id', $room_id)
            ->first();
    }

    public function checkMissingRoomBed($id, $bed_id){
        return RoomBed::whereNotIn('bed_id', $bed_id)
            ->where('room_id', $id)
            ->get();
    }

    public function checkRoomBedViaId($id){
        return RoomBed::where('room_id', $id)
            ->get();
    }

    public function checkMissingRoomImages($id, $image_id){

        return RoomImage::whereNotIn('id', $image_id)
            ->where('link_id', $id)
            ->where('target', 'room')
            ->get();
    }

    public function checkRoomImages($id){

        return RoomImage::where('link_id', $id)
            ->where('target', 'room')
            ->get();
    }

    public function addRoom($data, $hotel_id){
        // return $data['bed'];
        foreach(explode(",",$data['room_type_list']) as $result1):

            $room = Room::create([
                'hotel_id' => $hotel_id,
                'room_name' => $data['room_name'][$result1], 
                'description' => $data['description'][$result1], 
                'guest' => $data['person'][$result1],
                // 'sTime' => $data['sTime'][$result1] == "" ? NULL : $data['sTime'][$result1].":00",
                // 'eTime' => $data['eTime'][$result1] == "" ? NULL : $data['eTime'][$result1].":00",
                'max_guest' => $data['max_person'][$result1],
                'extra_bed' => $data['extra_bed'][$result1],
                'max_extra_bed' => $data['max_extra_bed'][$result1],
                'room_size' => $data['room_size'][$result1],
                'bathroom' => $data['bath_room'][$result1],
                // 'no_rooms' => $data['no_room'][$result1],
                'alert' => $data['alert'][$result1],
                'is_child_allowed' => $data['is_allow_children'][$result1],
                'free_child' => $data['child_allow'][$result1],
                'max_child' => $data['max_child'][$result1],
                'is_baby_cots' => $data['baby_cots'][$result1]
            ]);

            if(isset($data['amenities'])):

                $this->updateRoomAmenities($data['amenities'][$result1], $room->id);

            endif;

            DB::select("
                INSERT INTO `tbl_prices` (`id`, `link_id`, `sequence`, `target`, `description`, `price`, `created_by`, `timestamp`, `created_at`, `updated_at`, `deleted_at`) 
                    VALUES 
                    (NULL, '$room->id', '1', 'room', 'ADDITIONAL HOUR', '300.0000', '1', NULL, NULL, NULL, NULL), 
                    (NULL, '$room->id', '2', 'room', 'SERVICE FEE', '300.0000', '1', NULL, NULL, NULL, NULL), 
                    (NULL, '$room->id', '0', 'room', '12 HOURS MINIMUM STAY', '1500.0000', '1', NULL, NULL, NULL, NULL);

                ");

            if(isset($data['image'])):
                if(count($data['image'][$result1]) != 0):
                    foreach($data['image'][$result1] as $result):
                        $imageName = $this->base64_to_jpeg_room($result, '', $room->id, $hotel_id);
                    endforeach;

                endif;
            endif;

            if(isset($data['bed'][$result1])):
                $test = array();

                foreach($data['bed'][$result1] as $key => $bed):
                    array_push($test, $key);
                endforeach;

                // return $test;

                $check = $this->checkMissingRoomBed($result1, $test);

                foreach($check as $delete):
                    $delete->delete();
                endforeach;

                foreach($data['bed'][$result1] as $key => $bed):
                    $qty = $data['bedQty'][$result1][$key];
                    $bed_id = $key;
                    $room_id = $room->id;

                    $check = $this->checkRoomBedViaRoomBedId($room_id, $bed_id);

                    if($check != ""):
                        $check->qty = $qty;
                        $check->save();
                    else:
                        $this->addRoomBed($room_id, $bed_id, $qty);
                    endif;
                endforeach;
            else:
                $check = $this->checkRoomBedViaId($result1);

                foreach($check as $delete):
                    $delete->delete();
                endforeach;
            endif;

        endforeach;
    }

    public function fetchRoomTypeViaHotelId($hotel_id){
        return Room::with('images')
            ->with('bed')
            ->with('bed.bed')
            ->with('amenities.amenities')
            ->orderBy('sort')->where('hotel_id', $hotel_id)->get();
    }

    public function fetchRoomTypeViaId($id){
        return Room::with('images')
            ->with('availability')
            ->with('amenities')
            ->with('amenities.amenities')
            ->where('id', $id)->first();
    }

    public function base64_to_jpeg_room($base64_string, $output_file, $room_id, $hotel_id) {
            $base64_string;
            $image = explode(',', $base64_string);
            $image = $image[1];
            $imageName = str_random(10).'.'.'png';
            $path = public_path(). '/uploads/room/' . $imageName;
            \File::put($path, base64_decode($image));

            RoomImage::create([
                "link_id" =>$room_id,
                "target" => 'room',
                "image_file" => $imageName,
                "created_by" => Auth::user()->id
                ]);

            return $imageName;
        }

    public function fetchUserViaEmail($data){
        // $email="nerdvana.uxprogrammer2@victoriacourt.biz";
        $check = User::where('email', $data['email'])->first();

        if($check == ""):

            // return User::get();

            $query = $this->fetchHotelierViaId($data['hotelier_id']);

            $check = User::create([
                'name' => $query->fname,
                'email' => $query->email,
                'user_group_id' => 1,
                'created_by' => Auth::user()->id,
                'password' =>bcrypt('instastay'),
            ]);

        endif;

        return $check;
    }

    public function addHotel($data, $img_file){

        $check = $this->fetchUserViaEmail($data);

        $check1 = $this->fetchtyViaCity($data['city']);

        if($check1 != ""):
            $city_id = $check1->id;
        else:
            $city_id = NULL;
        endif;

        $hotel =  Hotel::create([
            "legal_entity_name" => $data['legal_entity_name'],
            "hotel_name" => $data['hotel_name'],
            "address" => $data['address'],
            "city" => $data['city'],
            "city_id" => $city_id,
            "about" => $data['about'],
            "postcode" => $data['postal_code'],
            "latitude" => $data['latitude'],
            "longitude" => $data['longitude'],
            "facebook" => $data['facebook'],
            "twitter" => $data['twitter'],
            "instagram" => $data['instagram'],
            "email" => $data['email'],
            "telnos" => $data['telnos'],
            "website" => $data['website'],
            "has_channel_manager" => $data['has_channel_manager'],
            "has_diff_name" => $data['has_diff_name'],
            "is_chain_hotel" => $data['is_chain_hotel'],
            "img_file" => $img_file,
            "created_by" => Auth::user()->id,
            "hotelier_id" => $data['hotelier_id']
        ]);

        UserHotel::create([
            "user_id" => $check->id,
            "hotel_id" => $hotel->id
            ]);

        // return $hotel;

        $hotelier = $this->fetchHotelierViaId($data['hotelier_id']);
        $hotelier->is_action = 3;
        $hotelier->action_at = date("Y-m-d H:i:s");
        $hotelier->save();

    }

    public function addHotels($data, $img_file){

        $check = $this->fetchtyViaCity($data['city']);

        if($check != ""):
            $city_id = $check->id;
        else:
            $city_id = NULL;
        endif;

        $hotel =  Hotel::create([
                "legal_entity_name" => $data['legal_entity_name'],
                "hotel_name" => $data['hotel_name'],
                "address" => $data['address'],
                "city" => $data['city'],
                "city_id" => $city_id,
                "about" => $data['about'],
                "postcode" => $data['postal_code'],
                "latitude" => $data['latitude'],
                "longitude" => $data['longitude'],
                "facebook" => $data['facebook'],
                "twitter" => $data['twitter'],
                "instagram" => $data['instagram'],
                "email" => $data['email'],
                "telnos" => $data['telnos'],
                "website" => $data['website'],
                "has_channel_manager" => $data['has_channel_manager'],
                "has_diff_name" => $data['has_diff_name'],
                "is_chain_hotel" => $data['is_chain_hotel'],
                "img_file" => $img_file,
                "created_by" => Auth::user()->id
            ]);

        UserHotel::create([
            "user_id" => Auth::user()->id,
            "hotel_id" => $hotel->id
            ]);

        return $hotel;
    }

    public function fetchtyViaCity($city){
        return City::where('city', $city)->first();
    }

    public function updateHotels($data, $id, $img_file){

    	$hotel =  $this->fetchHotelsViaId($id, Auth::user()->id, $img_file);

        $check = $this->fetchtyViaCity($data['city']);

        if($check != ""):
            $city_id = $check->id;
        else:
            $city_id = NULL;
        endif;

    	$hotel->legal_entity_name = $data['legal_entity_name'];
        $hotel->hotel_name = $data['hotel_name'];
    	$hotel->address = $data['address'];
        $hotel->about = $data['about'];
        $hotel->city = $data['city'];
        $hotel->city_id = $city_id;
        $hotel->postcode = $data['postal_code'];
    	$hotel->latitude = $data['latitude'];
    	$hotel->longitude = $data['longitude'];
    	$hotel->facebook = $data['facebook'];
    	$hotel->twitter = $data['twitter'];
    	$hotel->instagram = $data['instagram'];
    	$hotel->email = $data['email'];
    	$hotel->telnos = $data['telnos'];
    	$hotel->website = $data['website'];
        $hotel->has_channel_manager = $data['has_channel_manager'];
        $hotel->has_diff_name = $data['has_diff_name'];
        $hotel->is_chain_hotel = $data['is_chain_hotel'];
    	if($img_file != ""):
	    	$hotel->img_file = $img_file;
	    endif;
    	$hotel->save();
    }

    public function fetchImageRef($hotel_id, $user_id){
        return ImageRef::with('images')->get();
    }

    public function base64_to_jpeg($base64_string, $output_file, $image_type, $hotel_id) {
        $base64_string;
        $image = explode(',', $base64_string);
        $image = $image[1];
        $imageName = str_random(10).'.'.'png';
        $path = public_path(). '/uploads/room/' . $imageName;
        \File::put($path, base64_decode($image));

        Image::create([
            "image_ref_id" => $image_type,
            "hotel_id" => $hotel_id,
            "image" => $imageName,
            "created_by" => Auth::user()->id
            ]);

        return $imageName;
    }

    public function fetchFacilitiesRef(){
        return FacilitiesRef::with('facilities','creator')
            ->with('facilities.hotel')
            ->get();
    }

    public function updateHotelFacilities($data, $hotel_id){

        $hotel_facilities = $this->fetchHotelFacilitesViaFacilitiesIdHotelId($data['id'], $hotel_id);

        if($hotel_facilities == ""):
            $this->createHotelFacilities($data['id'], $hotel_id);
        else:
            if($data['data'] == 0):
                $hotel_facilities->delete();
            else:
                $this->createHotelFacilities($data['id'], $hotel_id);
            endif;
        endif;
    }

    public function fetchHotelFacilitesViaFacilitiesIdHotelId($facilities_id, $hotel_id){
        return HotelFacilities::where('facilities_id', $facilities_id)
            ->where('hotel_id', $hotel_id)
            ->first();
    }

    public function createHotelFacilities($facilities_id, $hotel_id){
        HotelFacilities::create([
            'facilities_id' => $facilities_id,
            'hotel_id' => $hotel_id,
            'created_by' => Auth::user()->id
            ]);
    }

    public function checkFacilitiesRefCountViaHotelId($facilities_id, $hotel_id){
        $facilities = FacilitiesRef::with('facilities')->where('id', $facilities_id)->first();

        $array = [];

        foreach($facilities->facilities as $result):
            array_push($array, $result->id);
        endforeach;

        return HotelFacilities::whereIn('facilities_id', $array)->where('hotel_id', $hotel_id)->get();
    }

    public function fetchTaxSettingsViaDate($sDate, $eDate, $hotel_id){
        return DateTaxSetting::where('sDate', $sDate)
            ->where('eDate', $eDate)
            ->where('hotel_id', $hotel_id)
            ->get();
    }

    public function fetchTaxSettings(){
        $tax_type_id = Tax::get(['id']);
        return TaxSetting::with('tax_type')
            ->with('tax_setting_type')
            ->with('creator')
            ->with('charge_type')
            ->whereIn('tax_type_id', $tax_type_id)
            ->get();
    }

    public function fetchTax(){
        return Tax::get();
    }

    public function updateTaxSettings($data, $hotel_id){

        $check = $this->checkMissingTaxSettings($data['sDate'], $data['eDate'], $hotel_id, array_values($data['tax_setting_id']));

        foreach($check as $result):
            $result->delete();
        endforeach;

        foreach($data['tax_setting_id'] as $x => $result):
            $tax_setting_id = $data['tax_setting_id'][$x];
            $trading = $data['trading'][$x];
            $value = $data['value'][$x];

            if($tax_setting_id == 0):

            DateTaxSetting::create([
                'hotel_id' => $hotel_id,
                'sDate' => $data['sDate'],
                'eDate' => $data['eDate'],
                'tax_setting_id' => $x,
                'is_free' => $trading,
                'value' => $value,
                'created_by' => Auth::user()->id,
                ]);
            else:
                $query = $this->fetchTaxSettingViaId($tax_setting_id);
                $query->is_free = $trading;
                $query->value = $value;
                $query->save();
            endif;
        endforeach;
    }

    public function fetchTaxSettingViaId($tax_setting_id){
        return DateTaxSetting::where('id', $tax_setting_id)
            ->first();
    }

    public function checkMissingTaxSettings($sDate, $eDate, $hotel_id, $tax_setting_id){
        return DateTaxSetting::where('sDate', $sDate)
            ->where('eDate', $eDate)
            ->where('hotel_id', $hotel_id)
            ->whereNotIn('id', $tax_setting_id)
            ->get();
    }

    public function fetchCountries(){
        return Countries::get();
    }

    public function fetchPropertyContractsViaHotelId($hotel_id){
        return PropertyContracts::where('hotel_id', $hotel_id)
            ->orderBy('fname')
            ->get();
    }

    public function fetchPropertyContractsViaId($id, $hotel_id){
        return PropertyContracts::where('id', $id)
            ->where('hotel_id', $hotel_id)
            ->orderBy('fname')
            ->first();
    }

    public function addPropertyContracts($data, $hotel_id, $imgFile){

        return PropertyContracts::create([
            "fname" => $data['fname'],
            "tel_nos" => $data['tel_nos'],
            "email" => $data['email'],
            "land_line" => $data['land_line'],
            "country_id" => $data['country_id'],
            "responsibility" => $data['responsibility'],
            "job_role" => $data['job_role'],
            "img_file" => $imgFile,
            "hotel_id" => $hotel_id,
            ]);
    }

    public function changeCalendar($data){
        $result = $this->fetchRoomAvailabilityViaRoomIdDate($data['room_type_id'],$data['date']);

        if($result != ""):
            $result->no_room = $data['allotment'];
            $result->save();
        else:
            return $this->addRoomAvailability($data['room_type_id'],$data['date'],$data['allotment']);
        endif;
    }

    public function addRoomAvailability($room_type_id, $date, $allotment){
        return RoomAvailability::create([
            "date_applied" => $date,
            "no_room" => $allotment,
            "room_id" => $room_type_id,
            "created_by" => Auth::user()->id,
            ]);
    }

    public function fetchRoomAvailabilityViaRoomIdDate($room_type_id, $date){
        return RoomAvailability::where('room_id', $room_type_id)
            ->where('date_applied', $date)
            ->first();
    }

    public function updatePropertyContracts($data, $hotel_id, $imgFile){

            $result = $this->fetchPropertyContractsViaId($data['id'], $hotel_id);

            $result->fname = $data['fname'];
            $result->tel_nos = $data['tel_nos'];
            $result->email = $data['email'];
            $result->land_line = $data['land_line'];
            $result->country_id = $data['country_id'];
            $result->responsibility = $data['responsibility'];
            $result->job_role = $data['job_role'];
            if($imgFile != ""):
                $result->img_file = $imgFile;
            endif;
            $result->save();
    }

    public function uploadImage($file, $path){
            $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension();
            $itemNo = time() . substr(str_replace(" ", "_", $filename), 5) . "." . $extension;
            $new_filename = $itemNo;
            $image_resize = Images::make($file->getRealPath());              
            $image_resize->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $image_resize->save($path .$itemNo);

            return $new_filename;
    }

    public function fetchRoomAvailabilityViaDate($year, $month, $hotel_id, $room_id){
        return RoomAvailability::whereRaw("MONTH(date_applied) = '$month' AND YEAR(date_applied) = '$year'")
            ->where('room_id', $room_id)
            ->with('creator')
            ->get();
    }

    public function fetchRoomAvailabilityViaYear($year, $hotel_id, $room_id){
        return RoomAvailability::whereRaw("YEAR(date_applied) = '$year'")
            ->where('room_id', $room_id)
            ->with('creator')
            ->get();
    }

    public function inventoryBulkApply($data){

        $room_available = $this->fetchRoomAvailabilityViaSDateEDate($data['sDate'], $data['eDate'], $data['room_type_id']);

        $booking = $this->fetchBookingViaSDateEDate($data['sDate'], $data['eDate'], $data['room_type_id']);

        $booking = [];

        $period = CarbonPeriod::create($data['sDate'], $data['eDate']);
        $day = array(
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"
            );
        // Iterate over the period
        foreach ($period as $date):
            $check = date("w", strtotime($date->format('Y-m-d')));

            if($check == 0):
                $check1 = 7;
            else:
                $check1 = $check;
            endif;

            // $day[$check]." = ".$date->format('Y-m-d')."<br>";

            if(isset($data['day'])):
                $test = array_search($check1, $data['day']);
                if($test !== false):
                    // echo array_search($check1, $data['day'])." Not Exist<br>";
                    $this->updateDateAvailable($data, $date->format('Y-m-d'), $room_available, $booking);
                endif;
            else:

                $this->updateDateAvailable($data, $date->format('Y-m-d'), $room_available, $booking);
            endif;
        endforeach;

        // Convert the period to an array of dates
        $dates = $period->toArray();

    }

    public function updateDateAvailable($data, $date, $room_available, $booking){

        $booking = collect($booking);

        $test1 = $room_available->where('date_applied', $date);
        $test2 = $booking->where('datestamp', $date);

        $change = array(
            "room_type_id" => $data['room_type_id'],
            "allotment" => $data['allotment'],
            "date" => $date,
        );

        if(isset($data['close_out']) && isset($data['no_reservation'])):
            
            if(count($test2) == 0):
                foreach($test1 as $test3):
                    if($test3->no_room == 0):
                        //execute update inventory
                    endif;
                endforeach;
            endif;
        elseif(isset($data['close_out'])):
            if(count($test1) != 0):
                foreach($test1 as $test3):
                    if($test3->no_room == 0):
                        $this->changeCalendar($change);
                    endif;
                endforeach;
            else:
                $this->changeCalendar($change);
            endif;
        elseif(isset($data['no_reservation'])):
            if(count($test2) == 0):
                //execute update inventory
                $this->changeCalendar($change);
            endif;
        else:
            $this->changeCalendar($change);
        endif;
    }
        

    public function fetchRoomAvailabilityViaSDateEDate($sDate, $eDate, $room_type_id){
        return RoomAvailability::whereBetween('date_applied', [$sDate, $eDate])
            ->where('room_id', $room_type_id)
            ->get();
    }

    public function fetchBookingViaSDateEDate($sDate, $eDate, $room_type_id){
        return Booking::whereBetween('created_at', [$sDate, $eDate])
            ->where('room_id', $room_type_id)
            ->get();
    }

    public function fetchMainFacilities(){
        return FacilitiesRef::with('facilities')->get();
    }

    public function addMainFacilities($data){
        return FacilitiesRef::create([
                "facilities_ref" => $data['main_facilities'],
                "created_by" => Auth::user()->id
            ]
            );
    }

    public function fetchMainFacilitiesViaId($id){
        return FacilitiesRef::with('facilities')->where('id', $id)->first();
    }

    public function updateMainFacilities($data, $id){
        $query = $this->fetchMainFacilitiesViaId($id);
        $query->facilities_ref = $data['main_facilities'];
        $query->save();
    }

    public function fetchSubFacilities(){
        return Facilities::with('facilities_main')->get();
    }

    public function addSubFacilities($data){
        return Facilities::create([
                "facilities_ref_id" => $data['main_facilities_id'],
                "facilities" => $data['sub_facilities'],
                "icon_file" => $data['icon_file'],
                "created_by" => Auth::user()->id
            ]
            );
    }

    public function fetchSubFacilitiesViaId($id){
        return Facilities::with('facilities_main')->where('id', $id)->first();
    }

    public function updateSubFacilities($data, $id){
        $query = $this->fetchSubFacilitiesViaId($id);
        $query->facilities_ref_id = $data['main_facilities_id'];
        $query->facilities = $data['sub_facilities'];
        $query->icon_file = $data['icon_file'];
        $query->save();
    }

    public function fetchGalleryFacilities(){
        return ImageRef::with('images')->get();
    }

    public function addGalleryFacilities($data){
        return ImageRef::create([
                "image_ref" => $data['gallery_facilities'],
                "created_by" => Auth::user()->id
            ]
            );
    }

    public function fetchGalleryFacilitiesViaId($id){
        return ImageRef::with('images')->where('id', $id)->first();
    }

    public function updateGalleryFacilities($data, $id){
        $query = $this->fetchGalleryFacilitiesViaId($id);
        $query->image_ref = $data['gallery_facilities'];
        $query->save();
    }

    public function fetchRoomRate(){
        return RoomRate::orderBy('hour','asc')->get();
    }

    public function addRoomRate($data){
        return RoomRate::create([
                "room_rate" => $data['room_rate'],
                "hour" => $data['hour'],
                "created_by" => Auth::user()->id
            ]
            );
    }

    public function fetchRoomRateViaId($id){
        return RoomRate::where('id', $id)->first();
    }

    public function updateRoomRate($data, $id){
        $query = $this->fetchRoomRateViaId($id);
        $query->room_rate = $data['room_rate'];
        $query->hour = $data['hour'];
        $query->save();
    }

    public function fetchRoomRatePriceViaDate($room_type_id, $dates){
        return RoomRatePrice::where('room_id', $room_type_id)
            ->whereIn('date_applied', $dates)
            ->get();
    }

    public function fetchRoomRatePriceNullDate($id){
        return RoomRatePrice::where('room_id', $id)
            ->whereNull('date_applied')
            ->get();
    }

    public function fetchRoomRatePriceNullViaRoomRateId($room_rate_id, $room_type_id){
        return RoomRatePrice::where('room_id', $room_type_id)
                ->where('room_rate_id', $room_rate_id)
                ->whereNull('date_applied')
                ->first();
    }

    public function addRoomRateAll($room_rate, $room_type_id){

        foreach($room_rate as $key => $rate):
            $check = $this->fetchRoomRatePriceNullViaRoomRateId($key, $room_type_id);

            if($rate != 0):

                if($check != ""):
                    if($check->amount != $rate):
                        $check->delete();
                        $this->addRoomRateList($key, $room_type_id, $rate, NULL, 0);
                    endif;
                else:
                    $this->addRoomRateList($key, $room_type_id, $rate, NULL, 0);
                endif;

            else:

                if($check != ""):
                    $check->delete();
                endif;

            endif;
        endforeach;
    }

    public function checkMissingTimeAvailability($time_availability_id, $room_type_id){
        return TimeAvailability::whereNotIn('id', $time_availability_id)
            ->where('room_id', $room_type_id)
            ->get();
    }

    public function fetchTimeAvailabilityViaRoomId($room_type_id){
        return TimeAvailability::where('room_id', $room_type_id)
            ->orderBy('c_in')
            ->get();
    }

    public function addTimeAvailabilityData($c_in, $c_out, $amount, $alloted, $room_type_id){

        return TimeAvailability::create([
            'room_id' => $room_type_id,
            'c_in' => $c_in,
            'c_out' => $c_out,
            'amount' => $amount,
            'alloted' => $alloted,
            'created_by' => Auth::user()->id
        ]);
    }

    public function addTimeAvailability($data, $room_type_id){

        if(!isset($data['time_availability_id'])):
            $check = $this->fetchTimeAvailabilityViaRoomId($room_type_id);

            foreach($check as $result):
                $result->delete();
            endforeach;
        else:

            $check = $this->checkMissingTimeAvailability($data['time_availability_id'], $room_type_id);

            foreach($check as $result):
                $result->delete();
            endforeach;

            for($x=0; $x < count($data['time_availability_id']); $x++):
                $time_availability_id = $data['time_availability_id'][$x];
                $c_in = $data['c_in'][$x].':00';
                $c_out = $data['c_out'][$x].':00';
                $amount = $data['amount'][$x];
                $alloted = $data['alloted'][$x];

                if($time_availability_id == 0):
                    $this->addTimeAvailabilityData($c_in, $c_out, $amount, $alloted, $room_type_id);
                else:
                    $check = $this->fetchTimeAvailabilityViaId($time_availability_id);

                    if($check != ""):
                        if($check->c_in != $c_in || $check->c_out != $c_out || $check->amount != $amount || $check->alloted != $alloted):
                            $check->delete();

                            $this->addTimeAvailabilityData($c_in, $c_out, $amount, $alloted, $room_type_id);
                        endif;
                    endif;
                endif;
            endfor;

        endif;
    }

    public function fetchTimeAvailabilityViaId($id){
        return TimeAvailability::where('id', $id)->first();
    }

    public function addRoomRateList($room_rate_id, $room_type_id, $rate, $date, $promotion){
        return RoomRatePrice::create([
            "room_id" => $room_type_id,
            "room_rate_id" => $room_rate_id,
            "amount" => $rate,
            "date_applied" => $date,
            "promotion" => $promotion,
            "created_by" => Auth::user()->id
            ]);
    }

    public function fetchRoomRatePriceViaId($room_rate_price_id){
        return RoomRatePrice::where('id', $room_rate_price_id)->first();
    }

    public function addRoomRateDailyAll($data){

        $room_rate_price = $this->fetchRoomRatePriceViaDate($data['room_type_id'], $data['date']);

        foreach($data['date'] as $date):

            foreach($data['rates'][$date] as $key => $rate):

                if(count($room_rate_price->where('date_applied', $date)
                    ->where('room_rate_id', $key)) != 0):
                    foreach($room_rate_price->where('date_applied', $date)
                    ->where('room_rate_id', $key) as $check):
                        if($rate > 0):
                            if($check->amount != $rate):

                                $check1 = $this->fetchRoomRatePriceViaId($check->id);
                                $check1->delete();

                                $this->addRoomRateList($key, $data['room_type_id'], $rate, $date, 0);
                            endif;
                        else:
                            $check1 = $this->fetchRoomRatePriceViaId($check->id);
                            $check1->delete();
                        endif;
                    endforeach;
                else:
                    if($rate > 0):
                        $this->addRoomRateList($key, $data['room_type_id'], $rate, $date, 0);
                    endif;
                endif;
            endforeach;
        endforeach;

        foreach($data['room_rate_orig'] as $key => $rate):
            $check = $this->fetchRoomRatePriceNullViaRoomRateId($key, $data['room_type_id']);

            if($check != ""):
                if($check->amount != $rate):
                    $check->delete();
                    $this->addRoomRateList($key, $data['room_type_id'], $rate, NULL, 0);
                endif;
            else:
                $this->addRoomRateList($key, $data['room_type_id'], $rate, NULL, 0);
            endif;
        endforeach;
    }

    public function fetchHotelAll(){
        return Hotel::with('creator')
            ->with('meta')
            ->with('facilities')
            ->orderBy('flag')
            ->get();
    }

    public function fetchHotelViaId($id){
        return Hotel::with('creator')
            ->with('meta')
            ->with('facilities')
            ->orderBy('flag')
            ->where('id', $id)
            ->first();
    }

    public function changeHotelFlag($data, $id){

        $check = $this->fetchHotelViaId($id);

        if($data['status'] == 0):
            $check->flag = NULL;
        else:
            $check->flag = 1;
            $check->approve_by = Auth::user()->id;
            $check->approve_at = date("Y-m-d H:i:s");
        endif;

        $check->save();
        
    }

    public function fetchImageViaId($id){

        return Image::where('id', $id)->first();
    }

    public function addAmenities($data){
        $amenities = Amenities::create([
                "amenities" => $data['amenities'],
                "icon_file" => $data['icon_file'],
                "created_by" => Auth::user()->id
            ]);

        $url = 'https://instastay.app/api/index.php?api_key=1nst4st4y&api_secret=EJYXjy1zMFC0JMekDQSf&args=instastay_action%2Fv1%2Faction%3Aauto_sync_amenities%2F';
        // $url = 'http://192.168.10.12/api/vc/ma/booked';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        $err = curl_error($ch);  //if you need
        curl_close ($ch);

        return $amenities;
    }

    public function fetchAmenitiesViaId($id){
        return Amenities::where('id', $id)->first();
    }

    public function updateAmenities($data, $id){
        $query = $this->fetchAmenitiesViaId($id);

        $query->icon_file = $data['icon_file'];
        $query->amenities = $data['amenities'];

        $query->save();

        $url = 'https://instastay.app/api/index.php?api_key=1nst4st4y&api_secret=EJYXjy1zMFC0JMekDQSf&args=instastay_action%2Fv1%2Faction%3Aauto_sync_amenities%2F';
        // $url = 'http://192.168.10.12/api/vc/ma/booked';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        $err = curl_error($ch);  //if you need
        curl_close ($ch);
    }

    public function addBed($data){
        return Bed::create([
                "bed" => $data['bed'],
                "created_by" => Auth::user()->id
            ]
            );
    }

    public function fetchBedViaId($id){
        return Bed::where('id', $id)->first();
    }

    public function updateBed($data, $id){
        $query = $this->fetchBedViaId($id);
        $query->bed = $data['bed'];
        $query->save();
    }

    public function fetchUser(){
        return User::with('creator')->get();
    }

    public function fetchUserAdmin(){
        return User::where('user_group_id', '!=', 1)->with('creator')->get();
    }

    public function getGroup(){
        return UserGroup::orderBy('user_group', 'asc')->get();
    }

    public function addUser($data){
        
        User::create([
            'name' =>$data['name'],
            'email' => $data['email'],
            // 'username' => $data['username'],
            'password' => bcrypt('123qwe'),
            'user_group_id' => $data['group_id'],
            'image_file' =>  $data['imageFile'],
            'created_by' => Auth::user()->id

        ]);
    }

    public function fetchUserViaId($user_id){
        return User::where('id', $user_id)->first();
    }

    public function updateUser($data, $id){
        // return $data;
        $user = $this->fetchUserViaId($id);

        $user->name = $data['name'];
        $user->email = $data['email'] == "" ? "" : $data['email'];
        $user->image_file = $data['imageFile'];
        $user->user_group_id = $data['group_id'];
        // $user->username = $data['username'];
        $user->save();
    }

    public function resetPassword($id){
        $user = $this->fetchUserViaId($id);
        $user->password = bcrypt('123qwe');
        $user->save();
    }

    public function fetchUserGroup(){
        return UserGroup::get();
    }

    public function fetchUserGroupViaId($id){
        return UserGroup::where('id', $id)->first();
    }

    public function updateUserGroup($data, $id){
        if(!isset($data['list'])):
            $data['list'] = [];
        endif;

        if(!isset($data['list1'])):
            $data['list1'] = [];
        endif;

        $user_group = $this->fetchUserGroupViaId($id);
        $user_group->access = implode(",",$data['list']);
        $user_group->editable = $list2 = implode(",",$data['list1']);

        $user_group->user_group = $data['name'];
        $user_group->flag = $data['flag'];
        $user_group->save();
    }

    public function addUserGroup($data){

        if(!isset($data['list'])):
            $data['list'] = [];
        endif;

        if(!isset($data['list1'])):
            $data['list1'] = [];
        endif;

        UserGroup::create([
            "access" => implode(",",$data['list']),
            "editable" => implode(",",$data['list1']),
            "user_group" => $data['name'],
            "flag" => $data['flag'],
            "created_by" => Auth::user()->id
            ]);
    }

    public function fetchTaxType(){
        return Tax::get();
    }

    public function addTaxType($data){
        return Tax::create([
                "tax_type" => $data['tax_type'],
                "created_by" => Auth::user()->id
            ]
            );
    }

    public function fetchTaxTypeViaId($id){
        return Tax::where('id', $id)->first();
    }

    public function updateTaxType($data, $id){
        $query = $this->fetchTaxTypeViaId($id);
        $query->tax_type = $data['tax_type'];
        $query->save();
    }

    public function fetchBooking(){
        return Booking::with('hotel')
            ->with('transaction')
            ->with('customer')
            ->with('customer.info')
            ->get();
    }

    public function fetchTransaction(){
        return Transaction::with('booking')
            ->with('hotel')
            ->with('room_type')
            ->with('customer')
            ->with('customer.info')
            ->get();
    }

    public function fetchTransactionViaDate($data){
        $sDate = $this->dateFormater($data['sDate']);
        $eDate = $this->dateFormater($data['eDate']);

        return Booking::whereBetween('created_at', [$sDate, $eDate])
            ->with('customer')
            ->with('customer.info')
            ->with('hotel')
            ->with('room_type')
            ->get();
    }

    public function dateFormater($date){
        $date = explode("/", $date);
        return $date = $date[2]."-".$date[1]."-".$date[0];
    }

    public function fetchCityViaCountryCode($country_code){
        return City::where('country_code', $country_code)
            ->orderBy('city')
            ->get();
    }

    public function addHotelier($data){
        return Hotelier::create([
            'fname' => $data['fname'],
            'jobtitle' => $data['jobtitle'],
            'mobile' => $data['mobile'],
            'email' => $data['email'],
            'hname' => $data['hname'],
            'address' => $data['address'],
            'city' => $data['city'],
            'postal_code' => $data['postal_code'],
            'contact' => $data['contact'],
            'hotel_email' => $data['hotel_email'],
            'company' => $data['company'],
            'c_address' => $data['c_address'],
            'c_city' => $data['c_city'],
            'c_postal_code' => $data['c_postal_code'],
            'no_hotel' => $data['no_hotel'],
            'description' => $data['description']
        ]);
    }

    public function fetchHotelier(){
        return Hotelier::get();
    }

    public function fetchHotelierViaSalesId($id){
        return Hotelier::where('sales_id', $id)->get();
    }

    public function fetchHotelierViaId($id){
        return Hotelier::with('hotel')
            ->where('id', $id)
            ->first();
    }

    public function fetchSaleRep(){
        return User::where('user_group_id', 3)->get();
    }

    public function assignSalesRep($hotelier_id, $data){

        $hotelier = $this->fetchHotelierViaId($hotelier_id);

        if($data['sale_id'] != "" && $hotelier->sales_id != $data['sale_id']):
            $this->addSaleHotelier($hotelier_id, $data['sale_id']);
        endif;

        if($hotelier != ""):
            $hotelier->sales_id = $data['sale_id'] == "" ? NULL : $data['sale_id'];
            $hotelier->save();
        endif;
    }

    public function addSaleHotelier($hotelier_id, $sale_id){
        return SalesHotelier::create([
            'sale_id' => $sale_id,
            'hotelier_id' => $hotelier_id,
            'created_by' => Auth::user()->id
            ]);
    }

    public function checkEmail($email){
        return User::where('email', $email)->first();
    }

    public function resetPasswordLink($data){

        $check = $this->checkResetPassword($data['id']);

        if($check == ""):
            $check = $this->addResetPassword($data['id']);
        endif;

        return $check;
    }

    public function addResetPassword($id){
        $code = $this->getCode();

        return ResetPassword::create([
            'user_id' => $id,
            'code' => $code
            ]);
    }

    public function getCode(){
        return hash_hmac('sha256', str_random(5), config('app.key'));
    }

    public function checkResetPassword($id){
        return ResetPassword::where('user_id', $id)->first();
    }

    public function fetchResetPasswordViaCode($code){
        return ResetPassword::where('code', $code)->first();
    }

    public function passwordReset($code, $data){
        $reset = $this->fetchResetPasswordViaCode($code);
        if($reset != ""):
            $user = $this->fetchUserViaId($reset->user_id);

            if($user != ""):
                $user->password = bcrypt($data['password']);
                $user->save();
            endif;

            $reset->delete();
        endif;

        return $user;
    }

    public function addContactMessage($data){
        return $data;
    }

    public function contacted($id){
        $hotelier = $this->fetchHotelierViaId($id);
        $hotelier->is_action = 1;
        $hotelier->action_at = date("Y-m-d H:i:s");
        $hotelier->save();
    }

    public function proposal($id){
        $hotelier = $this->fetchHotelierViaId($id);
        $hotelier->is_action = 2;
        $hotelier->action_at = date("Y-m-d H:i:s");
        $hotelier->save();
    }

    public function activate($id){
        $hotelier = $this->fetchHotelierViaId($id);

        if($hotelier->hotel != ""):

            $hotel = $hotelier->hotel;
            $hotel->flag = 1;
            $hotel->approve_by = Auth::user()->id;
            $hotel->approve_at = date("Y-m-d H:i:s");
            $hotel->save();

        endif;

        $hotelier->is_action = 4;
        $hotelier->action_at = date("Y-m-d H:i:s");
        $hotelier->save();
    }

    public function deactivate($id){
        $hotelier = $this->fetchHotelierViaId($id);

        if($hotelier->hotel != ""):

            $hotel = $hotelier->hotel;
            $hotel->flag = NULL;
            $hotel->save();

        endif;

        $hotelier->is_action = 5;
        $hotelier->action_at = date("Y-m-d H:i:s");
        $hotelier->save();
    }
}
