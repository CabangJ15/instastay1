<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomImage extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tbl_images';

    protected $fillable = [
        'link_id', 
        'image_file', 
        'target',
        'created_by'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
