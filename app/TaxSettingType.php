<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaxSettingType extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tax_setting_type';

    protected $fillable = [
        'tax_setting_type',
        'created_by'
    ];
}
