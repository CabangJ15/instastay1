<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserHotel extends Model
{
    //
    use SoftDeletes;
    protected $table = 'user_to_hotel';

    protected $fillable = [
        'user_id', 
        'hotel_id',
    ];

    public function hotel(){
        return $this->belongsTo('App\Hotel', 'hotel_id', 'id');
    }
}
