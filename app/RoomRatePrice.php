<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomRatePrice extends Model
{
    //
    use SoftDeletes;
    protected $table = 'room_rate_price';

    protected $fillable = [
        'room_rate_id', 
        'room_id', 
        'date_applied', 
        'amount', 
        'promotion', 
        'created_by'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
