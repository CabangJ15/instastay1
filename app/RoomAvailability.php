<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomAvailability extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tbl_room_availability';

    protected $fillable = [
        'room_id', 
        'no_room', 
        'created_by', 
        'date_applied'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
