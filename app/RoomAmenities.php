<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomAmenities extends Model
{
    //
    use SoftDeletes;
    protected $table = 'room_amenities';

    protected $fillable = [
        'room_id',
        'amenities_id',
        'created_by',
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function amenities(){
        return $this->belongsTo('App\Amenities', 'amenities_id', 'id');
    }
}
