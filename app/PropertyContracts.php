<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyContracts extends Model
{
    //
    use SoftDeletes;
    protected $table = 'property_contracts';

    protected $fillable = [
        'hotel_id',
        'fname',
        'email',
        'tel_nos',
        'job_role',
        'land_line',
        'img_file',
        'country_id',
        'responsibility',
        'created_by'
    ];

    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
