<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  	<title>@yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="{{URL('/')}}/assets/images/nerdvana.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{URL('/')}}/assets/images/nerdvana.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{URL('/')}}/assets/images/nerdvana.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{URL('/')}}/assets/images/nerdvana.png">
	<link rel="apple-touch-icon-precomposed" href="{{URL('/')}}/assets/images/nerdvana.png">
	<!-- <link href="https://fonts.googleapis.com/css2?family=Varela+Round&display=swap" rel="stylesheet"> -->
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{URL('/')}}/asset_website/css/style-starter.css">
  <link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" />
  <!-- web fonts -->
  <link href="//fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
  <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="{{URL('/')}}/assets/js/lib/dist/sweetalert.css" type="text/css" />

  <style>
  	h1 { font-family: "Arial Rounded MT Bold", "Helvetica Rounded", Arial, sans-serif; font-style: normal; font-variant: normal; } 
  	h5 { font-family: "Arial Rounded MT Bold", "Helvetica Rounded", Arial, sans-serif; font-style: normal; font-variant: normal; } 
  </style>
  <style>


    .myFooter{
          margin-left: 0%;
        }

    #cityImg{
      margin-top: 30%;
    }

    @media (min-width: 1200px){
        .myLogo {
            margin-left: 19%;
        }


    }

    @media (max-width: 500px){

        .myFooter{
          text-align: center !important;
        }

        .progressbar li {
            /*margin-top: 2em;*/
            /*margin-bottom: 2em;*/
            font-size: 0.7em;
        }

        #cityImg{
          margin-top: 0%;
          margin-bottom: 8%;
        }

        .cityImg1 {
          margin-top: 0% !important;
        }
    }
  </style>
  <!-- /web fonts -->
</head>

<body>
  <!-- header -->
  <header class="w3l-header">
    <div class="hero-header-11">
      <div class="hero-header-11-content">
        <div class="container">
          <nav class="navbar navbar-expand-xl navbar-light py-sm-2 py-1 px-0">


            <a class="myLogo" href="{{URL('/')}}"><img src="{{ asset('assets/images/logo.png') }}" style="width: 175px;" /></a>
            <!-- if logo is image enable this   
				<a class="navbar-brand" href="#index.html">
						<img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
				</a> -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon fa fa-bars"></span>
            </button>

            <div style="margin-right: 19%;" class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto">
                <!-- <li class="nav-item active">
                  <a class="nav-link" href="{{URL('/')}}" style="padding: .5em;">Home</a>
                </li> -->
                <li class="nav-item">
                  <a class="nav-link" href="{{URL('/')}}" style="padding: .5em;">About</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{URL('/')}}" style="padding: .5em;">Help</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{URL('/contact')}}" style="padding: .5em;">Contact Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{URL('/hotelier')}}"  style="text-transform: capitalize; border-radius: 4px; color: white; background: #316BD1; padding: .5em; text-transform: uppercase;">Be a Hotel Partner</a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>

@yield('content')
<footer class="w3l-footer">
	<div id="footers14-block" class="py-3" style="background: white;">
	  <div class="container">
		 <div class="row">
            <div class="col-md-3" style=" padding: 1em;">
            	<center>
            		<img style="max-width: 100px;" src="{{URL('/')}}/uploads/footer_logo.png"/>
	            	<p>support@instastay.app</p>
	            	<!-- <p>+63 917 XXX XXXX</p> -->
            	</center>
            </div>
            <div class="col-md-3 myFooter" style="text-align: left; padding: 1em;
                            float: right;">
            	<p style="font-size: 1em"><b>INSTASTAY</b></p>
            	<p style="font-size: 1em"><a style="color: black;" href="{{URL('/')}}">About us</a></p>
              <p style="font-size: 1em"><a style="color: black;" href="{{URL('/')}}">Help</a></p>
            	<p style="font-size: 1em"><a style="color: black;" href="{{URL('/contact')}}">Contact Us</a></p>
            </div>
            <div class="col-md-3 myFooter" style="text-align: left; padding: 1em;
                            float: right;">
            	<p style="font-size: 1em"><b>HOTEL PARTNERS</b></p>
            	<p style="font-size: 1em"><a style="color: black;"  href="{{URL('/hotelier')}}">Be a Hotel Partner</a></p>
            	<p style="font-size: 1em"><a style="color: black;"  href="{{URL('/instanet')}}">Login to Instanet</a></p>
              <p style="font-size: 1em"><a style="color: black;"  href="{{URL('/')}}">List of Hotel Partners</a></p>
            </div>
            <div class="col-md-3 myFooter" style="text-align: left; padding: 1em;
                            float: right;">
            	<p style="font-size: 1em"><b>INFORMATION</b></p>
            	<!-- <p style="font-size: 1em">Privacy Policy</p> -->
            	<p style="font-size: 1em"><a style="color: black;" href="{{URL('/terms-of-use')}}">Terms of Use</a></p>
            	<p style="font-size: 1em">Download Android</p>
            	<p style="font-size: 1em">Download iOS</p>
            </div>
        </div>

		 <div class="row">
		 	<div class="col-md-6">
		        <div class="social myFooter">

		            <a style="color: #316BD1" href="#instagram" class="instagram"><span style="color: #316BD1" class="fa fa-instagram" aria-hidden="true"></span></a>

		            <a style="color: #316BD1" href="#facebook" class="facebook"><span style="color: #316BD1" class="fa fa-facebook" aria-hidden="true"></span></a>
		            <a style="color: #316BD1" href="#twitter" class="twitter"><span style="color: #316BD1" class="fa fa-twitter" aria-hidden="true"></span></a>
		          </div>
	        </div>
	        <div class="col-md-6">
		        <div class="copyright mt-1">
		            <p class=" myFooter" style="text-align: right; color: #316BD1">©2019 Nerdvana Inc. All rights reserved. Confidential and Proprietary</a></p>
		          </div>
		     </div>
        </div>
	</div>
	<!-- <button onclick="topFunction()" id="movetop" title="Go to top">
      <span class="fa fa-angle-up" aria-hidden="true"></span>
    </button> -->
    <script>
      // When the user scrolls down 20px from the top of the document, show the button
      window.onscroll = function () {
        // scrollFunction()
      };

      // function scrollFunction() {
      //   if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      //     document.getElementById("movetop").style.display = "block";
      //   } else {
      //     document.getElementById("movetop").style.display = "none";
      //   }
      // }

      // When the user clicks on the button, scroll to the top of the document
      function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
      }
    </script>
    <!-- /move top -->

  </footer>
    <!-- Footer -->

  <!-- jQuery and Bootstrap JS -->
  <script src="{{URL('/')}}/asset_website/js/jquery-3.4.1.slim.min.js"></script>
  <script src="{{URL('/')}}/asset_website/js/bootstrap.min.js"></script>

  <!-- Template JavaScript -->
  <script src="{{URL('/')}}/asset_website/js/jquery.magnific-popup.min.js"></script>
  <script>
    $(document).ready(function () {
      $('.popup-with-zoom-anim').magnificPopup({
        type: 'inline',

        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
      });

      $('.popup-with-move-anim').magnificPopup({
        type: 'inline',

        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom'
      });
    });
  </script>

  <!-- disable body scroll which navbar is in active -->
  <script>
    $(function () {
      $('.navbar-toggler').click(function () {
        $('body').toggleClass('noscroll');
      })
    });
  </script>
  <!-- disable body scroll which navbar is in active -->
  <script src="{{URL('/')}}/assets/js/lib/dist/sweetalert.min1.js"></script>
  <script type="text/javascript" src="{{ asset('assets/js/select2.min.js') }}"></script>
  <script src="{{URL('/')}}/asset_website/js/smartphoto.js"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function () {
      const sm = new SmartPhoto(".js-img-viwer", {
        showAnimation: false
      });
      // sm.destroy();
    });
  </script>

  @yield('page-script')


</body>

</html>

    