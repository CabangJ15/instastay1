<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">
		<title>@yield('title')</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="JSOFT Admin - Responsive HTML5 Template">
		<meta name="author" content="JSOFT.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link rel="shortcut icon" href="{{URL('/')}}/assets/images/nerdvana.png"> 
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{URL('/')}}/assets/images/nerdvana.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{URL('/')}}/assets/images/nerdvana.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{URL('/')}}/assets/images/nerdvana.png">
        <link rel="apple-touch-icon-precomposed" href="{{URL('/')}}/assets/images/nerdvana.png">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{URL('/')}}/assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="{{URL('/')}}/assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="{{URL('/')}}/assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="{{URL('/')}}/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="{{URL('/')}}/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="{{URL('/')}}/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="{{URL('/')}}/assets/vendor/morris/morris.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{URL('/')}}/assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{URL('/')}}/assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{URL('/')}}/assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="{{URL('/')}}/assets/vendor/modernizr/modernizr.js"></script>

		<link rel="stylesheet" type="text/css" href="{{URL('/')}}/assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap.css"/>
		<link rel="stylesheet" type="text/css" href="{{URL('/')}}/assets/datatables/Buttons-1.5.2/css/buttons.bootstrap.css"/>
		<link href="{{URL('/')}}/assets/css/table.css" rel="stylesheet" type="text/css" media="all">

		<link rel="stylesheet" href="{{URL('/')}}/assets//js/lib/dist/sweetalert.css" type="text/css" />
		<script src="{{URL('/')}}/assets//js/lib/dist/sweetalert.min1.js"></script>

		<link rel="stylesheet" href="{{URL('/')}}/assets/css/token-input.css" type="text/css" />
		<link type="text/css" rel="stylesheet" href="{{URL('/')}}/assets/css/responsive-tabs.css" />
		<link href="{{URL('/')}}/assets/tags/bootstrap-tagsinput.css" rel='stylesheet' type='text/css' />
		<!-- <link href="https://127.0.0.1/posnerdvana/public/admin/assets/css/select2.min.css" rel="stylesheet" /> -->
		<link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
		<link rel="stylesheet" type="text/css" href="{{URL('/')}}/assets/css/jquery.datetimepicker.css"/>
		<link rel="stylesheet" href="{{URL('/')}}/assets/css/jquery-ui.css">
		<link media="all" type="text/css" rel="stylesheet" href="{{URL('/')}}/assets/css/monthly.css">
		<link media="all" type="text/css" rel="stylesheet" href="{{URL('/')}}/assets/css/MonthPicker.min.css">

		@yield('style')
	</head>
	<body>
		<section class="body">
			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="{{URL('hotels')}}" class="logo" style="margin: 0px 0 0 15px;">
						<img src="{{URL('/')}}/assets/images/logo.png" style="width: 145px;" alt="JSOFT Admin" /> <font style="color: #707070;">Partner Instanet</font>
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<div class="header-right">
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="" />
							</figure>
							<div class="profile-info" data-lock-name="" data-lock-email="">
								<span class="name">Welcome {{Auth::user()->name}}</span>
								<!-- <span class="role">{{Auth::user()->email}}</span> -->
							</div>
			
							<i style="color: black; font-size: 1.5em;" class="fa fa-caret-down"></i>
							<!-- <i class="fa custom-caret"></i> -->
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="#"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="{{URL('logout')}}"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			<div class="inner-wrapper" style="top:100px;">

				@yield('content')

			</div>

	
		</section>

		<script src="{{URL('/')}}/assets/vendor/jquery/jquery.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="{{URL('/')}}/assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="{{URL('/')}}/assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="{{URL('/')}}/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="{{URL('/')}}/assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		<script type="text/javascript" src="{{ asset('assets/js/select2.min.js') }}"></script>
		
		<!-- Specific Page Vendor -->
		<script src="{{URL('/')}}/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jquery-appear/jquery.appear.js"></script>
		<script src="{{URL('/')}}/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
		<script src="{{URL('/')}}/assets/vendor/flot/jquery.flot.js"></script>
		<script src="{{URL('/')}}/assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
		<script src="{{URL('/')}}/assets/vendor/flot/jquery.flot.pie.js"></script>
		<script src="{{URL('/')}}/assets/vendor/flot/jquery.flot.categories.js"></script>
		<script src="{{URL('/')}}/assets/vendor/flot/jquery.flot.resize.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>
		<script src="{{URL('/')}}/assets/vendor/raphael/raphael.js"></script>
		<script src="{{URL('/')}}/assets/vendor/morris/morris.js"></script>
		<script src="{{URL('/')}}/assets/vendor/gauge/gauge.js"></script>
		<script src="{{URL('/')}}/assets/vendor/snap-svg/snap.svg.js"></script>
		<script src="{{URL('/')}}/assets/vendor/liquid-meter/liquid.meter.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jqvmap/jquery.vmap.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>

		<script type="text/javascript" src="{{URL('/')}}/assets/datatables/JSZip-2.5.0/jszip.js"></script>
		<script type="text/javascript" src="{{URL('/')}}/assets/datatables/pdfmake-0.1.36/pdfmake.js"></script>
		<script type="text/javascript" src="{{URL('/')}}/assets/datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
		<script type="text/javascript" src="{{URL('/')}}/assets/datatables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="{{URL('/')}}/assets/datatables/DataTables-1.10.18/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="{{URL('/')}}/assets/datatables/Buttons-1.5.2/js/dataTables.buttons.js"></script>
		<script type="text/javascript" src="{{URL('/')}}/assets/datatables/Buttons-1.5.2/js/buttons.bootstrap.js"></script>
		<script type="text/javascript" src="{{URL('/')}}/assets/datatables/Buttons-1.5.2/js/buttons.colVis.js"></script>
		<script type="text/javascript" src="{{URL('/')}}/assets/datatables/Buttons-1.5.2/js/buttons.flash.js"></script>
		<script type="text/javascript" src="{{URL('/')}}/assets/datatables/Buttons-1.5.2/js/buttons.html5.js"></script>
		<script type="text/javascript" src="{{URL('/')}}/assets/datatables/Buttons-1.5.2/js/buttons.print.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
		<script type="text/javascript" src="{{ asset('assets/js/multifield.js') }}"></script>

		
		<!-- Theme Base, Components and Settings -->
		<script src="{{URL('/')}}/assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="{{URL('/')}}/assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="{{URL('/')}}/assets/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="{{URL('/')}}/assets/javascripts/dashboard/examples.dashboard.js"></script>
		<script type="text/javascript" src="{{URL('/')}}/assets/js/jquery.tokeninput.js"></script>
		<script src="{{URL('/')}}/assets/js/responsive-tabs.js"> </script>
		<script type="text/javascript" src="{{URL('/')}}/assets/tags/bootstrap-tagsinput.min.js"></script>
		<script type="text/javascript" src="{{URL('/')}}/assets/ckeditor_old/ckeditor.js"></script>
		<script src="{{URL('/')}}/assets/node_modules/php-date-formatter/js/php-date-formatter.min.js"></script>
		<script src="{{URL('/')}}/assets/node_modules/jquery-mousewheel/jquery.mousewheel.js"></script>
		<script src="{{URL('/')}}/assets/js/jquery.datetimepicker.js"></script>
		<script src="{{URL('/')}}/assets/js/jquery-ui.js"></script>
		<!-- <script src="{{URL('/')}}/assets/js/select2.min.js"></script> -->
		<script src="{{URL('/')}}/assets/js/monthly.js"></script>
		<script src="{{URL('/')}}/assets/js/jquery-ui.js"></script>
		<script src="{{URL('/')}}/assets/js/MonthPicker.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
		<script src="https://www.amcharts.com/lib/4/core.js"></script>
		<script src="https://www.amcharts.com/lib/4/charts.js"></script>
		<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
		@yield('page-script')
	</body>
</html>