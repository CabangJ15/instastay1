<!doctype html>
<html class="fixed" style="background: #2183ca;">
	<head>

		<!-- Basic -->
		<title>@yield('title')</title>
		<meta charset="UTF-8">
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="">
		<meta name="author" content="edgargarcia">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" href="{{URL('/')}}/assets/images/nerdvana.png"> 
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{URL('/')}}/assets/images/nerdvana.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{URL('/')}}/assets/images/nerdvana.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{URL('/')}}/assets/images/nerdvana.png">
        <link rel="apple-touch-icon-precomposed" href="{{URL('/')}}/assets/images/nerdvana.png">
        <link rel="apple-touch-icon-precomposed" href="{{URL('/')}}/assets/images/icon.png">
        <meta name="viewport" content="width=device-width">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{URL('/')}}/assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="{{URL('/')}}/assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="{{URL('/')}}/assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="{{URL('/')}}/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{URL('/')}}/assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{URL('/')}}/assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{URL('/')}}/assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="{{URL('/')}}/assets/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>

		@yield('content')

		<!-- start: page -->
		
		<!-- end: page -->

		<!-- Vendor -->
		<script src="{{URL('/')}}/assets/vendor/jquery/jquery.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="{{URL('/')}}/assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="{{URL('/')}}/assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="{{URL('/')}}/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="{{URL('/')}}/assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="{{URL('/')}}/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="{{URL('/')}}/assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="{{URL('/')}}/assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="{{URL('/')}}/assets/javascripts/theme.init.js"></script>
		@yield('page-script')

	</body>
</html>