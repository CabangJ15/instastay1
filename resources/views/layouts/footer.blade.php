<!-- copyright -->
    <div style="background: #3e26b6" class="cpy-right text-center py-3">
        <p style="color: white;">© 2019 INSTASTAY. All rights reserved | Design by
            <a style="color: white;" href="http://nerdvana.com.ph.com"> NERDVANA.</a>
        </p>

    </div>
    <!-- //copyright -->


    <script type="text/javascript" src="{{ asset('assets/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/toastr.min.js') }}"></script>

    @yield('scripts')
</body>

</html>