@extends('layouts.main')
@section('title', 'Instastay')
@section('content')
<style>
    #headerTitle{
        font-size: 36px;
        line-height: 1.1em;
    }

    blockquote.large:before {
        float:left;
    }

    .container{
        max-width: 100%;
    }

    .row {
        /*padding: 2em;*/
    }

   /* .first_cont{
        margin-top: 4em;
    }
*/
    @media (max-width: 768px){
        .first_cont{
            margin-top: 0em;
        } 

        .navbar-inverse .navbar-inner {
            padding: 0 0 0px;
        }
    }

    /*@import url(https://fonts.googleapis.com/css?family=Roboto:700);*/

    .progressbar {
      counter-reset: step;
    }

    .progressbar li {
      position: relative;
      list-style: none;
      float: left;
      width: 25%;
      text-align: center;
    }

    /* Circles */
    .progressbar li:before {
        content: counter(step);
        counter-increment: step;
        width: 40px;
        height: 40px;
        border: 2px solid #316BD1;
        display: block;
        text-align: center;
        margin: 0 auto 10px auto;
        border-radius: 50%;
        background-color: #ffffff;
        line-height: 39px;
    }

    .progressbar li.finish:before {
        background: #316BD1;
        content: "✔";
        color: white;
    }

    .progressbar li:after {
      content: "";
      position: absolute;
      width: 100%;
      height: 1px;
      background: #316BD1;
      top: 20px; /*half of height Parent (li) */
      left: -50%;
      z-index: -1;
    }

    .progressbar li:first-child:after {
      content: none;
    }

    .progressbar li.active:before {
      background: #316BD1;
      /*content: "✔";  */
      color: white;
    }

    .progressbar li.active + li:after {
      background: #316BD1;
    }

    .tab{
        /*border-bottom: solid 1px black;    */
        display: none;
    }

    input.invalid {
      background-color: #ffdddd;
    }

    .select2-selection__rendered {
        line-height: 31px !important;
    }
    .select2-container .select2-selection--single {
        height: 35px !important;
    }
    .select2-selection__arrow {
        height: 34px !important;
    }

    #cityImg{
        margin-top: 18%;
    }

</style>

<!-- 2nd Section -->

<!-- 2nd Section -->
<section id="pay2">
    <div class="container" style="
                    background: url({{URL('/')}}/uploads/website/3.png);
                    background-size: 100% 100%;
                    background-repeat: no-repeat;
                    background-position-y: 100%;
                    "
                    >
        <div class="row">
            <div class="col-md-6" style="padding: 5%; padding-bottom: 0%;
                margin-top: 8%;
                ">
                <div class="alignLeft flyIn" style="color: #316BD1;">
                    <h1>READY TO BE <br>AN INSTA-HOTEL?</h1>
                    <br>
                    <p style="font-size: larger; color: white;">Take the step towards<br>the Insta-innovation.<br><br>Registering will only take a few minutes.</p>
                    <center><img id="cityImg" style="" src="uploads/City.svg"/></center>
                </div>
            </div>
            <div class="col-md-6">
                <div class="container" style="margin-bottom: 8em;">
                  <ul class="progressbar">
                    <li class="step finish">Hotel Representative Information</li>
                    <li class="step finish">Hotel Information</li>
                    <li class="step finish">Corporate Information</li>
                    <li class="step finish">Successful Registration</li>
                  </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<button id="openModal" style="display: none;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="border-radius: 30px">
      <div class="modal-header" style="border-bottom: 0px;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <center><img style="max-width: 300px;" src="{{URL('/')}}/uploads/people_graphics.svg"/>
        <h4>Success! We Have Received your registration</h4>
        <br>
        <p>Thank you for registering at Instastay. A welcome email will be sent to your email address in a few moments.</p>
        <br>
        <a class="btn btn-info" href="{{URL('/')}}">Back to Home</a>
        </center>
      </div>
      <!-- div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>
@endsection

@section('page-script')
    <script>
        $(function(){
            $("#openModal").click();
            $('.mySelect').select2();

            // swal({
            //   title: "Success! We Have Received your registration",
            //   text: "Thank you for registering at Instastay. A welcome email will be sent to your email address in a few moments.",
            //   imageUrl: "{{URL('/')}}/uploads/people_graphics.svg"
            // });
        })
    </script>
    <script>
        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab

        function showTab(n) {
          // This function will display the specified tab of the form...
          var x = document.getElementsByClassName("tab");
          x[n].style.display = "block";
          //... and fix the Previous/Next buttons:
          if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
          } else {
            document.getElementById("prevBtn").style.display = "inline";
          }
          if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Submit";
          } else {
            document.getElementById("nextBtn").innerHTML = "Next";
          }
          //... and run a function that will display the correct step indicator:
          fixStepIndicator(n)
        }

        function nextPrev(n) {
          // This function will figure out which tab to display
          var x = document.getElementsByClassName("tab");
          // Exit the function if any field in the current tab is invalid:
          if (n == 1 && !validateForm()) return false;
          // Hide the current tab:
          x[currentTab].style.display = "none";
          // Increase or decrease the current tab by 1:
          currentTab = currentTab + n;
          // if you have reached the end of the form...
          if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
          }
          // Otherwise, display the correct tab:
          showTab(currentTab);
        }

        function validateForm() {
          // This function deals with validation of the form fields
          var x, y, i, z = 0, valid = true;
          x = document.getElementsByClassName("tab");
          y = x[currentTab].getElementsByTagName("input");

          // console.log(y);
          // A loop that checks every input field in the current tab:
          for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
              // add an "invalid" class to the field:

              if(y[i].required == true){
                y[i].className += " invalid";
                z++;
              }
              // and set the current valid status to false
              
            } else {
                if(y[i].type == "email"){
                    test = validateEmail(y[i].value);
                    if(test == false){
                        y[i].className += " invalid";
                        z++;
                    }
                } else {
                    
                }
            }

            if(z != 0){
                valid = false;
              }
          }
          // If the valid status is true, mark the step as finished and valid:
          if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
          }
          return valid; // return the valid status
        }

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

        function fixStepIndicator(n) {
          // This function removes the "active" class of all steps...
          var i, x = document.getElementsByClassName("step");
          for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", " ");
          }
          //... and adds the "active" class on the current step:
          // console.log(x[n].innerHTML);
          $('#myTitle').html(x[n].innerHTML);
          console.log(n);
          if(n == 1){
            // x[n].style.marginTop = "51%"
            $('#cityImg').css('margin-top', '67%');
          } else if(n == 2){
            $('#cityImg').css('margin-top', '51%');
          } else {
            $('#cityImg').css('margin-top', '30%');
          }
          x[n].className += " active";
        }
    </script>
@endsection