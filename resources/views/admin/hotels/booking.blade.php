@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')
    
@endsection

@section('content')
<?php
	function dateFormater($date){
	    $date = explode("/", $date);
	    return $date = $date[2]."-".$date[1]."-".$date[0];
	}
?>
	<!-- start: sidebar -->
	{!! $menu !!}
	<!-- end: sidebar -->
	<style>
		.unselectFacilities{
			background: #fdfdfd; border: 0.5px solid #D3D3D3; padding: .5em;margin: .5em;
		}
		.selectFacilities{
			background: #DEF5F8; border: 0.5px solid transparent; padding: .5em; margin: .5em;
		}

		img.output:hover {
		cursor: pointer;
		}

		img.output1:hover {
		cursor: pointer;
		}

		label span input {
			z-index: 999;
			line-height: 0;
			font-size: 50px;
			position: absolute;
			top: 0px;
			left: 0px;
			opacity: 0;
			filter: alpha(opacity = 0);
			-ms-filter: "alpha(opacity=0)";
			cursor: pointer;
			_cursor: hand;
			margin: 0;
			padding:0;
		}

		#pac-input {
	        background-color: #fff;
	        font-family: Roboto;
	        font-size: 15px;
	        font-weight: 300;
	        margin-left: 12px;
	        padding: 0 11px 0 13px;
	        text-overflow: ellipsis;
	        width: 100%;
	        max-width: 300px;
	      }

	      #pac-input:focus {
	        border-color: #4d90fe;
	      }

	</style>
	<style>
	#filtersubmit {
	    position: relative;
	    z-index: 1;
	    left: 13px;
	    top: -25px;
	    color: #7B7B7B;
	    cursor:pointer;
	    width: 0;
	}
	#filter {
	    padding-left: 30px;
	}
	</style>
	<style type="text/css">
		.upload-img-wrapper div.item{
		    float:left;
		    margin-right: 5px;
		    margin-bottom: 5px;
		}

		.upload-img-wrapper div img{
		    width: 200px;
		    height:200px;
		}

		.upload-img-wrapper div.upload div{
		    width: 200px;
		    height: 200px;
		    padding: 60px;
		    text-align: center;
		    cursor: pointer;
		}

		.upload-img-wrapper div.upload div i{
		    font-size: 35px;
		    margin-top:15px;
		    
		    color: #4E4E4E;
		    display: block;
		}

		.upload-img-wrapper div.upload div:hover i{
		    color:#333;
		}
		</style>
		<style>
		.switch {
		  position: relative;
		  display: inline-block;
		  width: 49px;
    	  height: 23px;
		}

		.switch input { 
		  opacity: 0;
		  width: 0;
		  height: 0;
		}

		.slider {
		  position: absolute;
		  cursor: pointer;
		  top: 0;
		  left: 0;
		  right: 0;
		  bottom: 0;
		  background-color: #ccc;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		.slider:before {
		  position: absolute;
		  content: "";
		  height: 14px;
		  width: 14px;
		  left: 4px;
		  bottom: 4px;
		  background-color: white;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		input:checked + .slider {
		  background-color: #2196F3;
		}

		input:focus + .slider {
		  box-shadow: 0 0 1px #2196F3;
		}

		input:checked + .slider:before {
		  -webkit-transform: translateX(26px);
		  -ms-transform: translateX(26px);
		  transform: translateX(26px);
		}

		/* Rounded sliders */
		.slider.round {
		  border-radius: 34px;
		}

		.slider.round:before {
		  border-radius: 50%;
		}
		.onoffswitch {
		    position: relative; width: 90px;
		    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox {
		    display: none;
		}
		.onoffswitch-label {
		    display: block; overflow: hidden; cursor: pointer;
		    border: 2px solid #999999; border-radius: 20px;
		}
		.onoffswitch-inner {
		    display: block; width: 200%; margin-left: -100%;
		    transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner:before, .onoffswitch-inner:after {
		    display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
		    font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
		    box-sizing: border-box;
		}
		.onoffswitch-inner:before {
		    content: "ON";
		    padding-left: 10px;
		    background-color: #34A7C1; color: #FFFFFF;
		}
		.onoffswitch-inner:after {
		    content: "OFF";
		    padding-right: 10px;
		    background-color: #EEEEEE; color: #999999;
		    text-align: right;
		}
		.onoffswitch-switch {
		    display: block; width: 18px; margin: 6px;
		    background: #FFFFFF;
		    position: absolute; top: 0; bottom: 0;
		    right: 56px;
		    border: 2px solid #999999; border-radius: 20px;
		    transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
		    margin-left: 0;
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
		    right: 0px; 
		}
	</style>
	<style>
	#ck-button {
	    margin:4px;
	    background-color:#EFEFEF;
	    border-radius:4px;
	    border:1px solid #D0D0D0;
	    height: 2.3em;
	    float:left;
	}

	#ck-button label {
	    float:left;
	    width:4.0em;
	}

	#ck-button label span {
	    text-align:center;
	    padding:5px 0px;
	    display:block;
	    cursor: pointer;
	}

	#ck-button label input {
	    position:absolute;
	    visibility: hidden;
	}

	#ck-button input:checked + span {
	    background-color: #2c9911;
	    color: #fff;
	    border-radius: 3px;
	}

	.user_active{
		box-shadow: 2px 0 0 #0088cc inset !important;
	}
	</style>
	<section role="main" class="content-body">
		<header class="page-header">
			<h2 style="font-size: 15px;"><img  style="background: white;
			    border-radius: 100%;
			    width: 40px;
			    height: 40px;
			    margin-right: 1em;
			" src="{{URL('/')}}/uploads/{{$hotels->img_file == '' ? 'No-Image-Available.png' : $hotels->img_file}}"/> {{strtoupper($hotels->hotel_name)}}'s DASHBOARD</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="index2.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Dashboard</span></li>
				</ol>
			</div>
		</header>

		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<div class="panel-body" style="padding-top: 0px;">
						<div class="row">
							<div class="col-md-12" style="padding: 0em;">
								<section class="panel">
									<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd; padding-bottom: 0px;">
										<div class="panel-actions">
											<!-- <a href="#" class="fa fa-caret-down"></a> -->
											<!-- <a href="#" class="fa fa-times"></a> -->
										</div>
										<h2 class="panel-title" style="font-weight:bold; color: black">Booking List</h2>

										<p class="panel-subtitle"></p>
										
									</header>
									<hr>
									<section class="panel" style="padding: 3em;">
										<div class="row">
											<div class="col-md-4">
												<input id="type" type="hidden" value="3"/>
												<div class="row">
													<div class="col-md-4" style="padding: 0em;margin-bottom: 1em;">
														<button onclick="getCheckIn();" id="checkin" style="box-shadow: 0px 3px 6px #00000033; width: 100%;" class="btn btn-default">Check-in</button>
													</div>
													<div class="col-md-4" style="padding: 0em;margin-bottom: 1em;">
														<button onclick="getCheckOut();" id="checkout" style="box-shadow: 0px 3px 6px #00000033; width: 100%;" class="btn btn-default">Check-out</button>
													</div>
													<div class="col-md-4" style="padding: 0em; margin-bottom: 1em;">
														<button id="booking" onclick="getBookings();" style="box-shadow: 0px 3px 6px #00000033; width: 100%;" class="btn btn-default active">Bookings</button>
													</div>
												</div>
											</div>
											<div class="col-md-2" style="margin-bottom: 1em;">
												<input type="text" value="{{date('01/m/Y')}}" name="sDate" class="form-control datetimepicker" id="sDate" autocomplete="off">
												<i style="font-size: 1.5em; float: right;margin-top: -1.25em; margin-right: .5em;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-2" style="margin-bottom: 1em;">
												<input type="text" value="{{date('t/m/Y')}}" name="eDate" class="form-control datetimepicker" id="eDate" autocomplete="off">
												<i style="font-size: 1.5em; float: right;margin-top: -1.25em; margin-right: .5em;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-4">
												<div class="row">
													<!-- <div class="col-md-6" style="margin-bottom: 1em;">
														<button style="color: #0DC3D8; border: 2px solid #0DC3D8; box-shadow: 0px 3px 6px #00000033; width: 100%;" class="btn btn-default">MORE FILTER <i style="color: #0DC3D8; font-size: 1em; float: right;margin-top: 0em; margin-right: 0em;" class="fa fa-caret-down"></i></button>

													</div> -->
													<div class="col-md-6" style="margin-bottom: 1em;">
														<button onclick="getBooking()" style="box-shadow: 0px 3px 6px #00000033; width: 100%;" class="btn btn-info">SEARCH</button>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<table style="width: 100%" class="table table-bordered table-striped mb-none" id="tablePending">
													<thead>
														<tr>
															<th>Guest Name</th>
															<th>Check-in Time & Date</th>
															<th>Check-out Time & Date</th>
															<th>Rooms</th>
															<th>Rate</th>
															<th>Booked On</th>
															<th>Status</th>
															<th>Total Price</th>
															<th>Comission</th>
															<th>Booking Number</th>
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div>
										</div>
									</section>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
	
@endsection

@section('page-script')
<script>
$(function(){
	$(".mySelect").select2();
	getBooking();
	$('#roomManagementMenu').addClass('nav-expanded');
	$('#roomManagementMenu').addClass('nav-active');
	$('#bookingMenu').addClass('nav-expanded');
	$('#bookingMenu').addClass('nav-active');
});

$('.datetimepicker').datetimepicker({
	timepicker:false,
	format:'d/m/Y',
	formatDate:'d/m/Y',
	// minDate:'-1970/01/02', // yesterday is minimum date
	// maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});

function getBookings(){
	$("#type").val('3');
	$("#booking").removeClass('active');
	$("#booking").addClass('active');
	$("#checkin").removeClass('active');
	$("#checkout").removeClass('active');
	getBooking()
}

function getCheckIn(){
	$("#type").val('1');
	$("#booking").removeClass('active');
	$("#checkin").removeClass('active');
	$("#checkin").addClass('active');
	$("#checkout").removeClass('active');
	getBooking()
}

function getCheckOut(){
	$("#type").val('2');
	$("#booking").removeClass('active');
	$("#checkin").removeClass('active');
	$("#checkout").removeClass('active');
	$("#checkout").addClass('active');
	getBooking()
}

function createCookie(name, value, days) {
  var expires;
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toGMTString();
  }
  else {
    expires = "";
  }
  document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}

function fixDate(date){
	res = date.split("/");
	return res[2]+'-'+res[1]+'-'+res[0];
}

function getBooking(){

		var type = $("#type").val();
		var sDate = $("#sDate").val();

		var eDate = $("#eDate").val();

		if(type == 3){
			var label = "Booking List as from "+moment(fixDate(sDate)).format('MMM DD, YYYY')+" - "+moment(fixDate(eDate)).format('MMM DD, YYYY')+"";
			var url = "{{URL('/')}}/fetchBooking/{{$hotels->id}}";
		}else if(type == 2){
			sDate = "{{date('d/m/Y')}}";
			eDate = "{{date('d/m/Y')}}";
			var label = "Check-Out List as of {{date('M d, Y')}}";
			var url = "{{URL('/')}}/fetchCheckOut/{{$hotels->id}}";
		} if(type == 1){
			// sDate = "{{date('d/m/Y')}}";
			// eDate = "{{date('d/m/Y')}}";
			var label = "Check-In List as of  {{date('M d, Y')}}";
			var url = "{{URL('/')}}/fetchCheckIn/{{$hotels->id}}";
		}

		var table = $('#tablePending').DataTable();

		table.destroy();

	    var table = $('#tablePending').DataTable( {
		"order": [],
		// "bPaginate": false,
		// "processing": true,
		"searching": false,
		dom: 'Bfrtip',
		scrollX: true,
	        buttons: [
		        {
		            extend: 'pageLength', className: 'datatable_button',
		            title: label
		        },
		        {
		            extend: 'excelHtml5', className: 'datatable_button',
		            title: label
		        },
		        // {
		        //     extend: 'pdfHtml5', className: 'datatable_button',
		        //     title: label
		        // },
		        {
		            extend: 'print', className: 'datatable_button',
		            title: label
		        },
		    ],
		    responsive: true,
		    "ajax": {
		        "url": url,
		        "type": "POST",
		        "data" : {
		            "_token": "{{ csrf_token() }}",
		            "sDate": sDate,
		            "eDate": eDate,
		        }
		    }
		} );
	}
</script>
@endsection