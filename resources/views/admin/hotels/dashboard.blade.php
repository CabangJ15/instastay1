@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')
    
@endsection

@section('content')
<!-- start: sidebar -->
{!! $menu !!}
<!-- end: sidebar -->
<style>
	/*#sidebar-left{
		display: none;
	}*/
	.inner-wrapper{
		background: white;
	}

	.dataTables_filter {
	display: none;
	}

	.sorting, .sorting_asc, .sorting_desc {
	    background : none;
	}

	.content-body{
		margin-left: 0px !important;
	}

	.padding-top{
		padding-top: 2em !important;
	}

	table.dataTable thead{
		display: none;
	}

	#sidebar-left{
		display: none;
	}

	.inner-wrapper{
		padding-top: 3em !important;
	}

	table.dataTable thead .sorting, 
	table.dataTable thead .sorting_asc, 
	table.dataTable thead .sorting_desc {
	    background : none;
	}

	.table-striped > tbody > tr:nth-child(odd),
	.table-striped > tbody > tr:nth-child(even) {
	    background-color: #ffffff;
	}

	/* Styles for wrapping the search box */

	.main {
	    width: 100%;
	    margin: .5em 0;
	}

	/* Bootstrap 3 text input with search icon */

	.has-search .form-control-feedback {
	    right: initial;
	    left: 0;
	    color: #ccc;
	    top: 8px;
	}

	.has-search .form-control {
	    padding-right: 12px;
	    padding-left: 34px;
	}

	.clearfix::after {
	  content: "";
	  clear: both;
	  display: table;
	}

</style>

<section role="main" class="content-body" style="padding-top: 0px;">
	<div class="row">
		<div class="col-md-12">
			<section class="panel">
				<div style="padding: 3em; background: #FFFFFF 0% 0% no-repeat padding-box; opacity: 1;">
					<div class="row" style="padding: .5em;
					    padding-bottom: .1em;
					    border: 1px black solid;
					    border-radius: 7px;">
						<div class="col-md-3">
							<p style="font-size: 1.25em; font-weight:bold; color: black">Portfolio Overview</p>
						</div>
						<div class="col-md-3">
							<p style="font-weight: normal; font-size: 1em; color: black">
								<font style="text-align: left;
							font-size: 1em;
							font-weight: bold;
							color: #0DC3D8;">{{count($hotels->hotels)}} </font>PROPERTY COUNT</p>
						</div>
						<div class="col-md-6">
							<!-- p style="font-weight: normal; font-size: 1em; color: black">
								<font style="text-align: left;
							font-size: 3em;
							font-weight: bold;
							vertical-align: middle;
							letter-spacing: 0;
							color: #0DC3D8;
							opacity: 1; margin-right: .5em;">PHP 254,999.20</font>Overall Revenue</p> -->
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<div class="main">
							  <!-- Actual search box -->
							  <div class="form-group has-feedback has-search">
							    <span class="glyphicon glyphicon-search form-control-feedback"></span>
							    <input style="
							        background: #F0F0F0 0% 0% no-repeat padding-box;
									height: 48px;
									" type="text" class="form-control" placeholder="SEARCH PROPERTY NAME OR ID">
							  </div>
							  
							</div>
							<div class="row" id="hotel_content">
								<!-- <div class="col-md-3">
									<a href="">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col">
													<center>
														<a href="{{URL('/')}}/hotels/create">
															<div class="summary">
																<div style="
																	width: 100%;
																	height: 200px;
																	background-size:100% 100%;
																	opacity: 1;
																	background: #F0F0F0;
																">
																	<i style="margin-top: 32%; color: #0DC3D8" class="fa fa-plus-circle fa-2x"></i><br>
																	<p style="color:#707070FA; font-weight: bold;">ADD PROPERTY</p>

																</div>
																<h4 class="title"><b></b></h4>
																<div class="info">
																	<span class="text-primary" style="color: black !important"></span>
																	<p style="font-weight: bold; color: red;"><br></p>
																</div>
															</div>
														</a>
													</center>
												</div>
											</div>
										</div>
									</a>
								</div> -->
								<?php  $x = 0; ?>
								@foreach($hotels->hotels as $result)
								<div class="col-md-3">
									<a href="{{URL('/')}}/hotels/{{$result->hotel->id}}">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col">
													<center>
													<div class="summary">
														<div style="
															width: 100%;
															height: 200px;
															background: transparent url('{{URL('/')}}/uploads/{{$result->hotel->img_file == '' ? 'No-Image-Available.png' : $result->hotel->img_file}}') 0% 0% no-repeat padding-box;
															background-size:100% 100%;
															opacity: 1;
														"></div>
														<h4 class="title"><b>{{$result->hotel->hotel_name}}</b></h4>
														<div class="info">
															<span class="text-primary" style="color: black !important">{{$result->hotel->address}}</span>
															@if($result->hotel->flag == ""):
															<p style="font-weight: bold; color: red;">Please contact Instastay<br>Administratorto enable this hotel.</p>
															@endif
														</div>
													</div>
													</center>
												</div>
											</div>
										</div>
								</a>
								</div>
								<?php $x++; ?>
								@if($x == 4)
								<div class="clearfix"></div>
									<?php $x=0; ?>
								@endif
								@endforeach
							</div>
							<!-- <a href="{{URL('/')}}/hotels/create" style="font-size: 2em;
							    border-radius: 100%;
							    position: absolute;
							    bottom: 0;
							    right: 0;
							    width: 2em;
							    height: 2em;
								" type="button" class="btn btn-primary btn-circle btn-xl">
								<i class="fa fa-plus"></i>
					        </a> -->
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</section>
	
@endsection

@section('page-script')
<script type="text/javascript">
	$(function(){
		
		$("#sidebar-left").hide();
		// $(".content-body").hide();
		

		$('#hotelListMenu').addClass('nav-active');
		$('#hotelListMenu').addClass('nav-expanded');


		$('#mycalendar').monthly({
			mode: 'event',
			//jsonUrl: 'events.json',
			//dataType: 'json'
			// xmlUrl: 'events.xml',
			stylePast: true,
			disablePast: true
		});

		var table = $('#table_id').DataTable({
			"bLengthChange": false
		});

		$('#myInputTextField').keyup(function(){
		      table.search($(this).val()).draw() ;
		})
	});

	
</script>
@endsection