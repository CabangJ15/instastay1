@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')

@endsection
@section('content')

	{!! $menu !!}
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>Hotel</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="{{URL('room_rate')}}">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Hotel</span></li>
				</ol>
			</div>
		</header>

		<section class="panel">
			<div class="panel-body">
				<div class="form-title">
					<!-- <a href="{{URL('/')}}/room_rate/create" style="float: right;" class="btn btn-primary btn-flat btn-pri">
						<i class="fa fa-plus"></i> Add
					</a> -->
				</div>

				<div class="form-body">
					<table style="width:100%" class="table table-bordered table-striped mb-none" id="table_id">
						<thead>
							<tr>
								<th>Hotel</th>
								<th>Address</th>
								<th>Created By</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</section>
@endsection

@section('page-script')
<script>
	$(function(){
		$('#hotelMenu').addClass('nav-expanded');
		$('#hotelMenu').addClass('nav-active');
		$('#settingMenu').addClass('nav-expanded');
		$('#settingMenu').addClass('nav-active');

		reFetch();
	});

	function reFetch(){

		var table = $('#table_id').DataTable();

		table.destroy();

	    table = $('#table_id').DataTable( {
			"order": [],
			// "bPaginate": false,
			// "processing": true,
			dom: 'Bfrtip',
	        buttons: [
		        {
		            extend: 'pageLength', className: 'datatable_button',
		            title: 'Hotel List as of {{date('M d, Y')}}'
		        },
		        {
		            extend: 'excelHtml5', className: 'datatable_button',
		            title: 'Hotel List as of {{date('M d, Y')}}'
		        },
		        {
		            extend: 'pdfHtml5', className: 'datatable_button',
		            title: 'Hotel List as of {{date('M d, Y')}}'
		        },
		        {
		            extend: 'print', className: 'datatable_button',
		            title: 'Hotel List as of {{date('M d, Y')}}'
		        },
		    ],
		    responsive: true,
		    "ajax": {
		        "url": "{{URL('/')}}/fetchHotel",
		        "type": "POST",
		        "data" : {
		            "_token": "{{ csrf_token() }}",
		        }
		    }
		} );

	}

	function changeStatus(id, status){
		if(status == 0){
			var label = 'Deactivate';
			var class_name = 'btn-danger';

		} else {
			var label = 'Activate';
			var class_name = 'btn-success';
		}

		swal("Are you sure?", "You want to "+ label +" this hotel!", {
			  buttons: {
			    cancel: "Cancel",
			    catch: {
			      text: "Yes",
			      value: "delete",
			      className: class_name,
			    }
			  },
			})
			.then((value) => {
			  switch (value) {
			 
			    case "delete":
			      // myrow.remove().draw();

				     $.ajax({
						url: "{{URL('/')}}/hotel/"+id,
						type: "POST",
						data: {
							_token: "{{ csrf_token() }}",
							"status": status,
							"_method": "PUT",
						},
						success: function(data){
							 reFetch();

						}        
				   });
			      break;
			 
			    default:
			      swal.close();
			  }
			});
		// $.ajax({
		// 	url: "{{URL('/')}}/hotel/"+id,
		// 	type: "POST",
		// 	data: {
		// 		_token: "{{ csrf_token() }}",
		// 		"status": status
		// 	},
		// 	success: function(data){
		// 		 reFetch();

		// 	}        
	 //   });
	}

</script>
@endsection