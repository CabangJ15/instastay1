@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')
    
@endsection

@section('content')
	<!-- start: sidebar -->
	{!! $menu !!}
	<!-- end: sidebar -->
	<style>
		img.output:hover {
		cursor: pointer;
		}

		img.output1:hover {
		cursor: pointer;
		}

		label span input {
			z-index: 999;
			line-height: 0;
			font-size: 50px;
			position: absolute;
			top: 0px;
			left: 0px;
			opacity: 0;
			filter: alpha(opacity = 0);
			-ms-filter: "alpha(opacity=0)";
			cursor: pointer;
			_cursor: hand;
			margin: 0;
			padding:0;
		}

		#pac-input {
	        background-color: #fff;
	        font-family: Roboto;
	        font-size: 15px;
	        font-weight: 300;
	        margin-left: 12px;
	        padding: 0 11px 0 13px;
	        text-overflow: ellipsis;
	        width: 100%;
	        max-width: 300px;
	      }

	      #pac-input:focus {
	        border-color: #4d90fe;
	      }

	</style>
	<style>
	#filtersubmit {
	    position: relative;
	    z-index: 1;
	    left: 13px;
	    top: -25px;
	    color: #7B7B7B;
	    cursor:pointer;
	    width: 0;
	}
	#filter {
	    padding-left: 30px;
	}
	</style>
	<style type="text/css">
		.upload-img-wrapper div.item{
		    float:left;
		    margin-right: 5px;
		    margin-bottom: 5px;
		}

		.upload-img-wrapper div img{
		    width: 200px;
		    height:200px;
		}

		.upload-img-wrapper div.upload div{
		    width: 200px;
		    height: 200px;
		    padding: 60px;
		    text-align: center;
		    cursor: pointer;
		}

		.upload-img-wrapper div.upload div i{
		    font-size: 35px;
		    margin-top:15px;
		    
		    color: #4E4E4E;
		    display: block;
		}

		.upload-img-wrapper div.upload div:hover i{
		    color:#333;
		}
		</style>
		<style>

			.dataTables_filter {
			display: none;
			}

			.sorting, .sorting_asc, .sorting_desc {
			    background : none;
			}

			table.dataTable thead{
				display: none;
			}

			table.dataTable thead .sorting, 
			table.dataTable thead .sorting_asc, 
			table.dataTable thead .sorting_desc {
			    background : none;
			}

			.table-striped > tbody > tr:nth-child(odd),
			.table-striped > tbody > tr:nth-child(even) {
			    background-color: #ffffff;
			}
		</style>

	<section role="main" class="content-body">
		<header class="page-header">
			<h2 style="font-size: 15px;"><img  style="background: white;
			    border-radius: 100%;
			    width: 40px;
			    height: 40px;
			    margin-right: 1em;
			" src="{{URL('/')}}/uploads/{{$hotels->img_file == '' ? 'No-Image-Available.png' : $hotels->img_file}}"/> {{strtoupper($hotels->hotel_name)}}'s DASHBOARD</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="index2.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Dashboard</span></li>
				</ol>
			</div>
		</header>

		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<div class="panel-body" style="padding-top: 0px;">
						<div class="row">
							<div class="col-md-12">
								<section class="panel" style="padding: 3em;">
									<div id="horizontalTab">
										<ul>
											<li><a href="#tab-1">All Reviews</a></li>
											<li><a href="#tab-2">Replied</a></li>
											<li><a href="#tab-3">Pending Replies</a></li>
										</ul>
									<div id="tab-1">
										<table id="allReview" class="table table-bordered table-striped mb-none no-footer dataTable">
											<thead>
												<tr>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th>
														<div class="row">
															<div class="col-md-1">
																<center>
																<img style="background: white;
																    border-radius: 100%;
																    width: 40px;
																    height: 40px;"
																    src="http://127.0.0.1/instastay1/public/uploads/canley.png"/>
																    <p style="margin-bottom: 1px;">Name</p>
																    <p style="margin-bottom: 1px; font-weight: normal"><i>Date</i></p>
																    <i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up" aria-hidden="true"></i>
																</center>
															</div>
															<div class="col-md-11">
																<p align="justify" style="font-weight: normal">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
																<button class="btn btn-info">REPLY</button>
															</div>
														</div>
													</th>
												</tr>
												<tr>
													<th>
														<div class="row">
															<div class="col-md-1">
																<center>
																<img style="background: white;
																    border-radius: 100%;
																    width: 40px;
																    height: 40px;"
																    src="http://127.0.0.1/instastay1/public/uploads/canley.png"/>
																    <p style="margin-bottom: 1px;">Name</p>
																    <p style="margin-bottom: 1px; font-weight: normal"><i>Date</i></p>
																    <i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up" aria-hidden="true"></i>
																</center>
															</div>
															<div class="col-md-11">
																<p align="justify" style="font-weight: normal">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
																<button class="btn btn-info">REPLY</button>
															</div>
														</div>
													</th>
												</tr>
												<tr>
													<th>
														<div class="row">
															<div class="col-md-1">
																<center>
																<img style="background: white;
																    border-radius: 100%;
																    width: 40px;
																    height: 40px;"
																    src="http://127.0.0.1/instastay1/public/uploads/canley.png"/>
																    <p style="margin-bottom: 1px;">Name</p>
																    <p style="margin-bottom: 1px; font-weight: normal"><i>Date</i></p>
																    <i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up" aria-hidden="true"></i>
																</center>
															</div>
															<div class="col-md-11">
																<p align="justify" style="font-weight: normal">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
																<hr>
																<div class="col-md-1" style="max-width: 4em;">
																	<img  style="background: white;
																	    border-radius: 100%;
																	    width: 40px;
																	    height: 40px;
																	    margin-right: 1em;
																	" src="{{URL('/')}}/uploads/{{$hotels->img_file}}">
																	
																</div>
																<div class="col-md-11">
																	<p style="margin-bottom: 1px;">{{$hotels->hotel_name}}
																	</p>
																	<p><i style="font-weight: normal"> Date</i>
																	</p>
																</div>
																<div class="col-md-12">
																	<p align="justify" style="font-weight: normal">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
																	<button class="btn btn-default">EDIT</button>
																	<button class="btn btn-danger">DELETE</button>
																</div>
															</div>
														</div>
													</th>
												</tr>
												<tr>
													<th>
														<div class="row">
															<div class="col-md-1">
																<center>
																<img style="background: white;
																    border-radius: 100%;
																    width: 40px;
																    height: 40px;"
																    src="http://127.0.0.1/instastay1/public/uploads/canley.png"/>
																    <p style="margin-bottom: 1px;">Name</p>
																    <p style="margin-bottom: 1px; font-weight: normal"><i>Date</i></p>
																    <i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up" aria-hidden="true"></i>
																</center>
															</div>
															<div class="col-md-11">
																<p align="justify" style="font-weight: normal">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
																<hr>
																<div class="col-md-1" style="max-width: 4em;">
																	<img  style="background: white;
																	    border-radius: 100%;
																	    width: 40px;
																	    height: 40px;
																	    margin-right: 1em;
																	" src="{{URL('/')}}/uploads/{{$hotels->img_file}}">
																	
																</div>
																<div class="col-md-11">
																	<p style="margin-bottom: 1px;">{{$hotels->hotel_name}}
																	</p>
																	<p><i style="font-weight: normal"> Date</i>
																	</p>
																</div>
																<div class="col-md-12">
																	<p align="justify" style="font-weight: normal">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
																	<button class="btn btn-default">EDIT</button>
																	<button class="btn btn-danger">DELETE</button>
																</div>
															</div>
														</div>
													</th>
												</tr>
											</tbody>
										</table>
									</div>
									<div id="tab-2">
										<table id="replied" class="table table-bordered table-striped mb-none">
											<thead>
												<tr>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th>
														<div class="row">
															<div class="col-md-1">
																<center>
																<img style="background: white;
																    border-radius: 100%;
																    width: 40px;
																    height: 40px;"
																    src="http://127.0.0.1/instastay1/public/uploads/canley.png"/>
																    <p style="margin-bottom: 1px;">Name</p>
																    <p style="margin-bottom: 1px; font-weight: normal"><i>Date</i></p>
																    <i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up" aria-hidden="true"></i>
																</center>
															</div>
															<div class="col-md-11">
																<p align="justify" style="font-weight: normal">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
																<hr>
																<div class="col-md-1" style="max-width: 4em;">
																	<img  style="background: white;
																	    border-radius: 100%;
																	    width: 40px;
																	    height: 40px;
																	    margin-right: 1em;
																	" src="{{URL('/')}}/uploads/{{$hotels->img_file}}">
																	
																</div>
																<div class="col-md-11">
																	<p style="margin-bottom: 1px;">{{$hotels->hotel_name}}
																	</p>
																	<p><i style="font-weight: normal"> Date</i>
																	</p>
																</div>
																<div class="col-md-12">
																	<p align="justify" style="font-weight: normal">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
																	<button class="btn btn-default">EDIT</button>
																	<button class="btn btn-danger">DELETE</button>
																</div>
															</div>
														</div>
													</th>
												</tr>
												<tr>
													<th>
														<div class="row">
															<div class="col-md-1">
																<center>
																<img style="background: white;
																    border-radius: 100%;
																    width: 40px;
																    height: 40px;"
																    src="http://127.0.0.1/instastay1/public/uploads/canley.png"/>
																    <p style="margin-bottom: 1px;">Name</p>
																    <p style="margin-bottom: 1px; font-weight: normal"><i>Date</i></p>
																    <i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up" aria-hidden="true"></i>
																</center>
															</div>
															<div class="col-md-11">
																<p align="justify" style="font-weight: normal">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
																<hr>
																<div class="col-md-1" style="max-width: 4em;">
																	<img  style="background: white;
																	    border-radius: 100%;
																	    width: 40px;
																	    height: 40px;
																	    margin-right: 1em;
																	" src="{{URL('/')}}/uploads/{{$hotels->img_file}}">
																	
																</div>
																<div class="col-md-11">
																	<p style="margin-bottom: 1px;">{{$hotels->hotel_name}}
																	</p>
																	<p><i style="font-weight: normal"> Date</i>
																	</p>
																</div>
																<div class="col-md-12">
																	<p align="justify" style="font-weight: normal">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
																	<button class="btn btn-default">EDIT</button>
																	<button class="btn btn-danger">DELETE</button>
																</div>
															</div>
														</div>
													</th>
												</tr>
											</tbody>
										</table>
									</div>
									<div id="tab-3">
										<table id="pending" class="table table-bordered table-striped mb-none">
											<thead>
												<tr>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th>
														<div class="row">
															<div class="col-md-1">
																<center>
																<img style="background: white;
																    border-radius: 100%;
																    width: 40px;
																    height: 40px;"
																    src="http://127.0.0.1/instastay1/public/uploads/canley.png"/>
																    <p style="margin-bottom: 1px;">Name</p>
																    <p style="margin-bottom: 1px; font-weight: normal"><i>Date</i></p>
																    <i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up" aria-hidden="true"></i>
																</center>
															</div>
															<div class="col-md-11">
																<p align="justify" style="font-weight: normal">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
																<button class="btn btn-info">REPLY</button>
															</div>
														</div>
													</th>
												</tr>
												<tr>
													<th>
														<div class="row">
															<div class="col-md-1">
																<center>
																<img style="background: white;
																    border-radius: 100%;
																    width: 40px;
																    height: 40px;"
																    src="http://127.0.0.1/instastay1/public/uploads/canley.png"/>
																    <p style="margin-bottom: 1px;">Name</p>
																    <p style="margin-bottom: 1px; font-weight: normal"><i>Date</i></p>
																    <i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
																	<i class="fa fa-hand-o-up" aria-hidden="true"></i>
																</center>
															</div>
															<div class="col-md-11">
																<p align="justify" style="font-weight: normal">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
																<button class="btn btn-info">REPLY</button>
															</div>
														</div>
													</th>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
						</section>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
	<style>

		.dt2019-12-25 { background-color: #F0FFF0; }
		.dt2019-12-25 .monthly-day-number:after { content: '\1F384'; }
		.fa-hand-o-up{
			  transform: rotate(-40deg)  scale(-1, 1);
		}
		.yellow{
			  color: #e0e024;
		}
	</style>
	
@endsection

@section('page-script')
<script type="text/javascript">
	$(function(){

		$('#catalogMenu').addClass('nav-expanded');
		$('#catalogMenu').addClass('nav-active');
		$('#reviewMenu').addClass('nav-expanded');
		$('#reviewMenu').addClass('nav-active');
	});

	$('#horizontalTab').responsiveTabs({
	    rotate: false,
        startCollapsed: 'accordion',
        collapsible: 'accordion',
        setHash: false,
        activate: function(e, tab) {
            $('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
        },
        activateState: function(e, state) {
            //console.log(state);
            $('.info').html('Switched from <strong>' + state.oldState + '</strong> state to <strong>' + state.newState + '</strong> state!');
        }
	});

	$('#allReview').DataTable({
		dom: 'Bfrtip',
		responsive: true,
		buttons: [
		        {
		            extend: 'pageLength', className: 'datatable_button',
		            title: 'Inventory Count Cycle List as of Sep 27, 2019'
		        }
		    ]
	});
	$('#replied').DataTable({
		dom: 'Bfrtip',
		responsive: true,
		buttons: [
		        {
		            extend: 'pageLength', className: 'datatable_button',
		            title: 'Inventory Count Cycle List as of Sep 27, 2019'
		        }
		    ]
	});
	$('#pending').DataTable({
		dom: 'Bfrtip',
		responsive: true,
		buttons: [
		        {
		            extend: 'pageLength', className: 'datatable_button',
		            title: 'Inventory Count Cycle List as of Sep 27, 2019'
		        }
		    ]
	});
</script>
@endsection