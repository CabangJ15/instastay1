@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')
    
@endsection

@section('content')
<?php
if(isset($_GET['date'])):
	$date = $_GET['date'];
else:
	$date = date('m/Y');

endif;
?>
	<!-- start: sidebar -->
	{!! $menu !!}
	<!-- end: sidebar -->
	<style>
		.calendarFont{
			font-weight: bold;
		    padding: .5em;
		 }

		.calendarGreen{
			background: #57D48D 0% 0% no-repeat padding-box;
    		color: #04BF55;
		}

		.calendarGreen:hover{
			background: #48af75 0% 0% no-repeat padding-box;
    		color: #57D48D;
		}

		.calendarRed{
			background: #E9788C 0% 0% no-repeat padding-box;
    		color: #E0425E;
		}

		.calendarRed:hover{
			background: #E0425E 0% 0% no-repeat padding-box;
    		color: #E9788C;
		}

		.calendarOrange{
			background: #EF953A 0% 0% no-repeat padding-box;
    		color: #b56f29;
		}

		.calendarOrange:hover{
			background: #b56f29 0% 0% no-repeat padding-box;
    		color: #EF953A;
		}

		.calendarModal{
			width: 100%;
		    background: white;
		    /*display: none;*/
		    position: absolute;
		    top: -50px;
		    right: 0px;
		    background: #FAFAFA 0% 0% no-repeat padding-box;
		    z-index: 1;
		    box-shadow: 4px 5px 6px #0000003D;
		}
	</style>
	<style>
		@media (max-width: 568px){
		  .seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1  {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }

		  .myCalendar{
				height: 88px !important;
			}

			.myCalendarText{
				color: white;
	    		font-size: 2em !important;
	    		font-weight:bold;
			}

			.myCalendarTag{
				width: 180% !important;
			}

			.calendarFont {
			    font-weight: bold;
			    padding: 0em;
			}

			.calendarModal {
			    width: 220% !important;
			   	left: 0px;
			}
		}
		@media (min-width: 667px){
		  .seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1  {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }
		}
		@media (min-width: 768px){
		  .seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1  {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }
		}

		@media (min-width: 992px) {
		  .seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1 {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }
		}

		/**
		 *  The following is not really needed in this case
		 *  Only to demonstrate the usage of @media for large screens
		 */    
		@media (min-width: 1040px) {
		  .seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1 {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }
		}

		@media (min-width: 1200px) {
		  .seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1 {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }
		}

		.seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1 {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }

		.myCalendar{
			height: 134px;
			cursor:pointer;
		}

		.myCalendarText{
			color: white;
    		font-size: 6em;
    		font-weight:bold;
		}

		.myCalendarTag{
			float: right;
		    padding: .1em;
		    background: #DC2747;
		    right: 0px;
		    color: white;
		    padding: 0em .5em;
		    width: 50%;
		    text-align: center;
		    margin-right: -1em;
		}
	</style>
	<style>
		.unselectFacilities{
			background: #fdfdfd; border: 0.5px solid #D3D3D3; padding: .5em;margin: .5em;
		}
		.selectFacilities{
			background: #DEF5F8; border: 0.5px solid transparent; padding: .5em; margin: .5em;
		}

		img.output:hover {
		cursor: pointer;
		}

		img.output1:hover {
		cursor: pointer;
		}

		label span input {
			z-index: 999;
			line-height: 0;
			font-size: 50px;
			position: absolute;
			top: 0px;
			left: 0px;
			opacity: 0;
			filter: alpha(opacity = 0);
			-ms-filter: "alpha(opacity=0)";
			cursor: pointer;
			_cursor: hand;
			margin: 0;
			padding:0;
		}

		#pac-input {
	        background-color: #fff;
	        font-family: Roboto;
	        font-size: 15px;
	        font-weight: 300;
	        margin-left: 12px;
	        padding: 0 11px 0 13px;
	        text-overflow: ellipsis;
	        width: 100%;
	        max-width: 300px;
	      }

	      #pac-input:focus {
	        border-color: #4d90fe;
	      }

	</style>
	<style>
	#filtersubmit {
	    position: relative;
	    z-index: 1;
	    left: 13px;
	    top: -25px;
	    color: #7B7B7B;
	    cursor:pointer;
	    width: 0;
	}
	#filter {
	    padding-left: 30px;
	}
	</style>
	<style type="text/css">
		.upload-img-wrapper div.item{
		    float:left;
		    margin-right: 5px;
		    margin-bottom: 5px;
		}

		.upload-img-wrapper div img{
		    width: 200px;
		    height:200px;
		}

		.upload-img-wrapper div.upload div{
		    width: 200px;
		    height: 200px;
		    padding: 60px;
		    text-align: center;
		    cursor: pointer;
		}

		.upload-img-wrapper div.upload div i{
		    font-size: 35px;
		    margin-top:15px;
		    
		    color: #4E4E4E;
		    display: block;
		}

		.upload-img-wrapper div.upload div:hover i{
		    color:#333;
		}
		</style>
		<style>
		.switch {
		  position: relative;
		  display: inline-block;
		  width: 49px;
    	  height: 23px;
		}

		.switch input { 
		  opacity: 0;
		  width: 0;
		  height: 0;
		}

		.slider {
		  position: absolute;
		  cursor: pointer;
		  top: 0;
		  left: 0;
		  right: 0;
		  bottom: 0;
		  background-color: #ccc;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		.slider:before {
		  position: absolute;
		  content: "";
		  height: 14px;
		  width: 14px;
		  left: 4px;
		  bottom: 4px;
		  background-color: white;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		input:checked + .slider {
		  background-color: #2196F3;
		}

		input:focus + .slider {
		  box-shadow: 0 0 1px #2196F3;
		}

		input:checked + .slider:before {
		  -webkit-transform: translateX(26px);
		  -ms-transform: translateX(26px);
		  transform: translateX(26px);
		}

		/* Rounded sliders */
		.slider.round {
		  border-radius: 34px;
		}

		.slider.round:before {
		  border-radius: 50%;
		}
		.onoffswitch {
		    position: relative; width: 90px;
		    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox {
		    display: none;
		}
		.onoffswitch-label {
		    display: block; overflow: hidden; cursor: pointer;
		    border: 2px solid #999999; border-radius: 20px;
		}
		.onoffswitch-inner {
		    display: block; width: 200%; margin-left: -100%;
		    transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner:before, .onoffswitch-inner:after {
		    display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
		    font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
		    box-sizing: border-box;
		}
		.onoffswitch-inner:before {
		    content: "ON";
		    padding-left: 10px;
		    background-color: #34A7C1; color: #FFFFFF;
		}
		.onoffswitch-inner:after {
		    content: "OFF";
		    padding-right: 10px;
		    background-color: #EEEEEE; color: #999999;
		    text-align: right;
		}
		.onoffswitch-switch {
		    display: block; width: 18px; margin: 6px;
		    background: #FFFFFF;
		    position: absolute; top: 0; bottom: 0;
		    right: 56px;
		    border: 2px solid #999999; border-radius: 20px;
		    transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
		    margin-left: 0;
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
		    right: 0px; 
		}
	</style>
	<style>
		.switch {
		  position: relative;
		  display: inline-block;
		  width: 49px;
    	  height: 23px;
		}

		.switch input { 
		  opacity: 0;
		  width: 0;
		  height: 0;
		}

		.slider {
		  position: absolute;
		  cursor: pointer;
		  top: 0;
		  left: 0;
		  right: 0;
		  bottom: 0;
		  background-color: #ccc;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		.slider:before {
		  position: absolute;
		  content: "";
		  height: 14px;
		  width: 14px;
		  left: 4px;
		  bottom: 4px;
		  background-color: white;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		input:checked + .slider {
		  background-color: #2196F3;
		}

		input:focus + .slider {
		  box-shadow: 0 0 1px #2196F3;
		}

		input:checked + .slider:before {
		  -webkit-transform: translateX(26px);
		  -ms-transform: translateX(26px);
		  transform: translateX(26px);
		}

		/* Rounded sliders */
		.slider.round {
		  border-radius: 34px;
		}

		.slider.round:before {
		  border-radius: 50%;
		}
		.onoffswitch {
		    position: relative; width: 90px;
		    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox {
		    display: none;
		}
		.onoffswitch-label {
		    display: block; overflow: hidden; cursor: pointer;
		    border: 2px solid #999999; border-radius: 20px;
		}
		.onoffswitch-inner {
		    display: block; width: 200%; margin-left: -100%;
		    transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner:before, .onoffswitch-inner:after {
		    display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
		    font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
		    box-sizing: border-box;
		}
		.onoffswitch-inner:before {
		    content: "ON";
		    padding-left: 10px;
		    background-color: #34A7C1; color: #FFFFFF;
		}
		.onoffswitch-inner:after {
		    content: "OFF";
		    padding-right: 10px;
		    background-color: #EEEEEE; color: #999999;
		    text-align: right;
		}
		.onoffswitch-switch {
		    display: block; width: 18px; margin: 6px;
		    background: #FFFFFF;
		    position: absolute; top: 0; bottom: 0;
		    right: 56px;
		    border: 2px solid #999999; border-radius: 20px;
		    transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
		    margin-left: 0;
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
		    right: 0px; 
		}
	</style>
	<style>
		.modal.left .modal-dialog,
		.modal.right .modal-dialog {
			position: fixed;
			margin: auto;
			width: 320px;
			height: 100%;
			-webkit-transform: translate3d(0%, 0, 0);
			    -ms-transform: translate3d(0%, 0, 0);
			     -o-transform: translate3d(0%, 0, 0);
			        transform: translate3d(0%, 0, 0);
		}

		.modal.left .modal-content,
		.modal.right .modal-content {
			height: 100%;
			overflow-y: auto;
		}
		
		.modal.left .modal-body,
		.modal.right .modal-body {
			padding: 15px 15px 80px;
		}

		.modal.right.fade .modal-dialog {
			right: -320px;
			-webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
			   -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
			     -o-transition: opacity 0.3s linear, right 0.3s ease-out;
			        transition: opacity 0.3s linear, right 0.3s ease-out;
		}
		
		.modal.right.fade.in .modal-dialog {
			right: 0;
		}
	</style>
	<style>
		#ck-button {
		    margin:4px;
		    background-color:#EFEFEF;
		    border-radius:4px;
		    border:1px solid #D0D0D0;
		    height: 2em;
		    float:left;
		}

		#ck-button label {
		    float:left;
		    width:4.0em;
		}

		#ck-button label span {
		    text-align:center;
		    padding:3px 0px;
		    display:block;
		    cursor: pointer;
		}

		#ck-button label input {
		    position:absolute;
		    visibility: hidden;
		}

		#ck-button input:checked + span {
		    background-color:#911;
		    color:#fff;
		}
	</style>
	<section role="main" class="content-body">
		<header class="page-header">
			<h2 style="font-size: 15px;"><img  style="background: white;
			    border-radius: 100%;
			    width: 40px;
			    height: 40px;
			    margin-right: 1em;
			" src="{{URL('/')}}/uploads/{{$hotels->img_file == '' ? 'No-Image-Available.png' : $hotels->img_file}}"/> {{strtoupper($hotels->hotel_name)}}'s DASHBOARD</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="index2.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Dashboard</span></li>
				</ol>
			</div>
		</header>

		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<div class="panel-body" style="padding-top: 0px;">
						<div class="row">
							<div class="col-md-12" style="padding: 0em;">
								<section class="panel">
									<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd; padding-bottom: 0px;">
										<div class="panel-actions">
											<!-- <a href="#" class="fa fa-caret-down"></a> -->
											<!-- <a href="#" class="fa fa-times"></a> -->
										</div>
										<div style="float: right">
											<p style="font-weight:bold; color: #000000; margin-left: 24px; margin-bottom: 0px;">{{$hotels->hotel_name}}
												</p>
											<p style="color: #000000; margin-left: 24px;">ID: {{str_pad($hotels->id, 8, "0", STR_PAD_LEFT)}}</p>
										</div>
										<h2 class="panel-title" style="margin-top: .3em;float:left; font-weight:bold; color: black">Room Rate</h2>

										<a href="{{URL('/')}}/adjust_daily_rate/{{$hotels->id}}" style="float: left;
    										margin-left: 2em;" class="btn btn-info">ADJUST DAILY RATE</a>

										<p class="panel-subtitle"></p>
										
									</header>
								</section>
								<br>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-4">
								<label>Room Type</label>
								<select onchange="fetchRoomRate();" id="room_type_id" class="form-control mySelect">
									@foreach($room_type as $result)
									<option value="{{$result->id}}">{{$result->room_name}}</option>
									@endforeach
									@if(count($room_type) == 0)
									<option value="">No Room Type Found</option>
									@endif
								</select>
							</div>
							<div class="col-md-4" style="margin-bottom: 1em;">
								<!-- <label>Date Range</label>
								<input type="text" value="{{$sDate}}" name="sDate" class="form-control datetimepicker" id="monthPicker" autocomplete="off" required="required">
								<i style="font-size: 1.5em; float: right;margin-top: -1.25em; margin-right: .5em;" class="fa fa-calendar"></i> -->
							</div>
							<div class="col-md-4" style="margin-bottom: 1em;">
								<!-- <label>&nbsp;</label>
								<input type="text" value="{{$eDate}}" name="eDate" class="form-control datetimepicker" id="monthPicker" autocomplete="off" required="required">
								<i style="font-size: 1.5em; float: right;margin-top: -1.25em; margin-right: .5em;" class="fa fa-calendar"></i> -->
							</div>
						</div>
						<hr>
						<div id="content" class="row">

						</div>
					</div>
				</section>
			</div>
		</div>
	</section>	
@endsection

@section('page-script')
<script>
$(function(){
	$('.mySelect').select2();
	$('#roomManagementMenu').addClass('nav-expanded');
	$('#roomManagementMenu').addClass('nav-active');
	$('#roomRateMainMenu').addClass('nav-expanded');
	$('#roomRateMainMenu').addClass('nav-active');

	@if(count($room_type) != 0)
		fetchRoomRate();
	@endif
})

$('.datetimepicker').datetimepicker({
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	// minDate:'-1970/01/02', // yesterday is minimum date
	// maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});

function fetchRoomRate(){
	room_type_id = $('#room_type_id').val();
	$.ajax({
		url: "{{URL('/')}}/createRoomRate",
		type: "POST",
		data: {
			_token: "{{ csrf_token() }}",
			"room_type_id": room_type_id,
			"hotel_id": "{{$hotels->id}}"
		},
		success: function(data){
			$("#content").html(data);
		}        
   });
}
</script>
@endsection