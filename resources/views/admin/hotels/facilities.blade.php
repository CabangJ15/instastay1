@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')
    
@endsection

@section('content')
	<!-- start: sidebar -->
	{!! $menu !!}
	<!-- end: sidebar -->
	<style>
		.unselectFacilities{
			background: #fdfdfd; border: 0.5px solid #D3D3D3; padding: .5em;margin: .5em;
		}
		.selectFacilities{
			background: #DEF5F8; border: 0.5px solid transparent; padding: .5em; margin: .5em;
		}

		img.output:hover {
		cursor: pointer;
		}

		img.output1:hover {
		cursor: pointer;
		}

		label span input {
			z-index: 999;
			line-height: 0;
			font-size: 50px;
			position: absolute;
			top: 0px;
			left: 0px;
			opacity: 0;
			filter: alpha(opacity = 0);
			-ms-filter: "alpha(opacity=0)";
			cursor: pointer;
			_cursor: hand;
			margin: 0;
			padding:0;
		}

		#pac-input {
	        background-color: #fff;
	        font-family: Roboto;
	        font-size: 15px;
	        font-weight: 300;
	        margin-left: 12px;
	        padding: 0 11px 0 13px;
	        text-overflow: ellipsis;
	        width: 100%;
	        max-width: 300px;
	      }

	      #pac-input:focus {
	        border-color: #4d90fe;
	      }

	</style>
	<style>
	#filtersubmit {
	    position: relative;
	    z-index: 1;
	    left: 13px;
	    top: -25px;
	    color: #7B7B7B;
	    cursor:pointer;
	    width: 0;
	}
	#filter {
	    padding-left: 30px;
	}
	</style>
	<style type="text/css">
		.upload-img-wrapper div.item{
		    float:left;
		    margin-right: 5px;
		    margin-bottom: 5px;
		}

		.upload-img-wrapper div img{
		    width: 200px;
		    height:200px;
		}

		.upload-img-wrapper div.upload div{
		    width: 200px;
		    height: 200px;
		    padding: 60px;
		    text-align: center;
		    cursor: pointer;
		}

		.upload-img-wrapper div.upload div i{
		    font-size: 35px;
		    margin-top:15px;
		    
		    color: #4E4E4E;
		    display: block;
		}

		.upload-img-wrapper div.upload div:hover i{
		    color:#333;
		}
		</style>
		<style>
		.switch {
		  position: relative;
		  display: inline-block;
		  width: 49px;
    	  height: 23px;
		}

		.switch input { 
		  opacity: 0;
		  width: 0;
		  height: 0;
		}

		.slider {
		  position: absolute;
		  cursor: pointer;
		  top: 0;
		  left: 0;
		  right: 0;
		  bottom: 0;
		  background-color: #ccc;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		.slider:before {
		  position: absolute;
		  content: "";
		  height: 14px;
		  width: 14px;
		  left: 4px;
		  bottom: 4px;
		  background-color: white;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		input:checked + .slider {
		  background-color: #2196F3;
		}

		input:focus + .slider {
		  box-shadow: 0 0 1px #2196F3;
		}

		input:checked + .slider:before {
		  -webkit-transform: translateX(26px);
		  -ms-transform: translateX(26px);
		  transform: translateX(26px);
		}

		/* Rounded sliders */
		.slider.round {
		  border-radius: 34px;
		}

		.slider.round:before {
		  border-radius: 50%;
		}
		.onoffswitch {
		    position: relative; width: 90px;
		    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox {
		    display: none;
		}
		.onoffswitch-label {
		    display: block; overflow: hidden; cursor: pointer;
		    border: 2px solid #999999; border-radius: 20px;
		}
		.onoffswitch-inner {
		    display: block; width: 200%; margin-left: -100%;
		    transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner:before, .onoffswitch-inner:after {
		    display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
		    font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
		    box-sizing: border-box;
		}
		.onoffswitch-inner:before {
		    content: "ON";
		    padding-left: 10px;
		    background-color: #34A7C1; color: #FFFFFF;
		}
		.onoffswitch-inner:after {
		    content: "OFF";
		    padding-right: 10px;
		    background-color: #EEEEEE; color: #999999;
		    text-align: right;
		}
		.onoffswitch-switch {
		    display: block; width: 18px; margin: 6px;
		    background: #FFFFFF;
		    position: absolute; top: 0; bottom: 0;
		    right: 56px;
		    border: 2px solid #999999; border-radius: 20px;
		    transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
		    margin-left: 0;
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
		    right: 0px; 
		}
	</style>

	<section role="main" class="content-body">
		<header class="page-header">
			<h2 style="font-size: 15px;"><img  style="background: white;
			    border-radius: 100%;
			    width: 40px;
			    height: 40px;
			    margin-right: 1em;
			" src="{{URL('/')}}/uploads/{{$hotels->img_file == '' ? 'No-Image-Available.png' : $hotels->img_file}}"/> {{strtoupper($hotels->hotel_name)}}'s DASHBOARD</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="index2.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Dashboard</span></li>
				</ol>
			</div>
		</header>

		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<div class="panel-body" style="padding-top: 0px;">
						<div class="row">
							<div class="col-md-12">
								<section class="panel" style="padding: 3em;">
									<div class="row">
										<div class="col-md-3">
											<div class="row">
											@foreach($facilities_ref as $result)
												@if(count($result->facilities) != 0)
													@if(isset($ref[$result->id]))
														<?php $count = $ref[$result->id]; ?>
													@else
														<?php $count = 0; ?>
													@endif
												<div id='facilities{{$result->id}}' ><a id='facilitiesButton{{$result->id}}' onclick='showDiv({{$result->id}})' style='text-align: left; width: 100%; margin-bottom: .5em' class='btn btn-info btn-flat btn-pri facilitiesButton'>{{$result->facilities_ref}}<span style="float: right;margin-top: .3em;"><span id="facilitiesValue{{$result->id}}">{{$count}}</span>/ {{count($result->facilities)}}</span></a><input type='hidden' value='{{$result->id}}' name='facilities_list[]'/></div>
												@endif
											@endforeach
											</div>
										</div>
										<div class="col-sm-9" id="facilitiesHolder">
											@foreach($facilities_ref as $result)
												@if(count($result->facilities) != 0)
													<div id="facilitiesContentDiv{{$result->id}}" class="facilitiesDiv">
														<div class="row">
															@foreach($result->facilities as $result1)
															<div class="col-md-6">
																<div id="facilitiesDivHolder{{$result1->id}}" class="form-group {{count($result1->hotel->where('hotel_id', $hotels->id)) == 0 ? 'unselectFacilities' : 'selectFacilities'}}" style="">
																	<label for="focusedinput" class="col-sm-9 control-label">{{$result1->facilities}}</label>
																	<div class="col-md-3">
																		<div id="ck-button">
																			<label class="switch"><input id="facilities{{$result1->id}}" type="hidden" name="facilities[{{$result1->id}}]" value="{{count($result1->hotel->where('hotel_id', $hotels->id))}}"><input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value; selectMe({{$result1->id}},{{$hotels->id}},{{$result->id}})"
																				 {{count($result1->hotel->where('hotel_id', $hotels->id)) == 0 ? '' : 'checked'}}
																				><span class="slider round"></span></label>
																		</div>
																	</div>
																</div>
															</div>
															@endforeach
														</div>
													</div>
												@endif
											@endforeach
										</div>
									</div>
								</section>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
	
@endsection

@section('page-script')
<script>
	$(function(){

		$('#catalogMenu').addClass('nav-expanded');
		$('#catalogMenu').addClass('nav-active');
		$('#facilitiesMainMenu').addClass('nav-expanded');
		$('#facilitiesMainMenu').addClass('nav-active');
	});

$(function(){
	<?php $x = 0; ?>
	@foreach($facilities_ref as $result)
		@if($x == 0 && count($result->facilities) != 0)
		showDiv({{$result->id}})
		<?php $x++; ?>
		@endif
	@endforeach;
});
function showDiv(id){
	// alert(id);
	var divsToHide = document.getElementsByClassName("facilitiesDiv");
		console.log(divsToHide);
	    for(var i = 0; i < divsToHide.length; i++){
	        divsToHide[i].style.display = "none";
	    }

	$("#facilitiesContentDiv"+id).show();
	$(".facilitiesButton").removeClass('active');
	$("#facilitiesButton"+id).addClass('active');
}

function selectMe(id, hotel_id, facilities_ref){
	$("#facilitiesDivHolder"+id).toggleClass('selectFacilities unselectFacilities');
	var facilities = $("#facilities"+id).val();

	$.ajax({
		url: "{{URL('/')}}/facilities_change/{{$hotels->id}}",
		type: "POST",
		data: {
			_token: "{{ csrf_token() }}",
			"id": id,
			"data": facilities,
			"facilities_ref": facilities_ref
		},
		success: function(data){
			 	$("#facilitiesValue"+facilities_ref).html(data);

		}        
   });
}
</script>
@endsection