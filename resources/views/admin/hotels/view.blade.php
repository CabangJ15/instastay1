@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')
    
@endsection

@section('content')
	<style>
		@media (max-width: 568px){
		  .seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1  {
		    width: 14%;
		    float: left;
		    margin: .1em;
		  }

		  .myCalendar{
				height: 88px !important;
				border: solid 1px #707070;
			}

			.myCalendarText{
				color: white;
	    		font-size: 2em !important;
	    		font-weight:bold;
			}

			.myCalendarTag{
				width: 180% !important;
			}

			p.calendarFont {
			    padding-left: 1em;
			    padding-top: .5em;
			    color: #707070FA;
			    font-weight: bold;
			    font-size: 15px;

			}

			.calendarModal {
			    width: 220% !important;
			   	left: 0px;
			}
		}
		@media (min-width: 667px){
		  .seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1  {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }
		}
		@media (min-width: 768px){
		  .seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1  {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }
		}

		@media (min-width: 992px) {
		  .seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1 {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }
		}

		/**
		 *  The following is not really needed in this case
		 *  Only to demonstrate the usage of @media for large screens
		 */    
		@media (min-width: 1040px) {
		  .seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1 {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }
		}

		@media (min-width: 1200px) {
		  .seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1 {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }
		}

		.seven-cols .col-md-1,
		  .seven-cols .col-sm-1,
		  .seven-cols .col-lg-1 {
		    width: 14.285714285714285714285714285714%;
		    float: left;
		  }

		.myCalendar{
			height: 134px;
			cursor:pointer;
		}

		.myCalendarText{
			color: white;
    		font-size: 6em;
    		font-weight:bold;
		}

		.myCalendarTag{
			float: right;
		    padding: .1em;
		    background: #DC2747;
		    right: 0px;
		    color: white;
		    padding: 0em .5em;
		    width: 50%;
		    text-align: center;
		    margin-right: -1em;
		}
	</style>
	<!-- start: sidebar -->
	{!! $menu !!}
	<!-- end: sidebar -->

	<section role="main" class="content-body">
		<header class="page-header">
			<h2 style="font-size: 15px;"><img  style="background: white;
			    border-radius: 100%;
			    width: 40px;
			    height: 40px;
			    margin-right: 1em;
			" src="{{URL('/')}}/uploads/{{$hotels->img_file == '' ? 'No-Image-Available.png' : $hotels->img_file}}"/> {{strtoupper($hotels->hotel_name)}}'s DASHBOARD</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="index2.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Dashboard</span></li>
				</ol>
			</div>
		</header>

		<!-- start: page -->
		<div class="row">

			<div class="col-md-12 col-lg-12 col-xl-12">
				<div class="row">
					<div class="col-md-3 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<!-- <div class="widget-summary-col widget-summary-col-icon">
										<div class="summary-icon bg-primary">
											<i class="fa fa-dollar"></i>
										</div>
									</div> -->
									<div class="widget-summary-col">
										<center>
										<div class="summary">
											<h4 class="title"><b>Room Rates</b></h4>
											<div class="info">
												<span class="text-primary" style="color: black !important">Manage your daily rates for your rooms</span>
											</div>
										</div>
										</center>
										<div class="summary-footer">
											<center>
												<a href="{{URL('/')}}/room_rate/{{$hotels->id}}" class="btn btn-info">Manage Room Rates</a>
											</center>
										</div>

									</div>
								</div>
							</div>
						</section>
					</div>
					<div class="col-md-3 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-secondary">
							<div class="panel-body">
								<div class="widget-summary">
									<!-- <div class="widget-summary-col widget-summary-col-icon">
										<div class="summary-icon bg-secondary">
											<i class="fa fa-bed"></i>
										</div>
									</div> -->
									<div class="widget-summary-col">
										<center>
											<div class="summary">
												<h4 class="title"><b>Room Availability</b></h4>
												<div class="info">
													<span class="text-primary" style="color: black !important">Manage availability of your rooms</span>
												</div>
											</div>
										</center>
										<div class="summary-footer">
											<center>
												<a href="{{URL('/')}}/inventory/{{$hotels->id}}" class="btn btn-info">Manage Room Availability</a>
											</center>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
					<div class="col-md-3 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-tertiary">
							<div class="panel-body">
								<div class="widget-summary">
									<!-- <div class="widget-summary-col widget-summary-col-icon">
										<div class="summary-icon bg-tertiary">
											<i class="fa fa-book"></i>
										</div>
									</div> -->
									<div class="widget-summary-col">
										<center>
											<div class="summary">
												<h4 class="title"><b>Booking List</b></h4>
												<div class="info">
													<span class="text-primary" style="color: black !important">See all incoming bookings</span>
												</div>
											</div>
										</center>
										<div class="summary-footer">
											<center>
												<a href="{{URL('/')}}/booking/{{$hotels->id}}" class="btn btn-info">See Booking List</a>
											</center>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
					<div class="col-md-3 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-secondary" style="border-color: transparent;">
							<div class="panel-body" style="padding: 0px;">
								<div class="widget-summary">
									<!-- <div class="widget-summary-col widget-summary-col-icon">
										<div class="summary-icon bg-secondary">
											<i class="fa fa-bed"></i>
										</div>
									</div> -->
									<div class="widget-summary-col" style="background: #FFEED6;">
										<center>
											<div class="summary">
												<h4 class="title" style="margin-top: .8em; color: #FF9500"><b>Attention</b></h4>
												<div class="info">
													<span class="text-primary" style="color: black !important">3 Suite Rooms Left in your Room Inventory</span>
												</div>
											</div>
										</center>
										<div class="summary-footer" style="background: #ecedf0;padding-top: 0px;">
												<button style="border-color: transparent;width: 50%; border-radius: 0px 0px 0px 20px;line-height: 3em;background: #FFF8EF;
    											color: #0000003D;" href="{{URL('/')}}/inventory/{{$hotels->id}}" class="btn btn-info">Later</button>
												<button style="width: 50%;border-color: transparent; border-radius: 0px 0px 20px 0px; float: right;line-height: 3em; background: #FF9500" href="{{URL('/')}}/inventory/{{$hotels->id}}" class="btn btn-info">Manage</button>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-7">
				<section class="panel">
					<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd;    padding-bottom: 0px;">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<!-- <a href="#" class="fa fa-times"></a> -->
						</div>

						<h2 class="panel-title" style="font-weight:bold; color: black">Today's Activity</h2>
						<p class="panel-subtitle"></p>
					</header>
					<div class="panel-body" style="padding-top: 0px;">
						<p style="font-weight:bold; color: #0DC3D8; margin-left: 24px;">New Bookings</p>
						<table style="width:100%" class="table" id="table_id">
							<thead>
								<tr>
									<th>Name</th>
									<th>Date of Check-in</th>
									<th>Room Type</th>
									<th>Time</th>
									<th>Payment Type</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Anna Lagro</td>
									<td>Sept 19, 2019</td>
									<td>2:00 PM</td>
									<td>(1) Standard</td>
									<td>Pay at Hotel</td>
								</tr>
								<tr>
									<td>Byron Yap</td>
									<td>Sept 19, 2019</td>
									<td>2:00 PM</td>
									<td>(1) Standard</td>
									<td>Pay at Hotel</td>
								</tr>
								<tr>
									<td>Ivana Alawi</td>
									<td>Sept 19, 2019</td>
									<td>2:00 PM</td>
									<td>(1) Standard</td>
									<td>Pay at Hotel</td>
								</tr>
							</tbody>
						</table>
						<!-- Flot: Basic -->
						<br>
						<div class="summary-footer" style="float:right">
								<a class="btn btn-info">SEE ALL BOOKINGS</a>
						</div>

						<hr style="margin-top: 4em;">
						<p style="font-weight:bold; color: #0DC3D8; margin-left: 24px;">Arrivals</p>
						<table style="width:100%" class="table" id="table_id">
							<thead>
								<tr>
									<th>Name</th>
									<th>Date of Check-in</th>
									<th>Room Type</th>
									<th>Time</th>
									<th>Payment Type</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Chris Scott</td>
									<td>Sept 19, 2019</td>
									<td>2:00 PM</td>
									<td>(1) Standard</td>
									<td>Credit Card</td>
								</tr>
								<tr>
									<td>Jane Santillan</td>
									<td>Sept 19, 2019</td>
									<td>2:00 PM</td>
									<td>(1) Standard</td>
									<td>Pay at Hotel</td>
								</tr>
								<tr>
									<td>Mike Jordan</td>
									<td>Sept 19, 2019</td>
									<td>2:00 PM</td>
									<td>(1) Standard</td>
									<td>Pay at Hotel</td>
								</tr>
							</tbody>
						</table>
						<!-- Flot: Basic -->
						<br>
						<div class="summary-footer" style="float:right">
								<a class="btn btn-info">SEE ALL ARRIVALS</a>
						</div>

						<hr style="margin-top: 4em;">
						<p style="font-weight:bold; color: #0DC3D8; margin-left: 24px;">Cancellation</p>
						<table style="width:100%" class="table" id="table_id">
							<thead>
								<tr>
									<th>Name</th>
									<th>Date of Check-in</th>
									<th>Room Type</th>
									<th>Time</th>
									<th>Payment Type</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Audrey Llamar</td>
									<td>Sept 19, 2019</td>
									<td>2:00 PM</td>
									<td>(1) Standard</td>
									<td>Credit Card</td>
								</tr>
								<tr>
									<td>Jake Gomez</td>
									<td>Sept 19, 2019</td>
									<td>2:00 PM</td>
									<td>(1) Standard</td>
									<td>Pay at Hotel</td>
								</tr>
							</tbody>
						</table>
						<!-- Flot: Basic -->
						<br>
						<div class="summary-footer" style="float:right">
								<a class="btn btn-info">SEE ALL ARRIVALS</a>
						</div>

					</div>
				</section>
			</div>
			<div class="col-md-5">
				<section class="panel">
					<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd;padding-bottom: 0px;">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<!-- <a href="#" class="fa fa-times"></a> -->
						</div>

						<h2 class="panel-title" style="font-weight:bold; color: black">Recent Hotel Reviews</h2>
						<p class="panel-subtitle"></p>
					</header>
					<div class="panel-body">
						<div class="well" style="background: white">
							<div class="row">
								<div class="col-md-6">
									<p style="color: #121212">Anika G.<br><span style="color: #D3D3D3">03-Sept-2019</span></p>
									<p></p>
								</div>
								<div class="col-md-6">
									<div style="float: right;">
										<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
										<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
										<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
										<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
										<i class="fa fa-hand-o-up" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-md-12">
									<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
								</div>
								<div class="summary-footer">
									<center><a class="btn btn-info">REPLY</a></center>
								</div>
							</div>
						</div>
						<div class="well" style="background: #EF9B9B">
							<div class="row">
								<div class="col-md-6">
									<p style="color: #121212">Johann L.<br><span style="color: #121212">03-Sept-2019</span></p>
									<p></p>
								</div>
								<div class="col-md-6">
									<div style="float: right;">
										<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
										<i class="fa fa-hand-o-up" aria-hidden="true"></i>
										<i class="fa fa-hand-o-up" aria-hidden="true"></i>
										<i class="fa fa-hand-o-up" aria-hidden="true"></i>
										<i class="fa fa-hand-o-up" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-md-12">
									<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
								</div>
								<div class="summary-footer">
									<center><a class="btn btn-info">REPLY</a></center>
								</div>
							</div>
						</div>
						<div class="well" style="background: white">
							<div class="row">
								<div class="col-md-6">
									<p style="color: #121212">Julyeth C.<br><span style="color: #D3D3D3">03-Sept-2019</span></p>
									<p></p>
								</div>
								<div class="col-md-6">
									<div style="float: right;">
										<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
										<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
										<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
										<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
										<i class="fa fa-hand-o-up yellow" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-md-12">
									<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
								</div>
								<div class="summary-footer">
									<center><a class="btn btn-info">REPLY</a></center>
								</div>
							</div>
						</div>
						<div class="summary-footer" style="float: right">
							<a>SEE MORE</a>
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd; padding-bottom: 0px;">
						<div class="panel-actions">
							<!-- <a href="#" class="fa fa-caret-down"></a> -->
							<!-- <a href="#" class="fa fa-times"></a> -->
						</div>
						<p class="panel-subtitle"></p>
					</header>
					<div class="panel-body" style="padding-top: 0px;">
						<div style="float: right">
							<p style="font-weight:bold; color: #0DC3D8; margin-left: 24px;">Victoria Court Balintawak
								</p>
							<p style="color: #0DC3D8; margin-left: 24px;">ID: 35562134</p>
						</div>
						<br>
						<br>
						<br>
						<section class="panel">
							<div class="container" style="width: 100%;">
							  <div class="row seven-cols" style="box-shadow: 0 0 0 1px #c0b8b8; background: #D3D3D3 0% 0% no-repeat padding-box;">
							    <div class="col-md-1"><h5 style="font-weight: bold;    text-align: center;">MON</h5></div>
							    <div class="col-md-1"><h5 style="font-weight: bold;    text-align: center;">TUE</h5></div>
							    <div class="col-md-1"><h5 style="font-weight: bold;    text-align: center;">WED</h5></div>
							    <div class="col-md-1"><h5 style="font-weight: bold;    text-align: center;">THU</h5></div>
							    <div class="col-md-1"><h5 style="font-weight: bold;    text-align: center;">FRI</h5></div>
							    <div class="col-md-1"><h5 style="font-weight: bold;    text-align: center;">SAT</h5></div>
							    <div class="col-md-1"><h5 style="font-weight: bold;    text-align: center;">SUN</h5></div>
							    
							  </div>
							  <div class="row seven-cols">
							  	<div id="calendarView">
								  	{!! $calendar !!}
								</div>
							  </div>
							</div>
						</section>
					</div>
				</section>
			</div>
		</div>
	</section>
	<style>

		.dt2019-12-25 { background-color: #F0FFF0; }
		.dt2019-12-25 .monthly-day-number:after { content: '\1F384'; }
		.fa-hand-o-up{
			  transform: rotate(-40deg)  scale(-1, 1);
		}
		.yellow{
			  color: #e0e024;
		}
	</style>
	
@endsection

@section('page-script')
<script type="text/javascript">
	$(function(){
		

		

		$('#dashboardMenu').addClass('nav-active');


		$('#mycalendar').monthly({
			mode: 'event',
			//jsonUrl: 'events.json',
			//dataType: 'json'
			// xmlUrl: 'events.xml',
			stylePast: true,
			disablePast: true
		});
	});
</script>
@endsection