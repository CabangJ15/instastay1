@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')

@endsection

@section('content')
<!-- start: sidebar -->
{!! $menu !!}
<!-- end: sidebar -->
<style>
.form-group{
width: 100%;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
-webkit-appearance: none; 
margin: 0; 
}
</style>
<style>
.switch {
position: relative;
display: inline-block;
width: 49px;
height: 23px;
}

.switch input { 
opacity: 0;
width: 0;
height: 0;
}

.slider {
position: absolute;
cursor: pointer;
top: 0;
left: 0;
right: 0;
bottom: 0;
background-color: #ccc;
-webkit-transition: .4s;
transition: .4s;
}

.slider:before {
position: absolute;
content: "";
height: 14px;
width: 14px;
left: 4px;
bottom: 4px;
background-color: white;
-webkit-transition: .4s;
transition: .4s;
}

input:checked + .slider {
background-color: #2196F3;
}

input:focus + .slider {
box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
-webkit-transform: translateX(26px);
-ms-transform: translateX(26px);
transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
border-radius: 34px;
}

.slider.round:before {
border-radius: 50%;
}
.onoffswitch {
position: relative; width: 90px;
-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
}
.onoffswitch-checkbox {
display: none;
}
.onoffswitch-label {
display: block; overflow: hidden; cursor: pointer;
border: 2px solid #999999; border-radius: 20px;
}
.onoffswitch-inner {
display: block; width: 200%; margin-left: -100%;
transition: margin 0.3s ease-in 0s;
}
.onoffswitch-inner:before, .onoffswitch-inner:after {
display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
box-sizing: border-box;
}
.onoffswitch-inner:before {
content: "ON";
padding-left: 10px;
background-color: #34A7C1; color: #FFFFFF;
}
.onoffswitch-inner:after {
content: "OFF";
padding-right: 10px;
background-color: #EEEEEE; color: #999999;
text-align: right;
}
.onoffswitch-switch {
display: block; width: 18px; margin: 6px;
background: #FFFFFF;
position: absolute; top: 0; bottom: 0;
right: 56px;
border: 2px solid #999999; border-radius: 20px;
transition: all 0.3s ease-in 0s; 
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
right: 0px; 
}
</style>
<style>
img.output:hover {
cursor: pointer;
}

img.output1:hover {
cursor: pointer;
}

label span input {
z-index: 999;
line-height: 0;
font-size: 50px;
position: absolute;
top: 0px;
left: 0px;
opacity: 0;
filter: alpha(opacity = 0);
-ms-filter: "alpha(opacity=0)";
cursor: pointer;
_cursor: hand;
margin: 0;
padding:0;
}

#pac-input {
background-color: #fff;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 100%;
max-width: 300px;
}

#pac-input:focus {
border-color: #4d90fe;
}

</style>
<style>
#filtersubmit {
position: relative;
z-index: 1;
left: 13px;
top: -25px;
color: #7B7B7B;
cursor:pointer;
width: 0;
}
#filter {
padding-left: 30px;
}
</style>
<style>
td.details-control {
background-image: url('https://datatables.net/examples/resources/details_open.png') !important;
background-position: center !important;
background-repeat: no-repeat !important;
background-color: #DEF5F8 !important;
cursor: pointer;
}
tr.shown td.details-control {
background-image: url('https://datatables.net/examples/resources/details_close.png') !important;
background-position: center !important;
background-repeat: no-repeat !important;
background-color: #DEF5F8 !important;
}
</style>
<style>

.dataTables_filter {
display: none;
}

.sorting, .sorting_asc, .sorting_desc {
background : none;
}

table.dataTable thead{
display: none;
}

table.dataTable thead .sorting, 
table.dataTable thead .sorting_asc, 
table.dataTable thead .sorting_desc {
background : none;
}

.table-striped > tbody > tr:nth-child(odd),
.table-striped > tbody > tr:nth-child(even) {
background-color: #ffffff;
}
</style>
<style type="text/css">
.upload-img-wrapper div.item{
float:left;
margin-right: 5px;
margin-bottom: 5px;
}

.upload-img-wrapper div img{
width: 100px;
height:100px;
}

.upload-img-wrapper div.upload div{
width: 100px;
height:100px;
padding:10px;
text-align: center;
cursor: pointer;
}

.upload-img-wrapper div.upload div i{
font-size: 35px;
margin-top:15px;

color: #4E4E4E;
display: block;
}

.upload-img-wrapper div.upload div:hover i{
color:#333;
}
</style>
<style>
.table-striped > tbody > tr:nth-child(odd) {
background: #DEF5F8;
border-radius: 25px;
}
.table-striped > tbody > tr:nth-child(even) > td {
background: #DEF5F8;
padding: 0px;
}
</style>

<section role="main" class="content-body">
<header class="page-header">
<h2 style="font-size: 15px;"><img  style="background: white;
border-radius: 100%;
width: 40px;
height: 40px;
margin-right: 1em;
" src="{{URL('/')}}/uploads/{{$hotels->img_file == '' ? 'No-Image-Available.png' : $hotels->img_file}}"/> {{strtoupper($hotels->hotel_name)}}'s DASHBOARD</h2>

<div class="right-wrapper pull-right" style="margin-right: 3em;">
<ol class="breadcrumbs">
<li>
<a href="index2.html">
<i class="fa fa-home"></i>
</a>
</li>
<li><span>Dashboard</span></li>
</ol>
</div>
</header>

<div class="row">
<div class="col-md-12">
<section class="panel">
<div class="panel-body" style="padding-top: 0px;">
<div class="row">
<div class="col-md-12">
<section class="panel" style="padding: 3em;">
<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd;    padding-bottom: 0px;">
<div class="panel-actions">
<!-- <a href="#" class="fa fa-caret-up"></a> -->
<!-- <a href="#" class="fa fa-times"></a> -->
</div>
<div style="float: right">
<p style="font-weight:bold; color: #0DC3D8; margin-left: 24px; margin-bottom: 0px;">{{$hotels->hotel_name}}
</p>
<p style="color: #0DC3D8; margin-left: 24px;">ID: {{str_pad($hotels->id, 8, "0", STR_PAD_LEFT)}}</p>
</div>
<h2 class="panel-title" style="font-weight:bold; color: black">Room Type</h2>
<p class="panel-subtitle"></p>


</header>
<hr>
<div class="row">
<div class="col-md-2">
<section class="panel" style="padding: 1em;">
<h2 class="panel-title" style="font-weight:bold; font-size: 16px">Room Setup</h2>
</section>
</div>
<div class="col-md-4">
<section class="panel" style="padding: 1em;">
<h2 class="panel-title" style="font-weight:bold; font-size: 16px">Total Room Type: <span style="font-weight: normal">{{count($rooms)}}</span></h2>
</section>
</div>
<div class="col-md-6">
<a href="{{URL('/')}}/room_type/{{$hotels->id}}/create" style="float: right; margin-left: .5em;" class="btn btn-success">Add New Room</a>
</div>
</div>
<div class="row">
<form enctype="multipart/form-data" method="POST" action="{{url('/')}}/room_type/{{$hotels->id}}/all">  
@csrf
<?php $room_type_list = ""; ?>
@foreach($rooms as $room)
<?php $room_type_list .= $room->id.","?>
@endforeach

<?php $room_type_list = substr($room_type_list, 0, -1); ?>
<input type="hidden" name="room_type_list" value="{{$room_type_list}}"/>
<table style="width: 100%; border: 1px solid gray;" id="allReview" class="table table-striped mb-none no-footer dataTable">
<thead>
<tr>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
@foreach($rooms as $room)
<?php $label = $room->flag == 1 ? "checked" : ""; ?>
<tr>
<td style="vertical-align: middle"><center><i id="icon{{$room->id}}" style="cursor: pointer" onclick="showContent({{$room->id}})" class="fa fa-caret-up"></i></center></td>
<td style="vertical-align: middle"><b>{{$room->id}}</b></td>
<td style="vertical-align: middle"><b>{{$room->room_name}}</b></td>
<td style="vertical-align: middle"><b>Photos:</b> {{count($room->images)}}</td>
<td style="vertical-align: middle"><b>Person:</b> {{$room->guest}}</td>
<td style="text-align: right; vertical-align: middle"><label style="font-weight: bold;" for="focusedinput">Order </label></td>
<td style="vertical-align: middle">
<div class="form-group">
<div  style="position: relative">
<input type="number" name="sort[{{$room->id}}]" class="form-control input-number" value="{{$room->sort}}" min="0" max="100">

<span class="input-group-btn" style="position: absolute;
top: 18%;
right: 44.2%;">
<button style="border-radius: 8px 0px 0px 8px;
font-size: 12px;
padding: 4px;
border: 0px transparent;
line-height: 0px; 
/*margin-left: -250%; */
z-index: 2;
width: 28px; height: 19px;
background: #468DF0 0% 0% no-repeat padding-box;
box-shadow: 0px 2px 5px #00000033;
border-radius: 15px 0px 0px 15px;
opacity: 1;
" type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="sort[{{$room->id}}]">
<span class="fa fa-minus"></span>
</button>
</span>
<span class="input-group-btn" style="position: absolute;
top: 18%;
right: 21.1%;">
<button style="border-radius: 0px 8px 8px 0px;
font-size: 12px;
padding: 4px;
border: 0px transparent;
line-height: 0px; 
/*margin-left: -250%;*/ 
z-index: 2;
width: 28px; height: 19px;
background: #468DF0 0% 0% no-repeat padding-box;
box-shadow: 0px 2px 5px #00000033;
border-radius: 0px 15px 15px 0px;
opacity: 1;
" type="button" class="btn btn-primary btn-number" data-type="plus" data-field="sort[{{$room->id}}]">
<span class="fa fa-plus"></span>
</button>
</span>
</div>
</div>
</td>
<td style="vertical-align: middle">
<div id="ck-button">
<label class="switch"><input type="hidden" name="flag[{{$room->id}}]" value="{{$room->flag}}"><input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" {{$label}}>
<span class="slider round"></span>
</label>
</div>

</td>
</tr>
<tr>
<td colspan="8">
<div id="content{{$room->id}}" class="well" style="background: white; margin-bottom: 0px; padding: 0px 14px 14px 14px;">
<div class="row">
<div class="col-md-4">
<div class="row">
<div class="col-md-12" style="background: #D3D3D3;
color: black;
padding: .3em;
font-weight: bold;margin-bottom: 1em;">
<center>Basic Info</center>
</div>
</div>
<div class="form-group" style="margin-bottom: 15px;">
<label for="focusedinput">Room Name</label><br>
<input style="width: 100%;" type="text" name="room_name[{{$room->id}}]" value="{{$room->room_name}}" class="form-control" id="focusedinput" placeholder="Room Name" required>
</div><br>
<div class="form-group" style="margin-bottom: 15px;">
<label for="focusedinput">Internal Room Name (Optional, Not Visible to Customer)</label><br>
<input style="width: 100%;" type="text" name="description[{{$room->id}}]" value="{{$room->description}}" class="form-control" id="focusedinput" placeholder="Internal Room Name (Optional, Not Visible to Customer)">
</div><br>
<div class="form-group" style="margin-bottom: 15px;">
<label for="focusedinput">External Code ID:</label><br>
<input style="width: 100%;" type="text" name="external_code_id" value="{{$room->id}}" class="form-control" id="focusedinput" placeholder="External Code ID" disabled>
</div><br><br>
<div class="row">
<div class="col-md-6">
	<div class="form-group" style="margin-bottom: 15px;">
		<label for="focusedinput">Standard Rate*</label>
		<input style="width: 100%;" type="number" step="any" name="standard_rate[{{$room->id}}]" value="0" class="form-control" id="focusedinput" placeholder="Standard Rate" required>
	</div>
</div>
<div class="col-md-6">
	<div class="form-group" style="margin-bottom: 15px;">
		<label for="focusedinput">Rate Protection*</label>
		<input style="width: 100%;" type="number" step="any" name="rate_protection[{{$room->id}}]" value="0" class="form-control" id="focusedinput" placeholder="Rate Protection" required>
	</div>
</div>
</div>
<div class="clearfix"></div>
<div class="form-group" style="margin-bottom: 15px;">
<label for="focusedinput">Cancellation Policy</label><br>
<select  style="width: 100%;" name="cancelation_id[{{$room->id}}]" class="form-control mySelect" required="required">
	<option value="test">Cancel 45 Days Upon Arrival 50%</option>
</select>
</div>
<div class="clearfix"></div>
</div>
<div class="col-md-8">
<div class="row">
<div class="col-md-12" style="background: #D3D3D3;
color: black;
padding: .3em;
font-weight: bold;margin-bottom: 1em;">
<center>Occupancy Settings</center>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="row">
<div class="form-group">
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">Person*</label><br>
			<div  style="position: relative">
	          <input style="width: 100%;" type="number" name="person[{{$room->id}}]" class="form-control input-number" value="{{$room->guest}}" min="1" max="100">
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 44.2%;">
	              <button style="border-radius: 8px 0px 0px 8px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%; */
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 15px 0px 0px 15px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="person[{{$room->id}}]">
	                <span class="fa fa-minus"></span>
	              </button>
	          </span>
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 21.1%;">
	              <button style="border-radius: 0px 8px 8px 0px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%;*/ 
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 0px 15px 15px 0px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="person[{{$room->id}}]">
	                  <span class="fa fa-plus"></span>
	              </button>
	          </span>
	      </div>
	     </div>
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">Extra Bed*</label><br>
			<div  style="position: relative">
	          <input style="width: 100%;" type="number" name="extra_bed[{{$room->id}}]" class="form-control input-number" value="{{$room->guest}}" min="1" max="100">
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 44.2%;">
	              <button style="border-radius: 8px 0px 0px 8px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%; */
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 15px 0px 0px 15px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="extra_bed[{{$room->id}}]">
	                <span class="fa fa-minus"></span>
	              </button>
	          </span>
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 21.1%;">
	              <button style="border-radius: 0px 8px 8px 0px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%;*/ 
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 0px 15px 15px 0px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="extra_bed[{{$room->id}}]">
	                  <span class="fa fa-plus"></span>
	              </button>
	          </span>
	      </div>
	     </div>
	</div>
</div>
<div class="form-group">
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">Maximum Extra Person*</label><br>
			<div  style="position: relative">
	          <input style="width: 100%;" type="number" name="max_person[{{$room->id}}]" class="form-control input-number" value="{{$room->guest}}" min="1" max="100">
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 44.2%;">
	              <button style="border-radius: 8px 0px 0px 8px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%; */
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 15px 0px 0px 15px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="max_person[{{$room->id}}]">
	                <span class="fa fa-minus"></span>
	              </button>
	          </span>
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 21.1%;">
	              <button style="border-radius: 0px 8px 8px 0px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%;*/ 
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 0px 15px 15px 0px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="max_person[{{$room->id}}]">
	                  <span class="fa fa-plus"></span>
	              </button>
	          </span>
	      </div>
	     </div>
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">Max Extra Bed*</label><br>
			<div  style="position: relative">
	          <input style="width: 100%;" type="number" name="max_extra_bed[{{$room->id}}]" class="form-control input-number" value="{{$room->guest}}" min="1" max="100">
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 44.2%;">
	              <button style="border-radius: 8px 0px 0px 8px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%; */
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 15px 0px 0px 15px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="max_extra_bed[{{$room->id}}]">
	                <span class="fa fa-minus"></span>
	              </button>
	          </span>
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 21.1%;">
	              <button style="border-radius: 0px 8px 8px 0px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%;*/ 
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 0px 15px 15px 0px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="max_extra_bed[{{$room->id}}]">
	                  <span class="fa fa-plus"></span>
	              </button>
	          </span>
	      </div>
	     </div>
	</div>
</div>
<div class="form-group" style="border: 0.5px solid #D3D3D3; padding: 1em;margin: 0em;">
	<label for="focusedinput" class="col-sm-9 control-label">Allow Children in this room type</label>
	<div class="col-md-3">
		<div id="ck-button">
			<label class="switch"><input type="hidden" name="is_allow_children[{{$room->id}}]" value="{{$room->is_child_allowed}}"><input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" {{$room->is_child_allowed == 1 ? "checked" : ""}}><span class="slider round"></span></label>
		</div>
	</div>
</div>
<div class="form-group">
	<div class="col-md-6">
		<label for="focusedinput">&nbsp;</label><br>
		<label style="line-height: 1em;" for="focusedinput" class="col-sm-9 control-label">Children allowed for free
			<br><span style="font-size: 11px;">11 years old and under</span>
			<br><a style="font-size: 12px;">Change child policy</a>
		</label>
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">&nbsp</label><br>
			<div  style="position: relative">
	          <input style="width: 100%;" type="number" name="child_allow[{{$room->id}}]" class="form-control input-number" value="{{$room->guest}}" min="1" max="100">
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 44.2%;">
	              <button style="border-radius: 8px 0px 0px 8px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%; */
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 15px 0px 0px 15px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="child_allow[{{$room->id}}]">
	                <span class="fa fa-minus"></span>
	              </button>
	          </span>
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 21.1%;">
	              <button style="border-radius: 0px 8px 8px 0px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%;*/ 
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 0px 15px 15px 0px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="child_allow[{{$room->id}}]">
	                  <span class="fa fa-plus"></span>
	              </button>
	          </span>
	      </div>
	     </div>
	</div>
</div>
<div class="form-group">
	<div class="col-md-6">
		<label for="focusedinput">&nbsp;</label>
		<label style="margin-top: 1.25em;" for="focusedinput" class="col-sm-9 control-label">Maximum Children Allowed*
		</label>
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">&nbsp</label><br>
			<div  style="position: relative">
	          <input style="width: 100%;" type="number" name="max_child[{{$room->id}}]" class="form-control input-number" value="{{$room->guest}}" min="1" max="100">
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 44.2%;">
	              <button style="border-radius: 8px 0px 0px 8px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%; */
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 15px 0px 0px 15px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="max_child[{{$room->id}}]">
	                <span class="fa fa-minus"></span>
	              </button>
	          </span>
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 21.1%;">
	              <button style="border-radius: 0px 8px 8px 0px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%;*/ 
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 0px 15px 15px 0px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="max_child[{{$room->id}}]">
	                  <span class="fa fa-plus"></span>
	              </button>
	          </span>
	      </div>
	     </div>
	</div>
</div>
<div class="form-group" style="border: 0.5px solid #D3D3D3; padding: 1em;margin: 0em;">
	<label for="focusedinput" class="col-sm-9 control-label">Baby Cots</label>
	<div class="col-md-3">
		<div id="ck-button">
			<label class="switch"><input type="hidden" name="baby_cots[{{$room->id}}]" value="{{$room->is_baby_cots}}"><input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" {{$room->is_baby_cots == 1 ? "checked" : ""}}><span class="slider round"></span></label></div>
	</div>

</div>
</div>
</div>
<div class="col-md-6">
<div class="row">
<div class="form-group">
	<div class="col-md-12">
		<div class="upload-img-wrapper">
            <div class="img-selected-wrapper{{$room->id}}">
            	@foreach($room->images as $result1)
                <div class='item' ondblclick="removeDiv(this)">
                	<input type="hidden" name="image_id[{{$room->id}}][]" value="{{$result1->id}}"/>
                	<input type="hidden" name="image[{{$room->id}}][]" value="{{$result1->image_file}}"/>
					<img class='img-thumbnail' src="{{URL('/')}}/uploads/room/{{$result1->image_file}}">
				</div>
				@endforeach
            </div>
            <div class="upload">
                <div class="img-thumbnail" id="btn-upload{{$room->id}}" >
                    <i class="fa fa-cloud-upload"></i>
                    <span>upload</span> 
                </div>
                <input type="file" class="hidden" id="file-upload{{$room->id}}" multiple name="images" accept="image/*">
            </div>
        </div>
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">Room Size</label><br>
			<input style="width: 100%;" type="number" step="any" name="room_size[{{$room->id}}]" value="{{$room->room_size}}" class="form-control" id="focusedinput" placeholder="Standard Rate" required>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">Bathroom</label><br>
			<div  style="position: relative">
	          <input style="width: 100%;" type="number" name="bath_room[{{$room->id}}]" class="form-control input-number" value="{{$room->guest}}" min="1" max="100">
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 44.2%;">
	              <button style="border-radius: 8px 0px 0px 8px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%; */
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 15px 0px 0px 15px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="bath_room[{{$room->id}}]">
	                <span class="fa fa-minus"></span>
	              </button>
	          </span>
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 21.1%;">
	              <button style="border-radius: 0px 8px 8px 0px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%;*/ 
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 0px 15px 15px 0px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="bath_room[{{$room->id}}]">
	                  <span class="fa fa-plus"></span>
	              </button>
	          </span>
	      </div>
	     </div>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group">
		<div class="col-md-6">
			<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">Allotment Quantity Alert*</label><br>
			<div  style="position: relative">
	          <input style="width: 100%;" type="number" name="alert[{{$room->id}}]" class="form-control input-number" value="{{$room->alert}}" min="1" max="100">
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 44.2%;">
	              <button style="border-radius: 8px 0px 0px 8px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%; */
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 15px 0px 0px 15px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="alert[{{$room->id}}]">
	                <span class="fa fa-minus"></span>
	              </button>
	          </span>
	          <span class="input-group-btn" style="position: absolute;
				    top: 18%;
				     right: 21.1%;">
	              <button style="border-radius: 0px 8px 8px 0px;
				    font-size: 12px;
				    padding: 4px;
				    border: 0px transparent;
				    line-height: 0px; 
				    /*margin-left: -250%;*/ 
				    z-index: 2;
				    width: 28px; height: 19px;
				    background: #468DF0 0% 0% no-repeat padding-box;
					box-shadow: 0px 2px 5px #00000033;
					border-radius: 0px 15px 15px 0px;
					opacity: 1;
				    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="alert[{{$room->id}}]">
	                  <span class="fa fa-plus"></span>
	              </button>
	          </span>
	      </div>
	     </div>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group" style="margin-bottom: 15px;">
		<div class="col-md-12">
			<label for="focusedinput">Amenities</label><br>
			<a style="width: 100%;" type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalA{{$room->id}}">Select Amenities</a>
		</div>
	</div>
	<div id="myModalA{{$room->id}}" class="modal fade" role="dialog">
	  <div class="modal-dialog" style="width:100%; max-width:800px">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <a type="button" class="close" data-dismiss="modal">&times;</a>
	        <h4 class="modal-title">Room Amenities </h4>
	      </div>
	      <div class="modal-body">
	      	<div class="row">
	        @foreach($amenities as $result)
				
				<div class="col-md-6">
					<div class="checkbox">
						 <label for="amenities{{$result->id}}"> 
							<input id="amenities{{$result->id}}" type="checkbox" name="amenities[{{$room->id}}][{{$result->id}}]"
							@if(count($room->amenities->where('amenities_id', $result->id)) != 0)
								checked
							@endif
							> {{$result->amenities}}
						</label> 
					</div>
				</div>
				
			@endforeach

	         </div>
	      <div class="modal-footer">
	      </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<div class="row">
	<div class="form-group" style="margin-bottom: 15px;">
		<div class="col-md-12">
			<label for="focusedinput">Bedding</label><br>
			<a style="width: 100%;" type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalB{{$room->id}}">Select Bed Type</a>
			<div id="myModalB{{$room->id}}" class="modal fade" role="dialog">
			  <div class="modal-dialog" style="width:100%; max-width:800px">
			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <a type="button" class="close" data-dismiss="modal">&times;</a>
			        <h4 class="modal-title">Bed Type </h4>
			      </div>
			      <div class="modal-body">
			      	<div class="row">

			       	@foreach($bed as $result)
			       	<?php $qty = 0; ?>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6">
									<div class="checkbox">
										 <label for="bed{{$result->id}}"> 
											<input id="bed{{$result->id}}" type="checkbox" name="bed[{{$room->id}}][{{$result->id}}]"
											@foreach($room->bed->where('bed_id', $result->id) as $test)
												checked
												<?php $qty = $test->qty; ?>
											@endforeach
											> {{$result->bed}}
										</label> 
									</div>
								</div>
								<div class="col-md-6">
									<input class="form-control" type="number" name="bedQty[{{$room->id}}][{{$result->id}}]" value="{{$qty}}"/>
								</div>
							</div>
						</div>
						@endforeach

			      </div>
			      <div class="modal-footer">
			      </div>
			    </div>
			  </div>
			</div>
		</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</td>
</tr>
@endforeach
</tbody>
</table>
<div class="text-right mr-lg" style="margin-top: 2em;">

<button style="float: right; margin-left: .5em;" class="btn btn-success btn-flat btn-pri">
<i class="fa fa-floppy-o"></i> SAVE ALL
</button>

</div>
</form>
</div>
</section>
</div>
</div>
</section>
</div>
</div>
</section>
<style>

.dt2019-12-25 { background-color: #F0FFF0; }
.dt2019-12-25 .monthly-day-number:after { content: '\1F384'; }
.fa-hand-o-up{
transform: rotate(-40deg)  scale(-1, 1);
}
.yellow{
color: #e0e024;
}
</style>

@endsection

@section('page-script')
<script type="text/javascript">
$(function(){
$('#roomManagementMenu').addClass('nav-expanded');
$('#roomManagementMenu').addClass('nav-active');
$('#roomTypeMenu').addClass('nav-expanded');
$('#roomTypeMenu').addClass('nav-active');
$(".mySelect").select2();
});
</script>
<script>
$('.btn-number').click(function(e){
e.preventDefault();

fieldName = $(this).attr('data-field');
type      = $(this).attr('data-type');
var input = $("input[name='"+fieldName+"']");
var currentVal = parseInt(input.val());
if (!isNaN(currentVal)) {
if(type == 'minus') {

if(currentVal > input.attr('min')) {
input.val(currentVal - 1).change();
} 
if(parseInt(input.val()) == input.attr('min')) {
$(this).attr('disabled', true);
}

} else if(type == 'plus') {

if(currentVal < input.attr('max')) {
input.val(currentVal + 1).change();
}
if(parseInt(input.val()) == input.attr('max')) {
$(this).attr('disabled', true);
}

}
} else {
input.val(0);
}
});
$('.input-number').focusin(function(){
$(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {

minValue =  parseInt($(this).attr('min'));
maxValue =  parseInt($(this).attr('max'));
valueCurrent = parseInt($(this).val());

name = $(this).attr('name');
if(valueCurrent >= minValue) {
$(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
} else {
alert('Sorry, the minimum value was reached');
$(this).val($(this).data('oldValue'));
}
if(valueCurrent <= maxValue) {
$(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
} else {
alert('Sorry, the maximum value was reached');
$(this).val($(this).data('oldValue'));
}


});

$(".input-number").keydown(function (e) {
// Allow: backspace, delete, tab, escape, enter and .
if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
// Allow: Ctrl+A
(e.keyCode == 65 && e.ctrlKey === true) || 
// Allow: home, end, left, right
(e.keyCode >= 35 && e.keyCode <= 39)) {
// let it happen, don't do anything
return;
}
// Ensure that it is a number and stop the keypress
if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
e.preventDefault();
}
});

@foreach($rooms as $room)
$("#content{{$room->id}}").hide();
@endforeach

function showContent(id){
$("#content"+id).toggle();
$("#icon"+id).toggleClass('fa-caret-down fa-caret-up');
}

function removeDiv(test){

swal("Are you sure?", "You will not be able to recover this Photo!", {
buttons: {
cancel: "Cancel",
catch: {
text: "Yes",
value: "delete",
className: "btn-danger",
}
},
})
.then((value) => {
switch (value) {

case "delete":
// myrow.remove().draw();

test.remove();
break;

default:
swal.close();
}
});
}

@foreach($rooms as $room)
$("#btn-upload{{$room->id}}").click(function(e){ 
$("#file-upload{{$room->id}}").click();
}); 
$("#file-upload{{$room->id}}").change(function(){
if($(this).get(0).files.length){
$.each($(this).get(0).files, function(index, file) {

var reader = new FileReader(); 
reader.onload = function (e) {

var html = "<div class='item'>";
html +=      " <img class='img-thumbnail' src='"+ reader.result+"'>"; 
html +=      " <input type='hidden' name='image[{{$room->id}}][]' value='"+ e.target.result+"'>"; 
html +=      " <input type='hidden' name='image_id[{{$room->id}}][]' value='0'>"; 
html += "</div>";

$(".img-selected-wrapper{{$room->id}}").prepend(html);                        
$('#blah').attr('src', reader.result);
} 
reader.readAsDataURL(file); 
}); 
}
});
@endforeach
</script>
@endsection