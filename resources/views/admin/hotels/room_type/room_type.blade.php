@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')
    
@endsection

@section('content')
	<!-- start: sidebar -->
	{!! $menu !!}
	<!-- end: sidebar -->
<style>
	img.output:hover {
	cursor: pointer;
	}

	img.output1:hover {
	cursor: pointer;
	}

	label span input {
		z-index: 999;
		line-height: 0;
		font-size: 50px;
		position: absolute;
		top: 0px;
		left: 0px;
		opacity: 0;
		filter: alpha(opacity = 0);
		-ms-filter: "alpha(opacity=0)";
		cursor: pointer;
		_cursor: hand;
		margin: 0;
		padding:0;
	}

	#pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 100%;
        max-width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

</style>
<style>
#filtersubmit {
    position: relative;
    z-index: 1;
    left: 13px;
    top: -25px;
    color: #7B7B7B;
    cursor:pointer;
    width: 0;
}
#filter {
    padding-left: 30px;
}
</style>
<style type="text/css">
	.upload-img-wrapper div.item{
	    float:left;
	    margin-right: 5px;
	    margin-bottom: 5px;
	}

	.upload-img-wrapper div img{
	    width: 100px;
	    height:100px;
	}

	.upload-img-wrapper div.upload div{
	    width: 100px;
	    height:100px;
	    padding:10px;
	    text-align: center;
	    cursor: pointer;
	}

	.upload-img-wrapper div.upload div i{
	    font-size: 35px;
	    margin-top:15px;
	    
	    color: #4E4E4E;
	    display: block;
	}

	.upload-img-wrapper div.upload div:hover i{
	    color:#333;
	}
	</style>
	<style>

		.dataTables_filter {
		display: none;
		}

		.sorting, .sorting_asc, .sorting_desc {
		    background : none;
		}

		table.dataTable thead{
			display: none;
		}

		table.dataTable thead .sorting, 
		table.dataTable thead .sorting_asc, 
		table.dataTable thead .sorting_desc {
		    background : none;
		}

		.table-striped > tbody > tr:nth-child(odd),
		.table-striped > tbody > tr:nth-child(even) {
		    background-color: #ffffff;
		}
	</style>
	<style>
		.switch {
		  position: relative;
		  display: inline-block;
		  width: 49px;
    	  height: 23px;
		}

		.switch input { 
		  opacity: 0;
		  width: 0;
		  height: 0;
		}

		.slider {
		  position: absolute;
		  cursor: pointer;
		  top: 0;
		  left: 0;
		  right: 0;
		  bottom: 0;
		  background-color: #ccc;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		.slider:before {
		  position: absolute;
		  content: "";
		  height: 14px;
		  width: 14px;
		  left: 4px;
		  bottom: 4px;
		  background-color: white;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		input:checked + .slider {
		  background-color: #2196F3;
		}

		input:focus + .slider {
		  box-shadow: 0 0 1px #2196F3;
		}

		input:checked + .slider:before {
		  -webkit-transform: translateX(26px);
		  -ms-transform: translateX(26px);
		  transform: translateX(26px);
		}

		/* Rounded sliders */
		.slider.round {
		  border-radius: 34px;
		}

		.slider.round:before {
		  border-radius: 50%;
		}
		.onoffswitch {
		    position: relative; width: 90px;
		    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox {
		    display: none;
		}
		.onoffswitch-label {
		    display: block; overflow: hidden; cursor: pointer;
		    border: 2px solid #999999; border-radius: 20px;
		}
		.onoffswitch-inner {
		    display: block; width: 200%; margin-left: -100%;
		    transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner:before, .onoffswitch-inner:after {
		    display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
		    font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
		    box-sizing: border-box;
		}
		.onoffswitch-inner:before {
		    content: "ON";
		    padding-left: 10px;
		    background-color: #34A7C1; color: #FFFFFF;
		}
		.onoffswitch-inner:after {
		    content: "OFF";
		    padding-right: 10px;
		    background-color: #EEEEEE; color: #999999;
		    text-align: right;
		}
		.onoffswitch-switch {
		    display: block; width: 18px; margin: 6px;
		    background: #FFFFFF;
		    position: absolute; top: 0; bottom: 0;
		    right: 56px;
		    border: 2px solid #999999; border-radius: 20px;
		    transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
		    margin-left: 0;
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
		    right: 0px; 
		}
	</style>

	<section role="main" class="content-body">
		<header class="page-header">
			<h2 style="font-size: 15px;"><img  style="background: white;
			    border-radius: 100%;
			    width: 40px;
			    height: 40px;
			    margin-right: 1em;
			" src="{{URL('/')}}/uploads/{{$hotels->img_file == '' ? 'No-Image-Available.png' : $hotels->img_file}}"/> {{strtoupper($hotels->hotel_name)}}'s DASHBOARD</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="index2.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Dashboard</span></li>
				</ol>
			</div>
		</header>

		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<div class="panel-body" style="padding-top: 0px;">
						<div class="row">
							<div class="col-md-12">
								<form enctype="multipart/form-data" method="POST" action="{{url('/')}}/room_type/{{$hotels->id}}">  
									<input type="hidden" name="room_type_list" value="0"/>
                        			@csrf
								<section class="panel" style="padding: 3em;">
									<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd;    padding-bottom: 0px;">
										<div class="panel-actions">
											<!-- <a href="#" class="fa fa-caret-down"></a> -->
											<!-- <a href="#" class="fa fa-times"></a> -->
										</div>
										<div style="float: right">
											<p style="font-weight:bold; color: #0DC3D8; margin-left: 24px; margin-bottom: 0px;">{{$hotels->hotel_name}}
												</p>
											<p style="color: #0DC3D8; margin-left: 24px;">ID: {{str_pad($hotels->id, 8, "0", STR_PAD_LEFT)}}</p>
										</div>
										<h2 class="panel-title" style="font-weight:bold; color: black">Room Type Setup</h2>
										<p class="panel-subtitle"></p>

										
									</header>
									<hr>
									
									<div class="row" style="border: 1px solid #d1bebe">
										<div class="col-md-4">
											<section class="panel" style="padding: 1em;">
												<h2 class="panel-title" style="font-weight:bold; font-size: 16px">Basic Info</h2>
											</section>
											<div class="form-group">
												<label for="focusedinput">Room Name</label>
												<input type="text" name="room_name[0]" value="" class="form-control" id="focusedinput" placeholder="Room Name" required>
											</div>
											<div class="form-group">
												<label for="focusedinput">Internal Room Name (Optional, Not Visible to Customer)</label>
												<input type="text" name="description[0]" value="" class="form-control" id="focusedinput" placeholder="Internal Room Name (Optional, Not Visible to Customer)" required>
											</div>
											<div class="form-group">
												<label for="focusedinput">External Code ID:</label>
												<input type="text" name="external_code_id" value="" class="form-control" id="focusedinput" placeholder="External Code ID">
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group" style="margin-bottom: 15px;">
														<label for="focusedinput">Standard Rate*</label>
														<input type="number" step="any" name="standard_rate[0]" value="" class="form-control" id="focusedinput" placeholder="Standard Rate" required>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group" style="margin-bottom: 15px;">
														<label for="focusedinput">Rate Protection*</label>
														<input type="number" step="any" name="rate_protection[0]" value="" class="form-control" id="focusedinput" placeholder="Rate Protection" required>
													</div>
												</div>
												<div class="clearfix"></div>
												<!-- <div class="col-md-6">
													<div class="form-group" style="margin-bottom: 15px;">
														<label for="focusedinput">No. of Rooms*</label>
														<div  style="position: relative">
												          <input type="number" name="no_room[0]" class="form-control input-number" value="0" min="0" max="100">
												          <span class="input-group-btn" style="position: absolute;
															    top: 13%;
															    right: 42.1%;">
												              <button style="border-radius: 8px 0px 0px 8px;
															    font-size: 12px;
															    padding: 4px;
															    border: 0px transparent;
															    line-height: 0px; 
															    /*margin-left: -250%; */
															    z-index: 2;
															    width: 28px; height: 19px;
															    background: #468DF0 0% 0% no-repeat padding-box;
																box-shadow: 0px 2px 5px #00000033;
																border-radius: 15px 0px 0px 15px;
																opacity: 1;
															    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="no_room[0]">
												                <span class="fa fa-minus"></span>
												              </button>
												          </span>
												          <span class="input-group-btn" style="position: absolute;
															    top: 13%;
															    right: 21.1%;">
												              <button style="border-radius: 0px 8px 8px 0px;
															    font-size: 12px;
															    padding: 4px;
															    border: 0px transparent;
															    line-height: 0px; 
															    /*margin-left: -250%;*/ 
															    z-index: 2;
															    width: 28px; height: 19px;
															    background: #468DF0 0% 0% no-repeat padding-box;
																box-shadow: 0px 2px 5px #00000033;
																border-radius: 0px 15px 15px 0px;
																opacity: 1;
															    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="no_room[0]">
												                  <span class="fa fa-plus"></span>
												              </button>
												          </span>
												      </div>
												     </div>
												</div> -->
												<br>
											</div>
											
										</div>
										<div class="col-md-8" style="border: 1px solid #d1bebe">
											<section class="panel" style="padding: 1em;">
												<h2 class="panel-title" style="font-weight:bold; font-size: 16px">Occupancy Settings</h2>
											</section>
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<div class="form-group">
															<div class="col-md-6">
																<div class="form-group" style="margin-bottom: 15px;">
																	<label for="focusedinput">Person*</label>
																	<div  style="position: relative">
															          <input type="number" name="person[0]" class="form-control input-number" value="0" min="1" max="100">
															          <span class="input-group-btn" style="position: absolute;
																		    top: 13%;
																		    right: 42.1%;">
															              <button style="border-radius: 8px 0px 0px 8px;
																		    font-size: 12px;
																		    padding: 4px;
																		    border: 0px transparent;
																		    line-height: 0px; 
																		    /*margin-left: -250%; */
																		    z-index: 2;
																		    width: 28px; height: 19px;
																		    background: #468DF0 0% 0% no-repeat padding-box;
																			box-shadow: 0px 2px 5px #00000033;
																			border-radius: 15px 0px 0px 15px;
																			opacity: 1;
																		    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="person[0]">
															                <span class="fa fa-minus"></span>
															              </button>
															          </span>
															          <span class="input-group-btn" style="position: absolute;
																		    top: 13%;
																		    right: 21.1%;">
															              <button style="border-radius: 0px 8px 8px 0px;
																		    font-size: 12px;
																		    padding: 4px;
																		    border: 0px transparent;
																		    line-height: 0px; 
																		    /*margin-left: -250%;*/ 
																		    z-index: 2;
																		    width: 28px; height: 19px;
																		    background: #468DF0 0% 0% no-repeat padding-box;
																			box-shadow: 0px 2px 5px #00000033;
																			border-radius: 0px 15px 15px 0px;
																			opacity: 1;
																		    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="person[0]">
															                  <span class="fa fa-plus"></span>
															              </button>
															          </span>
															     </div>
															     </div>
															</div>
															<div class="col-md-6">
																<div class="form-group" style="margin-bottom: 15px;">
																	<label for="focusedinput">Extra Bed*</label>
																	<div  style="position: relative">
															          <input type="number" name="extra_bed[0]" class="form-control input-number" value="0" min="0" max="100">
															          <span class="input-group-btn" style="position: absolute;
																		    top: 13%;
																		    right: 42.1%;">
															              <button style="border-radius: 8px 0px 0px 8px;
																		    font-size: 12px;
																		    padding: 4px;
																		    border: 0px transparent;
																		    line-height: 0px; 
																		    /*margin-left: -250%; */
																		    z-index: 2;
																		    width: 28px; height: 19px;
																		    background: #468DF0 0% 0% no-repeat padding-box;
																			box-shadow: 0px 2px 5px #00000033;
																			border-radius: 15px 0px 0px 15px;
																			opacity: 1;
																		    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="extra_bed[0]">
															                <span class="fa fa-minus"></span>
															              </button>
															          </span>
															          <span class="input-group-btn" style="position: absolute;
																		    top: 13%;
																		    right: 21.1%;">
															              <button style="border-radius: 0px 8px 8px 0px;
																		    font-size: 12px;
																		    padding: 4px;
																		    border: 0px transparent;
																		    line-height: 0px; 
																		    /*margin-left: -250%;*/ 
																		    z-index: 2;
																		    width: 28px; height: 19px;
																		    background: #468DF0 0% 0% no-repeat padding-box;
																			box-shadow: 0px 2px 5px #00000033;
																			border-radius: 0px 15px 15px 0px;
																			opacity: 1;
																		    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="extra_bed[0]">
															                  <span class="fa fa-plus"></span>
															              </button>
															          </span>
															     </div>
															     </div>
															</div>
														</div>
														<div class="form-group">
															<div class="col-md-6">
																<div class="form-group" style="margin-bottom: 15px;">
																	<label for="focusedinput">Maximum Person*</label>
																	<div  style="position: relative">
															          <input type="number" name="max_person[0]" class="form-control input-number" value="0" min="1" max="100">
															          <span class="input-group-btn" style="position: absolute;
																		    top: 13%;
																		    right: 42.1%;">
															              <button style="border-radius: 8px 0px 0px 8px;
																		    font-size: 12px;
																		    padding: 4px;
																		    border: 0px transparent;
																		    line-height: 0px; 
																		    /*margin-left: -250%; */
																		    z-index: 2;
																		    width: 28px; height: 19px;
																		    background: #468DF0 0% 0% no-repeat padding-box;
																			box-shadow: 0px 2px 5px #00000033;
																			border-radius: 15px 0px 0px 15px;
																			opacity: 1;
																		    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="max_person[0]">
															                <span class="fa fa-minus"></span>
															              </button>
															          </span>
															          <span class="input-group-btn" style="position: absolute;
																		    top: 13%;
																		    right: 21.1%;">
															              <button style="border-radius: 0px 8px 8px 0px;
																		    font-size: 12px;
																		    padding: 4px;
																		    border: 0px transparent;
																		    line-height: 0px; 
																		    /*margin-left: -250%;*/ 
																		    z-index: 2;
																		    width: 28px; height: 19px;
																		    background: #468DF0 0% 0% no-repeat padding-box;
																			box-shadow: 0px 2px 5px #00000033;
																			border-radius: 0px 15px 15px 0px;
																			opacity: 1;
																		    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="max_person[0]">
															                  <span class="fa fa-plus"></span>
															              </button>
															          </span>
															     </div>
															     </div>
															</div>
															<div class="col-md-6">
																<div class="form-group" style="margin-bottom: 15px;">
																	<label for="focusedinput">Maximum Extras Bed*</label>
																	<div  style="position: relative">
															          <input type="number" name="max_extra_bed[0]" class="form-control input-number" value="0" min="0" max="100">
															          <span class="input-group-btn" style="position: absolute;
																		    top: 13%;
																		    right: 42.1%;">
															              <button style="border-radius: 8px 0px 0px 8px;
																		    font-size: 12px;
																		    padding: 4px;
																		    border: 0px transparent;
																		    line-height: 0px; 
																		    /*margin-left: -250%; */
																		    z-index: 2;
																		    width: 28px; height: 19px;
																		    background: #468DF0 0% 0% no-repeat padding-box;
																			box-shadow: 0px 2px 5px #00000033;
																			border-radius: 15px 0px 0px 15px;
																			opacity: 1;
																		    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="max_extra_bed[0]">
															                <span class="fa fa-minus"></span>
															              </button>
															          </span>
															          <span class="input-group-btn" style="position: absolute;
																		    top: 13%;
																		    right: 21.1%;">
															              <button style="border-radius: 0px 8px 8px 0px;
																		    font-size: 12px;
																		    padding: 4px;
																		    border: 0px transparent;
																		    line-height: 0px; 
																		    /*margin-left: -250%;*/ 
																		    z-index: 2;
																		    width: 28px; height: 19px;
																		    background: #468DF0 0% 0% no-repeat padding-box;
																			box-shadow: 0px 2px 5px #00000033;
																			border-radius: 0px 15px 15px 0px;
																			opacity: 1;
																		    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="max_extra_bed[0]">
															                  <span class="fa fa-plus"></span>
															              </button>
															          </span>
															     </div>
															     </div>
															</div>
														</div>
														<div class="form-group" style="border: 0.5px solid #D3D3D3;    padding: 1em;margin: 1em;">
															<label for="focusedinput" class="col-sm-9 control-label">Allow Children in this room type</label>
															<div class="col-md-3">
																<div id="ck-button">
																	<label class="switch"><input type="hidden" name="is_allow_children[0]" value="0"><input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" ><span class="slider round"></span></label></div>
															</div>

														</div>
														<div class="form-group">
															<div class="col-md-6">
																<label for="focusedinput">&nbsp;</label>
																<label style="line-height: 1em;" for="focusedinput" class="col-sm-9 control-label">Children allowed for free
																	<br><span style="font-size: 11px;">11 years old and under</span>
																	<br><a style="font-size: 12px;">Change child policy</a>
																</label>
															</div>
															<div class="col-md-6">
																<div class="form-group" style="margin-bottom: 15px;">
																	<label for="focusedinput">&nbsp;</label>
																	<div  style="position: relative">
															          <input type="number" name="child_allow[0]" class="form-control input-number" value="0" min="0" max="100">
															          <span class="input-group-btn" style="position: absolute;
																		    top: 13%;
																		    right: 42.1%;">
															              <button style="border-radius: 8px 0px 0px 8px;
																		    font-size: 12px;
																		    padding: 4px;
																		    border: 0px transparent;
																		    line-height: 0px; 
																		    /*margin-left: -250%; */
																		    z-index: 2;
																		    width: 28px; height: 19px;
																		    background: #468DF0 0% 0% no-repeat padding-box;
																			box-shadow: 0px 2px 5px #00000033;
																			border-radius: 15px 0px 0px 15px;
																			opacity: 1;
																		    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="child_allow[0]">
															                <span class="fa fa-minus"></span>
															              </button>
															          </span>
															          <span class="input-group-btn" style="position: absolute;
																		    top: 13%;
																		    right: 21.1%;">
															              <button style="border-radius: 0px 8px 8px 0px;
																		    font-size: 12px;
																		    padding: 4px;
																		    border: 0px transparent;
																		    line-height: 0px; 
																		    /*margin-left: -250%;*/ 
																		    z-index: 2;
																		    width: 28px; height: 19px;
																		    background: #468DF0 0% 0% no-repeat padding-box;
																			box-shadow: 0px 2px 5px #00000033;
																			border-radius: 0px 15px 15px 0px;
																			opacity: 1;
																		    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="child_allow[0]">
															                  <span class="fa fa-plus"></span>
															              </button>
															          </span>
															     </div>
															     </div>
															</div>
														</div>
														<div class="form-group">
															<div class="col-md-6">
																<label for="focusedinput">&nbsp;</label>
																<label for="focusedinput" class="col-sm-9 control-label">Maximum Children Allowed*
																</label>
															</div>
															<div class="col-md-6">
																<div class="form-group" style="margin-bottom: 15px;">
																	<label for="focusedinput">&nbsp;</label>
																	<div  style="position: relative">
															          <input type="number" name="max_child[0]" class="form-control input-number" value="0" min="0" max="100">
															          <span class="input-group-btn" style="position: absolute;
																		    top: 13%;
																		    right: 42.1%;">
															              <button style="border-radius: 8px 0px 0px 8px;
																		    font-size: 12px;
																		    padding: 4px;
																		    border: 0px transparent;
																		    line-height: 0px; 
																		    /*margin-left: -250%; */
																		    z-index: 2;
																		    width: 28px; height: 19px;
																		    background: #468DF0 0% 0% no-repeat padding-box;
																			box-shadow: 0px 2px 5px #00000033;
																			border-radius: 15px 0px 0px 15px;
																			opacity: 1;
																		    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="max_child[0]">
															                <span class="fa fa-minus"></span>
															              </button>
															          </span>
															          <span class="input-group-btn" style="position: absolute;
																		    top: 13%;
																		    right: 21.1%;">
															              <button style="border-radius: 0px 8px 8px 0px;
																		    font-size: 12px;
																		    padding: 4px;
																		    border: 0px transparent;
																		    line-height: 0px; 
																		    /*margin-left: -250%;*/ 
																		    z-index: 2;
																		    width: 28px; height: 19px;
																		    background: #468DF0 0% 0% no-repeat padding-box;
																			box-shadow: 0px 2px 5px #00000033;
																			border-radius: 0px 15px 15px 0px;
																			opacity: 1;
																		    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="max_child[0]">
															                  <span class="fa fa-plus"></span>
															              </button>
															          </span>
															     </div>
															     </div>
															</div>
														</div>
														<div class="form-group" style="border: 0.5px solid #D3D3D3;    padding: 1em;margin: 1em;">
															<label for="focusedinput" class="col-sm-9 control-label">Baby Cots</label>
															<div class="col-md-3">
																<div id="ck-button">
																	<label class="switch"><input type="hidden" name="baby_cots[0]" value="0"><input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value"><span class="slider round"></span></label></div>
															</div>

														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
													<label style="padding-left: 0px;" for="focusedinput" class="col-sm-12 control-label">Bed Type</label>
													<div class="row">
													@foreach($bed as $result)
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-6">
																<div class="checkbox">
																	 <label for="bed{{$result->id}}"> 
																		<input id="bed{{$result->id}}" type="checkbox" name="bed[0][{{$result->id}}]"
																		> {{$result->bed}}
																	</label> 
																</div>
															</div>
															<div class="col-md-6">
																<input class="form-control" type="number" name="bedQty[0][{{$result->id}}]" value="0"/>
															</div>
														</div>
													</div>
													@endforeach
												</div>
												</div>
												<div class="form-group">
												<div class="col-md-6">
													<div class="form-group" style="margin-bottom: 15px;">
														<label for="focusedinput">Room Size</label>
														<input type="number" step="any" name="room_size[0]" value="20.00" class="form-control" id="focusedinput" placeholder="Standard Rate" required>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group" style="margin-bottom: 15px;">
														<label for="focusedinput">Bathroom</label>
														<div  style="position: relative">
												          <input type="number" name="bath_room[0]" class="form-control input-number" value="0" min="0" max="100">
												          <span class="input-group-btn" style="position: absolute;
															    top: 13%;
															    right: 44.1%;">
												              <button style="border-radius: 8px 0px 0px 8px;
															    font-size: 12px;
															    padding: 4px;
															    border: 0px transparent;
															    line-height: 0px; 
															    /*margin-left: -250%; */
															    z-index: 2;
															    width: 28px; height: 19px;
															    background: #468DF0 0% 0% no-repeat padding-box;
																box-shadow: 0px 2px 5px #00000033;
																border-radius: 15px 0px 0px 15px;
																opacity: 1;
															    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="bath_room[0]">
												                <span class="fa fa-minus"></span>
												              </button>
												          </span>
												          <span class="input-group-btn" style="position: absolute;
															    top: 13%;
															    right: 21.1%;">
												              <button style="border-radius: 0px 8px 8px 0px;
															    font-size: 12px;
															    padding: 4px;
															    border: 0px transparent;
															    line-height: 0px; 
															    /*margin-left: -250%;*/ 
															    z-index: 2;
															    width: 28px; height: 19px;
															    background: #468DF0 0% 0% no-repeat padding-box;
																box-shadow: 0px 2px 5px #00000033;
																border-radius: 0px 15px 15px 0px;
																opacity: 1;
															    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="bath_room[0]">
												                  <span class="fa fa-plus"></span>
												              </button>
												          </span>
												     </div>
												     </div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
														<label for="focusedinput">Cancellation Policy</label>
														<select name="cancelation_id[0]" class="form-control mySelect" required="required">
															<option value="test">Cancel 45 Days Upon Arrival 50%</option>
														</select>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group" style="margin-bottom: 15px;">
														<label for="focusedinput">Allotment Quantity Alert*</label>
														<div  style="position: relative">
												          <input type="number" name="alert[0]" class="form-control input-number" value="0" min="0" max="100">
												          <span class="input-group-btn" style="position: absolute;
															    top: 13%;
															    right: 44.1%;">
												              <button style="border-radius: 8px 0px 0px 8px;
															    font-size: 12px;
															    padding: 4px;
															    border: 0px transparent;
															    line-height: 0px; 
															    /*margin-left: -250%; */
															    z-index: 2;
															    width: 28px; height: 19px;
															    background: #468DF0 0% 0% no-repeat padding-box;
																box-shadow: 0px 2px 5px #00000033;
																border-radius: 15px 0px 0px 15px;
																opacity: 1;
															    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="alert[0]">
												                <span class="fa fa-minus"></span>
												              </button>
												          </span>
												          <span class="input-group-btn" style="position: absolute;
															    top: 13%;
															    right: 21.1%;">
												              <button style="border-radius: 0px 8px 8px 0px;
															    font-size: 12px;
															    padding: 4px;
															    border: 0px transparent;
															    line-height: 0px; 
															    /*margin-left: -250%;*/ 
															    z-index: 2;
															    width: 28px; height: 19px;
															    background: #468DF0 0% 0% no-repeat padding-box;
																box-shadow: 0px 2px 5px #00000033;
																border-radius: 0px 15px 15px 0px;
																opacity: 1;
															    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="alert[0]">
												                  <span class="fa fa-plus"></span>
												              </button>
												          </span>
												     </div>
												     </div>
												</div>
											</div>
										</div>
									</div>
									</div>
									<div class="row" style="border: 1px solid #d1bebe">
										<section class="panel" style="padding: 1em;">
											<h2 class="panel-title" style="font-weight:bold; font-size: 16px">Content</h2>
										</section>
										<div class="col-md-12">
										<div class="upload-img-wrapper" style="margin: 1em;">
		                                    <div class="img-selected-wrapper">
		                                    	
		                                    </div>
		                                    <div class="upload">
		                                        <div class="img-thumbnail" id="btn-upload" >
		                                            <i class="fa fa-cloud-upload"></i>
		                                            <span>upload</span> 
		                                        </div>
		                                        <input type="file" class="hidden" id="file-upload" multiple name="images[0]" accept="image/*">
		                                    </div>
		                                </div>
											<p>Total Photos: <span id="photoCount">0</span></p>
		                            </div>
									</div>
									<div class="row" style="border: 1px solid #d1bebe">
										<section class="panel" style="padding: 1em;">
											<h2 class="panel-title" style="font-weight:bold; font-size: 16px">Room Amenities</h2>
										</section>
										@foreach($amenities as $result)
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-6">
													<div class="checkbox">
														 <label for="amenities{{$result->id}}"> 
															<input id="amenities{{$result->id}}" type="checkbox" name="amenities[0][{{$result->id}}]"
															> {{$result->amenities}}
														</label> 
													</div>
												</div>
											</div>
										</div>
										@endforeach
									</div>
									<br>
									<div class="text-right mr-lg">

										<a style="border: 1px solid #b8c7ce; float: right; margin-left: .5em;" href="{{URL('/')}}/room_type/{{$hotels->id}}" style="float: right;" class="btn btn-info btn-flat btn-pri">
											<i class="fa fa-arrow-left"></i> Back
										</a>

										<button style="float: right; margin-left: .5em;" class="btn btn-success btn-flat btn-pri">
											<i class="fa fa-floppy-o"></i> SAVE
										</button>

									</div>
								</section>
								</form>
							</div>
						</div>
					</section>
				</div>
			</div>
		</section>
	<style>

		.dt2019-12-25 { background-color: #F0FFF0; }
		.dt2019-12-25 .monthly-day-number:after { content: '\1F384'; }
		.fa-hand-o-up{
			  transform: rotate(-40deg)  scale(-1, 1);
		}
		.yellow{
			  color: #e0e024;
		}
	</style>
	
@endsection

@section('page-script')
<script type="text/javascript">

	$(function(){
		$(".mySelect").select2();
		$('#roomManagementMenu').addClass('nav-expanded');
		$('#roomManagementMenu').addClass('nav-active');
		$('#roomTypeMenu').addClass('nav-expanded');
		$('#roomTypeMenu').addClass('nav-active');
	});
//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});

$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

$("#btn-upload").click(function(e){ 
	    $("#file-upload").click();
	}); 
	$("#file-upload").change(function(){
	    if($(this).get(0).files.length){
	        // $(".img-selected-wrapper").html("");
	        $.each($(this).get(0).files, function(index, file) {
	             
	            var reader = new FileReader(); 
	            reader.onload = function (e) {

				  test = parseInt($("#photoCount").html());

				  $("#photoCount").html(test + 1);
				  var html = "<div class='item'>";
			                    html +=      " <img class='img-thumbnail' src='"+ reader.result+"'>"; 
			                    html +=      " <input type='hidden' name='image[0][]' value='"+ e.target.result+"'>"; 
			                    html += "</div>";
			                    
			                $(".img-selected-wrapper").prepend(html);                        
			                $('#blah').attr('src', reader.result);
	            } 
	            reader.readAsDataURL(file); 
	        }); 
	    }
	});
</script>
@endsection