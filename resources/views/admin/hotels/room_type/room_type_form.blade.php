<style>
.form-group{
	width: 100%;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>
<div class="well" style="background: white; padding: 0px 14px 14px 14px;">
<div class="row">
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-12" style="background: #D3D3D3;
		    color: black;
		    font-weight: bold;margin-bottom: 1em;">
			<center>Basic Info</center>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">Room Name</label><br>
			<input style="width: 100%;" type="text" name="room_name[{{$room->id}}]" value="{{$room->room_name}}" class="form-control" id="focusedinput" placeholder="Room Name" required>
		</div><br>
		<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">Internal Room Name (Optional, Not Visible to Customer)</label><br>
			<input style="width: 100%;" type="text" name="description[{{$room->id}}]" value="{{$room->description}}" class="form-control" id="focusedinput" placeholder="Internal Room Name (Optional, Not Visible to Customer)">
		</div><br>
		<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">External Code ID:</label><br>
			<input style="width: 100%;" type="text" name="external_code_id" value="{{$room->id}}" class="form-control" id="focusedinput" placeholder="External Code ID" disabled>
		</div><br><br>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 15px;">
					<label for="focusedinput">Standard Rate*</label>
					<input style="width: 100%;" type="number" step="any" name="standard_rate[{{$room->id}}]" value="0" class="form-control" id="focusedinput" placeholder="Standard Rate" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 15px;">
					<label for="focusedinput">Rate Protection*</label>
					<input style="width: 100%;" type="number" step="any" name="rate_protection[{{$room->id}}]" value="0" class="form-control" id="focusedinput" placeholder="Rate Protection" required>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<!-- <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 15px;">
					<label for="focusedinput">No. of Rooms*</label><br>
			          <input style="width: 100%;" type="number" name="no_room[{{$room->id}}]" class="form-control input-number" value="0" min="0" max="100">
			          <span class="input-group-btn" style="position: absolute;
						    top: 43%;
						    right: 40.55%;">
			              <button style="border-radius: 8px 0px 0px 8px;
						    font-size: 12px;
						    padding: 4px;
						    border: 0px transparent;
						    line-height: 0px; 
						    /*margin-left: -250%; */
						    z-index: 2;
						    width: 28px; height: 19px;
						    background: #468DF0 0% 0% no-repeat padding-box;
							box-shadow: 0px 2px 5px #00000033;
							border-radius: 15px 0px 0px 15px;
							opacity: 1;
						    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="no_room[{{$room->id}}]">
			                <span class="fa fa-minus"></span>
			              </button>
			          </span>
			          <span class="input-group-btn" style="position: absolute;
						    top: 43%;
						    right: 27%;">
			              <button style="border-radius: 0px 8px 8px 0px;
						    font-size: 12px;
						    padding: 4px;
						    border: 0px transparent;
						    line-height: 0px; 
						    /*margin-left: -250%;*/ 
						    z-index: 2;
						    width: 28px; height: 19px;
						    background: #468DF0 0% 0% no-repeat padding-box;
							box-shadow: 0px 2px 5px #00000033;
							border-radius: 0px 15px 15px 0px;
							opacity: 1;
						    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="no_room[{{$room->id}}]">
			                  <span class="fa fa-plus"></span>
			              </button>
			          </span>
			     </div>
			</div> -->
		</div>
		<div class="form-group" style="margin-bottom: 15px;">
			<label for="focusedinput">Cancellation Policy</label><br>
			<select  style="width: 100%;" name="cancelation_id[{{$room->id}}]" class="form-control mySelect" required="required">
				<option value="test">Cancel 45 Days Upon Arrival 50%</option>
			</select>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-12" style="background: #D3D3D3;
		    color: black;
		    font-weight: bold;margin-bottom: 1em;">
			<center>Occupancy Settings</center>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
			<div class="row">
			<div class="form-group">
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 15px;">
						<label for="focusedinput">Person*</label><br>
				          <input style="width: 100%;" type="number" name="person[{{$room->id}}]" class="form-control input-number" value="{{$room->guest}}" min="1" max="100">
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 40.55%;">
				              <button style="border-radius: 8px 0px 0px 8px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%; */
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 15px 0px 0px 15px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="person[{{$room->id}}]">
				                <span class="fa fa-minus"></span>
				              </button>
				          </span>
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 27%;">
				              <button style="border-radius: 0px 8px 8px 0px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%;*/ 
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 0px 15px 15px 0px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="person[{{$room->id}}]">
				                  <span class="fa fa-plus"></span>
				              </button>
				          </span>
				     </div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 15px;">
						<label for="focusedinput">Extra Bed*</label><br>
				          <input style="width: 100%;" type="number" name="extra_bed[{{$room->id}}]" class="form-control input-number" value="{{$room->extra_bed}}" min="0" max="100">
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 40.55%;">
				              <button style="border-radius: 8px 0px 0px 8px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%; */
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 15px 0px 0px 15px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="extra_bed[{{$room->id}}]">
				                <span class="fa fa-minus"></span>
				              </button>
				          </span>
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 27%;">
				              <button style="border-radius: 0px 8px 8px 0px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%;*/ 
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 0px 15px 15px 0px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="extra_bed[{{$room->id}}]">
				                  <span class="fa fa-plus"></span>
				              </button>
				          </span>
				     </div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 15px;">
						<label for="focusedinput">Maximum Person*</label><br>
				          <input style="width: 100%;" type="number" name="max_person[{{$room->id}}]" class="form-control input-number" value="{{$room->max_extra_bed}}" min="1" max="100">
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 40.55%;">
				              <button style="border-radius: 8px 0px 0px 8px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%; */
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 15px 0px 0px 15px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="max_person[{{$room->id}}]">
				                <span class="fa fa-minus"></span>
				              </button>
				          </span>
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 27%;">
				              <button style="border-radius: 0px 8px 8px 0px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%;*/ 
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 0px 15px 15px 0px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="max_person[{{$room->id}}]">
				                  <span class="fa fa-plus"></span>
				              </button>
				          </span>
				     </div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 15px;">
						<label for="focusedinput">Maximum Extras Bed*</label><br>
				          <input type="number" style="width: 100%;" name="max_extra_bed[{{$room->id}}]" class="form-control input-number" value="{{$room->max_guest}}" min="0" max="100">
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 40.55%;">
				              <button style="border-radius: 8px 0px 0px 8px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%; */
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 15px 0px 0px 15px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="max_extra_bed[{{$room->id}}]">
				                <span class="fa fa-minus"></span>
				              </button>
				          </span>
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 27%;">
				              <button style="border-radius: 0px 8px 8px 0px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%;*/ 
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 0px 15px 15px 0px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="max_extra_bed[{{$room->id}}]">
				                  <span class="fa fa-plus"></span>
				              </button>
				          </span>
				     </div>
				</div>
			</div>
			<div class="form-group" style="border: 0.5px solid #D3D3D3; padding: 1em;margin: 0em;">
				<label for="focusedinput" class="col-sm-9 control-label">Allow Children in this room type</label>
				<div class="col-md-3">
					<div id="ck-button">
						<label class="switch"><input type="hidden" name="is_allow_children[{{$room->id}}]" value="{{$room->is_child_allowed}}"><input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" {{$room->is_child_allowed == 1 ? "checked" : ""}}><span class="slider round"></span></label></div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6">
					<label for="focusedinput">&nbsp;</label><br>
					<label style="line-height: 1em;" for="focusedinput" class="col-sm-9 control-label">Children allowed for free
						<br><span style="font-size: 11px;">11 years old and under</span>
						<br><a style="font-size: 12px;">Change child policy</a>
					</label>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 15px;">
						<label for="focusedinput">&nbsp;</label>
				          <input style="width: 100%;" type="number" name="child_allow[{{$room->id}}]" class="form-control input-number" value="{{$room->free_child}}" min="0" max="100">
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 40.55%;">
				              <button style="border-radius: 8px 0px 0px 8px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%; */
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 15px 0px 0px 15px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="child_allow[{{$room->id}}]">
				                <span class="fa fa-minus"></span>
				              </button>
				          </span>
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 27%;">
				              <button style="border-radius: 0px 8px 8px 0px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%;*/ 
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 0px 15px 15px 0px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="child_allow[{{$room->id}}]">
				                  <span class="fa fa-plus"></span>
				              </button>
				          </span>
				     </div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6">
					<label for="focusedinput">&nbsp;</label>
					<label style="margin-top: 1.25em;" for="focusedinput" class="col-sm-9 control-label">Maximum Children Allowed*
					</label>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 15px;">
						<label for="focusedinput">&nbsp;</label>
				          <input style="width: 100%;" type="number" name="max_child[{{$room->id}}]" class="form-control input-number" value="{{$room->max_child}}" min="0" max="100">
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 40.55%;">
				              <button style="border-radius: 8px 0px 0px 8px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%; */
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 15px 0px 0px 15px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="max_child[{{$room->id}}]">
				                <span class="fa fa-minus"></span>
				              </button>
				          </span>
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 27%;">
				              <button style="border-radius: 0px 8px 8px 0px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%;*/ 
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 0px 15px 15px 0px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="max_child[{{$room->id}}]">
				                  <span class="fa fa-plus"></span>
				              </button>
				          </span>
				     </div>
				</div>
			</div>
			<div class="form-group" style="border: 0.5px solid #D3D3D3; padding: 1em;margin: 0em;">
				<label for="focusedinput" class="col-sm-9 control-label">Baby Cots</label>
				<div class="col-md-3">
					<div id="ck-button">
						<label class="switch"><input type="hidden" name="baby_cots[{{$room->id}}]" value="{{$room->is_baby_cots}}"><input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" {{$room->is_baby_cots == 1 ? "checked" : ""}}><span class="slider round"></span></label></div>
				</div>

			</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
			<div class="form-group">
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 15px;">
						<label for="focusedinput">Room Size</label><br>
						<input style="width: 100%;" type="number" step="any" name="room_size[{{$room->id}}]" value="{{$room->room_size}}" class="form-control" id="focusedinput" placeholder="Standard Rate" required>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 15px;">
						<label for="focusedinput">Bathroom</label><br>
				          <input style="width: 100%;" type="number" name="bath_room[{{$room->id}}]" class="form-control input-number" value="{{$room->bathroom}}" min="0" max="100">
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 40.55%;">
				              <button style="border-radius: 8px 0px 0px 8px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%; */
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 15px 0px 0px 15px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="bath_room[{{$room->id}}]">
				                <span class="fa fa-minus"></span>
				              </button>
				          </span>
				          <span class="input-group-btn" style="position: absolute;
							    top: 43%;
							    right: 27%;">
				              <button style="border-radius: 0px 8px 8px 0px;
							    font-size: 12px;
							    padding: 4px;
							    border: 0px transparent;
							    line-height: 0px; 
							    /*margin-left: -250%;*/ 
							    z-index: 2;
							    width: 28px; height: 19px;
							    background: #468DF0 0% 0% no-repeat padding-box;
								box-shadow: 0px 2px 5px #00000033;
								border-radius: 0px 15px 15px 0px;
								opacity: 1;
							    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="bath_room[{{$room->id}}]">
				                  <span class="fa fa-plus"></span>
				              </button>
				          </span>
				     </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group">
					<div class="col-md-6">
						<div class="form-group" style="margin-bottom: 15px;">
							<label for="focusedinput">Allotment Quantity Alert*</label><br>
					          <input style="width: 100%;" type="number" name="alert[{{$room->id}}]" class="form-control input-number" value="{{$room->alert}}" min="0" max="100">
					          <span class="input-group-btn" style="position: absolute;
								    top: 43%;
								    right: 40.55%;">
					              <button style="border-radius: 8px 0px 0px 8px;
								    font-size: 12px;
								    padding: 4px;
								    border: 0px transparent;
								    line-height: 0px; 
								    /*margin-left: -250%; */
								    z-index: 2;
								    width: 28px; height: 19px;
								    background: #468DF0 0% 0% no-repeat padding-box;
									box-shadow: 0px 2px 5px #00000033;
									border-radius: 15px 0px 0px 15px;
									opacity: 1;
								    " type="button" class="btn btn-primary btn-number"  data-type="minus" data-field="alert[{{$room->id}}]">
					                <span class="fa fa-minus"></span>
					              </button>
					          </span>
					          <span class="input-group-btn" style="position: absolute;
								    top: 43%;
								    right: 27%;">
					              <button style="border-radius: 0px 8px 8px 0px;
								    font-size: 12px;
								    padding: 4px;
								    border: 0px transparent;
								    line-height: 0px; 
								    /*margin-left: -250%;*/ 
								    z-index: 2;
								    width: 28px; height: 19px;
								    background: #468DF0 0% 0% no-repeat padding-box;
									box-shadow: 0px 2px 5px #00000033;
									border-radius: 0px 15px 15px 0px;
									opacity: 1;
								    " type="button" class="btn btn-primary btn-number" data-type="plus" data-field="alert[{{$room->id}}]">
					                  <span class="fa fa-plus"></span>
					              </button>
					          </span>
					     </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group" style="margin-bottom: 15px;">
					<div class="col-md-12">
						<label for="focusedinput">Amenities</label><br>
						<button style="width: 100%;" class="btn btn-info">Select Amenities</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group" style="margin-bottom: 15px;">
					<div class="col-md-12">
						<label for="focusedinput">Bedding</label><br>
						<button style="width: 100%;" class="btn btn-info">Select Bedding</button>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
</div>
<script>
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});

$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>
