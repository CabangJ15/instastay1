@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')
    
@endsection

@section('content')
	<!-- start: sidebar -->
	{!! $menu !!}
	<!-- end: sidebar -->
	<style>
		.unselectFacilities{
			background: #fdfdfd; border: 0.5px solid #D3D3D3; padding: .5em;margin: .5em;
		}
		.selectFacilities{
			background: #DEF5F8; border: 0.5px solid transparent; padding: .5em; margin: .5em;
		}

		img.output:hover {
		cursor: pointer;
		}

		img.output1:hover {
		cursor: pointer;
		}

		label span input {
			z-index: 999;
			line-height: 0;
			font-size: 50px;
			position: absolute;
			top: 0px;
			left: 0px;
			opacity: 0;
			filter: alpha(opacity = 0);
			-ms-filter: "alpha(opacity=0)";
			cursor: pointer;
			_cursor: hand;
			margin: 0;
			padding:0;
		}

		#pac-input {
	        background-color: #fff;
	        font-family: Roboto;
	        font-size: 15px;
	        font-weight: 300;
	        margin-left: 12px;
	        padding: 0 11px 0 13px;
	        text-overflow: ellipsis;
	        width: 100%;
	        max-width: 300px;
	      }

	      #pac-input:focus {
	        border-color: #4d90fe;
	      }

	</style>
	<style>
	#filtersubmit {
	    position: relative;
	    z-index: 1;
	    left: 13px;
	    top: -25px;
	    color: #7B7B7B;
	    cursor:pointer;
	    width: 0;
	}
	#filter {
	    padding-left: 30px;
	}
	</style>
	<style type="text/css">
		.upload-img-wrapper div.item{
		    float:left;
		    margin-right: 5px;
		    margin-bottom: 5px;
		}

		.upload-img-wrapper div img{
		    width: 200px;
		    height:200px;
		}

		.upload-img-wrapper div.upload div{
		    width: 200px;
		    height: 200px;
		    padding: 60px;
		    text-align: center;
		    cursor: pointer;
		}

		.upload-img-wrapper div.upload div i{
		    font-size: 35px;
		    margin-top:15px;
		    
		    color: #4E4E4E;
		    display: block;
		}

		.upload-img-wrapper div.upload div:hover i{
		    color:#333;
		}
		</style>
		<style>
		.switch {
		  position: relative;
		  display: inline-block;
		  width: 49px;
    	  height: 23px;
		}

		.switch input { 
		  opacity: 0;
		  width: 0;
		  height: 0;
		}

		.slider {
		  position: absolute;
		  cursor: pointer;
		  top: 0;
		  left: 0;
		  right: 0;
		  bottom: 0;
		  background-color: #ccc;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		.slider:before {
		  position: absolute;
		  content: "";
		  height: 14px;
		  width: 14px;
		  left: 4px;
		  bottom: 4px;
		  background-color: white;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		input:checked + .slider {
		  background-color: #2196F3;
		}

		input:focus + .slider {
		  box-shadow: 0 0 1px #2196F3;
		}

		input:checked + .slider:before {
		  -webkit-transform: translateX(26px);
		  -ms-transform: translateX(26px);
		  transform: translateX(26px);
		}

		/* Rounded sliders */
		.slider.round {
		  border-radius: 34px;
		}

		.slider.round:before {
		  border-radius: 50%;
		}
		.onoffswitch {
		    position: relative; width: 90px;
		    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
		}
		.onoffswitch-checkbox {
		    display: none;
		}
		.onoffswitch-label {
		    display: block; overflow: hidden; cursor: pointer;
		    border: 2px solid #999999; border-radius: 20px;
		}
		.onoffswitch-inner {
		    display: block; width: 200%; margin-left: -100%;
		    transition: margin 0.3s ease-in 0s;
		}
		.onoffswitch-inner:before, .onoffswitch-inner:after {
		    display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
		    font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
		    box-sizing: border-box;
		}
		.onoffswitch-inner:before {
		    content: "ON";
		    padding-left: 10px;
		    background-color: #34A7C1; color: #FFFFFF;
		}
		.onoffswitch-inner:after {
		    content: "OFF";
		    padding-right: 10px;
		    background-color: #EEEEEE; color: #999999;
		    text-align: right;
		}
		.onoffswitch-switch {
		    display: block; width: 18px; margin: 6px;
		    background: #FFFFFF;
		    position: absolute; top: 0; bottom: 0;
		    right: 56px;
		    border: 2px solid #999999; border-radius: 20px;
		    transition: all 0.3s ease-in 0s; 
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
		    margin-left: 0;
		}
		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
		    right: 0px; 
		}
	</style>
	<style>
	#ck-button {
	    margin:4px;
	    background-color:#EFEFEF;
	    border-radius:4px;
	    border:1px solid #D0D0D0;
	    height: 2.3em;
	    float:left;
	}

	#ck-button label {
	    float:left;
	    width:4.0em;
	}

	#ck-button label span {
	    text-align:center;
	    padding:5px 0px;
	    display:block;
	    cursor: pointer;
	}

	#ck-button label input {
	    position:absolute;
	    visibility: hidden;
	}

	#ck-button input:checked + span {
	    background-color: #2c9911;
	    color: #fff;
	    border-radius: 3px;
	}

	.user_active{
		box-shadow: 2px 0 0 #0088cc inset !important;
	}
	</style>
	<section role="main" class="content-body">
		<header class="page-header">
			<h2 style="font-size: 15px;"><img  style="background: white;
			    border-radius: 100%;
			    width: 40px;
			    height: 40px;
			    margin-right: 1em;
			" src="{{URL('/')}}/uploads/{{$hotels->img_file == '' ? 'No-Image-Available.png' : $hotels->img_file}}"/> {{strtoupper($hotels->hotel_name)}}'s DASHBOARD</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="index2.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Dashboard</span></li>
				</ol>
			</div>
		</header>

		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					@if($status != 0)
					<form class="form-horizontal" enctype="multipart/form-data" action="{{URL('/')}}/add_property_contact{{$hotels->id != '' ? '/'.$hotels->id : ''}}" method="POST">
					@else
					<form class="form-horizontal" enctype="multipart/form-data" action="{{URL('/')}}/property_contact{{$hotels->id != '' ? '/'.$hotels->id : ''}}" method="POST">
						@csrf
					@endif
					<div class="panel-body" style="padding-top: 0px;">
						<div class="row">
							<div class="col-md-12" style="padding: 0em;">
								<section class="panel">
									<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd; padding-bottom: 0px;">
										<div class="panel-actions">
											<!-- <a href="#" class="fa fa-caret-down"></a> -->
											<!-- <a href="#" class="fa fa-times"></a> -->
										</div>
										@if($status == 0)
										<input type="hidden" name="id" value="{{$select_id}}">
										<input type="hidden" name="_method" value="PUT">
										@endif

										@if($status == 0)
											<?php $x = 0; ?>
											<a href="{{URL('/')}}/property_contacts/{{$hotels->id}}/create" type="reset" style="float: right; margin-left: .5em;" class="btn btn-info btn-flat btn-pri"><i class="fa fa-plus"></i> Create New Contact</a>

											<button style="float: right; margin-left: .5em;" class="btn btn-success btn-flat btn-pri"><i class="fa fa-floppy-o"></i> Update Contact</button>
										@else
											<?php $x = 1; ?>
											<button style="float: right; margin-left: .5em;" class="btn btn-info btn-flat btn-pri"><i class="fa fa-plus"></i> Add Contact</button>
										@endif
										<h2 class="panel-title" style="font-weight:bold; color: black">Property Contacts</h2>
										<p class="panel-subtitle"></p>
										
									</header>
								</section>
								<section class="panel" style="padding: 3em;">
									<div class="row">
										<div class="col-md-2">
											<input id="searchUser" style="padding-right: 2em;" onkeyup="myFunction();" class="form-control" placeholder="Search Property Contacts" type="text" />
											<i style="float: right;
												    margin-top: -1.7em;
												    margin-right: .5em;" class="fa fa-search"></i>
											<div id="contactHolder" style="padding: 0em;">
												{!! $list !!}
											</div>
										</div>
										<div class="col-md-10">
											<div class="contactDivHolder">
												<div id="userContentId">
													<div class="row">
													<div class="col-md-3">
														<div style="margin: 10%; margin-top: 0em;">
															<label for="focusedinput" class="col-sm-2 control-label">
																&nbsp;
															</label>
															<center>
																<label class="filebutton" style="width: 100%;">
																<img style="width: 150px; height: 150px;
    																border-radius: 100%;" id="output" src="{{URL('/')}}/{{$imgFile}}"/>
																<span><input style="width: 100%;" type="file" id="myfile" name="myfile" onchange="loadFile(event)" accept=".jpg, .png, .PNG, .JPG" 

																></span>
																</label>
															</center>
															<script>
															  var loadFile = function(event) {
																var output = document.getElementById('output');
																output.src = URL.createObjectURL(event.target.files[0]);
															  };
															</script>
															<br>
															<center>
																<a id="my-button" style="max-width: 150px; width: 100%;" class="btn btn-default btn-flat btn-pri"><i class="fa fa-camera"></i> UPLOAD LOGO</a>
															</center>
														</div>
													</div>
													<div class="col-md-9">
														<div class="row">
															<div class="col-md-12">
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<label>Full Name</label>
																<input class="form-control" name="fname" value="{{$fname}}" placeholder="Full Name" type="text"  required>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<label>Business Phone Number</label>
																<input class="form-control" placeholder="Business Phone Number" type="text" value="{{$tel_nos}}" name="tel_nos">
															</div>
															<div class="col-md-6">
																<label>Primary Email</label>
																<input class="form-control" placeholder="Primary Email" type="email" value="{{$email}}" name="email">
															</div>
															<div class="col-md-6">
																<label>Landline Number</label>
																<input class="form-control" placeholder="Landline Number" type="text" value="{{$land_line}}" name="land_line">
															</div>
															<div class="col-md-6">
																<label>Job Role</label>
																<input class="form-control" placeholder="Job Role" name="job_role" value="{{$job_role}}" type="text"  required>
															</div>
															<div class="col-md-12">
																<label>Country</label>
																<select class="form-control mySelect" name="country_id" required="required">
																	<option value="">Select a Country</option>
																	@foreach($countries as $data)
																	<option value="{{$data->id}}"
																		@if($country_id == $data->id)
																			selected
																		@endif
																		>{{$data->name}}</option>
																	@endforeach
																</select>
															</div>
															<div class="col-md-12">
																<label>Reponsibility</label>
																<textarea class="form-control" name="responsibility">{{$responsibility}}</textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</section>
							</div>
						</div>
									</div>
								</section>
							</div>
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>
	
@endsection

@section('page-script')
<script>
$(function(){

	$('#catalogMenu').addClass('nav-expanded');
	$('#catalogMenu').addClass('nav-active');
	$('#contactMenu').addClass('nav-expanded');
	$('#contactMenu').addClass('nav-active');
});
$('#my-button').click(function(){
    $('#myfile').click();
});
	
$(function(){
	$(".mySelect").select2();
});
</script>
<script>
function myFunction(){
	var searchUser = $('#searchUser').val();
		$("#contactHolder").html('');

		$.ajax({
			url: "{{URL('/')}}/searchPropertyContact",
			type: "POST",
			data: {
				_token: "{{ csrf_token() }}",
				"hotel_id" : "{{$hotels->id}}",
				"search" : searchUser,
				"select_id": "{{$select_id}}"
				// "_method": "DELETE",
			},
			success: function(data){
				console.log(data);
				 $("#contactHolder").html(data);
			}        
	   });
}
</script>
@endsection