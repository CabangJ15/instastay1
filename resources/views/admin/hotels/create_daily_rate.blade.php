<?php
$day = array(
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
	);
?>
<form class="form-horizontal" enctype="multipart/form-data" action="{{URL('/')}}/addRoomRateDaily/{{$hotel_id}}" method="POST">
<table class="table table-bordered table-striped mb-none" id="table_id">
	@csrf
	<input type="hidden" name="room_type_id" value="{{$room_type_id}}"/>
	<thead>
		<tr>
			<th style="width:300px"></th>
			@foreach($room_rate as $result)
			<th>{{$result->room_rate}}</th>
			@endforeach
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width:300px"></td>
		@foreach($room_rate as $data)
			<td>
			@if(count($room_rate_price_null->where('room_rate_id', $data->id)) != 0)
				@foreach($room_rate_price_null->where('room_rate_id', $data->id) as $data1)
					<?php $value = $data1->amount; ?>
				@endforeach
			@else
				<?php $value = 0; ?>
			@endif
			<input class="form-control" type="number" name="room_rate_orig[{{$data->id}}]" step="any" value="{{$value}}"/>
			</td>
		@endforeach
			<td></td>
		</tr>
		<tr>
			<th style="width:300px">Date</th>
			@foreach($room_rate as $result)
			<th>{{$result->room_rate}}</th>
			@endforeach
			<!-- <th>Promotion</th> -->
		</tr>
		@foreach($dates as $result)
		<tr>
			<?php $w = date('w', strtotime($result))?>
			<td style="width:300px"><textarea disabled>{{$day[$w]}} {{date('d M, Y', strtotime($result))}}</textarea><input type="hidden" name="date[]" value="{{$result}}"/>
			@foreach($room_rate as $result1)
			<td>
			@if(count($room_rate_price->where('date_applied', $result)->where('room_rate_id', $result1->id)) != 0)
				@foreach($room_rate_price->where('date_applied', $result)->where('room_rate_id', $result1->id) as $result3)
					<input class="form-control" type="number" step="any" name="rates[{{$result}}][{{$result1->id}}]" value="{{$result3->amount}}" required="required">

					<?php $promo = $result3->promotion; ?>
				@endforeach
			@else
				<?php $value = 0; ?>
				@foreach($room_rate_price_null->where('room_rate_id', $result1->id) as $data1)
					<?php 
					// $value = $data1->amount;
					?>
				@endforeach
				<input class="form-control" type="number" step="any" name="rates[{{$result}}][{{$result1->id}}]" value="{{$value}}" required="required">
				<?php $promo = 0; ?>
			@endif
			</td>
			@endforeach
			<!-- <td><input class="form-control" type="number" step="any" name="promo[{{$result}}]" value="{{$promo}}" required="required"></td> -->
		</tr>
		@endforeach
	</tbody>
</table>
<div class="col-md-10">

</div>
<div class="col-md-2">
	<button style="width: 100%; margin-top: 1em;" class="btn btn-info">Save</button>
</div>
</form>
<script>
var table = $('#table_id').DataTable( {
		"order": [],
		"bPaginate": false,
		"bFilter": false,
		"scrollX": false,
	    "paging": false,
	    "ordering": false,
	    "bInfo" : false,
	    "autoWidth": false,
		// "processing": true,
		// dom: 'Bfrtip',
        scrollX: true,
	    responsive: true
	} );
</script>