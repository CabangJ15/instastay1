@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')
    
@endsection

@section('content')
	<!-- start: sidebar -->
	{!! $menu !!}
	<!-- end: sidebar -->
	<style>
		img.output:hover {
		cursor: pointer;
		}

		img.output1:hover {
		cursor: pointer;
		}

		label span input {
			z-index: 999;
			line-height: 0;
			font-size: 50px;
			position: absolute;
			top: 0px;
			left: 0px;
			opacity: 0;
			filter: alpha(opacity = 0);
			-ms-filter: "alpha(opacity=0)";
			cursor: pointer;
			_cursor: hand;
			margin: 0;
			padding:0;
		}

		#pac-input {
	        background-color: #fff;
	        font-family: Roboto;
	        font-size: 15px;
	        font-weight: 300;
	        margin-left: 12px;
	        padding: 0 11px 0 13px;
	        text-overflow: ellipsis;
	        width: 100%;
	        max-width: 300px;
	      }

	      #pac-input:focus {
	        border-color: #4d90fe;
	      }

	</style>
	<style>
	#filtersubmit {
	    position: relative;
	    z-index: 1;
	    left: 13px;
	    top: -25px;
	    color: #7B7B7B;
	    cursor:pointer;
	    width: 0;
	}
	#filter {
	    padding-left: 30px;
	}
	</style>
	<style type="text/css">
		.upload-img-wrapper div.item{
		    float:left;
		    margin-right: 5px;
		    margin-bottom: 5px;
		}

		.upload-img-wrapper div img{
		    width: 200px;
		    height:200px;
		}

		.upload-img-wrapper div.upload div{
		    width: 200px;
		    height: 200px;
		    padding: 60px;
		    text-align: center;
		    cursor: pointer;
		}

		.upload-img-wrapper div.upload div i{
		    font-size: 35px;
		    margin-top:15px;
		    
		    color: #4E4E4E;
		    display: block;
		}

		.upload-img-wrapper div.upload div:hover i{
		    color:#333;
		}
		</style>

	<section role="main" class="content-body">
		<header class="page-header">
			<h2 style="font-size: 15px;"><img  style="background: white;
			    border-radius: 100%;
			    width: 40px;
			    height: 40px;
			    margin-right: 1em;
			" src="{{URL('/')}}/uploads/{{$hotels->img_file == '' ? 'No-Image-Available.png' : $hotels->img_file}}"/> {{strtoupper($hotels->hotel_name)}}'s DASHBOARD</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="index2.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Dashboard</span></li>
				</ol>
			</div>
		</header>

		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<div class="panel-body" style="padding-top: 0px;">
						<div class="row">
							<div class="col-md-12">
								<section class="panel" style="padding: 3em;">
									<div id="horizontalTab">
										<ul>
											@foreach($image_ref as $result)
											<li><a href="#tab-{{$result->id}}">{{$result->image_ref}}</a></li>
											@endforeach
										</ul>
									@foreach($image_ref as $result)
									<div id="tab-{{$result->id}}">
										<div class="upload-img-wrapper">
		                                    <div class="img-selected-wrapper{{$result->id}}">
		                                    	@foreach($result->images as $result1)
		                                        <div id="photo{{$result1->id}}" ondblclick="removeDiv({{$result1->id}})" class='item'>
			                    					<img class='img-thumbnail' src="{{URL('/')}}/uploads/room/{{$result1->image}}">
			                    				</div>
			                    				@endforeach
		                                    </div>
		                                    <div class="upload">
		                                        <div class="img-thumbnail" id="btn-upload{{$result->id}}" >
		                                            <i class="fa fa-cloud-upload"></i>
		                                            <span>upload</span> 
		                                        </div>
		                                        <input type="file" class="hidden" id="file-upload{{$result->id}}" multiple name="images" accept="image/*">
		                                    </div>
		                                </div>
									</div>
									@endforeach
								</div>
						</section>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
	<style>

		.dt2019-12-25 { background-color: #F0FFF0; }
		.dt2019-12-25 .monthly-day-number:after { content: '\1F384'; }
		.fa-hand-o-up{
			  transform: rotate(-40deg)  scale(-1, 1);
		}
		.yellow{
			  color: #e0e024;
		}
	</style>
	
@endsection

@section('page-script')
<script type="text/javascript">
	$(function(){
		$('#catalogMenu').addClass('nav-expanded');
		$('#catalogMenu').addClass('nav-active');
		$('#photoMenu').addClass('nav-expanded');
		$('#photoMenu').addClass('nav-active');
	})
	$('#horizontalTab').responsiveTabs({
	    rotate: false,
        startCollapsed: 'accordion',
        collapsible: 'accordion',
        setHash: false,
        activate: function(e, tab) {
            $('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
        },
        activateState: function(e, state) {
            //console.log(state);
            $('.info').html('Switched from <strong>' + state.oldState + '</strong> state to <strong>' + state.newState + '</strong> state!');
        }
	});

	function removeDiv(id){

		swal("Are you sure?", "You will not be able to recover this Photo!", {
			  buttons: {
			    cancel: "Cancel",
			    catch: {
			      text: "Yes",
			      value: "delete",
			      className: "btn-danger",
			    }
			  },
			})
			.then((value) => {
			  switch (value) {
			 
			    case "delete":
			      // myrow.remove().draw();

				     $.ajax({
						url: "{{URL('/')}}/removeImage",
						type: "POST",
						data: {
							_token: "{{ csrf_token() }}",
							"id": id
						},
						success: function(data){
							$("#photo"+id).remove();
						}        
				   });
			      break;
			 
			    default:
			      swal.close();
			  }
			});

		
	}
</script>
<script type="text/javascript">
	@foreach($image_ref as $result)
	$("#btn-upload{{$result->id}}").click(function(e){ 
	    $("#file-upload{{$result->id}}").click();
	}); 
	$("#file-upload{{$result->id}}").change(function(){
	    if($(this).get(0).files.length){
	        // $(".img-selected-wrapper{{$result->id}}").html("");
	        $.each($(this).get(0).files, function(index, file) {
	             
	            var reader = new FileReader(); 
	            reader.onload = function (e) {

	            	$.ajax({
						url: "{{URL('/')}}/uploadImage/{{$hotels->id}}",
						type: "POST",
						data: {
							_token: "{{ csrf_token() }}",
							"file": e.target.result,
							"image_type": "{{$result->id}}"
						},
						success: function(data){
							 // var html = "<div class='item'>";
			     //                html +=      " <img class='img-thumbnail' src='"+ data+"'>"; 
			     //                html += "</div>";
			                    
			     //            $(".img-selected-wrapper{{$result->id}}").prepend(html);                        
			     //            $('#blah').attr('src', data);
			     			location.reload();

						}        
				   });
	            } 
	            reader.readAsDataURL(file); 
	        }); 
	    }
	});
	@endforeach

 
</script>
@endsection