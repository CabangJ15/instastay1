@if(count($property_contracts) != 0)
	@foreach($property_contracts as $result)
		<?php
		$imgFile1 = $result->img_file == "" ? "uploads/profile/noprof.png" : "uploads/profile/".$result->img_file;
		?>
		<a href="{{URL('/')}}/property_contact/{{$hotels->id}}?id={{$result->id}}">
			<?php $x = 0; ?>
		@if($x == 0)
			@if($select_id == $result->id)
			<div id="userid" class="user_active" style="padding: 1em;
					    box-shadow: 0px 3px 6px #00000029;
					    height: 5em; cursor: pointer; margin-bottom: 1em;">
				<img style="float: left; width: 40px; height: 40px; border-radius: 100%;" src="{{URL('/')}}/{{$imgFile1}}"/>
				<p style="line-height: 1em; margin-left: 1em; margin-top: 0em; float: left;"><b>{{$result->fname}}<br><br><span style="font-weight: normal">{{$result->job_role}}</span></b></p>
			</div>
			@else
			<div id="userid" style="padding: 1em;
			    box-shadow: 0px 3px 6px #00000029;
			    height: 5em; cursor: pointer; margin-bottom: 1em;">
					<img style="float: left; width: 40px; height: 40px; border-radius: 100%;" src="{{URL('/')}}/{{$imgFile1}}"/>
					<p style="line-height: 1em; margin-left: 1em; margin-top: 0em; float: left;"><b>{{$result->fname}}<br><br><span style="font-weight: normal">{{$result->job_role}}</span></b></p>
					<p style="line-height: .5px; margin-left: 1em; margin-top: .5em; float: left;"></p>
				</div>
			@endif
		@else
	@if($select_id != "")
	@if($select_id == $result->id)
	<div id="userid" class="user_active" style="padding: 1em;
			    box-shadow: 0px 3px 6px #00000029;
			    height: 5em; cursor: pointer; margin-bottom: 1em;">
		<img style="float: left; width: 40px; height: 40px; border-radius: 100%;" src="{{URL('/')}}/{{$imgFile1}}"/>
		<p style="line-height: 1em; margin-left: 1em; margin-top: 0em; float: left;"><b>{{$result->fname}}<br><br><span style="font-weight: normal">{{$result->job_role}}</span></b></p>
	</div>
	@else
	<div id="userid" style="padding: 1em;
    box-shadow: 0px 3px 6px #00000029;
    height: 5em; cursor: pointer; margin-bottom: 1em;">
		<img style="float: left; width: 40px; height: 40px; border-radius: 100%;" src="{{URL('/')}}/{{$imgFile1}}"/>
		<p style="line-height: 1em; margin-left: 1em; margin-top: 0em; float: left;"><b>{{$result->fname}}<br><br><span style="font-weight: normal">{{$result->job_role}}</span></b></p>
		<p style="line-height: .5px; margin-left: 1em; margin-top: .5em; float: left;"></p>
	</div>
	@endif
	@else
	<div id="userid" style="padding: 1em;
    box-shadow: 0px 3px 6px #00000029;
    height: 5em; cursor: pointer; margin-bottom: 1em;">
		<img style="float: left; width: 40px; height: 40px; border-radius: 100%;" src="{{URL('/')}}/{{$imgFile1}}"/>
		<p style="line-height: 1em; margin-left: 1em; margin-top: 0em; float: left;"><b>{{$result->fname}}<br><br><span style="font-weight: normal">{{$result->job_role}}</span></b></p>
		<p style="line-height: .5px; margin-left: 1em; margin-top: .5em; float: left;"></p>
	</div>
	@endif
	</a>
		@endif
	<?php $x++; ?>
	@endforeach
@else
	<div>
		<center>No Contacts Found</center>
	</div>
@endif