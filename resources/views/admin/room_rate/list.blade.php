@extends('layouts.dashboardAdmin')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')

@endsection
@section('content')
	<style>
	.sidebar-left{
		width: 210px;
	}

	.dataTables_filter {
		display: none;
	}

	@media only screen and (min-width: 768px){
		html.fixed .content-body {
		    margin-left: 210px;
		}
	}

	@media only screen and (min-width: 768px){
		html.fixed .inner-wrapper {
		    padding-top: 50px;
		}
	}

	.sidebar-left .sidebar-header .sidebar-toggle {
	    visibility: hidden;
	}

	.sidebar-title{
		display: none;
	}

	.widget-summary .summary .info {
		height: 0em;
	}

	.text-primary{
		color: black !important;
		font-size: 2em;
	}

	.panel-featured-primary {
	    border-color: #fdfdfd;
	    background: #FFFFFF 0% 0% no-repeat padding-box;
		box-shadow: 0px 3px 6px #2C28284A;
		border-radius: 10px;
		opacity: 1;
	}

	.row{
		padding: .5em;
	}

	.content-body {
	    padding: 0px !important;
	    padding-top: 57px !important;
	}

	.dropdown-menu {
	    right: 0 !important;
	    left: 100 !important;
	}

	.dataTables_scrollBody{
		min-height: 17em;
	}
</style>
	{!! $menu !!}
	<section role="main" class="content-body" style="padding-top: 10px !important;">
		<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd;    padding-bottom: 0px;">
			<div class="panel-actions">
				<!-- <a href="#" class="fa fa-caret-down"></a> -->
				<!-- <a href="#" class="fa fa-times"></a> -->
			</div>
			<!-- <button style="float: right; margin-left: .5em;" class="btn btn-success btn-flat btn-pri"><i class="fa fa-floppy-o"></i> Save</button> -->
			<!-- <input type="text" placeholder="Search" class="form-control" id="myInputTextField" style="float: right;max-width: 200px;"> -->
			<h2 class="panel-title" style="font-weight:bold; color: black">Room Rates</h2>
			<p class="panel-subtitle"></p>
			
		<hr>
		</header>

		<section class="panel">
			<div class="panel-body">
				<div class="form-title">
					<a href="{{URL('/')}}/room_rate/create" style="float: right;" class="btn btn-primary btn-flat btn-pri">
						<i class="fa fa-plus"></i> Add
					</a>
				</div>

				<div class="form-body">
					<table style="width:100%" class="table table-bordered table-striped mb-none" id="table_id">
						<thead>
							<tr>
								<th>Room Rates</th>
								<th>Hours</th>
								<th>Created By</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</section>
@endsection

@section('page-script')
<script>
	$(function(){
		$('#roomRateMenu').addClass('nav-expanded');
		$('#roomRateMenu').addClass('nav-active');
		$('#masterFileMenu').addClass('nav-expanded');
		$('#masterFileMenu').addClass('nav-active');
	});

    var table = $('#table_id').DataTable( {
		"order": ['0', 'asc'],
		// "bPaginate": false,
		// "processing": true,
		dom: 'Bfrtip',
        buttons: [
	        {
	            extend: 'pageLength', className: 'datatable_button',
	            title: 'Room Rate List as of {{date('M d, Y')}}'
	        },
	        {
	            extend: 'excelHtml5', className: 'datatable_button',
	            title: 'Room Rate List as of {{date('M d, Y')}}'
	        },
	        {
	            extend: 'pdfHtml5', className: 'datatable_button',
	            title: 'Room Rate List as of {{date('M d, Y')}}'
	        },
	        {
	            extend: 'print', className: 'datatable_button',
	            title: 'Room Rate List as of {{date('M d, Y')}}'
	        },
	    ],
	    responsive: true,
	    "ajax": {
	        "url": "{{URL('/')}}/fetchRoomRate",
	        "type": "POST",
	        "data" : {
	            "_token": "{{ csrf_token() }}",
	        }
	    }
	} );

	$('#table_id tbody').on( 'click', 'button.icon-delete', function () {
				var getid = $(this).data('id');

				var myrow = table
			        .row( $(this).parents('tr') );

			swal("Are you sure?", "You will not be able to recover this Room Rates!", {
			  buttons: {
			    cancel: "Cancel",
			    catch: {
			      text: "Yes",
			      value: "delete",
			      className: "btn-danger",
			    }
			  },
			})
			.then((value) => {
			  switch (value) {
			 
			    case "delete":
			      myrow.remove().draw();

				     $.ajax({
						url: "{{URL('/')}}/room_rate/"+getid,
						type: "POST",
						data: {
							_token: "{{ csrf_token() }}",
							"_method": "DELETE",
						},
						success: function(data){
							 // console.log(data);

						}        
				   });
			      break;
			 
			    default:
			      swal.close();
			  }
			});
		    
		} );

</script>
@endsection