@extends('layouts.dashboardAdmin')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')

@endsection
@section('content')
	<style>
	.sidebar-left{
		width: 210px;
	}

	.dataTables_filter {
		display: none;
	}

	@media only screen and (min-width: 768px){
		html.fixed .content-body {
		    margin-left: 210px;
		}
	}

	@media only screen and (min-width: 768px){
		html.fixed .inner-wrapper {
		    padding-top: 50px;
		}
	}

	.sidebar-left .sidebar-header .sidebar-toggle {
	    visibility: hidden;
	}

	.sidebar-title{
		display: none;
	}

	.widget-summary .summary .info {
		height: 0em;
	}

	.text-primary{
		color: black !important;
		font-size: 2em;
	}

	.panel-featured-primary {
	    border-color: #fdfdfd;
	    background: #FFFFFF 0% 0% no-repeat padding-box;
		box-shadow: 0px 3px 6px #2C28284A;
		border-radius: 10px;
		opacity: 1;
	}

	.row{
		padding: .5em;
	}

	.content-body {
	    padding: 0px !important;
	    padding-top: 57px !important;
	}

	.dropdown-menu {
	    right: 0 !important;
	    left: 100 !important;
	}

	.dataTables_scrollBody{
		min-height: 17em;
	}
</style>
	{!! $menu !!}
	<section role="main" class="content-body" style="padding-top: 10px !important;">
		<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd;    padding-bottom: 0px;">
			<div class="panel-actions">
				<!-- <a href="#" class="fa fa-caret-down"></a> -->
				<!-- <a href="#" class="fa fa-times"></a> -->
			</div>
			<!-- <button style="float: right; margin-left: .5em;" class="btn btn-success btn-flat btn-pri"><i class="fa fa-floppy-o"></i> Save</button> -->
			<!-- <input type="text" placeholder="Search" class="form-control" id="myInputTextField" style="float: right;max-width: 200px;"> -->
			<h2 class="panel-title" style="font-weight:bold; color: black">{{$label}} Room Rates</h2>
			<p class="panel-subtitle"></p>
			
		<hr>
		</header>

		<section class="panel">
			<div class="panel-body">
				<form class="form-horizontal" enctype="multipart/form-data" action="{{URL('/')}}/room_rate{{$room_rate_id != '' ? '/'.$room_rate_id : ''}}" method="POST">
					<div class="form-title">
						<button style="float: right; margin-left: .5em;" class="btn btn-success btn-flat btn-pri"><i class="fa fa-floppy-o"></i> {{$label1}}</button>

						<a style="border: 1px solid #b8c7ce; float: right;" href="{{URL('/')}}/room_rate" style="float: right;" class="btn btn-info btn-flat btn-pri">
								<i class="fa fa-arrow-left"></i> Back
							</a>
					</div>
					<br>
					<br>
					@if(!empty($query))
					<input type="hidden" name="_method" value="PUT">
					@else
					@endif
					@csrf
					<div class="form-body">
						<div id="horizontalTab">
					        <ul>
					            <li><a href="#tab-1">General</a></li>
					            <!-- <li><a href="#tab-3">Data</a></li>
					            <li><a href="#tab-2">Bank</a></li> -->
					        </ul>

					        <div id="tab-1">
					           <div class="row">
									<div class="form-three widget-shadow">
										<div class="form-group">
											<label for="focusedinput" class="col-sm-2 control-label">Room Rate</label>
											<div class="col-sm-8">
												<input type="text" value="{{ $room_rate }}" name="room_rate" class="form-control" id="focusedinput" placeholder="Room Rate" required>
											</div>
											<div class="col-sm-2">
												<p class="help-block" style="color: red">* Required</p>
											</div>
										</div>
										<div class="form-group">
											<label for="focusedinput" class="col-sm-2 control-label">Hour</label>
											<div class="col-sm-8">
												<input type="number" min="0" value="{{ $hour }}" name="hour" class="form-control" id="focusedinput" placeholder="Hour" required>
											</div>
											<div class="col-sm-2">
												<p class="help-block" style="color: red">* Required</p>
											</div>
										</div>
									</div>
								</div>
					        </div>
					    </div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
		</section>
	</section>
@endsection

@section('page-script')
<script type="text/javascript">
	$(function(){
		$('#roomRateMenu').addClass('nav-expanded');
		$('#roomRateMenu').addClass('nav-active');
		$('#masterFileMenu').addClass('nav-expanded');
		$('#masterFileMenu').addClass('nav-active');
	});
	

	$(function(){

		$('#horizontalTab').responsiveTabs({
		    rotate: false,
	        startCollapsed: 'accordion',
	        collapsible: 'accordion',
	        setHash: false,
	        activate: function(e, tab) {
	            $('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
	        },
	        activateState: function(e, state) {
	            //console.log(state);
	            $('.info').html('Switched from <strong>' + state.oldState + '</strong> state to <strong>' + state.newState + '</strong> state!');
	        }
		});
	});
</script>
@endsection