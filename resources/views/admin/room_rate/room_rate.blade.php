<form class="form-horizontal" enctype="multipart/form-data" action="{{URL('/')}}/addRoomRate/{{$room_type_id}}" method="POST">
	@csrf
	<div id="horizontalTab" style="padding: 1em">
        <ul>
            <li><a href="#tab-1">Room Rate</a></li>
            <li><a href="#tab-2">Time Availability</a></li>
            <!-- <li><a href="#tab-3">Data</a></li>
            <li><a href="#tab-2">Bank</a></li> -->
        </ul>
        <div id="tab-2">
        	<table style="width:100%" class="table table-bordered table-striped mb-none" id="table_id1">
	            <thead>
	                <tr>
	                    <th>Check-in</th>
	                    <th>Check-out</th>
	                    <th>Amount</th>
	                    <th>Alloted</th>
	                    <th></th>
	                </tr>
	            </thead>
	            <tbody>
                @foreach($time_availability as $result)
                <tr>
                      <td><input type="hidden" name="time_availability_id[]" value="{{$result->id}}"><input type="time" placeholder="Name" class="form-control" name="c_in[]" value="{{substr($result->c_in,0,-3)}}" required="required"></td>
                      <td><input type="time" placeholder="Name" class="form-control" name="c_out[]" value="{{substr($result->c_out,0,-3)}}" required="required"></td>
                      <td><input type="number" step="any" placeholder="Amount" class="form-control" name="amount[]" value="{{$result->amount}}"  required="required"></td>
                      <td><input type="number" placeholder="Amount" class="form-control" name="alloted[]" value="{{$result->alloted}}" required="required"></td>
                      <td><a style="border-radius: 5px;" title="Remove" style="" class="btn btn-danger"><i style="color: white;" class="fa fa-trash-o fa-1x icon-delete"></i></a></td>
                  </tr>
                @endforeach
	            </tbody>
	            <tfooter>
                    <tr>
                        <td colspan="5">
                        <a id="addRow_alacart" style="margin-left: 1em; float: right; cursor: pointer; color: white;" class="addButton btn btn-small btn-info"><i style="color: white;" class="fa fa-plus"></i></a>
                          
                      </td>
                  </tr>
                </tfooter>
	        </table>
        </div>
        <div id="tab-1">
           <div class="row">
				<div class="form-three widget-shadow">
					<div class="form-group">
						<div class="col-md-12">
							<input type="hidden" value="{{$hotel_id}}" name="hotel_id"/>
							@foreach($room_rate as $data)
							<div class="col-md-2">
								@if(count($room_rate_price->where('room_rate_id', $data->id)) != 0)
									@foreach($room_rate_price->where('room_rate_id', $data->id) as $data1)
										<?php $value = $data1->amount; ?>
									@endforeach
								@else
									<?php $value = 0; ?>
								@endif
								<label>{{$data->room_rate}}</label><br>
								<input class="form-control" type="number" name="room_rate[{{$data->id}}]" step="any" value="{{$value}}"/>

							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
        </div>
        <button style="width: 100%; margin-top: 1em;" class="btn btn-info">Save</button>
    </div>
</form>

<script>
var alacart_table = $('#table_id1').DataTable( {
    "paging":   false,
    "ordering": false,
    "info":     false,
    "xScroll": true,
    "searching": false
});

$('#addRow_alacart').on( 'click', function () {

     alacart_table.row.add( [
      '<input type="hidden" name="time_availability_id[]" value="0"><input type="time" placeholder="Name" class="form-control" name="c_in[]" value="" required="required">',
      '<input type="time" class="form-control" name="c_out[]" required="required">',
      '<input type="number" step="any" placeholder="Amount" class="form-control" name="amount[]" value="0"  required="required">',
      '<input type="number" placeholder="Amount" class="form-control" name="alloted[]" value="0" required="required">',
      '<a style="border-radius: 5px;" title="Remove" style="" class="btn btn-danger"><i style="color: white;" class="fa fa-trash-o fa-1x icon-delete"></i></a>',
      ] ).draw( false ); 

    $('.product_select').select2();

} );

$('#table_id1 tbody').on( 'click', 'i.icon-delete', function () {
      var getid = $(this).data('id');

      var myrow = alacart_table
          .row( $(this).parents('tr') );

      swal("Are you sure?", "You will not be able to recover this Time Availability!", {
        buttons: {
          cancel: "Cancel",
          catch: {
            text: "Yes",
            value: "delete",
            className: "btn-danger",
          }
        },
      })
      .then((value) => {
        switch (value) {
       
          case "delete":
            myrow.remove().draw();
            break;
       
          default:
            swal.close();
        }
      });
      
  } );

$('#horizontalTab').responsiveTabs({
	    rotate: false,
        startCollapsed: 'accordion',
        collapsible: 'accordion',
        setHash: false,
        activate: function(e, tab) {
            $('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
        },
        activateState: function(e, state) {
            //console.log(state);
            $('.info').html('Switched from <strong>' + state.oldState + '</strong> state to <strong>' + state.newState + '</strong> state!');
        }
	});

</script>
