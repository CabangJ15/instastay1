@extends('layouts.dashboardAdmin')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')

@endsection
@section('content')
@section('style')
<style>
	img.output:hover {
	cursor: pointer;
	}

	img.output1:hover {
	cursor: pointer;
	}

	label span input {
		z-index: 999;
		line-height: 0;
		font-size: 50px;
		position: absolute;
		top: 0px;
		left: 0px;
		opacity: 0;
		filter: alpha(opacity = 0);
		-ms-filter: "alpha(opacity=0)";
		cursor: pointer;
		_cursor: hand;
		margin: 0;
		padding:0;
	}
</style>
@endsection

	{!! $menu !!}
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>{{$label}} User</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="{{URL('user_group')}}">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>{{$label}} User</span></li>
				</ol>
			</div>
		</header>

		<section class="panel">
			<div class="panel-body">
				<form class="form-horizontal" enctype="multipart/form-data" action="{{URL('/')}}/user_group{{$user_group_id != '' ? '/'.$user_group_id : ''}}" method="POST">
					<div class="form-title">
						<button style="float: right; margin-left: .5em;" class="btn btn-success btn-flat btn-pri"><i class="fa fa-floppy-o"></i> {{$label1}}</button>

						<a style="border: 1px solid #b8c7ce; float: right;" href="{{URL('/')}}/userGroup" style="float: right;" class="btn btn-info btn-flat btn-pri">
								<i class="fa fa-arrow-left"></i> Back
							</a>
					</div>
					<br>
					<br>
					@if(!empty($query))
					<input type="hidden" name="_method" value="PUT">
					@else
					@endif
					@csrf
					<div class="form-body">
						<div id="horizontalTab">
					        <ul>
					            <li><a href="#tab-1">General</a></li>
					        </ul>

					        <div id="tab-1">
					           <div class="row">
									<div class="form-three widget-shadow">
										<div class="form-group">
											<label for="focusedinput" class="col-sm-2 control-label">User Group</label>
											<div class="col-sm-8" style="padding: 0em 1em;">
												<input type="text" value="{{ $user_group == '' ? '' : $user_group }}" name="name" class="form-control" id="focusedinput" placeholder="User Group" required>
												@if ($errors->has('name'))
							                    <span class="help-block">
							                        <strong>{{ $errors->first('name') }}</strong>
							                    </span>
							                	@endif
											</div>
											<div class="col-sm-2">
												<p class="help-block" style="color: red">* Required</p>
											</div>
										</div>
										<div class="form-group">
								          <label class="col-sm-2 control-label">Access</label>

								          <div class="col-sm-8" style="padding: 0em 1em;">
								            <div class="well">
								            @foreach($access_list as $data)
								              <label for="{{ $data->id }}a"><input class="view" id="{{ $data->id }}a" value="{{ $data->id }}" type="checkbox" name="list[]" 
								                @if(isset($editable1[$data->id]))
								                  checked
								                @endif
								              > {{ $data->label }}</label><br>
								            @endforeach
								            </div>
								          </div>
								        </div>
								        <div class="form-group">
								          <label for="focusedinput" class="col-sm-2 control-label">
								            &nbsp;
								          </label>

								          <div class="col-sm-8" style="padding: 0em 1em;">
								          <a style="cursor: pointer;" id="select-all">Select All</a> / <a style="cursor: pointer;" id="select-no">Unselect All</a>
								          </div>
								        </div>
										<div class="form-group">
								          <label class="col-sm-2 control-label">Editable</label>

								          <div class="col-sm-8" style="padding: 0em 1em;">
								            <div class="well">
								            @foreach($access_list as $data)
								            	@if($data->with_editable == 1)
								              <label for="{{ $data->id }}b"><input class="edit" id="{{ $data->id }}b" value="{{ $data->id }}" type="checkbox" name="list1[]" 
								                @if(isset($editable2[$data->id]))
								                  checked
								                @endif
								              > {{ $data->label }}</label><br>
								              @endif
								            @endforeach
								            </div>
								          </div>
								        </div>
								        <div class="form-group">
								          <label for="focusedinput" class="col-sm-2 control-label">
								            &nbsp;
								          </label>

								          <div class="col-sm-8" style="padding: 0em 1em;">
								          <a style="cursor: pointer;" id="select-all1">Select All</a> / <a style="cursor: pointer;" id="select-no1">Unselect All</a>
								          </div>
								        </div>
										<div class="form-group">
											<label for="focusedinput" class="col-sm-2 control-label">Status</label>
											<div class="col-sm-8">
												<select class="form-control mySelect" for="focusedinput" name="flag">
													<option value="0" {{$flag == 0 ? 'selected' : ''}}>Disable</option>
													<option value="1" {{$flag == 1 ? 'selected' : ''}}>Enable</option>
												</select>
											</div>
											<div class="col-sm-2">
												<p class="help-block" style="color: red"></p>
											</div>
										</div>
									</div>
								</div>
			        		</div>
					        
					    </div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
		</section>
	</section>
@endsection

@section('page-script')
<script type="text/javascript">
	$(function(){
		$('.mySelect').select2();
		$('#accountMenu').addClass('nav-expanded');
		$('#accountMenu').addClass('nav-active');
		$('#userGroupMenu').addClass('nav-expanded');
		$('#userGroupMenu').addClass('nav-active');
	});
	

	function openKCFinder(div) {
	    // console.log(div.innerHTML);
	    window.KCFinder = {
	        callBack: function(url) {
	            window.KCFinder = null;
	            div.innerHTML = "";
	            div.innerHTML = '<img class="well" style="width: 100%; max-width: 150px;" margin-left: 0px; margin-top: 22px; visibility: visible;" id="img" src="' + url + '" /><span><input type="hidden" name="imageFile" value="'+ url +'"/></span>';
	        }
	    };
	    window.open('{{URL('/')}}/kcfinder/vc/browse.php?type=images&dir=images/public',
	        'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
	        'directories=0, resizable=1, scrollbars=0, width=800, height=600'
	    );
	}

</script>
<script>
$('#select-all').click(function(event) {   
    // Iterate each checkbox
    $(':checkbox.view').each(function() {
        this.checked = true;                        
    });
});

$('#select-no').click(function(event) {   
        // Iterate each checkbox
        $(':checkbox.view').each(function() {
            this.checked = false;                       
    });
});

$('#select-all1').click(function(event) {   
        // Iterate each checkbox
        $(':checkbox.edit').each(function() {
            this.checked = true;                        
        });
});

$('#select-no1').click(function(event) {   
        // Iterate each checkbox
        $(':checkbox.edit').each(function() {
            this.checked = false;                       
    });
});
$(function(){
	$("input#branchCode").on({
	  keydown: function(e) {
	    if (e.which === 32)
	      return false;
	  },
	  change: function() {
	    this.value = this.value.replace(/\s/g, "");
	  }
	});

	$('#horizontalTab').responsiveTabs({
	    rotate: false,
        startCollapsed: 'accordion',
        collapsible: 'accordion',
        setHash: false,
        activate: function(e, tab) {
            $('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
        },
        activateState: function(e, state) {
            //console.log(state);
            $('.info').html('Switched from <strong>' + state.oldState + '</strong> state to <strong>' + state.newState + '</strong> state!');
        }
	});

});
</script>
@endsection