@extends('layouts.dashboardAdmin')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')

@endsection
@section('content')
	<style>
	.sidebar-left{
		width: 210px;
	}

	.dataTables_filter {
		display: none;
	}

	@media only screen and (min-width: 768px){
		html.fixed .content-body {
		    margin-left: 210px;
		}
	}

	@media only screen and (min-width: 768px){
		html.fixed .inner-wrapper {
		    padding-top: 50px;
		}
	}

	.sidebar-left .sidebar-header .sidebar-toggle {
	    visibility: hidden;
	}

	.sidebar-title{
		display: none;
	}

	.widget-summary .summary .info {
		height: 0em;
	}

	.text-primary{
		color: black !important;
		font-size: 2em;
	}

	.panel-featured-primary {
	    border-color: #fdfdfd;
	    background: #FFFFFF 0% 0% no-repeat padding-box;
		box-shadow: 0px 3px 6px #2C28284A;
		border-radius: 10px;
		opacity: 1;
	}

	.row{
		padding: .5em;
	}

	.content-body {
	    padding: 0px !important;
	    padding-top: 57px !important;
	}

	.dropdown-menu {
	    right: 0 !important;
	    left: 100 !important;
	}

	.dataTables_scrollBody{
		min-height: 17em;
	}
</style>
	{!! $menu !!}
	<section role="main" class="content-body" style="padding-top: 10px !important;">
		<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd;    padding-bottom: 0px;">
			<div class="panel-actions">
				<!-- <a href="#" class="fa fa-caret-down"></a> -->
				<!-- <a href="#" class="fa fa-times"></a> -->
			</div>
			<!-- <button style="float: right; margin-left: .5em;" class="btn btn-success btn-flat btn-pri"><i class="fa fa-floppy-o"></i> Save</button> -->
			<input type="text" placeholder="Search" class="form-control" id="myInputTextField" style="float: right;max-width: 200px;">
			<h2 class="panel-title" style="font-weight:bold; color: black">Properties</h2>
			<p class="panel-subtitle"></p>
			
		<hr>
		</header>

		<section class="panel">
			<div class="panel-body">
				<div class="form-title">
					<!-- <a href="{{URL('/')}}/bed/create" class="btn btn-primary btn-flat btn-pri">
						<i class="fa fa-plus"></i> Add
					</a> -->
				</div>

				<div class="form-body">
					<table style="width:100%" class="table table-bordered table-striped mb-none" id="table_id">
							<thead>
								<tr>
									<th style="width: 100%; min-width: 100px;">ID</th>
									<th style="width: 100%; min-width: 150px;">Hotel Name</th>
									<th style="width: 100%; min-width: 100px;">Contact Person</th>
									<th style="width: 100%; min-width: 100px;">Email</th>
									<th style="width: 100%; min-width: 100px;">Position Title</th>
									<th style="width: 100%; min-width: 100px;">Telephone #</th>
									<th style="width: 100%; min-width: 100px;">Mobile #</th>
									<th style="width: 100%; min-width: 200px;">Address</th>
									<th style="width: 100%; min-width: 100px;">City</th>
									<th style="width: 100%; min-width: 100px;">Created Date</th>
									<th style="width: 100%; min-width: 100px;">Sales Officer</th>
									<th style="width: 100%; min-width: 100px;">Last Action Note</th>
									<th style="width: 100%; min-width: 100px;">Last Action Date and Time</th>
									<th style="width: 100%; min-width: 100px;"></th>
								</tr>
							</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</section>
	<form class="form-horizontal" enctype="multipart/form-data" action="{{URL('/')}}/addHotel" method="POST">
		@csrf
		<div class="modal" id="myModal">
		  <div class="modal-dialog" style="width: 100%; max-width: 800px;">
		    <div class="modal-content">

		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title">Onboarding</h4>
		        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
		      </div>

		      <!-- Modal body -->
		      <div class="modal-body" style="overflow: auto;
    			height: 450px;">
				
		      </div>

		      <!-- Modal footer -->
		      <div class="modal-footer">
		        <input type="submit" value="Save" class="btn btn-success"/>
		        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		      </div>

		    </div>
		  </div>
		</div>
	</form>
@endsection

@section('page-script')
<script>
	$(function(){
		$('#propertiesMenu').addClass('nav-expanded');
		$('#propertiesMenu').addClass('nav-active');
		makeDatatable();
	});

	function makeDatatable(){
		table = $('#table_id').DataTable();
		table.destroy();

		var table = $('#table_id').DataTable( {
			"order": ['0', 'asc'],
			// "bPaginate": false,
			// "processing": true,
			// "searching": false,
			"autoWidth": false,
			"scrollX": true,
			dom: 'Bfrtip',
	        buttons: [
		        {
		            extend: 'pageLength', className: 'datatable_button',
		            title: 'Property List as of {{date('M d, Y')}}'
		        },
		        {
		            extend: 'excelHtml5', className: 'datatable_button',
		            title: 'Property List as of {{date('M d, Y')}}'
		        },
		        {
		            extend: 'pdfHtml5', className: 'datatable_button',
		            title: 'Property List as of {{date('M d, Y')}}'
		        },
		        {
		            extend: 'print', className: 'datatable_button',
		            title: 'Property List as of {{date('M d, Y')}}'
		        },
		    ],
		    responsive: true,
		    "ajax": {
		        "url": "{{URL('/')}}/fetchLeads",
		        "type": "POST",
		        "data" : {
		            "_token": "{{ csrf_token() }}",
		            "type": 1
		        }
		    },
	        initComplete: function () {
		        $(".mySelect").select2();
		        table.columns.adjust().draw();
		    }
		} );
	}

	$('#myInputTextField').on( 'keyup', function () {
	    table.search( this.value ).draw();
	    console.log(table);
	} );

	function assignSalesRep(id, lead_id){
		$.ajax({
			url: "{{URL('/')}}/assignSalesRep/"+lead_id,
			type: "POST",
			data: {
				_token: "{{ csrf_token() }}",
				"sale_id": id
			},
			success: function(data){
				 // console.log(data);
				makeDatatable();

			}        
	   });
	}

	function fetchAddHotel(id){
		$.ajax({
			url: "{{URL('/')}}/fetchAddHotel/"+id,
			type: "POST",
			data: {
				_token: "{{ csrf_token() }}",
			},
			success: function(data){
				 // console.log(data);
				$(".modal-body").html(data);
				$(".mySelect").select2();

			}        
	   });
	}

</script>
@endsection