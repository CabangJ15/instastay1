@extends('layouts.dashboardAdmin')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')
    
@endsection

@section('content')
	<style>
		.sidebar-left{
			width: 210px;
		}

		@media only screen and (min-width: 768px){
			html.fixed .content-body {
			    margin-left: 210px;
			}
		}

		@media only screen and (min-width: 768px){
			html.fixed .inner-wrapper {
			    padding-top: 50px;
			}
		}

		.sidebar-left .sidebar-header .sidebar-toggle {
		    visibility: hidden;
		}

		.sidebar-title{
			display: none;
		}

		.widget-summary .summary .info {
			height: 0em;
		}

		.text-primary{
			color: black !important;
    		font-size: 2em;
		}

		.panel-featured-primary {
		    border-color: #fdfdfd;
		    background: #FFFFFF 0% 0% no-repeat padding-box;
			box-shadow: 0px 3px 6px #2C28284A;
			border-radius: 10px;
			opacity: 1;
		}

		.row{
			padding: .5em;
		}

		.btn {
			display: inline-block !important;
		    margin-bottom: 0 !important;
		    font-weight: normal !important;
		    text-align: center !important;
		    vertical-align: middle !important;
		    touch-action: manipulation !important;
		    cursor: pointer !important;
		    background-image: none !important;
		    border: 1px solid transparent !important;
		    white-space: nowrap !important;
		    padding: 6px 12px !important;
		    font-size: 14px !important;
		    line-height: 1.42857143 !important;
		    border-radius: 4px !important;
		    -webkit-user-select: none !important;
		    -moz-user-select: none !important;
		    -ms-user-select: none !important;
		    user-select: none !important;
		    width: 100% !important;
		    height: 2.5em !important;
		}
	</style>
	<style>
	#chartdiv {
	  width: 100%;
	  height: 300px;
	}

	</style>
	<!-- start: sidebar -->
	{!! $menu !!}
	<!-- end: sidebar -->

	<section role="main" class="content-body">

		<!-- start: page -->
		<div class="row">

			<div class="col-md-12 col-lg-12 col-xl-12">
				<div class="row">
					<div class="col-md-4 col-lg-4 col-xl-4">
						<section class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<!-- <div class="widget-summary-col widget-summary-col-icon">
										<div class="summary-icon bg-primary">
											<i class="fa fa-dollar"></i>
										</div>
									</div> -->
									<div class="widget-summary-col">
										<center>
										<div class="summary">
											<h4 class="title" style="font-size: 12px;
										    text-align: left;">TOTAL PROPERTIES</h4>
											<div class="info" style="text-align: left;">
												<span class="text-primary" style="color: black !important">755</span>
												<img style="width: 50px;
											    height: 50px;
											    float: right;
											    margin-top: -5%;
    											margin-right: 5%;" src="{{URL('/')}}/uploads/icon/upchart.jpg"/>
											</div>
										</div>
										</center>
										<div class="summary-footer">
											<center>
												<p style="text-align: left"><span style="color: #57D48D;
    											font-size: 1.12em;">+3.48%</span> Since last month</p>
											</center>
										</div>

									</div>
								</div>
							</div>
						</section>
					</div>
					<div class="col-md-4 col-lg-4 col-xl-4">
						<section style="background: #FFFFFF 0% 0% no-repeat padding-box;
					    box-shadow: 0px 3px 6px #2C282840;
					    border-radius: 10px;
					    opacity: 1;" class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<!-- <div class="widget-summary-col widget-summary-col-icon">
										<div class="summary-icon bg-primary">
											<i class="fa fa-dollar"></i>
										</div>
									</div> -->
									<div class="widget-summary-col">
										<center>
										<div class="summary">
											<h4 class="title" style="font-size: 12px;
										    text-align: left;">USERS</h4>
											<div class="info" style="text-align: left;">
												<span class="text-primary" style="color: black !important">1,000</span>
												<img style="width: 50px;
											    height: 50px;
											    float: right;
											    margin-top: -5%;
    											margin-right: 5%;" src="{{URL('/')}}/uploads/icon/downward.jpg"/>
											</div>
										</div>
										</center>
										<div class="summary-footer">
											<center>
												<p style="text-align: left"><span style="color: #DC2747;
    											font-size: 1.12em;">-1.00%</span> Since last month</p>
											</center>
										</div>

									</div>
								</div>
							</div>
						</section>
					</div>
					<div class="col-md-4 col-lg-4 col-xl-4">
						<section style="background: #FFFFFF 0% 0% no-repeat padding-box;
					    box-shadow: 0px 3px 6px #2C282840;
					    border-radius: 10px;
					    opacity: 1;" class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<!-- <div class="widget-summary-col widget-summary-col-icon">
										<div class="summary-icon bg-primary">
											<i class="fa fa-dollar"></i>
										</div>
									</div> -->
									<div class="widget-summary-col">
										<center>
										<div class="summary">
											<h4 class="title" style="font-size: 12px;
										    text-align: left;">NEW LEADS</h4>
											<div class="info" style="text-align: left;">
												<span class="text-primary" style="color: black !important">20</span>
												<img style="width: 50px;
											    height: 50px;
											    float: right;
											    margin-top: -5%;
    											margin-right: 5%;" src="{{URL('/')}}/uploads/icon/upchart.jpg"/>
											</div>
										</div>
										</center>
										<div class="summary-footer">
											<center>
												<p style="text-align: left"><span style="color: #57D48D;
    											font-size: 1.12em;">+1.01%</span> Since last month</p>
											</center>
										</div>

									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-lg-6 col-xl-12">
						<section class="panel">
						<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd;    padding-bottom: 0px;">
							<div class="panel-actions">
								<button style="background: #717171" class="btn btn-info">CALENDAR</button>
							</div>

							
							<p class="panel-subtitle">Current Sales</p><br>
							<h2 class="panel-title" style="font-size: 2em; font-weight:bold; color: black">Php 137,000</h2>
							<hr>
						</header>
						<div class="panel-body" style="padding-top: 0px;">
							<!-- <p style="font-weight:bold; color: #0DC3D8; margin-left: 24px;">New Bookings</p> -->
							<div id="chartdiv"></div>
							<hr>
							<div class="row">
								<div class="col-md-3 col-lg-3 col-xl-12" style="padding: 1em;">
									<p style="font-size: .9em;">SALES TARGET</p>
									<h4 style="font-size: 1.6rem;">PHP 11,231</h4>
								</div>
								<div class="col-md-3 col-lg-3 col-xl-12" style="padding: 1em;">
									<p style="font-size: .9em;">SALES ACTUAL</p>
									<h4 style="font-size: 1.6rem;">PHP 12,000</h4>
								</div>
								<div class="col-md-3 col-lg-3 col-xl-12" style="padding: 1em;">
									<p style="font-size: .9em;">VARIANCE</p>
									<h4 style="font-size: 1.6rem;">+1.00%</h4>
								</div>
								<div class="col-md-3 col-lg-3 col-xl-12" style="padding: 1em;background: #E6BA2A 0% 0% no-repeat padding-box;
									box-shadow: 0px 3px 6px #00000029;
									border-radius: 9px;
									opacity: 1; color: white;">
									<p style="font-size: .9em;">% Achievement</p>
									<h4 style="font-size: 1.6rem;">60%</h4>
								</div>
							</div>

						</div>
					</section>
					</div>
					<div class="col-md-6 col-lg-6 col-xl-12">
						<div class="row">
							<section class="panel">
								<header class="panel-heading" style="background: #fdfdfd; border-bottom: 1px solid #fdfdfd;    padding-bottom: 0px;">
									<div class="panel-actions">
										<a href="{{URL('/user')}}" class="btn btn-success">ADD</a>
										<!-- <a href="#" class="fa fa-times"></a> -->
									</div>

									<h2 class="panel-title" style="font-weight:bold; color: black">Team Members</h2>

									<p class="panel-subtitle"></p>
									<hr>
								</header>
								<div class="panel-body" style="padding-top: 0px;">
									<!-- <p style="font-weight:bold; color: #0DC3D8; margin-left: 24px;">Team Members</p> -->
									<table style="width:100%" class="table" id="table_id">
										
										<tbody>
											<tr>
												<td><img style="width: 30px; height: 30px; border-radius: 100%;" src="{{URL('/')}}/uploads/No-Image-Available.png"</td>
												<td>John Miachel<br><i style="font-size: 11px;">Account Manager</i></td>
												<td>Sales Target<br><span style="color: #04BF55">PHP 5,000</span></td>
												<td>Sales Actual<br><span style="color: #04BF55">PHP 4,000</span></td>
												<td>Progress Track<br>
													<div class="progress" style="margin-top: 6px; height: 6px;">
													  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80"
													aria-valuemin="0" aria-valuemax="100" style="width:80%;">
													    <span class="sr-only">80% Complete</span>
													  </div>
													</div>
												</td>
											</tr>
											<tr>
												<td><img style="width: 30px; height: 30px; border-radius: 100%;" src="{{URL('/')}}/uploads/No-Image-Available.png"</td>
												<td>Alex Smith<br><i style="font-size: 11px;">Account Manager</i></td>
												<td>Sales Target<br><span style="color: #04BF55">PHP 5,000</span></td>
												<td>Sales Actual<br><span style="color: #04BF55">PHP 2,400</span></td>
												<td>Progress Track<br>
													<div class="progress" style="margin-top: 6px; height: 6px;">
													  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="48"
													aria-valuemin="0" aria-valuemax="100" style="width:48%">
													    <span class="sr-only">48% Complete</span>
													  </div>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
									<hr>
									<div class="row">
										<div class="col-md-3 col-lg-3 col-xl-12" style="padding: 1em;">
											<p style="font-size: .9em;">SALES TARGET</p>
											<h4 style="font-size: 1.6rem;">PHP 11,231</h4>
										</div>
										<div class="col-md-3 col-lg-3 col-xl-12" style="padding: 1em;">
											<p style="font-size: .9em;">SALES ACTUAL</p>
											<h4 style="font-size: 1.6rem;">PHP 11,231</h4>
										</div>
										<div class="col-md-3 col-lg-3 col-xl-12" style="padding: 1em;">
											<p style="font-size: .9em;">VARIANCE</p>
											<h4 style="font-size: 1.6rem;">PHP 11,231</h4>
										</div>
										<div class="col-md-3 col-lg-3 col-xl-12" style="padding: 1em; background: #04BF55 0% 0% no-repeat padding-box;
											box-shadow: 0px 3px 6px #00000029;
											border-radius: 9px;
											opacity: 1; color: white;">
											<p style="font-size: .9em;">% Achievement</p>
											<h4 style="font-size: 1.6rem;">80%</h4>
										</div>
									</div>

								</div>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>

		
	</section>
	<style>

		.dt2019-12-25 { background-color: #F0FFF0; }
		.dt2019-12-25 .monthly-day-number:after { content: '\1F384'; }
		.fa-hand-o-up{
			  transform: rotate(-40deg)  scale(-1, 1);
		}
		.yellow{
			  color: #e0e024;
		}
	</style>
	
@endsection

@section('page-script')
	<!-- Chart code -->
<script>
	am4core.ready(function() {

	// Themes begin
	am4core.useTheme(am4themes_animated);
	// Themes end



	// Create chart instance
	var chart = am4core.create("chartdiv", am4charts.XYChart);

	// Add data
	chart.data = [{
	  "date": "2012-03-01",
	  "price": 20
	}, {
	  "date": "2012-03-02",
	  "price": 75
	}, {
	  "date": "2012-03-03",
	  "price": 15
	}, {
	  "date": "2012-03-04",
	  "price": 75
	}, {
	  "date": "2012-03-05",
	  "price": 158
	}, {
	  "date": "2012-03-06",
	  "price": 57
	}, {
	  "date": "2012-03-07",
	  "price": 107
	}, {
	  "date": "2012-03-08",
	  "price": 89
	}, {
	  "date": "2012-03-09",
	  "price": 75
	}, {
	  "date": "2012-03-10",
	  "price": 132
	}, {
	  "date": "2012-03-11",
	  "price": 380
	}, {
	  "date": "2012-03-12",
	  "price": 56
	}, {
	  "date": "2012-03-13",
	  "price": 169
	}, {
	  "date": "2012-03-14",
	  "price": 24
	}, {
	  "date": "2012-03-15",
	  "price": 147
	}];

	// Create axes
	var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
	dateAxis.renderer.grid.template.location = 0;
	dateAxis.renderer.minGridDistance = 50;

	var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
	valueAxis.logarithmic = true;
	valueAxis.renderer.minGridDistance = 20;

	// Create series
	var series = chart.series.push(new am4charts.LineSeries());
	series.dataFields.valueY = "price";
	series.dataFields.dateX = "date";
	series.tensionX = 0.8;
	series.strokeWidth = 3;

	var bullet = series.bullets.push(new am4charts.CircleBullet());
	bullet.circle.fill = am4core.color("#fff");
	bullet.circle.strokeWidth = 3;

	// Add cursor
	chart.cursor = new am4charts.XYCursor();
	chart.cursor.fullWidthLineX = true;
	chart.cursor.xAxis = dateAxis;
	chart.cursor.lineX.strokeWidth = 0;
	chart.cursor.lineX.fill = am4core.color("#000");
	chart.cursor.lineX.fillOpacity = 0.1;

	// Add scrollbar
	chart.scrollbarX = new am4core.Scrollbar();

	// Add a guide
	let range = valueAxis.axisRanges.create();
	range.value = 90.4;
	range.grid.stroke = am4core.color("#396478");
	range.grid.strokeWidth = 1;
	range.grid.strokeOpacity = 1;
	range.grid.strokeDasharray = "3,3";
	range.label.inside = true;
	range.label.text = "Average";
	range.label.fill = range.grid.stroke;
	range.label.verticalCenter = "bottom";

	}); // end am4core.ready()
</script>
@endsection