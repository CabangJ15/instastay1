<style>
#filtersubmit {
    position: relative;
    z-index: 1;
    left: 13px;
    top: -25px;
    color: #7B7B7B;
    cursor:pointer;
    width: 0;
}
#filter {
    padding-left: 30px;
}
</style>
<div class="row">
	<div class="form-three widget-shadow">
		<div class="form-group">
			<label class="col-sm-2 control-label"></label>
			<div class="col-sm-8">
				<center>
					<div class="col-sm-5">
						<div class="well">
							<label class="col-sm-2 control-label">
								&nbsp;
							</label>
							<center>
								<input type="hidden" name="hotelier_id" value="{{$id}}"/>
								<label class="filebutton" style="width: 100%;">
									<img style="width: 100%;" id="output" src="{{URL('/')}}/uploads/{{ $hotels->img_file == '' ? 'No-Image-Available.png' : $hotels->img_file }}"/>
									<span>
										<input style="width: 100%;" type="file" id="myfile" name="myfile" onchange="loadFile(event)" accept=".jpg, .png, .PNG, .JPG">
									</span>
								</label>
							</center>
							<script>
							  var loadFile = function(event) {
								var output = document.getElementById('output');
								output.src = URL.createObjectURL(event.target.files[0]);
							  };
							</script>
							<br>
						<a id="my-button" style="width: 100%;" class="btn btn-default btn-flat btn-pri"><i class="fa fa-camera"></i> UPLOAD LOGO</a>
					</div>
				</div>
			</center>
			</div>
			<div class="col-sm-2">
			</div>
		</div>
		<hr>
		<div class="form-group">
			<label class="col-sm-4 control-label">
				<h2 class="panel-title" style="font-weight:bold; color: #707070FA; font-size: 16px">Personal Information</h2>
			</label>
			<div class="col-sm-8">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Property Name</label>
			<div class="col-sm-8">
				<input type="text" name="hotel_name" value="{{$hotels->hotel_name}}" class="form-control" placeholder="Property Name" required>
			</div>
			<div class="col-sm-2">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Legal Entity Name</label>
			<div class="col-sm-8">
				<input type="text" name="legal_entity_name" value="{{$hotels->legal_entity_name}}" class="form-control" placeholder="Legal Entity Name">
			</div>
			<div class="col-sm-2">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Maps</label>
			<div class="col-sm-8">
				<center>
					<input id="pac-input" class="controls" type="text" placeholder="Search Box">
					<div id="map" style="width: 95%; height: 300px;"></div>
				</center>
			</div>
			<div class="col-sm-2">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Description</label>
			<div class="col-sm-8">
				<textarea name="about" class="form-control">{{$hotels->about}}</textarea>
			</div>
			<div class="col-sm-2">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Address</label>
			<div class="col-sm-8">
				<textarea name="address" class="form-control" required="required">{{$hotels->address}}</textarea>
			</div>
			<div class="col-sm-2">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">City</label>
			<div class="col-sm-3">
				<select name="city" class="mySelect form-control" required="required">
                    <option value="">Select a City</option>
                    @foreach($city as $result)
                    <option value="{{$result->city}}"
                    	@if($hotels->city == $result->city)
                    		selected
                    	@endif
                    	>{{$result->city}}</option>
                    @endforeach
                </select>
			</div>
			<label class="col-sm-2 control-label">Postal Code</label>
			<div class="col-sm-3">
				<input type="text" name="postal_code" value="{{$hotels->postcode}}" class="form-control" placeholder="Postal Code" required>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Latitude</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" id="lat" value="{{ $hotels->latitude == ''? 1000 : $hotels->latitude }}" name="latitude" placeholder="Latitude" required="required" readonly>
			</div>
			<label class="col-sm-2 control-label">Longitude</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" id="lng" value="{{ $hotels->longitude == ''? 1000 : $hotels->longitude }}" name="longitude" placeholder="Longitude" required="required" readonly>
			</div>
		</div>
		<hr>
		<div class="form-group">
			<label class="col-sm-4 control-label">
				<h2 class="panel-title" style="font-weight:bold; color: #707070FA; font-size: 16px">Contact Information</h2>
			</label>
			<div class="col-sm-8">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-5 control-label">
				Does This Property Use Channel Manager System?</h2>
			</label>
			<div class="col-sm-5">
				<label for="has_channel_manager" style="margin-right: 1em;">
				<input type="radio" 
			       id="has_channel_manager"
			       name="has_channel_manager"
			       value="1"
			       @if($hotels->has_channel_manager == 1)
			       	checked
			       @endif
			       />
			        Yes
				</label>

				<label for="has_channel_manager1" style="margin-right: 1em;">
				<input type="radio" 
			       id="has_channel_manager1"
			       name="has_channel_manager"
			       value="0"
			       @if($hotels->has_channel_manager == 0)
			       	checked
			       @endif
			       />
			        No
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-5 control-label">
				Does This Property Have Different Name Previously?</h2>
			</label>
			<div class="col-sm-5">
				<label for="has_diff_name" style="margin-right: 1em;">
				<input type="radio" 
			       id="has_diff_name"
			       name="has_diff_name"
			       value="1"
			       @if($hotels->has_diff_name == 1)
			       	checked
			       @endif
			       />
			        Yes
				</label>

				<label for="has_diff_name1" style="margin-right: 1em;">
				<input type="radio" 
			       id="has_diff_name1"
			       name="has_diff_name"
			       value="0"
			       @if($hotels->has_diff_name == 0)
			       	checked
			       @endif
			       />
			        No
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-5 control-label">
				Does This Property Belong to Particular Hotel Chain?</h2>
			</label>
			<div class="col-sm-5">
				<label for="is_chain_hotel" style="margin-right: 1em;">
				<input type="radio" 
			       id="is_chain_hotel"
			       name="is_chain_hotel"
			       value="1"
			       @if($hotels->is_chain_hotel == 1)
			       	checked
			       @endif
			       />
			        Yes
				</label>

				<label for="is_chain_hotel1" style="margin-right: 1em;">
				<input type="radio" 
			       id="is_chain_hotel1"
			       name="is_chain_hotel"
			       value="0"
			       @if($hotels->is_chain_hotel == 0)
			       	checked
			       @endif
			       />
			        No
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-1 control-label"></label>
			<div class="col-md-3">
				<input id="filter" class="form-control" type="text" value="{{$hotels->facebook}}" name="facebook" placeholder="Connect Facebook" />
			<i id="filtersubmit" class="fa fa-facebook"></i>
			</div>
			<div class="col-md-3">
				<input id="filter" class="form-control" type="text" value="{{$hotels->twitter}}" name="twitter"  placeholder="Connect Twitter" />
			<i id="filtersubmit" class="fa fa-twitter"></i>
			</div>
			<div class="col-md-3">
				<input id="filter" class="form-control" value="{{$hotels->instagram}}" type="text" name="instagram"  placeholder="Connect Instagram" />
			<i id="filtersubmit" class="fa fa-instagram"></i>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Primary Email</label>
			<div class="col-sm-8">
				<input type="text" name="email" value="{{$hotels->email}}" class="form-control" placeholder="Primary Email" required>
			</div>
			<div class="col-sm-2">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Business Mobile Number</label>
			<div class="col-sm-8">
				<input type="text" name="telnos" value="{{$hotels->telnos}}" class="form-control" placeholder="Business Mobile Number" required>
			</div>
			<div class="col-sm-2">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Business Landline Number</label>
			<div class="col-sm-8">
				<input type="text" name="biznos" value="{{$hotels->biznos}}" class="form-control" placeholder="Business Landline Number">
			</div>
			<div class="col-sm-2">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Website</label>
			<div class="col-sm-8">
				<input type="url" name="website" value="{{$hotels->website}}" class="form-control" placeholder="Website">
			</div>
			<div class="col-sm-2">
			</div>
		</div>
	</div>
</div>
<script>
  var map, markers = [];
  var styleMap = [
        {
          "featureType": "administrative",
          "elementType": "geometry",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "transit",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        }
      ];
      
  function initMap() {
  	console.clear();
  	
    	geoSuccess(14.5712393, 121.0660037);

  }

  function geoSuccess(lat, lng){
  	@if($hotels->id != '')
    	var pos = {
            lat: {{$hotels->latitude == ''? 1000 : $hotels->latitude}},
            lng: {{$hotels->longitude == ''? 1000 : $hotels->longitude}}
          };
    @else
  		var pos = {
            lat: lat,
            lng: lng
          };
     @endif

          $('#lat').val(lat);
    	  $('#lng').val(lng);

          map = new google.maps.Map(document.getElementById('map'), {
	          center: pos,
	          zoom: 15,
	          disableDefaultUI: true,
      		  styles: styleMap
	        });

          marker= new google.maps.Marker({
	              map: map,
				  draggable:true,
	              position: pos
	            });

          google.maps.event.addListener(marker, 'dragend', function (event) {
		         $('#lat').val(this.getPosition().lat());
	        	 $('#lng').val(this.getPosition().lng());
			});

			markers.push(marker);

            var input = document.getElementById('pac-input');
	        var searchBox = new google.maps.places.SearchBox(input);
	        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	        // Bias the SearchBox results towards current map's viewport.
	        map.addListener('bounds_changed', function() {
	          searchBox.setBounds(map.getBounds());
	        });

	        // Listen for the event fired when the user selects a prediction and retrieve
	        // more details for that place.
	        searchBox.addListener('places_changed', function() {
	          var places = searchBox.getPlaces();

	          if (places.length == 0) {
	            return;
	          }

	          // Clear out the old markers.
	          markers.forEach(function(marker) {
	            marker.setMap(null);
	          });

	          // For each place, get the icon, name and location.
	          var bounds = new google.maps.LatLngBounds();
	          places.forEach(function(place) {
	            if (!place.geometry) {
	              console.log("Returned place contains no geometry");
	              return;
	            }
	            clearMarkers();
	            // Create a marker for each place.
	            marker= new google.maps.Marker({
	              map: map,
				  draggable:true,
	              position: place.geometry.location
	            });

	            google.maps.event.addListener(marker, 'dragend', function (event) {
			         $('#lat').val(this.getPosition().lat());
		        	 $('#lng').val(this.getPosition().lng());
				});

	            $('#lat').val(place.geometry.location.lat());
		        $('#lng').val(place.geometry.location.lng());

		        markers.push(marker);

	            if (place.geometry.viewport) {
	              // Only geocodes have viewport.
	              bounds.union(place.geometry.viewport);
	            } else {
	              bounds.extend(place.geometry.location);
	            }
	          });
	          map.fitBounds(bounds);
	        });

          google.maps.event.addListener(map, 'click', function( event ){
          	  clearMarkers();
			  var pos1 = {
		            lat: event.latLng.lat(),
		            lng: event.latLng.lng()
		          };

		        $('#lat').val(event.latLng.lat());
		        $('#lng').val(event.latLng.lng());
		        var marker = new google.maps.Marker({
				    position: pos1,
				    map: map,
				    draggable:true,
				  });
		        

		        google.maps.event.addListener(marker, 'dragend', function (event) {
			         $('#lat').val(this.getPosition().lat());
		        	 $('#lng').val(this.getPosition().lng());
				});

				markers.push(marker);

			});

  }

  function clearMarkers() {
    setMapOnAll(null);
    markers = [];
  }

  function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdjwFLNZw_KngodwPrdlYV1n_49O-vx8c&libraries=places&callback=initMap"
    async defer></script>
