@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')

@endsection
@section('content')
@section('style')
<style>
	img.output:hover {
	cursor: pointer;
	}

	img.output1:hover {
	cursor: pointer;
	}

	label span input {
		z-index: 999;
		line-height: 0;
		font-size: 50px;
		position: absolute;
		top: 0px;
		left: 0px;
		opacity: 0;
		filter: alpha(opacity = 0);
		-ms-filter: "alpha(opacity=0)";
		cursor: pointer;
		_cursor: hand;
		margin: 0;
		padding:0;
	}
</style>
<style>
.dataTables_filter, .dataTables_info { display: none; }
.dataTables_wrapper .ui-toolbar {
    visibility: hidden;
}
</style>
@endsection

	{!! $menu !!}
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>{{$label}} Main Facilities</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="{{URL('sub_facilities')}}">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>{{$label}} Main Facilities</span></li>
				</ol>
			</div>
		</header>

		<section class="panel">
			<div class="panel-body">
				<form class="form-horizontal" enctype="multipart/form-data" action="{{URL('/')}}/sub_facilities{{$sub_facilities_id != '' ? '/'.$sub_facilities_id : ''}}" method="POST">
					<div class="form-title">
						<button style="float: right; margin-left: .5em;" class="btn btn-success btn-flat btn-pri"><i class="fa fa-floppy-o"></i> {{$label1}}</button>

						<a style="border: 1px solid #b8c7ce; float: right;" href="{{URL('/')}}/sub_facilities" style="float: right;" class="btn btn-info btn-flat btn-pri">
								<i class="fa fa-arrow-left"></i> Back
							</a>
					</div>
					<br>
					<br>
					@if(!empty($query))
					<input type="hidden" name="_method" value="PUT">
					@else
					@endif
					@csrf
					<div class="form-body">
						<div id="horizontalTab">
					        <ul>
					            <li><a href="#tab-1">General</a></li>
					            <!-- <li><a href="#tab-3">Data</a></li>
					            <li><a href="#tab-2">Bank</a></li> -->
					        </ul>

					        <div id="tab-1">
					           <div class="row">
									<div class="form-three widget-shadow">
										<div class="form-group">
											<label for="focusedinput" class="col-sm-2 control-label">Icon</label>
											<div class="col-sm-8">
												<div style="width: 150px" class="image" onclick="openKCFinder(this)"><img style="width: 100%; max-width: 150px; padding: 10px;" class="well" src="{{$icon_file}}"/><input type="hidden" name="icon_file" value="{{$icon_file}}"></div>
											</div>
											<div class="col-sm-2">
											</div>
										</div>
										<div class="form-group">
											<label for="focusedinput" class="col-sm-2 control-label">Main Facilities</label>
											<div class="col-sm-8">
												<select class="form-control mySelect" name="main_facilities_id" required="required">
													<option value="">Select Main Facilties</option>
													@foreach($main_facilities as $result)
													<option value="{{$result->id}}"
														@if($main_facilities_id == $result->id)
															SELECTED
														@endif
														>{{$result->facilities_ref}}</option>
													@endforeach
												</select>
											</div>
											<div class="col-sm-2">
												<p class="help-block" style="color: red">* Required</p>
											</div>
										</div>
										<div class="form-group">
											<label for="focusedinput" class="col-sm-2 control-label">Sub Facilities</label>
											<div class="col-sm-8">
												<input type="text" value="{{ $sub_facilities }}" name="sub_facilities" class="form-control" id="focusedinput" placeholder="Sub Facilities" required>
											</div>
											<div class="col-sm-2">
												<p class="help-block" style="color: red">* Required</p>
											</div>
										</div>
									</div>
								</div>
					        </div>
					    </div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
		</section>
	</section>
@endsection

@section('page-script')
<script type="text/javascript">
	$(function(){
		$('#masterFileMenu').addClass('nav-expanded');
		$('#masterFileMenu').addClass('nav-active');
		$('#facilitiesMenu').addClass('nav-expanded');
		$('#facilitiesMenu').addClass('nav-active');
		$('#subFacilitiesMenu').addClass('nav-expanded');
		$('#subFacilitiesMenu').addClass('nav-active');
		$('#settingMenu').addClass('nav-expanded');
		$('#settingMenu').addClass('nav-active');
		$(".mySelect").select2();
	});
	

	$(function(){

		$('#horizontalTab').responsiveTabs({
		    rotate: false,
	        startCollapsed: 'accordion',
	        collapsible: 'accordion',
	        setHash: false,
	        activate: function(e, tab) {
	            $('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
	        },
	        activateState: function(e, state) {
	            //console.log(state);
	            $('.info').html('Switched from <strong>' + state.oldState + '</strong> state to <strong>' + state.newState + '</strong> state!');
	        }
		});
	});

	function openKCFinder(div) {
	    // console.log(div.innerHTML);
	    window.KCFinder = {
	        callBack: function(url) {
	            window.KCFinder = null;
	            div.innerHTML = "";
	            div.innerHTML = '<img class="well" style="width: 100%; max-width: 150px;" margin-left: 0px; margin-top: 22px; visibility: visible;" id="img" src="' + url + '" /><span><input type="hidden" name="icon_file" value="'+ url +'"/></span>';
	        }
	    };
	    window.open('{{URL('/')}}/kcfinder/vc/browse.php?type=images&dir=images/public',
	        'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
	        'directories=0, resizable=1, scrollbars=0, width=800, height=600'
	    );
	}
</script>
@endsection