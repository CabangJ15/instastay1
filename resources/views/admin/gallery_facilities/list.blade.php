@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')

@endsection
@section('content')

	{!! $menu !!}
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>Gallery Facilities</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="{{URL('gallery_facilities')}}">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Gallery Facilities</span></li>
				</ol>
			</div>
		</header>

		<section class="panel">
			<div class="panel-body">
				<div class="form-title">
					<a href="{{URL('/')}}/gallery_facilities/create" style="float: right;" class="btn btn-primary btn-flat btn-pri">
						<i class="fa fa-plus"></i> Add
					</a>
				</div>

				<div class="form-body">
					<table style="width:100%" class="table table-bordered table-striped mb-none" id="table_id">
						<thead>
							<tr>
								<th>Gallery Facilities</th>
								<th>Created By</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</section>
@endsection

@section('page-script')
<script>
	$(function(){
		$('#masterFileMenu').addClass('nav-expanded');
		$('#masterFileMenu').addClass('nav-active');
		$('#facilitiesMenu').addClass('nav-expanded');
		$('#facilitiesMenu').addClass('nav-active');
		$('#galleryFacilitiesMenu').addClass('nav-expanded');
		$('#galleryFacilitiesMenu').addClass('nav-active');
		$('#settingMenu').addClass('nav-expanded');
		$('#settingMenu').addClass('nav-active');
	});

    var table = $('#table_id').DataTable( {
		"order": ['0', 'asc'],
		// "bPaginate": false,
		// "processing": true,
		dom: 'Bfrtip',
        buttons: [
	        {
	            extend: 'pageLength', className: 'datatable_button',
	            title: 'Gallery Facilities List as of {{date('M d, Y')}}'
	        },
	        {
	            extend: 'excelHtml5', className: 'datatable_button',
	            title: 'Gallery Facilities List as of {{date('M d, Y')}}'
	        },
	        {
	            extend: 'pdfHtml5', className: 'datatable_button',
	            title: 'Gallery Facilities List as of {{date('M d, Y')}}'
	        },
	        {
	            extend: 'print', className: 'datatable_button',
	            title: 'Gallery Facilities List as of {{date('M d, Y')}}'
	        },
	    ],
	    responsive: true,
	    "ajax": {
	        "url": "{{URL('/')}}/fetchGalleryFacilities",
	        "type": "POST",
	        "data" : {
	            "_token": "{{ csrf_token() }}",
	        }
	    }
	} );

	$('#table_id tbody').on( 'click', 'button.icon-delete', function () {
				var getid = $(this).data('id');

				var myrow = table
			        .row( $(this).parents('tr') );

			swal("Are you sure?", "You will not be able to recover this Gallery Facilities!", {
			  buttons: {
			    cancel: "Cancel",
			    catch: {
			      text: "Yes",
			      value: "delete",
			      className: "btn-danger",
			    }
			  },
			})
			.then((value) => {
			  switch (value) {
			 
			    case "delete":
			      myrow.remove().draw();

				     $.ajax({
						url: "{{URL('/')}}/gallery_facilities/"+getid,
						type: "POST",
						data: {
							_token: "{{ csrf_token() }}",
							"_method": "DELETE",
						},
						success: function(data){
							 // console.log(data);

						}        
				   });
			      break;
			 
			    default:
			      swal.close();
			  }
			});
		    
		} );

</script>
@endsection