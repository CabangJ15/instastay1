@extends('layouts.dashboardAdmin')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')

@endsection
@section('content')
@section('style')
<style>
	img.output:hover {
	cursor: pointer;
	}

	img.output1:hover {
	cursor: pointer;
	}

	label span input {
		z-index: 999;
		line-height: 0;
		font-size: 50px;
		position: absolute;
		top: 0px;
		left: 0px;
		opacity: 0;
		filter: alpha(opacity = 0);
		-ms-filter: "alpha(opacity=0)";
		cursor: pointer;
		_cursor: hand;
		margin: 0;
		padding:0;
	}
</style>
@endsection

	{!! $menu !!}
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>{{$label}} User</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="{{URL('user')}}">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>{{$label}} User</span></li>
				</ol>
			</div>
		</header>

		<section class="panel">
			<div class="panel-body">
				<form class="form-horizontal" enctype="multipart/form-data" action="{{URL('/')}}/user{{$user_id != '' ? '/'.$user_id : ''}}" method="POST">
					<div class="form-title">
						<button style="float: right; margin-left: .5em;" class="btn btn-success btn-flat btn-pri"><i class="fa fa-floppy-o"></i> {{$label1}}</button>

						<a style="border: 1px solid #b8c7ce; float: right;" href="{{URL('/')}}/user" style="float: right;" class="btn btn-info btn-flat btn-pri">
								<i class="fa fa-arrow-left"></i> Back
							</a>
					</div>
					<br>
					<br>
					@if(!empty($query))
					<input type="hidden" name="_method" value="PUT">
					@else
					@endif
					@csrf
					<div class="form-body">
						<div id="horizontalTab">
					        <ul>
					            <li><a href="#tab-1">General</a></li>
					        </ul>

					        <div id="tab-1">
					           <div class="row">
									<div class="form-three widget-shadow">
										<div class="form-group">
											<label for="focusedinput" class="col-sm-2 control-label"></label>
											<div class="col-sm-8">
												<div style="width: 150px" class="image" onclick="openKCFinder(this)"><img style="width: 100%; max-width: 150px; padding: 10px;" class="well" src="{{$img_file}}"/><input type="hidden" name="imageFile" value="{{$img_file}}"></div>
											</div>
											<div class="col-sm-2">
											</div>
										</div>
										<div class="form-group">
											<label for="focusedinput" class="col-sm-2 control-label">Full Name</label>
											<div class="col-sm-8">
												<input type="text" value="{{ $fname == '' ? old('name') : $fname}}" name="name" class="form-control" id="focusedinput" placeholder="Full Name" required>
												@if ($errors->has('name'))
							                    <span class="help-block">
							                        <strong>{{ $errors->first('name') }}</strong>
							                    </span>
							                	@endif
											</div>
											<div class="col-sm-2">
												<p class="help-block" style="color: red">* Required</p>
											</div>
										</div>
										<div class="form-group">
											<label for="focusedinput" class="col-sm-2 control-label">Email Address</label>
											<div class="col-sm-8">
												<input type="text" value="{{ $email == '' ? old('email') : $email}}" name="email" class="form-control" placeholder="Email Address">
												@if ($errors->has('email'))
							                    <span class="help-block">
							                        <strong>{{ $errors->first('email') }}</strong>
							                    </span>
							                	@endif
											</div>
											<div class="col-sm-2">
												<p class="help-block" style="color: red">* Unique</p>
											</div>
										</div>
										<div class="form-group">
											<label for="focusedinput" class="col-sm-2 control-label">User Group</label>
											<div class="col-sm-8">
												<select type="text" name="group_id" class="form-control mySelect" required="required">
													@foreach($group_list->where('id', '!=', 1) as $data1)
														<option value="{{$data1->id}}"
															@if(!empty($query))
																@if($group_id == $data1->id)
																	selected
																@endif
															@endif
															>{{$data1->user_group}}</option>
													@endforeach;
												</select>
											</div>
											<div class="col-sm-2">
												<p class="help-block" style="color: red">* Required</p>
											</div>
										</div>
									</div>
								</div>
					        </div>
					        
					    </div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
		</section>
	</section>
@endsection

@section('page-script')
<script type="text/javascript">
	$(function(){
		$('.mySelect').select2();
		$('#accountMenu').addClass('nav-expanded');
		$('#accountMenu').addClass('nav-active');
		$('#adminUserMenu').addClass('nav-expanded');
		$('#adminUserMenu').addClass('nav-active');
	});
	

	function openKCFinder(div) {
	    // console.log(div.innerHTML);
	    window.KCFinder = {
	        callBack: function(url) {
	            window.KCFinder = null;
	            div.innerHTML = "";
	            div.innerHTML = '<img class="well" style="width: 100%; max-width: 150px;" margin-left: 0px; margin-top: 22px; visibility: visible;" id="img" src="' + url + '" /><span><input type="hidden" name="imageFile" value="'+ url +'"/></span>';
	        }
	    };
	    window.open('{{URL('/')}}/kcfinder/vc/browse.php?type=images&dir=images/public',
	        'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
	        'directories=0, resizable=1, scrollbars=0, width=800, height=600'
	    );
	}

</script>
<script>
$(function(){
	$("input#branchCode").on({
	  keydown: function(e) {
	    if (e.which === 32)
	      return false;
	  },
	  change: function() {
	    this.value = this.value.replace(/\s/g, "");
	  }
	});

	$('#horizontalTab').responsiveTabs({
	    rotate: false,
        startCollapsed: 'accordion',
        collapsible: 'accordion',
        setHash: false,
        activate: function(e, tab) {
            $('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
        },
        activateState: function(e, state) {
            //console.log(state);
            $('.info').html('Switched from <strong>' + state.oldState + '</strong> state to <strong>' + state.newState + '</strong> state!');
        }
	});

});
</script>
@endsection