@extends('layouts.dashboardAdmin')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')

@endsection
@section('content')

	{!! $menu !!}
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>User</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="{{URL('user')}}">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>User</span></li>
				</ol>
			</div>
		</header>

		<section class="panel">
			<div class="panel-body">
				<div class="form-title">
					<a href="{{URL('/')}}/user/create" style="float: right;" class="btn btn-primary btn-flat btn-pri">
						<i class="fa fa-plus"></i> Add
					</a>
				</div>

				<div class="form-body">
					<div class="form-body">
						<table style="width:100%" class="table table-bordered table-striped mb-none" id="table_id">
								<thead>
									<tr>
										<th>Name</th>
										<th>Email</th>
										<th>Position</th>
										<th>Created By</th>
										<th>Action</th>
									</tr>
								</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</section>
@endsection

@section('page-script')
<script>
	$(function(){
		$('#accountMenu').addClass('nav-expanded');
		$('#accountMenu').addClass('nav-active');
		$('#adminUserMenu').addClass('nav-expanded');
		$('#adminUserMenu').addClass('nav-active');
	});

    var table = $('#table_id').DataTable( {
		"order": ['0', 'asc'],
		// "bPaginate": false,
		// "processing": true,
		dom: 'Bfrtip',
        buttons: [
	        {
	            extend: 'pageLength', className: 'datatable_button',
	            title: 'User List as of {{date('M d, Y')}}'
	        },
	        {
	            extend: 'excelHtml5', className: 'datatable_button',
	            title: 'User List as of {{date('M d, Y')}}'
	        },
	        {
	            extend: 'pdfHtml5', className: 'datatable_button',
	            title: 'User List as of {{date('M d, Y')}}'
	        },
	        {
	            extend: 'print', className: 'datatable_button',
	            title: 'User List as of {{date('M d, Y')}}'
	        },
	    ],
	    responsive: true,
	    "ajax": {
	        "url": "{{URL('/')}}/fetchUser",
	        "type": "POST",
	        "data" : {
	            "_token": "{{ csrf_token() }}",
	        }
	    }
	} );

	$('#table_id tbody').on( 'click', 'button.icon-reset', function () {
				var getid = $(this).data('id');

				var myrow = table
			        .row( $(this).parents('tr') );

			swal("Are you sure?", "You want to reset this User's password!", {
			  buttons: {
			    cancel: "Cancel",
			    catch: {
			      text: "Yes",
			      value: "delete",
			      className: "btn-danger",
			    }
			  },
			})
			.then((value) => {
			  switch (value) {
			 
			    case "delete":
			      // myrow.remove().draw();

				     $.ajax({
						url: "{{URL('/')}}/resetPassword/"+getid,
						type: "POST",
						data: {
							_token: "{{ csrf_token() }}",
						},
						success: function(data){
							 // console.log(data);
							 swal("123qwe", "Reset Password Completed", "success");

						}        
				   });
			      break;
			 
			    default:
			      swal.close();
			  }
			});
		    
		} );

	$('#table_id tbody').on( 'click', 'button.icon-delete', function () {
				var getid = $(this).data('id');

				var myrow = table
			        .row( $(this).parents('tr') );

			swal("Are you sure?", "You will not be able to recover this User!", {
			  buttons: {
			    cancel: "Cancel",
			    catch: {
			      text: "Yes",
			      value: "delete",
			      className: "btn-danger",
			    }
			  },
			})
			.then((value) => {
			  switch (value) {
			 
			    case "delete":
			      myrow.remove().draw();

				     $.ajax({
						url: "{{URL('/')}}/user/"+getid,
						type: "POST",
						data: {
							_token: "{{ csrf_token() }}",
							"_method": "DELETE",
						},
						success: function(data){
							 // console.log(data);

						}        
				   });
			      break;
			 
			    default:
			      swal.close();
			  }
			});
		    
		} );

</script>
@endsection