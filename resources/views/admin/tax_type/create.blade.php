@extends('layouts.dashboard')
@section('title', 'Instastay')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')

@endsection
@section('content')
@section('style')
<style>
	img.output:hover {
	cursor: pointer;
	}

	img.output1:hover {
	cursor: pointer;
	}

	label span input {
		z-index: 999;
		line-height: 0;
		font-size: 50px;
		position: absolute;
		top: 0px;
		left: 0px;
		opacity: 0;
		filter: alpha(opacity = 0);
		-ms-filter: "alpha(opacity=0)";
		cursor: pointer;
		_cursor: hand;
		margin: 0;
		padding:0;
	}
</style>
<style>
.dataTables_filter, .dataTables_info { display: none; }
.dataTables_wrapper .ui-toolbar {
    visibility: hidden;
}
</style>
@endsection

	{!! $menu !!}
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>{{$label}} tax Type</h2>
		
			<div class="right-wrapper pull-right" style="margin-right: 3em;">
				<ol class="breadcrumbs">
					<li>
						<a href="{{URL('tax')}}">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>{{$label}} tax Type</span></li>
				</ol>
			</div>
		</header>

		<section class="panel">
			<div class="panel-body">
				<form class="form-horizontal" enctype="multipart/form-data" action="{{URL('/')}}/tax{{$tax_type_id != '' ? '/'.$tax_type_id : ''}}" method="POST">
					<div class="form-title">
						<button style="float: right; margin-left: .5em;" class="btn btn-success btn-flat btn-pri"><i class="fa fa-floppy-o"></i> {{$label1}}</button>

						<a style="border: 1px solid #b8c7ce; float: right;" href="{{URL('/')}}/tax" style="float: right;" class="btn btn-info btn-flat btn-pri">
								<i class="fa fa-arrow-left"></i> Back
							</a>
					</div>
					<br>
					<br>
					@if(!empty($query))
					<input type="hidden" name="_method" value="PUT">
					@else
					@endif
					@csrf
					<div class="form-body">
						<div id="horizontalTab">
					        <ul>
					            <li><a href="#tab-1">General</a></li>
					            <!-- <li><a href="#tab-3">Data</a></li>
					            <li><a href="#tab-2">Bank</a></li> -->
					        </ul>

					        <div id="tab-1">
					           <div class="row">
									<div class="form-three widget-shadow">
										<div class="form-group">
											<label for="focusedinput" class="col-sm-2 control-label">Tax Type</label>
											<div class="col-sm-8">
												<input type="text" value="{{ $tax_type }}" name="tax_type" class="form-control" id="focusedinput" placeholder="tax Type" required>
											</div>
											<div class="col-sm-2">
												<p class="help-block" style="color: red">* Required</p>
											</div>
										</div>
									</div>
								</div>
					        </div>
					    </div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
		</section>
	</section>
@endsection

@section('page-script')
<script type="text/javascript">
	$(function(){
		$('#masterFileMenu').addClass('nav-expanded');
		$('#masterFileMenu').addClass('nav-active');
		$('#taxTypeMenu').addClass('nav-expanded');
		$('#taxTypeMenu').addClass('nav-active');
	});
	

	$(function(){

		$('#horizontalTab').responsiveTabs({
		    rotate: false,
	        startCollapsed: 'accordion',
	        collapsible: 'accordion',
	        setHash: false,
	        activate: function(e, tab) {
	            $('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
	        },
	        activateState: function(e, state) {
	            //console.log(state);
	            $('.info').html('Switched from <strong>' + state.oldState + '</strong> state to <strong>' + state.newState + '</strong> state!');
	        }
		});
	});
</script>
@endsection