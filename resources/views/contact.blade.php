@extends('layouts.main')
@section('title', 'Instastay')
@section('content')
<style>
    #headerTitle{
        font-size: 36px;
        line-height: 1.1em;
    }

    blockquote.large:before {
        float:left;
    }

    .container{
        max-width: 100%;
    }

    .row {
        /*padding: 2em;*/
    }

   /* .first_cont{
        margin-top: 4em;
    }
*/
    @media (max-width: 768px){
        .first_cont{
            margin-top: 0em;
        } 

        .navbar-inverse .navbar-inner {
            padding: 0 0 0px;
        }
    }

    .cityImg1{

        margin-top: 47%;  
    }
        

</style>

<!-- 2nd Section -->

<!-- 2nd Section -->

    <div class="container first_cont">
        <div class="row">
            <div class="col-md-6" style="padding: 5%">
                <center><img src="{{URL('/')}}/assets/images/contact.png"/></center>
            </div>
            <div class="col-md-6" style="padding: 5%">
            <form id="regForm" method="POST" action="{{URL('contactMail')}}">
                @csrf
                <h2>Contact Us</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Name</label>
                        <input type="text" name="fname" class="form-control" id="focusedinput" placeholder="Full Name" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Email Address</label>
                        <input type="email" name="email" class="form-control" id="focusedinput" placeholder="myemail@address.com" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Mesage</label>
                        <textarea rows="4" name="text" class="form-control" id="focusedinput" placeholder="Message" required="required"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <br>
                        <button  style="width: 100%;" class="btn btn-info">Send</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</section>
<hr>


@endsection