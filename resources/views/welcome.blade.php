@extends('layouts.main')
@section('title', 'Instastay')
@section('content')
<style>
    #headerTitle{
        font-size: 36px;
        line-height: 1.1em;
    }

    blockquote.large:before {
        float:left;
    }

    .container{
        max-width: 100%;
    }

    .row {
        /*padding: 2em;*/
    }

   /* .first_cont{
        margin-top: 4em;
    }
*/
    @media (max-width: 768px){
        .first_cont{
            margin-top: 0em;
        } 

        .navbar-inverse .navbar-inner {
            padding: 0 0 0px;
        }
    }

    .cityImg1{

        margin-top: 47%;  
    }
        

</style>

<!-- 2nd Section -->

<!-- 2nd Section -->

    <div class="container first_cont">
        <div class="row">
            <div class="col-md-7" style="
                    background: url({{URL('/')}}/uploads/website/1.png);
                    background-size: 250% 250%;
                    /* background-position: right; */
                    background-position-y: 72%;
                    background-position-x: 97%;
                    background-repeat: no-repeat;">
                <div class="alignright flyLeft">

                    <img style="padding: 4%;" src="uploads/instastay_phone.png"/>
                </div>
            </div>
            <div class="col-md-5" style="padding: 5%">
                <div class="alignright flyRight">
                    <h1><b>BOOK ANYWHERE</b></h1>
                    <h1><b>CHECK-IN NOW</b></h1>
                    <br>
                    <p style="font-size: larger" class="alignRight">Instastay is a hotel booking platform that gives people the power to book a room of choice to rest, recharge and relax anytime, anywhere instantly</p>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-sm-6" style="width: 45%;
                            float: right;
                            ">
                            <img src="uploads/android.png"/>
                        </div>
                        <div class="col-sm-6" style="width: 45%;
                            float: right;
                            ">
                            <img src="uploads/apple.png"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- 2nd Section -->
<section id="pay">
    <div class="container" style="">
        <div class="row">
            <div class="col-md-5" style="padding: 5%;
                margin-top: 9%;">
                <div class="alignLeft flyIn">
                    <h1><b>PAY ONLY FOR THE HOURS YOU STAY</b></h1>
                    <br>
                    <p style="font-size: larger" class="">Be free from check-in times and pay only for the hours you actually stay for.</p>
                </div>
            </div>
            <div class="col-md-7" style="
                    background: url({{URL('/')}}/uploads/website/4.png);
                    background-size: 100% 100%;
                    /* background-position: right; */
                    background-position-y: 100%;
                    /*background-position-x: 97%;*/
                    background-repeat: no-repeat;">
                <div class="alignLeft flyLeft">
                    <center><img style="padding: 4%;
                    max-width: 69%;" src="uploads/people_graphics.svg"/></center>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- 2nd Section -->
<section id="pay1">
    <div class="container" style="">
        <div class="row">
            <div class="col-md-7" style="
                    background: url({{URL('/')}}/uploads/website/2.png);
                    background-size: 200% 200%;
                    /* background-position: right; */
                    background-position-y: 33%;
                    background-position-x: 92%;
                    background-repeat: no-repeat;">
                <div class="alignright flyLeft">

                    <center><img style="padding: 4%;
                    max-width: 49%;" src="uploads/instamap.png"/></center>
                </div>
            </div>
            <div class="col-md-5" style="padding: 5%;
                margin-top: 9%;">
                <div class="alignright flyRight">
                    <h1><b>ON DEMAND <br>HOTEL BOOKING <br>IN A FEW TAPS</b></h1>
                    <br>
                    <p style="font-size: larger" class="alignright">Find a hotel anytime you need one<br>wherever you are, even if its the<br>wee hours for the morning or very late at night.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- 2nd Section -->
<section id="pay2">
    <div class="container" style="
                    background: url({{URL('/')}}/uploads/website/3.png);
                    background-size: cover;
                    background-repeat: no-repeat;"
                    >
        <div class="row">
            <div class="col-md-5" style="padding: 5%;
                /*margin-top: 11%;*/
                padding-top: 11em;
                ">
                <div class="alignLeft flyIn" style="color: white;">
                    <h1><b>BE OUR HOTEL PARTNER</b></h1>
                    <br>
                    <p style="font-size: larger">Increase your guest turnover rates for your room segments. Scale your hotel business with instant booking and express check-in</p>
                    <br>
                    <p style="font-size: larger">Find ease of use with our included channel manager built specifically for Instastay.</p>
                    <br>
                    <br>
                    <a href="{{URL('/hotelier')}}" class="btn btn-warning">BE A HOTEL PARTNER</a>
                </div>
            </div>
            <div class="col-md-7">
                <div class="alignLeft flyLeft">
                    <center><img class="cityImg1" style="padding: 0%; width: 80%;" src="uploads/City.svg"/></center>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection