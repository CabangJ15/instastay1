@extends('layouts.main')
@section('title', 'Instastay')
@section('content')
<style>
    #headerTitle{
        font-size: 36px;
        line-height: 1.1em;
    }

    blockquote.large:before {
        float:left;
    }

    .container{
        max-width: 100%;
    }

    .row {
        /*padding: 2em;*/
    }

   /* .first_cont{
        margin-top: 4em;
    }
*/
    @media (max-width: 768px){
        .first_cont{
            margin-top: 0em;
        } 

        .navbar-inverse .navbar-inner {
            padding: 0 0 0px;
        }
    }

    /*@import url(https://fonts.googleapis.com/css?family=Roboto:700);*/

    .progressbar {
      counter-reset: step;
    }

    .progressbar li {
      position: relative;
      list-style: none;
      float: left;
      width: 25%;
      text-align: center;
    }

    /* Circles */
    .progressbar li:before {
        content: counter(step);
        counter-increment: step;
        width: 40px;
        height: 40px;
        border: 2px solid #316BD1;
        display: block;
        text-align: center;
        margin: 0 auto 10px auto;
        border-radius: 50%;
        background-color: #ffffff;
        line-height: 39px;
    }

    .progressbar li.finish:before {
        background: #316BD1;
        content: "✔";
        color: white;
    }

    .progressbar li:after {
      content: "";
      position: absolute;
      width: 100%;
      height: 1px;
      background: #316BD1;
      top: 20px; /*half of height Parent (li) */
      left: -50%;
      z-index: -1;
    }

    .progressbar li:first-child:after {
      content: none;
    }

    .progressbar li.active:before {
      background: #316BD1;
      /*content: "✔";  */
      color: white;
    }

    .progressbar li.active + li:after {
      background: #316BD1;
    }

    .tab{
        /*border-bottom: solid 1px black;    */
        display: none;
    }

    input.invalid {
      background-color: #ffdddd;
    }

    .select2-selection__rendered {
        line-height: 31px !important;
    }
    .select2-container .select2-selection--single {
        height: 35px !important;
    }
    .select2-selection__arrow {
        height: 34px !important;
    }

    #cityImg{
        margin-top: 18%;
    }

</style>

<!-- 2nd Section -->

<!-- 2nd Section -->
<section id="pay2">
    <div class="container" style="
                    background: url({{URL('/')}}/uploads/website/3.png);
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position-y: 100%;
                    "
                    >
        <div class="row">
            <div class="col-md-6" style="padding: 5%; padding-bottom: 0%;
                margin-top: 8%;
                ">
                <div class="alignLeft flyIn" style="color: #316BD1;">
                    <h1>READY TO BE <br>AN INSTA-HOTEL?</h1>
                    <br>
                    <p style="font-size: larger; color: white;">Take the step towards<br>the Insta-innovation.<br><br>Registering will only take a few minutes.</p>
                    <center><img id="cityImg" style="" src="uploads/City.svg"/></center>
                </div>
            </div>
            <div class="col-md-6">
                <div class="container" style="margin-bottom: 8em;">
                  <ul class="progressbar">
                    <li class="step">Hotel Representative Information</li>
                    <li class="step">Hotel Information</li>
                    <li class="step">Corporate Information</li>
                    <li class="step">Successful Registration</li>
                  </ul>
                </div>
                <center>
                    <div class="alignLeft flyLeft myForm" style="max-width: 500px;
                        margin: 1em;
                        padding: 2em;
                        border-radius: 19px;
                        /* border: solid black 1px; */
                        box-shadow: 5px 7px 6px #00000029;
                        text-align: left;
                        background: #F0F0F0;">
                        <div class="row">
                            
                            <div class="col-sm-12">
                                <h5 id="myTitle" style="color: #316BD1"></h5>
                            <hr>
                            </div>
                            
                        </div>
                        <form id="regForm" method="POST" action="{{URL('hotelier_inquiries')}}">
                            @csrf
                            <div class="tab">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Name*</label>
                                        <input type="text" value="" oninput="this.className = 'form-control'" name="fname" name="fname" class="form-control" id="focusedinput" placeholder="John Doe" required>
                                    </div>
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Job Title</label>
                                        <input type="text" oninput="this.className = 'form-control'" value="" name="jobtitle" class="form-control" id="focusedinput" placeholder="Marketing Specialist">
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Mobile Number*</label>
                                        <input type="text" onkeypress='validate(event)' oninput="this.className = 'form-control'" value="" name="mobile" min="11" class="form-control" id="focusedinput" placeholder="0917 XXX XXXX" required>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Telephone Number</label>
                                        <input type="text" min="8" oninput="this.className = 'form-control'" value="" name="telnos" class="form-control" id="focusedinput" placeholder="02 XXXX XXXX">
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Email Address*</label>
                                        <input type="email" oninput="this.className = 'form-control'" value="" name="email" class="form-control" id="focusedinput" placeholder="johndoe@gmail.com" required>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="tab">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Hotel Name*</label>
                                        <input type="text" value="" oninput="this.className = 'form-control'" name="hname" class="form-control" id="focusedinput" placeholder="Instastay Hotel" required>
                                    </div>
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Hotel Address*</label>
                                        <input type="text" oninput="this.className = 'form-control'" value="" name="address" class="form-control" id="focusedinput" placeholder="Building No., Street Name" required>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-8">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">City</label>
                                        <select name="city" class="mySelect form-control" required="required">
                                            <option value="">Select a City</option>
                                            @foreach($city as $result)
                                            <option value="{{$result->city}}">{{$result->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Postal Code</label>
                                        <input type="text" oninput="this.className = 'form-control'" value="" name="postal_code" class="form-control" id="focusedinput" placeholder="1109">
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Telephone Number / Mobile Number</label>
                                        <input type="text" oninput="this.className = 'form-control'" value="" name="contact" class="form-control" id="focusedinput" placeholder="02 XXXX XXXX">
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Hotel Email Address*</label>
                                        <input type="email" oninput="this.className = 'form-control'" value="" name="hotel_email" class="form-control" id="focusedinput" placeholder="reservation@instastay.app" required>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Hotel Website</label>
                                        <input type="text" oninput="this.className = 'form-control'" value="" name="hotel_email" class="form-control" id="focusedinput" placeholder="instastay.app">
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Hotel Official Facebook Page</label>
                                        <input type="text" oninput="this.className = 'form-control'" value="" name="hotel_email" class="form-control" id="focusedinput" placeholder="facebook.com/instastay">
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="tab">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Parent Company Name</label>
                                        <input type="text" value="" oninput="this.className = 'form-control'" name="company" class="form-control" id="focusedinput" placeholder="Instastay Hotel">
                                    </div>
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Corporate Office Address</label>
                                        <input type="text" oninput="this.className = 'form-control'" value="" name="c_address" class="form-control" id="focusedinput" placeholder="Building No., Street Name">
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-8">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">City</label>
                                        <select name="c_city" class="mySelect form-control" required="required">
                                            <option value="">Select a City</option>
                                            @foreach($city as $result)
                                            <option value="{{$result->city}}">{{$result->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Postal Code</label>
                                        <input type="text" oninput="this.className = 'form-control'" value="" name="c_postal_code" class="form-control" id="focusedinput" placeholder="1109">
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-6">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Accommodation Type</label>
                                        <select class="mySelect form-control" required="required">
                                            <option value="">Select a Type</option>
                                            
                                            <option value="Hotel">Hotel</option>
                                            <option value="Motel">Motel</option>
                                            <option value="Homestays">Homestays</option>
                                            <option value="Bed and Breakfast">Bed and Breakfast</option>
                                            
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Total No of Hotels (Group)</label>
                                        <input type="number" oninput="this.className = 'form-control'" value="" name="no_hotel" class="form-control" id="focusedinput" placeholder="1109">
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Star Rating</label>
                                        <select class="mySelect form-control" required="required">
                                            <option value="">Select a Type</option>
                                            
                                            <option value="5">5 Stars</option>
                                            <option value="4">4 Stars</option>
                                            <option value="3">3 Stars</option>
                                            <option value="2">2 Stars</option>
                                            <option value="2">1 Star</option>
                                            
                                        </select>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <label style="margin-top: 1em; margin-bottom: 1em" for="focusedinput" class="control-label">Hotel Main Description</label>
                                        <textarea placeholder="Please provide a brief description of your hotel. This will highlight your hotel's unique selling point. (Short phrase only, maximum 100 characters)" class="form-control" name="description"></textarea>
                                    </div>
                                    
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6">
                                <br>
                                <button style="width: 48%; float: right;" id="nextBtn"onclick="nextPrev(1)" class="btn btn-info">NEXT</button>

                                <button style="width: 48%; float: right; margin-right: .5em;" id="prevBtn" class="btn btn-default"onclick="nextPrev(-1)">PREV</button>
                            </div>
                            <div class="col-sm-1">
                            </div>
                        </div>
                    </div>
                </center>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-script')
    <script>
        $(function(){
            $('.mySelect').select2();
        })
    </script>
    <script>
        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab

        function showTab(n) {
          // This function will display the specified tab of the form...
          var x = document.getElementsByClassName("tab");
          x[n].style.display = "block";
          //... and fix the Previous/Next buttons:
          if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
          } else {
            document.getElementById("prevBtn").style.display = "inline";
          }
          if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Submit";
          } else {
            document.getElementById("nextBtn").innerHTML = "Next";
          }
          //... and run a function that will display the correct step indicator:
          fixStepIndicator(n)
        }

        function nextPrev(n) {
          // This function will figure out which tab to display
          var x = document.getElementsByClassName("tab");
          // Exit the function if any field in the current tab is invalid:
          if (n == 1 && !validateForm()) return false;
          // Hide the current tab:
          x[currentTab].style.display = "none";
          // Increase or decrease the current tab by 1:
          currentTab = currentTab + n;
          // if you have reached the end of the form...
          if (currentTab >= x.length) {
            // ... the form gets submitted:
            test = document.getElementsByClassName("myForm");

            test[0].style.display = "none";
            document.getElementById("regForm").submit();
            return false;
          }
          // Otherwise, display the correct tab:
          showTab(currentTab);
        }

        function validateForm() {
          // This function deals with validation of the form fields
          var x, y, i, z = 0, valid = true;
          x = document.getElementsByClassName("tab");
          y = x[currentTab].getElementsByTagName("input");

          // console.log(y);
          // A loop that checks every input field in the current tab:
          for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
              // add an "invalid" class to the field:

              if(y[i].required == true){
                y[i].className += " invalid";
                z++;
              }
              // and set the current valid status to false
              
            } else {

                if(y[i].min != ""){
                    
                   if(y[i].min > y[i].value.length){

                       y[i].className += " invalid";
                        z++; 
                        swal('','Mobile Number consist of 11 Digits', 'warning');
                    } 
                    
                }

                if(y[i].type == "email"){
                    test = validateEmail(y[i].value);
                    if(test == false){
                        y[i].className += " invalid";
                        z++;
                    }
                } else {
                    
                }
            }

            if(z != 0){
                valid = false;
              }
          }
          // If the valid status is true, mark the step as finished and valid:
          if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
          }
          return valid; // return the valid status
        }

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

        function fixStepIndicator(n) {
          // This function removes the "active" class of all steps...
          var i, x = document.getElementsByClassName("step");
          for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", " ");
          }
          //... and adds the "active" class on the current step:
          // console.log(x[n].innerHTML);
          $('#myTitle').html(x[n].innerHTML);
          console.log(n);
          if(n == 1){
            // x[n].style.marginTop = "51%"
            $('#cityImg').css('margin-top', '67%');
          } else if(n == 2){
            $('#cityImg').css('margin-top', '51%');
          } else {
            $('#cityImg').css('margin-top', '30%');
          }
          x[n].className += " active";
        }

    function validate(evt) {
      var theEvent = evt || window.event;

      // Handle paste
      if (theEvent.type === 'paste') {
          key = event.clipboardData.getData('text/plain');
      } else {
      // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
      }
    }
    </script>
@endsection