<style>
  body{
    background: #ecedf0 !important;
    width: 100% !important;
  }
</style>
<aside id="sidebar-left" class="sidebar-left">
  <div class="sidebar-header">
    <div style="color: white;" class="sidebar-title">
      Navigation
    </div>
    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
      <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
    </div>
  </div>

  <div class="nano">
    <div class="nano-content">
      <nav id="menu" class="nav-main" role="navigation">
        <ul class="nav nav-main">
          <li id="hotelListMenu" class="">
            <a href="{{URL('hotels')}}">
              <i class="fa fa-th-large" aria-hidden="true"></i>
              <span>Hotels</span>
            </a>
          </li>
          @if(count($room_type) != 0)
          <li id="dashboardMenu" class="">
            <a href="{{URL('hotels')}}/{{$hotel_id}}">
              <i class="fa fa-tachometer" aria-hidden="true"></i>
              <span>Dashboard</span>
            </a>
          </li>
          @else
          @endif
          @if($hotel_id != "")
          @if(isset($access[18]))
          <li id="catalogMenu" class="nav-parent">
            <a href="#">
              <i class="fa fa-hospital-o" aria-hidden="true"></i>
              <span>Property Detail</span>
            </a>
            <ul class="nav nav-children">
              @if(isset($access[19]))
              <li id="hotenInfoMenu" class="">
                <a href="{{URL('hotels')}}/{{$hotel_id}}/edit">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Hotel Infomation
                </a>
              </li>
              @endif
              @if(isset($access[23]))
              <li id="facilitiesMainMenu" class="">
                <a href="{{URL('facilities')}}/{{$hotel_id}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Facilities
                </a>
              </li>
              @endif
              @if(isset($access[25]))
              <!-- <li id="contactMenu" class="">
                <a href="{{URL('property_contact')}}/{{$hotel_id}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Property Contact
                </a>
              </li> -->
              @endif
              <!-- <li id="contactMenu" class="">
                <a href="{{URL('paymentInfo')}}/{{$hotel_id}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Payment Information
                </a>
              </li> -->
              @if(isset($access[21]))
              <!-- <li id="photoMenu" class="">
                <a href="{{URL('photos')}}/{{$hotel_id}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Photos
                </a>
              </li> -->
              @endif
              @if(isset($access[24]))
             <!--  <li id="taxMenu" class="">
                <a href="{{URL('tax_setting')}}/{{$hotel_id}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Tax Settings
                </a>
              </li> -->
              @endif
            </ul>
          </li>
          @endif
          
          @if(isset($access[26]))
          <li id="roomManagementMenu" class="nav-parent">
            <a href="#">
              <i class="fa fa-bed" aria-hidden="true"></i>
              <span>Room Management</span>
            </a>
            <ul class="nav nav-children">
              @if(isset($access[20]))
              <li id="roomTypeMenu" class="">
                <a href="{{URL('room_type')}}/{{$hotel_id}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Hotel Room
                </a>
              </li>
              @endif
              @if(count($room_type) != 0)
              @if(isset($access[28]))
              <li id="roomRateMainMenu" class="">
                <a href="{{URL('room_rate')}}/{{$hotel_id}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Room Rate
                </a>
              </li>
              @endif
              @if(isset($access[29]))
              <li id="inventoryMenu" class="">
                <a href="{{URL('inventory')}}/{{$hotel_id}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Room Availability
                </a>
              </li>
              @endif
              @endif
            </ul>
          </li>
          @if(count($room_type) != 0)
          <li id="catalogMenu" class="nav-parent">
            <a href="#">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <span>Booking</span>
            </a>
            <ul class="nav nav-children">
              @if(isset($access[27]))
              <li id="bookingMenu" class="">
                <a href="{{URL('booking')}}/{{$hotel_id}}">
                  <i class="fa fa-list-alt" aria-hidden="true"></i>
                   Booking List
                </a>
              </li>
              @endif
              <!-- <li id="bookingMenu" class="">
                <a href="{{URL('invoice')}}/{{$hotel_id}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Invoice List
                </a>
              </li>
              <li id="bookingMenu" class="">
                <a href="{{URL('invoice')}}/{{$hotel_id}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Payment Collection
                </a>
              </li>
              <li id="bookingMenu" class="">
                <a href="{{URL('invoice')}}/{{$hotel_id}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Deposit Transaction
                </a>
              </li> -->
            </ul>
          </li>
          @endif
          @if(isset($access[22]))
          <!-- <li>  
            <a href="{{URL('reviews')}}/{{$hotel_id}}">
              <i class="fa fa-comment-o" aria-hidden="true"></i>
              <span>Guest Review</span>
            </a>
          </li> -->
          @endif
          @endif
          @endif
          <li>  
            <a href="{{URL('logout')}}">
              <i class="fa fa-power-off" aria-hidden="true"></i>
              <span>Logout</span>
            </a>
          </li>
      </nav>

      <!--  -->
    </div>

  </div>

</aside>