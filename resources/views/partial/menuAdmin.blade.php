<style>
  body{
    background: #ecedf0 !important;
    width: 100% !important;
  }
</style>
<aside id="sidebar-left" class="sidebar-left">
  <div class="sidebar-header" style="display: none;">
    <div style="color: white;" class="sidebar-title">
      Navigation
    </div>
    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
      <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
    </div>
  </div>

  <div class="nano">
    <div class="nano-content">
      <nav id="menu" class="nav-main" role="navigation">
        <ul class="nav nav-main">
          <center>
          <div style=" border-radius: 100%; margin: 2em;
            width: 150px;
            height: 150px;
            background: transparent url('{{URL('/')}}/uploads/No-Image-Available.png') 0% 0% no-repeat padding-box;
            background-size:100% 100%;
            opacity: 1;
          "></div>
          <p style="font-size: 1.2em; margin-bottom: 0em;
          color: white;
          font-weight: bold;">{{Auth::user()->name}}</p>
          <p style="color: white; margin-bottom: 0em; ">Business Development Manager</p>
          <p style="font-size: .8em; margin-bottom: 0em;
          color: white;"><i>{{Auth::user()->email}}</i></p>
        </center>
          <li id="adminDashboardListMenu" class="">
            <a href="{{URL('adminDashboard')}}">
              <i class="fa fa-home" aria-hidden="true"></i>
              <span>Home</span>
            </a>
          </li>
          <li id="propertiesMenu" class="">
            <a href="{{URL('properties')}}">
              <i class="fa fa-hospital-o" aria-hidden="true"></i>
              <span>Properties</span>
            </a>
          </li>
          <!-- <li id="customerMenu" class="">
            <a href="{{URL('adminUsers')}}">
              <i class="fa fa-users" aria-hidden="true"></i>
              <span>Customer</span>
            </a>
          </li> -->
          <li id="masterFileMenu" class="nav-parent">
            <a href="#">
              <i class="fa fa-tags" aria-hidden="true"></i>
              <span>Master File</span>
            </a>
            <ul class="nav nav-children">
              <li id="roomRateMenu" class="">
                <a href="{{URL('room_rate')}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  <span>Room Rate</span>
                </a>
              </li>
              <li id="amenitiesMenu" class="">
                <a href="{{URL('amenities')}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  <span>Amenities</span>
                </a>
              </li>
            </ul>
          </li>
          
          <li id="leadsMenu" class="">
            <a href="{{URL('leads')}}">
              <i class="fa fa-file-text" aria-hidden="true"></i>
              <span>New Leads</span>
            </a>
          </li>
          <li id="accountMenu" class="nav-parent">
            <a href="#">
              <i class="fa fa-user" aria-hidden="true"></i>
              <span>Accounts</span>
            </a>
            <ul class="nav nav-children">
              <li id="adminUserMenu" class="">
                <a href="{{URL('user')}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   Users
                </a>
              </li>
              <li id="userGroupMenu" class="">
                <a href="{{URL('userGroup')}}">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                   User Group
                </a>
              </li>
            </ul>
          </li>
          <li>  
            <a href="{{URL('logout')}}">
              <i class="fa fa-power-off" aria-hidden="true"></i>
              <span>Logout</span>
            </a>
          </li>
      </nav>

      <!--  -->
    </div>

  </div>

</aside>