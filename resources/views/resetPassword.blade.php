@extends('layouts.login-main')
@section('title', 'Instastay | Instanet')
@section('sub_title_system', 'Point of Sales')
@section('user', 'Admin')
@section('user_group', 'Administrator')
@section('menu')
    
@endsection

@section('content')
<section class="body-sign" style="max-width: 100%;">
	<!-- <div class="row" style="margin: 1em 0%;">
		<div class="col-sm-6 text-center">
			<p style="color: white">Not yet in Instastay? <a class="btn btn-warning">BE A HOTEL PARTNER</a></p>
		</div>
		<div class="col-sm-6 text-right">
		</div>
	</div> -->
	<div class="row" style="margin: 1em 0%;">
		<div class="col-sm-6 text-right">
		</div>
		<div class="col-sm-6 text-right">
			<section class="body-sign">
				<div class="center-sign">
						<center><img src="{{URL('/')}}/assets/images/insta_logo.png" style="width: 249px; height: 249px;" alt="Porto Admin" />
							<p style="text-align: center;
							font-size: 25px;
							letter-spacing: 0;
							color: #FFFFFF;
							opacity: 1;">
								Reset Password
							</p>
						</center>

					<div class="panel panel-sign">
						<div class="panel-title-sign mt-xl text-right">
							<!-- <h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign In</h2> -->
						</div>
						<div class="panel-body" style="background: transparent; border-top-color: transparent; padding: 0px;     box-shadow: 0 0px 0px rgba(0, 0, 0, 0.1);">
							
							<form action="{{URL('passwordReset')}}/{{$code}}" method="post">
								@csrf
								@if (session('notification'))
								<div class="bg-warning dark pv20 text-white fw600 text-center">
								{{ session('notification') }}  
								</div>        
								@endif
								<div class="form-group mb-lg">
									<div class="clearfix">
										<!-- <a href="pages-recover-password.html" class="pull-right">Lost Password?</a> -->
									</div>
									<div class="input-group input-group-icon">
										<input style="font-size: 1.7rem;" name="password" type="password" placeholder="Password" class="form-control input-lg" required="required"/>
										@if ($errors->has('password'))
			                                <div class="bg-warning dark pv20 text-white fw600 text-center">
			                                    <center>
			                                    	<strong>
			                                    		{{ $errors->first('password') }}
			                                    	</strong>
			                                    </center>
			                                </div>  
			                            @endif
										<span class="input-group-addon">
											<span class="icon icon-lg">
												<i class="fa fa-lock"></i>
											</span>
										</span>
									</div>
								</div>

								<div class="form-group mb-lg">
									<div class="clearfix">
										<!-- <a href="pages-recover-password.html" class="pull-right">Lost Password?</a> -->
									</div>
									<div class="input-group input-group-icon">
										<input style="font-size: 1.7rem;" name="password_confirmation" type="password" placeholder="Comfirm Password" class="form-control input-lg" required="required"/>
										<span class="input-group-addon">
											<span class="icon icon-lg">
												<i class="fa fa-lock"></i>
											</span>
										</span>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-12 text-right">
										<button style="width: 100%; height: 40px;" type="submit" class="btn btn-info hidden-xs">Reset Password</button>
										<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Reset Password</button>
									</div>
								</div>

							</form>
						</div>
					</div>
					<center>
					
				</center>
				</div>

			</section>
			<!-- <p style="color: white !important;" class="text-center text-muted mt-md mb-md">&copy; 2019 Nerdvana Inc. All rights reserved. Confidential and Proprietary.</p> -->
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 text-center">
			<div class="row">
				<div class="col-sm-4 text-center">
					<a style="color: white;">Contact Instastay</a>
				</div>
				<div class="col-sm-4 text-center">
					<a style="color: white;">Terms of Use</a>
				</div>
				<div class="col-sm-4 text-center">
					<a style="color: white;">Privacy Policy</a>
				</div>
			</div>
		</div>
		<div class="col-sm-6 text-right">
			<p style="color: white !important;" class="text-center text-muted mt-md mb-md">&copy; 2019 Nerdvana Inc. All rights reserved. Confidential and Proprietary.</p>
		</div>
	</div>
</section>
@endsection

@section('page-script')
<script>
    $('#catalog').addClass('active');
</script>
@endsection