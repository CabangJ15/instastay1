@extends('main')
@section('style')
<style>
        .buttonInside{
          position:relative;
          margin-bottom:10px;
        }
        #container{
          /*width: 300px;*/
        }
        input{
            height: 50px;
            width: 100%;
            padding-left: 22px;
            border-radius: 30px;
            border: none;
            outline: none;
            background: transparent;
            color: white;
            border: 2px white solid;
        }

        .email_whitelist::placeholder {
              color: white;
            }
        button{
            border-radius: 20px;
            color: white;
            background: #b71eeb;
            border: none;
            width: 100%;
            max-width: 300px;
            outline: none;
            text-align: center;
            font-weight: bold;
            padding: 9px;
            background: #b71eeb;
            color: white;
            margin-top: 1em;
        }
        button:hover{
          cursor:pointer;
        }
        #help{
          display:none;
          font-style:italic;
          font-size:0.8em;
        }

        .swal-button {
            width: 70px;
        }

        .swal-modal {
            height: 250px;
        }

    </style>
@endsection
@section('content')
    <div class="main-sec">
        <!-- //header -->
        <header class="py-sm-3 pt-3 pb-2" id="home">
            <div class="container">
                <!-- nav -->
                <div class="top-w3pvt d-flex">
                    <div id="logo">
                        <h1> <a href="{{ url('/') }}"><img style="max-width: 200px;" src="{{ asset('assets/images/logo.png') }}"/></h1>
                    </div>

                    <div class="forms ml-auto">
                        <a class="btn" style="font-family: Open sans;" href="{{ url('/') }}" class="drop-text">OVERVIEW</a>
                        <a class="btn active" style="font-family: Open sans;" href="{{ url('/wait') }}" class="drop-text">GET EARLY ACCESS</a>
                        <a class="btn" style="font-family: Open sans;" href="{{ url('/contact') }}" class="drop-text">CONTACT</a>
                    </div>
                </div>
            </div>
        </header>
        <!-- //header -->
        <!--/banner-->
        <div class="banner-wthree-info container">
            <div class="row">
                <div style="margin-top: 8em;" class="col-lg-12 banner-left-info buttonwrapper">
                    <h1 style="font-size: 6em;
                        font-weight: bold;
                        text-align: center;
                        margin-bottom: .2em;
                        color: white;">
                        <span>Be the First.</span></h1>
                    <center><p style="margin-bottom: 20px; color: #2182c9; font-size: 1em;">To book anywhere with anytime check-in</p></center>
                    
                </div>
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6">
                    <center>
                        <div class="buttonInside">
                            <input class="email_whitelist" id="email" placeholder="ENTER YOUR EMAIL">
                            <button id="emailBtn"><i class="fa fa-arrow-right"></i></button>
                        </div>
                    </center>
                </div>
                <div class="col-lg-3">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    
    $(document).ready(function(){

        /*
        on clicks
        */
        $(document).on('click', '#emailBtn', function(){
            var email = $('#email').val();
            if(email == ""){
                // toastr.error("Email should not be empty");
                swal("", "Email should not be empty", "error");
            }else{
                sendData(email);
            }
        });
    });

    /*
    functions for on clicks
    */
    function sendData(email){
        $.ajax({
            url : "{{ url('api/sendEmail') }}",
            method : "POST",
            data : {
                email : email
            },
            statusCode : {
                422 : function(){
                    toastr.info("Email Already Exist");
                    swal("", "Email Already Exist", "info");
                }
            }
        }).done(function(r){
            // console.log(r);
            if(r.response){
                // toastr.success(r.message);
                swal("", r.message, "success");
                $('#email').val('');
            }
        });
    }
    /*
    on load functions
    */

</script>
@endsection