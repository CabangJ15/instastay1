<!DOCTYPE html>
<html>

<head>
	<title>Thank you for expressing your interest in Instastay.</title>
	<!-- Custom Theme files -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Thank you for expressing your interest in Instastay." />
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,700;0,800;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<!-- //Custom Theme files -->
	<!-- Responsive Styles and Valid Styles -->
	<style type="text/css">
		body {
			width: 100%;
			text-align: -webkit-center;
			vertical-align: top;
			background: #F9F9F9;
		}

		html {
			width: 100%;
		}

		table {
			font-size: 14px;
			border: 0;
		}
		/* --responsivity-- */

		@media only screen and (max-width: 768px) {
			.container {
				width: 650px;
			}
			.container-middle {
				width: 416px !important;
			}
			table.ban {
				background: url(images/banner.jpg);
				background-size: cover;
				height:450px!important;
			}
		}
		@media only screen and (max-width:736px) {
		    table.ser-section {
				float: left!important;
				width: 40%!important;
				margin: 2%!important;
			}
			a.log {
				font-size: 3.5em!important;
			}
			td.line span {
				 font-size: 3.5em!important;
            }
		}
		@media only screen and (max-width:640px) {
		     table.ser-section {
				float: none!important;
				width: 100%!important;
				margin: 0!important;
			}
			td.line.sec {
				text-align: center!important;
			}
			td.price-para.sec {
				text-align: center!important;
			}
			td.join {
				text-align: center!important;
			}
			.container {
				width: 580px;
			}
		}
		@media only screen and (max-width: 622px) {

			/*------ top header ------ */
			.header-bg {
				width: 422px !important;
				height: 10px !important;
			}
		    table.ser-section {
				float: none!important;
				width: 100%!important;
				margin: 0!important;
			}

			.container {
				width: 500px !important;
			}
			.container-middle {
				width: 416px !important;
			}
			.mainContent {
				    width: 550px !important;
			}

			.main-image {
				width: 216px !important;
				height: auto !important;
			}
			.banner {
				width: 216px !important;
				height: auto !important;
			}
			.img-responsive {
				width: 100%;
			}
			table.social {
				width: 60%!important;
			}
			table.mainContent img {
				height: auto!important;
			}
	
		}

		@media only screen and (max-width:600px) {

			/*-- top header-- */
			.header-bg {
				width: 280px !important;
				height: 10px !important;
			}
			.main-header {
				line-height: 28px !important;
				text-align: center !important;
			}
			.main-subheader {
				line-height: 28px !important;
				text-align: center !important;
			}
			.container {
				width: 560px !important;
			}
			.container-middle {
				width: 260px !important;
			}
			.mainContent {
			    width: 500px !important;
			}

			.main-image {
				width: 560px !important;
				height: auto !important;
			}
			table {
				width: 100% !important;
			}
			table.ban {
				width: 100%!important;
				height:440px!important;
			}
			td.price {
				font-size: 1.8em!important;
			}
			td.top-text {
				height: 22px;
			}
			td.h-title {
				font-size: 1.8em!important;
			}
			a.learn {
				padding: 9px 12px!important;
				width: 115px!important;
			}
			td.line {
				font-size: 0.65em!important;
			}
			td.price-para {
				font-size: 0.8em;
				line-height: 1.9em;
				padding: 0 0px 0 0px;
			}
			td.price {
				line-height: 25px!important;
			}
			td.l-bottom {
				height: 20px!important;
			}
			table.ban2 {
				background: url(images/bottom.jpg);
				background-size: cover;
				height: 280px!important;
			}
			td.sub-ht {
				font-size: 1.5em!important;
			}
			table.b-text {
				width: 75%!important;
				margin: 0 auto;
			}
			table.menu {
				width: 58%!important;
				margin: 0 auto!important;
			}
			a.log {
				font-size: 3em!important;
			}
			table.place_main {
				width: 80% !important;
			}
			table.ban3 {
				width: 100%!important;
				height: 300px!important;
			}
			td.high-top_w3layouts {
				height:50px!important;
			}
			td.high-top_w3layouts.two{
				height:2px!important;
			}
			td.h_bottom_w3ls_agile {
				height:35px!important;
			}

		}

		@media only screen and (max-width:568px) {

			.container {
				width: 540px !important;
			}
			.container-middle {
				width: 260px !important;
			}
			.mainContent {
				width: 450px !important;
			}
			td.high-top_w3layouts {
				height:40px!important;
			}
			td.h_bottom_w3ls_agile {
				height: 25px!important;
			}
			td.scale-center-both {
				font-size: 1.4em!important;
			}
		}
		@media only screen and (max-width:480px) {
			/*-- top header-- */
			.header-bg {
				width: 280px !important;
				height: 10px !important;
			}
			.top-header-left {
				width: 260px !important;
				text-align: center !important;
			}
			.top-header-right {
				width: 260px !important;
			}
			.main-header {
				line-height: 28px !important;
				text-align: center !important;
			}
			.main-subheader {
				line-height: 28px !important;
				text-align: center !important;
			}
			.logo {
				width: 260px !important;
			}
			.container {
				width: 400px !important;
			}
			.container-middle {
				width: 260px !important;
			}
			.mainContent {
				width:380px !important;
			}
			td.h-t {
				font-size: 2.5em!important;
			}
			.main-image {
				width: 222px !important;
				height: auto !important;
			}
			.top-bottom-bg {
				width: 260px !important;
				height: auto !important;
			}
			table {
				width: 100% !important;
			}
			a.log {
				font-size: 2.2em!important;
			}
			table.ban {
				width: 100%!important;
				height: 300px!important;
			}
			td.price {
				font-size: 1.8em!important;
			}
			td.top-text {
				height: 22px;
			}
			td.h-title {
				font-size: 1.8em!important;
			}
			a.learn {
				padding: 9px 12px!important;
				width: 115px!important;
			}
			td.line {
				font-size: 0.65em!important;
			}
			td.price-para {
				font-size: 0.8em;
				line-height: 1.9em;
				padding: 0 0px 0 0px;
			}
			td.price {
				line-height: 25px!important;
			}
			td.l-bottom {
				height: 20px!important;
			}
			table.orenge {
				padding: 0 2em!important;
			}
			td.orenge-h {
				height: 30px!important;
			}
			td.sub-ht {
				font-size: 1.5em!important;
			}
			td.sub-ht {
				font-size: 1.3em!important;
			}
			td.top-text {
				height: 6px!important;
			}
			table.b-text {
				width: 75%!important;
				margin: 9% auto 0!important;
			}
			table.menu {
				width: 76%!important;
				margin: 0 auto!important;
			}
			td.line span {
				font-size: 3em!important;
			}
		}

		@media only screen and (max-width:414px) {
			td.scale-center-both img {
				height: 372px!important;
				width: 290px!important;
			}
			a.log {
				font-size: 2.2em!important;
			}
			table.ban {
				width: 100%!important;
				height: 300px!important;
			}
			a.learn {
				padding: 8px 10px!important;
				width: 70px!important;
			}
			td.icon-social a img {
				Width: 32px!important;
				height: 32px!important;
				margin: 0 2em;
			}
			.container {
				width: 370px !important;
			}
			a.nav {
					padding: 37px 8px!important;
					margin-right:0px!important;
				}
			a.nav.logo {
					margin-right:5px!important;
					padding: 37px 8px!important;
					font-size: 2.3em!important;
			}
			.mainContent {
				width: 336px !important;
			}
			td.scale-center-both img {
				height: auto!important;
				width: 100%!important;
			}
		}
		@media only screen and (max-width:384px) {
			.container {
				width: 350px !important;
			}
			a.nav {
				padding: 37px 4px!important;
				margin-right: 0px!important;
			}
			td.sub-ht {
				font-size: 1.1em!important;
			}
		}
		@media only screen and (max-width:320px) {
			.container {
				width: 290px !important;
			}
			.mainContent {
				width: 270px !important;
			}
			table.b-text {
				width: 90%!important;
				margin: 13% auto 0!important;
			}
			td.h-t {
				font-size: 1.9em!important;
			}
			table.menu {
				width: 88%!important;
				margin: 0 auto!important;
			}
			a.nav.logo {
				margin-right: 0px!important;
				padding: 37px 4px!important;
				font-size: 2em!important;
			}
		}

		a.btn {

			background-color: #316BD1; /* Green */
		    border: none;
		    color: #fdfdfd;
		    padding: 15px 32px;
		    text-align: center;
		    text-decoration: none;
		    display: inline-block;
		    font-size: 20px;
		    border-radius: 8px	
		}
	</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td height="60"></td>
		</tr>

		<tr>
			<td width="100%" align="center" valign="top">
				<!-- main content -->
				<table width="800" border="0" cellpadding="0" cellspacing="0" align="center" class="container top-header-left">

					<!-- banner -->
					<tr>
						<td>
						</td>

					</tr>

					<!-- main-welcome-->
					<tr>
						<tr>
							<td class="high-top_w3layouts" bgcolor="#FFF" height="40"></td>
						</tr>
						<td bgcolor="ffffff">
							<table width="560" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent">
								<tr>
									<td class="high-top_w3layouts two" height="30"></td>
								</tr>
								<tr>
									<td class="h-t" align="center" mc:edit="title1" class="main-header" style="color: #333; font-size:3em; font-family: 'Open Sans', sans-serif; font-weight:bold;">
										<img style="max-width: 189px;" src="http://35.188.211.15//assets/images/logo.png"/>
									</td>
								</tr>
								<tr>
									<td height="10"></td>
								</tr>
								<tr>
									<td class="sub-ht" align="center" mc:edit="title1" class="main-header" style="color: #E01931;font-size:1.6em; font-weight: bold; font-family: 'Open Sans', sans-serif;font-weight:600;">
										<p style="color: black; text-align: left; font-size: 24px;">Hello {{$name}},</p>
									</td>
								</tr>

								<tr>
									<td height="15"></td>
								</tr>
								<tr bgcolor="ffffff">
									<td class="main-subheader" style="color: black; font-size:16px;font-family: 'Open Sans', sans-serif; font-weight: normal; line-height: 2em; text-align: left;">
										<p>Tap the button bellow to reset your instastay account password.<br>If you did not request a new password, you can safely delete this email.</p>
										<br>
										<center>
											<a href="{{URL('/resetPassword')}}/{{$code}}" class ="btn">Reset Password</a>
										</center>
										<br>
										<p><b>Thank you, </b></p>
										<p><b>Best Regards,<br>Instastay Team</b></p>
									</td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="high-top_w3layouts" bgcolor="ffffff" height="80"></td>
					</tr>
					<tr>
						<td>
							<table class="ban3" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="FFFFFF" style=""
							    height="400">
								<tbody>
									<tr style="background: #f9f9f9">
										<td>
											<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent">
												<tbody>
													<tr>
														<td align="center">
															<table class="lost-test" border="0" align="center" cellpadding="0" cellspacing="0" style="padding-top:0em;">


																<td align="center">
															</table>
															<table border="0" class="social" align="center" cellpadding="0" cellspacing="0">
																<tbody>
																	<tr>
																		<td>&nbsp;&nbsp;</td>
																		<td>
																			<table border="0"  align="center" cellpadding="0" cellspacing="0">
																				<tbody>
																					<tr>
																						<td><img style="width: 42px; height: 42px; padding: 10px;" src="http://35.188.211.15//images/facebook.png"</td>
																						<td></td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																		<td>
																			<table border="0"  align="center" cellpadding="0" cellspacing="0">
																				<tbody>
																					<tr>
																						<td><img style="width: 42px; height: 42px; padding: 10px;" src="http://35.188.211.15//images/instagram.png"</td>
																						<td></td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																</tbody>
															</table>
															</td>
													</tr>
													<tr>
														<td height="10"></td>
													</tr>
													<tr align="center">
														<td style="color:#939393; font-size:0.9em;font-family: 'Open Sans', sans-serif;line-height:2em;tex-align:center;">
															If you have any questions please contact us support@instastay.app
														</td>
													</tr>
													<tr align="center">
														
														<td style="color:#939393; font-size:0.9em;font-family: 'Open Sans', sans-serif;line-height:2em;tex-align:center;">
															
															Unsubscribe from our mailing lists
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr align="center">
														<td style="color: #151313;
														    font-size: 1em;
														    font-family: 'Open Sans', sans-serif;
														    line-height: 2em;
														    tex-align: center;
														    font-weight: bold;">
															<br>
															Download Our App
														</td>
													</tr>
													<tr align="center">

														<td style="font-family: 'Open Sans', sans-serif; font-size:0.9em; color: #ffffff; line-height: 24px;" class="editable">
															<table border="0" class="social" align="center" cellpadding="0" cellspacing="0">
																<tbody>
																	<tr>
																		<td>&nbsp;&nbsp;</td>
																		<td>
																			<table border="0"  align="center" cellpadding="0" cellspacing="0">
																				<tbody>
																					<tr>
																						<td><img style="width: 100px; height: 37px; padding: 10px;" src="http://35.188.211.15//images/android.png"</td>
																						<td></td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																		<td>
																			<table border="0"  align="center" cellpadding="0" cellspacing="0">
																				<tbody>
																					<tr>
																						<td><img style="width: 100px; height: 37px; padding: 10px;" src="http://35.188.211.15//images/apple.png"</td>
																						<td></td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
											</td>
									</tr>
								</tbody>
							</table>
							</td>
					</tr>

					<tr>
						<td class="high-top_w3layouts" height="30"></td>
					</tr>
</body>

</html>