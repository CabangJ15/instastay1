@extends('layouts.main')
@section('title', 'Instastay')
@section('content')
<style>
    #headerTitle{
        font-size: 36px;
        line-height: 1.1em;
    }

    blockquote.large:before {
        float:left;
    }

    .container{
        max-width: 100%;
    }

    .row {
        /*padding: 2em;*/
    }

   /* .first_cont{
        margin-top: 4em;
    }
*/
    @media (max-width: 768px){
        .first_cont{
            margin-top: 0em;
        } 

        .navbar-inverse .navbar-inner {
            padding: 0 0 0px;
        }
    }

    .cityImg1{

        margin-top: 47%;  
    }
        

</style>

<!-- 2nd Section -->

<!-- 2nd Section -->

    <div class="container first_cont">
        <div class="row">
            <div class="col-md-12" style="max-width: 1085px;
			    margin-left: 5%;
			    margin-right: 5%;
			    text-align: justify;">

				<br>
				<h2>TERMS & CONDITIONS </h2>
				<br>
                <p>INSTASTAY (“Instastay”, or “us”, “our”, “we”) is an online booking platform for accommodation reservation services with a registered domain name of www.instastay.app, under the registered company name of Nerdvana Corp with business address at 1 Canley Road, Barangay Bagong Ilog, Pasig City. INSTASTAY allows users to reserve and stay for hotel rooms through a mobile application (the “Services”). These Terms of Service (“Terms”) govern your access and use of the Services. “You” means any user of the Services.</p>
                <br>
                <br>
				<p>Please read these Terms carefully. You must accept these Terms prior to using the Services. By using the Services, you signify your consent to these Terms, including without limitation from privacy policy, which may be modified by INSTASTAY in its sole discretion from time to time, and a current copy of which is available at https://.instastay.app/privacy/ (the “Privacy Policy”).</p>
				<br>
				<p><b>A. Use of Service</b></p>
				<br>
				<div>
				<p style="margin-left: 1em;">
				1. INSTASTAY allows you to access and use the Services subject to these Terms. Through the mobile app, INSTASTAY provides a platform where you can browse different types of temporary accommodation and lodging. INSTASTAY may, in its sole discretion and at any time, modify or discontinue providing the Services or any part thereof without notice. By using the Services, you represent and permit that:</p>
				<br>
				<p style="margin-left: 2em;">
					a) any and all registration information you submit is truthful and accurate;
				<p style="margin-left: 2em;">
					b) you will maintain the accuracy of such information;</p>
				<p style="margin-left: 2em;">
					c) you either are 18 years of age or older, or else have the permission of your parents</p>
				<p style="margin-left: 2em;">
					or guardians to use the Services and</p>
				<p style="margin-left: 2em;">
					d) your use of the Services does not violate any applicable law or regulation or any</p>
				<br>
				<p style="margin-left: 1em;">2. When you reserve or book any accommodations through the Service, you permit that:</p>
				<br>
				<p style="margin-left: 2em;">a) all payment and credit card information you supply is correct, accurate and complete;</p>
				<p style="margin-left: 2em;">b) charges incurred by you will be honored by your credit card company and</p>
				<p style="margin-left: 2em;">c) you will pay the posted price for the hotel room, even if you are unable to stay in that hotel room</p>
				<br>
				
				<p style="font-weight: bold;">B. Registration</p><br>

				<p>You represent and permit that all information provided in the creation of an account is correct and accurate. Instastay reserves the right to refuse to open an account if not all appropriate information is provided. You can register your account through email address or contact number. Instastay will not be liable for any losses or damage arising from unauthorized use of the Services, and you agree to indemnify and hold Instastay harmless for any improper or illegal use of the Services, and any charges and taxes incurred, unless you have notified us via e-mail at <a href="mailto:support@instastay.app">support@instastay.app</a> that your account have been compromised. Instastay cannot guarantee that we will learn of or prevent, any inappropriate use of the Services.</p><br>

				<p style="font-weight: bold;">C. Changes to Personal Information</p><br>

				<p style="margin-left: 1em;">User with registered account in Instastay may at any time inspect, modify, add to, delete, or suspend the use of the Personal Information registered by himself/herself. In principle, only the User himself/herself may request for notification of "the Purpose of Use of Personal Information".</p><br>

				<p style="font-weight: bold;">D. Accuracy of Personal Information</p><br>

				<p style="margin-left: 1em;">Instastay will make all efforts to process the Personal Information received accurately. However, the User will be held responsible for the accuracy of the content of the Personal Information provided and for keeping the same up-to-date. The User may request for the updating and/or correction of his/her Personal Information in the event there are errors or inconsistencies.</p><br>

				<p style="font-weight: bold;">E. Fees and Taxes</p><br>

				<p style="margin-left: 1em;">The room rate displayed on the Instastay mobile application is the rates posted by the Hotel. When you book a room using the Service, we facilitate your booking and charge your method of payment which includes the room rate displayed on the application, plus any tax recovery charges, service fees, and where applicable, taxes on the Services. In some cities, the tax may include government imposed service fees or other fees not paid directly to a taxing authority but still required by law. Once you book for accommodation, you are responsible for paying the hotel directly based from the displayed amount from the mobile application.</p><br>

				<p style="font-weight: bold;">F. Ratings and Reviews</p><br>

				<p style="margin-left: 1em;">Rating or Reviews is only indicative of the level of comfort you can expect to find in a particular accommodation, with a higher rating generally being indicative of a higher level of comfort and amenities. Please be aware that your ratings and reviews can vary greatly on the basis of staycation experience, location, available alternatives, local market conditions, practices or other circumstances. If you have any concern or feedback from your accommodation experience, please contact our Instastay Support team or email us at <a href="mailto:support@instastay.app">support@instastay.app</a></p><br>

				<p style="font-weight: bold;">G. Over- Bookings and Cancellations</p><br>
				<p style="margin-left: 1em;">If for any reason a hotel is unable to honor your reservation, contact us immediately <a href="mailto:support@instastay.app">support@instastay.app</a>, and we will work with you and the hotel to find you alternative accommodations (such as an available room at a different location).</p><br>
				<p style="font-weight: bold;">H. Payment by Pay at the Hotel or Credit Card</p><br>

				<p style="margin-left: 1em;">For payment of reserved bookings placed through Instastay, you can directly settle it either by pay at the hotel or credit card. If you’ll settle it from your credit card you will be billed by Instastay for the full price at the time of booking and confirmation of the booking (any refund that may be given will depend on the conditions of the existing reservations). You must check the booking details thoroughly each time before you make a reservation. Instastay will process any refund, as applicable, within a reasonable period. In order to safeguard and encrypt your credit card information when in transit to us.</p><br>

				<p style="font-weight: bold;">I. Access Logs</p><br>
				<p style="margin-left: 1em;">Instastay may obtain access logs in order to improve convenience and prepare statistical data. Unless legally required, the Instastay will not disclose access logs to third parties. If User does not agree with the sending of such User's access logs, they should not use and register to mobile app.</p><br>
				<p style="font-weight: bold;">J. Force Majeure</p><br>
				<p style="margin-left: 1em;">Instastay shall not be responsible for any damages or losses caused by any means to any party because of the Force Majeure Event.</p><br>
				<p style="margin-left: 1em;">Force Majeure - an event or effect that cannot be reasonably anticipated or controlled. Include but are not limited to natural disaster (floods, earthquakes), epidemic, riot, a declaration of war, war, military action, terrorist action, embargo, sanctions, changes in laws or regulations, lightning, hurricanes / typhoons / cyclones, labor strikes, demonstrations, airline or hotel bankruptcy or insolvency, and cyber-attacks.</p><br>
				<p style="font-weight: bold;">K. Intellectual Property Rights</p><br>
				<p style="margin-left: 1em;">Instastay exclusively retains ownership of all rights, title and interest in and to copyright, patents, domain names, trademarks, service marks, logos, symbols or other designs etc (all intellectual property rights of) (the look and feel (including infrastructure) of) the Platform on which the service is made available (including the guest reviews and translated content) and you are not entitled to copy, scrape, (hyper-/deep)link to, publish, promote, market, integrate, utilize, combine or otherwise use the content (including any translations thereof and the guest reviews) or our brand without our express written permission. If you violate these rights, Instastay under the registered company name of Nerdvana Corp reserves the right to bring a civil claim for the full amount of damages or losses suffered. These violations may also constitute criminal offences.</p><br>
				<p style="font-weight: bold;">L. Copyright</p><br>
				<p style="margin-left: 1em;">You agree not to use any content provided in the Instastay site and mobile application or the Services, without the Company's prior approval, beyond the scope of your private use as set forth in the Copyright Act.</p><br>
				<p style="font-weight: bold;">M. Privacy Policy</p><br>
				<p style="margin-left: 1em;">Instastay (<a href="{{URL('/')}}">www.instastay.app</a>), under the registered company name of Nerdvana Corp with business address at 1 Canley Road, Barangay Bagong Ilog, Pasig City may make additions and deletions to, or otherwise modify this Privacy Policy without any prior notice. Use of the Service after any modifications are made to this Privacy Policy means the User agrees with the modified Privacy Policy.</p><br><br>
            </div>
        </div>
    </div>
</section>
<hr>


@endsection